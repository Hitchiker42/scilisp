#!/usr/bin/env perl
use open ':locale'; #same as pasing -C to perl
use v5.20;
use warnings;
#minimum version to use references with things like push,each,etc
use File::Find;
use File::Basename;
use Data::Dumper qw(Dumper); #for debugging
use autodie; #builtin functions die on error
no warnings "experimental";
my $initial_symbols = [];
my $debug = 0;
sub debug_print {
  my $msg = shift;
  if($debug){
    say(STDERR $msg);
  }
}
sub read_file {
  my $filename = shift;
  my $buffer;
  open(my $fh, "<:raw", $filename);
  #sometimes I really hate perl names, -s is a function to find the size of a file
  #However it's faster than anything I could write (in perl at least)
  sysread($fh, $buffer, (-s $fh));
  close($fh);
  return $buffer;
}

#Since translations should be constant we could do this:
#write $translation_table to a file using Data::Dumper, read this
#when starting the script, each time translate_symbol_names is called
#add the translation to $translation_table if not already present,
#write the (possibly) updated  $translation_table to a file when
#ending the script, this is probably an unecessary optimization.
my $translation_table =
  { "+" => "add", "-" => "sub", "*" => "mul", "/" => "div", "^" => "exp",
    "=" => "num_eq", "/=" => "num_ne", "<=" => "num_le", "<" => "num_lt",
    ">=" => "num_ge", ">" => "num_gt", "1+" => "add1", "1-" => "sub1",
    "set!" => "setq"};
#translates the lisp name of a symbol into the c-name
sub translate_symbol_name {
  my $symbol = shift;
  if ($translation_table->{$symbol}) {
    return $translation_table->{$symbol};
  }
  $symbol =~ s/\?$/p/; #symbol? -> symbolp
  $symbol =~ s/\*(.*)\*/$1/;#*symbol* -> symbol
  #translate characters to names
  my %char_to_name = {"*" => "star", "+" => "plus"};#add more if necessary
  while(my ($char, $name) = each(%char_to_name)){
    $char = quotemeta($char);#examples given below are for
    $name = quotemeta($name);#char = "*', name = "star"
    $symbol =~ s/(.*)$char/$1_$name/;#symbol* -> symbol_star
    $symbol =~ s/$char(.*)/${name}_$1/;#*symbol -> star_symbol
    $symbol =~ s/(.*)$char(.*)/$1_${name}_$2/g;
  }
  $symbol =~ s/(.*)->(.*)/$1_to_$2/;#sym->bol -> sym_to_bol
  $symbol =~ s/-/_/g;#dashes to underscores
  $symbol =~ s/^(.+)!$/n$1/;#symbol! -> nsymbol
  $symbol =~ s/(.+)\/(.+)/$1_$2/;#a/b -> a_b
  #could add =->eq,>->gt,>=->ge,<->lt,<=->le,!=->ne, at some point
  return $symbol;
}
sub defun_args {
  my $numargs = shift;

  unless($numargs){return "()";}

  if(lc("$numargs") eq "many"){
    return "(uint64_t, sl_obj*)";
  } elsif (lc("$numargs") eq "unevaled"){
    return "(sl_obj)";
  } elsif ($numargs == 0){
    return "(void)";
  }
  my $arguments = "(";
  while($numargs-- > 1){
    #there's a max of 8, concatenation is fine
    $arguments = $arguments . "sl_obj, ";
  }
  return $arguments . "sl_obj)";
}
#Use File::Find instead of glob to handle nested directories
#Should be trivial to extend to finding c files as well
sub find_sources {
  my $dirname = shift;
  my $headers = [];
  my $sources = [];
  my $wanted = sub {
    if (m|/[^.][^/]+?\.c$|){
      push($sources->@*,$File::Find::name);
    }
    return;
  };
  find({wanted => $wanted, no_chdir => 1}, $dirname);
  return $sources;
}
sub find_init_fun {
  my $file = shift;
  my $filename = basename($file);
  $filename =~ m/(?:sl_(.*)\.[ch])|(.*)\.[ch]/;  
  #removes any trailing s, so that something like
  #init_vector_symbols, in the file vectors.c would work
  my $short_name = (($1 ?"$1" :"$2") =~ s/s$//r);
  my $buffer = read_file($file);
  #si == treat string as one line + match case insensitively
  #allow optional 's' at end of name
  if ($buffer =~ m/void init_${short_name}s?_symbols\(void\)\{\n(.*)\n\}/si) {
    #The function body is stored in $1
    return $1;
  } else {
    debug_print("No symbols found in $file");
    return;
  }
}

my $symbol_types = ["subr", "variable", "keyword", "constant"];
my $gen_symbol_declarations = { subr => \&gen_subr_decl,
                                variable => \&gen_var_decl,
                                constant => \&gen_const_decl,
                                keyword => \&gen_key_decl };

#perl can't inline, but trying to maintain the same code in
#4 seperate functions is a bitch, so I'll take the performance hit

sub gen_subr_decl {
  my ($symbol,$info) = @_;
  my @info = split(/, */, $info);
  return sprintf("sl_obj F$symbol%s;\n", defun_args($info[0]));
}
sub gen_const_decl {
  return "";
}
sub gen_var_decl {
  return "";
}
sub gen_key_decl {
  my $symbol = shift;
  #strip _key from K prefixed symbol name
  my $symbol_basename = ($symbol =~ s/_key$//r);
  return "sl_obj K$symbol_basename;\n";
}
sub gen_symbol_decl {
  my ($symbol, $type, $extra) = @_; #assume translated symbol name
#  $symbol = translate_symbol_name($symbol);
  my $typed_decl = $gen_symbol_declarations->{$type}($symbol, $extra);
  my $decl = join("","sl_obj Q$symbol;\n", $typed_decl, "\n");
  return $decl;
}
#Some explaination of naming:
#All symbols in the form of sl_objs are named Q##name,
#The actual sl_symbols are named __Q##name
#Functions have a struct sexp named S##name and a C function named F##name
#Variables have a pointer to an sl_obj named V##name, this points to the actual
#value of the variable, such that changing it changes the value of S##name
#Keywords are variables who's value is equivlent to their own symbol,
#i.e S##name->val = V##name = K##name


sub parse_init_fun {  
  my $init_fun = shift;
  my $symbols = {"functions" => [],
                 "variables" => [],
                 "constants" => [],
                 "keywords" => []};
  for my $line (split('\n',$init_fun)){
    if($line =~ m/^ *INIT_SUBR\((.*), ?(.*)\);/){
      debug_print("found function $1");
      push($symbols->{"functions"}->@*, gen_symbol_decl($1,"subr",$2));
      push($initial_symbols->@*,"&Q${1}");
    } elsif($line =~ m/^ *DEFKEY\("[^"]*", ?(.*)\);/){
      debug_print("found keyword $1");
      push($symbols->{"keywords"}->@*, gen_symbol_decl($1 . "_key", "keyword"));
      push($initial_symbols->@*,"&Q${1}_key");
      #DEFVAR and DEFCONST take 3+ arguments and can span multiple lines
      #but we only need the 2nd argument, which we can assume is on the
      #first line
    } elsif($line =~ m/^ *DEFVAR\("[^"]*", ?(.*),/){
      debug_print("found variable $1");
      push($symbols->{"variables"}->@*, gen_symbol_decl($1, "variable"));
      push($initial_symbols->@*,"&Q${1}");
    } elsif($line =~ m/^ *DEFCONST(?:ANT)?\("[^"]*", ?(.*),/){
      #This might not be needed
      debug_print("found constant $1");
      push($symbols->{"constants"}->@*, gen_symbol_decl($1, "constant"));
      push($initial_symbols->@*,"&Q${1}");
    # } elsif($line =~ m/^ *DEFCONST(?:ANT)?_C\((.*)\);/){      
    #   push($initial_symbols,"&Q${1}")
    }
    #if we don't match anything it's fine, just go to the next line
  }
  return $symbols;
}
sub write_header {
  debug_print("Writing header");
  my ($symbol_table,$fd) = @_;
  unless($fd){
    $fd = *STDOUT;
  }
  debug_print("printing to $fd");
  print($fd
        ("/* This file was automatically generated by symbol_parse.pl */\n",
         "/* Do not edit by hand */\n",
        "#ifndef _SL_SYMBOL_LIST_H_\n#define _SL_SYMBOL_LIST_H_\n"));
  my ($file,$symbols);
  #this loop is what actually prints all the symbol declarations
  while(($file,$symbols) = each($symbol_table->%*)){
    print($fd "/* Symbols from $file */\n");
    for my $type (keys($symbols->%*)){
      if(scalar(@{$symbols->{$type}}) > 0){
        print($fd ("/* ",ucfirst($type)," */\n",@{$symbols->{$type}},"\n"));
      }
    }
  }
  my $num_initial_symbols = scalar(@{$initial_symbols});
  print($fd "static const unsigned long num_initial_symbols = $num_initial_symbols;\n");
  print($fd "static sl_obj* initial_symbols[$num_initial_symbols] = {\n");
  #deal with trailing comma(by printing the comma before the symbol in the loop
  my $sym = pop($initial_symbols->@*);
  print($fd "    $sym");
  map {print($fd ",\n    $_");} @{$initial_symbols};
  print($fd " };\n#endif\n");  
}
sub usage {
  # For now assume sources is just a single directory
  print("Usage: symbol_parse.pl source_dir [output_file]\n",
        "\tsource dir is searched recursively for .c files which are scanned\n",
        "\tfor symbols, output is printed to output_file (default stdout)\n");
  exit(1);
}
if(scalar(@ARGV) < 1){
  usage();
}
#Really hacky option parsing
if($ARGV[0] eq "--debug"){
  say(STDERR "Debugging enabled");
  $debug = 1;
  shift;
}
#$symbol_table is a hash of hashes of arrays/hashes,
# i.e {{[],[],...}, {[],[],...}, ...}
#the keys of the first hash are the header names, the keys of the second hash
#are symbol types and the values of the second hash are the actual symbols
#Along with any symbol specific info
my $symbol_table = {};
my $outfile;
my $srcdir = shift; #first command line arg
my $outfile_path = shift;
if($outfile_path){
  open($outfile, ">", $outfile_path);
} else {
  $outfile = \*STDOUT;
}
my $sources = find_sources($srcdir);
if($sources == []){
  die "No sources found";
}
#hack untill I get a better way to do constants
debug_print("Source files found:\n@{$sources}");
for my $source (@{$sources}) {
  debug_print("parsing $source");
  if (my $init_fun = find_init_fun($source)) {
    $symbol_table->{$source} = parse_init_fun($init_fun);
  }

}
#TODO: Save a copy of the initial contents of outfile and if we mess up
#revert the file back to it's initial state
write_header($symbol_table, $outfile);

# Local Variables:
# perl-indent-level: 2
# cperl-indent-level: 2
# End:
