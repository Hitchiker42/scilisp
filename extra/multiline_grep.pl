#!/usr/bin/env perl
use v5.18.0;
use autodie;
my $re = shift;
for my $file (@ARGV) {
  open(my $fh, $file);
  my $buffer;
  sysread($fh, $buffer, (-s $fh));
  close($fh);
  if($buffer =~ /$re/s){
    print("$file:\n",$1,"\n");
  }
}
