#!/usr/bin/env perl
use open ':locale'; #same as pasing -C to perl
use v5.14;
use warnings;
#minimum version to use references with things like push,each,etc
use File::Find;
use File::Basename;
use Data::Dumper qw(Dumper); #for debugging
use autodie; #builtin functions die on error
no warnings "experimental";
my $num_initial_symbols = 0;
my $initial_symbols = [];
my $symbol_init = [];#probably unecessary
#acts like @arr = map {code} arr
#but without creating a new array
sub destructive_map (&@) {
  my ($code, $arr) = @_;
  my $i;
  for($i = 0;$i<scalar(@{$arr});$i++){
    $_ = $arr->[$i];
    $arr->[$i] = &{$code};
  }
  return $arr;
}

sub read_file {
  my $filename = shift;
  my $buffer;
  open(my $fh, "<", $filename);
  #sometimes I really hate perl names, -s is a function to find the size of a file
  #However it's faster than anything I could write (in perl at least)
  sysread($fh, $buffer, (-s $fh));
  close($fh);
  return $buffer;
}
#Since translations should be constant we could do this:
#write $translation_table to a file using Data::Dumper, read this
#when starting the script, each time translate_symbol_names is called
#add the translation to $translation_table if not already present,
#write the (possibly) updated  $translation_table to a file when
#ending the script, this is probably an unecessary optimization.
my $translation_table =
  { "+" => "add", "-" => "sub", "*" => "mul", "/" => "div", "^" => "exp",
    "=" => "num_eq", "!=" => "num_ne", "<=" => "num_le", "<" => "num_lt",
    ">=" => "num_ge", ">" => "num_gt", "1+" => "add1", "1-" => "sub1"};                              
#translates the lisp name of a symbol into the c-name
sub translate_symbol_name {
  my $symbol = shift;
  if ($translation_table->{$symbol}) {
    return $translation_table->{$symbol};
  }
  $symbol =~ s/\?$/p/; #symbol? -> symbolp
  $symbol =~ s/\*(.*)\*/$1/;#*symbol* -> symbol
  $symbol =~ s/(.*)->(.*)/$1_to_$2/;#sym->bol -> sym_to_bol
  $symbol =~ s/-/_/g;#dashes to underscores
  $symbol =~ s/^(.+)!$/n$1/;#symbol! -> nsymbol
  #could add =->eq,>->gt,>=->ge,<->lt,<=->le,!=->ne, at some point
  return $symbol;
}
sub defun_args {
  my $numargs = shift;
  
  unless($numargs){return "()";}
  
  if($numargs eq "many"){
    return "(uint64_t, sl_obj*)";
  } elsif ($numargs eq "unevalled"){
    return "(sl_obj)";
  } elsif ($numargs == 0){
    return "(void)";
  }
  my $arguments = "(";
  while($numargs-- > 1){
    #there's a max of 8, concatenation is fine 
    $arguments = $arguments . "sl_obj, ";
  }
  return $arguments . "sl_obj)";
}
#Use File::Find instead of glob to handle nested directories
#Should be trivial to extend to finding c files as well
sub find_headers {
  my $dirname = shift;
  my $headers = [];
  my $sources = [];
  my $wanted = sub {
    if (m/\.h$/) {
      push($headers,$File::Find::name);
    } elsif (m/\.c$/){
      push($sources,$File::Find::name);
    }
    return;
  };
  find({wanted => $wanted, no_chdir => 1}, $dirname);
  return $headers;
}
#looks for a c comment similar to the following
#/* symbol list:
#   actual symbol declarations */
#capturing the symbol declarations in $1 and returning them
my $symbol_block_re = qr|/\*\s+[sS]ymbol[_ ]?[lL]ist:?\n(.+?)\*/\s*|s;
sub find_symbol_list {
  my $file = shift;
  my $buffer = read_file($file);
  if($buffer =~ m/$symbol_block_re/){
    return $1;
  } else {
    return;
  }
}
sub find_symbol_init_fun {
  my $file = shift;    
  $file =~ m/(?:sl_(.*)\.c)|(.*)\.c/
  my $short_name = "$1$2";
  my $buffer = read_file($file);
  if ($buffer =~ m/void init_${short_name}_symbols(void){\n(.*)\n}/s) {
    return $1;
  } else {
    return;
  }
}

my $symbol_types = ["functions", "variables", "keywords", "constants"];
my $gen_symbol_declarations = { functions => \&gen_funct_decl,
                                variables => \&gen_var_decl,
                                constants => \&gen_const_decl,
                                Keywords => \&gen_key_decl };
#I don't know how to make this work
#my $symbol_decl = 'sl_obj Q$symbol;\nsl_symbol __Q$symbol;\n';
#I still need to figure out the format for the symbol lists
#perl can't inline, but trying to maintain the same code in 
#4 seperate functions is a bitch, so I'll take the hit

sub gen_funct_decl {
  my ($symbol,$info) = @_;
  return sprintf("sl_obj F$symbol%s;\nsl_subr __S$symbol;\n",
                 defun_args($info->{"args"}));
}
sub gen_const_decl {
  return "";
}
sub gen_var_decl {
  return "";
}
sub gen_key_decl {
  my ($symbol, $info) = @_;
  #strip _key from K prefixed symbol name
  my $symbol_basename = ($symbol =~ s/_key$//r);
  return "sl_obj K$symbol_basename = Q$symbol\n";
}
sub gen_symbol_decl {
  my ($symbol, $info, $type) = @_; #assume translated symbol name
  $symbol = translate_symbol_name($symbol);
  my $typed_decl = $gen_symbol_declarations->{$type}($symbol, $info);
#  my $typed_init = $gen_symbol_initialization->{$type}($symbol, $info);
  push($initial_symbols, "__Q$symbol");
  my $alias = $info->{"alias"};
  if($alias){
    $info->{"alias"} = undef; #prevent infinite recursion
    my @aliases = split(",",$alias);
    $alias = join("",map {gen_symbol_decl($_, $info, $type);} @aliases);
  } else {
    $alias = "";
  }
  my $decl = join("","sl_obj Q$symbol;\n",#"sl_symbol __Q$symbol;\n",
                  $typed_decl,$alias,"\n");
  return $decl;
}  
#Some explaination of naming:
#All symbols in the form of sl_objs are named Q##name,
#The actual sl_symbols are named __Q##name
#Functions have a struct sexp named S##name and a C function named F##name
#Variables have a pointer to an sl_obj named V##name, this points to the actual
#value of the variable, such that changing it changes the value of S##name
#Keywords are variables who's value is equivlent to their own symbol,
#i.e S##name->val = V##name = K##name

#note \s is for whitespace characters and \S is for non-whitespace characters
#the format is symbol_type symbol_name optional_info
#optional info is in the format type:info
my $symbol_decl_re = qr|^\s*(\S+)\s+(\S+)(?:\s+((?:\S+?:\S+?\s*)+))?\s*$|;

sub parse_symbol_list {
  my $symbol_list = shift;
  my $symbols = {};
  #say("parsing symbol list\n@$symbol_list"
  #put symbols into arrays according to symbol type
  #then run map on each of the arrays to generate the needed code
  for my $line (split("\n", $symbol_list)){
    if($line =~ m/$symbol_decl_re/){
      my ($type,$name,$extra) = ($1,$2,$3);
      $type = lc($type); #ignore case of variable type
      unless($symbols->{$type . "s"}){
        $symbols->{$type . "s"} = [];
      }
      my $extra_info = {};
      if ($extra) {
        while ( $extra =~ m/ *(.*?):([^ ]*)/g) {
          $extra_info->{$1} = $2;
        }
      }
      push($symbols->{$type . "s"}, 
           gen_symbol_decl($name,$extra_info,$type . "s"));
      $num_initial_symbols++;
    }
  }
  return $symbols;
}
sub parse_init_fun {
  my $init_fun = shift;
  my $symbols = {"functions" => [],
                 "variables" => [],
                 "constants" => [],
                 "keywords" => []};
  for my $line (split(@{$init_fun})){
    if($line =~ m/ *INIT_SUBR\((.*)\);/){
      #funciton stuff
    } else if($line =~
sub write_header {
  say("Writing header");
  my ($symbol_table,$fd) = @_;
  unless($fd){
    $fd = *STDOUT;
  }
  say("printing to $fd");
  print($fd
        ("/* This file was automatically generated by symbol_parse.pl */\n",
         "/* Do not edit by hand */\n"));
  my ($file,$symbols);
  while(($file,$symbols) = each($symbol_table)){
    print($fd "/* Symbols from $file */\n");
    for my $type (keys($symbols)){
        print($fd ("/* ",ucfirst($type)," */\n",@{$symbols->{$type}},"\n"));
    }
  }
  print($fd "const unsigned int num_initial_symbols = $num_initial_symbols;\n");
  print($fd "sl_symbol *initial_symbols[$num_initial_symbols] = {\n");
  #deal with trailing comma
  my $sym = pop($initial_symbols);
  print($fd "    $sym");
  map {print($fd ",\n    $_");} @{$initial_symbols};
  print($fd " };\n");
  
  print($fd "void initialize_symbols(void){\n");
  map {print($fd "$_");} @{$symbol_init};
}
sub usage {
  # For now assume sources is just a single directory
  print("Usage: symbol_parse.pl source_dir [output_file]\n",
        "\tsource dir is searched recursively for .h files which are scanned\n",
        "\tfor symbols, output is printed to output_file (default stdout)\n");
  exit(1);
}
if(scalar(@ARGV) < 1){
  usage();
}
#$symbol_table is a hash of hashes of arrays/hashes,
# i.e {{[],[],...}, {[],[],...}, ...}
#the keys of the first hash are the header names, the keys of the second hash
#are symbol types and the values of the second hash are the actual symbols
#Along with any symbol specific info
my $symbol_table = {};
my $outfile;
my $srcdir = shift; #first command line arg
my $outfile_path = shift;
if($outfile_path){
  open($outfile, ">", $outfile_path);
} else {
  $outfile = \*STDOUT;
}
my $headers = find_headers($srcdir);
if($headers == []){
  die "No headers found";
}
say("@$headers");
for my $header (@$headers) {
  say("parsing $header");
  if (my $symbol_list = find_symbol_list($header)) {
    $symbol_table->{$header} = parse_symbol_list($symbol_list);
  }

}
write_header($symbol_table, $outfile);

# Local Variables:
# perl-indent-level: 2
# cperl-indent-level: 2
# End:
