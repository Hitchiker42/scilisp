#!/bin/bash
#A simple script to run make check from a freshly checked out repository
if [ ! -d build_aux ]; then
    autoreconf -i -I m4
else
    autoreconf -I m4
fi
./configure --enable-debug
if [ $? -eq 0 ]; then
    make check
fi;
exit $?
