#| macros for various commonly used control structures built upon
   the few special forms (if, while, progn) for control flow |#
(defmacro when (test &rest body)
  `(if ,test (progn ,@body)))
(defmacro unless (test &rest body)
  `(if (not ,test) (progn ,@ body)))
(defmacro prog1 (form &rest forms)
  (let ((retval (gensym)))
    `(let ((,retval ,form))
       ,@forms
       retval)))
(defmacro prog2 (form-1 form-2 &rest forms)
  `(progn
     ,form-1
     (prog1 ,form-2 ,@forms)))
(defmacro until (test &rest body)
  `(while (not ,test) ,@body))
(defmacro do (bindings test &rest body)
  (let ((init 
         (mapcar (lambda (x) (list (car x) (cadr x))) bindings))
        (step 
         (mapcar (lambda (x) (if (cons? (cddr x)) (caddr x) nil)) bindings)))
    `(let (,@init)
       (until ,test
              ,@body
              ,@step))))
(defmacro dolist (binding &rest body)
  (let ((ls (cadr binding))
        (var (car binding)))    
    `(let (,var)
       (while (cons? ,ls)
         (set! ,var (car ,ls))
         (set! ,ls (cdr ,ls))
         ,@body))))
        
