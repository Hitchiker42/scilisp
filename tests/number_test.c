#include "scilisp_test.h"
#include "scilisp.h"
//bignums ought to be transparent for the most part, so I don't
//know if I'm going to include bignum.h in scilisp.h
#include "bignum.h"
static const double e = 2.781828;
//allow changing of the number used eaisly
static const long test_int = 17;
/*
  tests for numbers and basic math functions
*/
START_TEST(test_make_double){
  sl_obj double_test = make_double(e);
  ck_assert(NUMBERP(double_test));
  sl_number *num = XNUMBER(double_test);
  ck_assert_non_null(num);
  ck_assert(num->float64 == e);
  ck_assert(XDOUBLE(double_test) == e);
  ck_assert(NUMBERP(double_test));
  ck_assert(IND_NUMBERP(double_test));
  ck_assert(FLOATP(double_test));
  sl_assert_eq(double_test, double_test);
  sl_assert_eqv(double_test, double_test);
  sl_assert_equal(double_test, double_test);
}
END_TEST
START_TEST(test_make_bigint){
  sl_bigint *bigint = int_to_bigint(test_int);
  sl_obj bigint_test = make_bigint(bigint);
  ck_assert(NUMBERP(bigint_test));
  ck_assert(INTEGERP(bigint_test));
  sl_number *num = XNUMBER(bigint_test);
  ck_assert_non_null(num);
  ck_assert(SL_number_is_bignum(num));
  sl_bigint *bigint2 = num->bigint;
  ck_assert_ptr_eq(bigint, bigint2);
  ck_assert(bigint_uint_eq(bigint_test, PACK_INT(test_int)));
}
END_TEST
START_TEST(test_make_bigfloat){
  sl_bigfloat *bigfloat = float_to_bigfloat(e);
  sl_obj bigfloat_test = make_bigfloat(bigfloat);
  ck_assert(NUMBERP(bigfloat_test));
  sl_number *num = XNUMBER(bigfloat_test);
  ck_assert_non_null(num);
  ck_assert(SL_number_is_bignum(num));
  sl_bigfloat *bigfloat2 = num->bigfloat;
  ck_assert_ptr_eq(bigfloat, bigfloat2);
  ck_assert(bigfloat_float_eq(bigfloat_test, make_double(e)));
}
END_TEST

int main(int argc, const char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 1, test_make_double);
/*  TCase *tc_sanity = gen_test_case("sanity", 3, test_make_double,
                                   test_make_bigint, test_make_bigfloat);*/
  Suite *s = gen_test_suite("number", 1, tc_sanity);
  int num_failed = run_test_suite(s);
  return (num_failed == 0 ? 0 : 1);
}
