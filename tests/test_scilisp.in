#!/bin/bash
#this file is appended to another file which sets the variable scilisp to
#a path to the scilisp interpreter, the line above is just for emacs
#sanity test
#scilisp is defined as a path to the scilisp interpreter

tests_failed=0
test_number=0
#this holds (the code needed to find) the file, function, and line number from
#which the currently executing function was called from
#To actually use it do the following: eval local temp = ${called_from};
#then temp will hold the desired information
called_from='$(basename ${BASH_SOURCE[2]}):${FUNCNAME[1]}:${BASH_LINENO[1]}'
#or call this (which allows for finding the call site of a function an arbitary$
#way up the call stack
find_called_from() {
    local depth=$1;
    if [ -n "$depth" ]; then
        depth=$((depth+2));
    else
        depth=2;
    fi
    #need to use variables because the whole string won't fit on one line
    local source_file="$(basename ${BASH_SOURCE[$((depth+1))]})";
    local location="${FUNCNAME[$((depth))]}:${BASH_LINENO[$((depth))]}";
    return "${source_file}:${location}";
}
is_integer(){
    return $(echo "$1" | egrep -qvw "[0-9]+")
}

update_results(){
    eval local call_site="${called_from}";
    local result;
    if [ $? -ne 0 ]; then
        result="${call_site}: Failed, returned value $?"
    else
        result="${call_site}: Passed"
    fi

    1>&2 echo "$result"
}
fail_test(){
    local depth="$1"
    local msg="$2"
    1>&2 echo "$(basename ${BASH_SOURCE[$((depth+1))]}):${FUNCNAME[$((depth))]}:"\
"${BASH_LINENO[$((depth))]}: Failed with message $msg"
    exit 1;
}
#clever little function I got from stack overflow
#it creates a signal handler for each signal given that calls func with the
#signal number of that signal
trap_with_arg(){
    func="$1"; shift;
    for sig in "$@"; do
        trap "$func $sig" "$sig"
    done
}
catch_signal(){
    local signum,signame;
    if $(is_integer "$1"); then
        signum="$1";
        signame=$(kill -l "$1");
    else
        signame="$1";
        signum=$(kill -l "$1");
    fi
    1>&2 echo "$(basename ${BASH_SOURCE[2]}):${FUNCNAME[1]}:${BASH_LINENO[1]}: "\
"Failure: Signal $signum ($signame) received"
    exit 2;
}
run_test_safely(){
    #evaluate the given arguments in a subshell, trapping all possible signals
    #the test itself is responsable for updating the results
    (trap_with_arg catch_signal $(seq -s " " 1 30) ; "$@")
    exit_status=$?
    test_number=$((test_number+1))
    [[ "$exit_status" -eq 0 ]] || tests_failed=$((tests_failed+1));
}
run_tests(){
    for sl_test in "$@"; do
        echo "$sl_test:";
        run_test_safely "$sl_test"
    done
    if [ $tests_failed -eq 0 ]; then
        exit 0;
    else
        exit 1;
    fi
}
sanity_test(){
    #just make sure the program runs without crashing
    $scilisp -Q;
    update_results;
}
2>&1 echo -e "Running tests:\n"
run_tests 'sanity_test'
