#include "scilisp_test.h"
#include "../src/scilisp.h"
sl_symbol **generate_symbols(int num){
  sl_symbol **syms = sl_malloc(sizeof(sl_symbol*)*num);
  int i;
  for(i=0;i<num;i++){
    syms[i] = XSYMBOL(Fgensym());
  }
  return syms;
}
sl_symbol **gen_add_symbols(sl_symbol_table *table, int num){
  int i;
  sl_symbol **syms = sl_malloc(sizeof(sl_symbol*)*num);
  for(i=0;i<num;i++){
    syms[i] = add_symbol(table, XSYMBOL(Fgensym()));
  }
  return syms;
}
//returns 0 on success or the index+1 of the first element that didn't match
int check_symbols_present(sl_symbol_table *ht, sl_symbol **syms, int num){
  int i;
  for(i=0;i<num;i++){
    if(syms[i] != find_symbol_by_name(ht, &syms[i]->name)){
      return i+1;
    }
  }
  return 0;
}
START_TEST(test_symbol_table_basic){
  sl_symbol_table *table = make_symbol_table(0,0);
  mark_point();
  sl_string *name = make_string_const_unboxed("symbol");
  mark_point();
  sl_symbol *sym = XSYMBOL(make_symbol_from_string(name));
  mark_point();
  add_symbol(table, sym);
  mark_point();
  sl_symbol *test = find_symbol_by_name(table, name);
  mark_point();
  ck_assert_ptr_eq(sym, test);
}
END_TEST

START_TEST(test_gensym){
  sl_symbol_table *table = make_symbol_table(0,0);
  //this is generally a bad idea, this is just for testing
  sl_string *name = sl_sprintf("#:G%d", 0, gensym_counter+1);
  sl_symbol *gensym_sym = XSYMBOL(Fgensym());
  sl_symbol *gensym_sym2 = XSYMBOL(Fgensym());
  ck_assert_ptr_ne(gensym_sym, gensym_sym2);//I don't know how this could fail
  sl_assert_not_equal(PACK_STRING(&gensym_sym->name),
                      PACK_STRING(&gensym_sym2->name));
  DEBUG_PRINTF("generated symbol %.*s\nExpected name %.*s\n",gensym_sym->name.len,
               gensym_sym->name.str,name->len,name->str);
  sl_assert_equal(PACK_STRING(&gensym_sym->name),
                  PACK_STRING(name));
  add_symbol(table, gensym_sym);
  ck_assert_ptr_eq(PACK_SYMBOL(gensym_sym),
                   PACK_SYMBOL(find_symbol_by_name(table,name)));
}
END_TEST
START_TEST(test_symbol_table_multi){
  sl_symbol_table *table = make_symbol_table(256,0.8);
  sl_symbol **syms = gen_add_symbols(table,100);
  sl_assert_eq(sl_0, PACK_INT(check_symbols_present(table,syms,100)));
}
END_TEST
START_TEST(test_find_symbol){
  //tests finding builtin symbols
  mark_point();
  scilisp_init();
  ck_assert(thread_env->obarray);
  ck_assert(global_obarray);
  ck_assert(global_obarray == thread_env->obarray);
  mark_point();
  sl_string *name = make_string_const_unboxed("cons");
  sl_symbol *cons_sym = find_symbol_by_name(global_obarray, name);
  mark_point();
  ck_assert_non_null(cons_sym);
  DEBUG_PRINTF("((sl_subr*)cons_sym->val)->f2 == %p, Fcons = %p\n",
               XSUBR(cons_sym->val)->f2, Fcons);
  ck_assert(XSUBR(cons_sym->val)->f2 == Fcons);
  sl_symbol *cons_sym2 = find_symbol_by_name(thread_env->obarray,
                                            make_string_const_unboxed("cons"));
  ck_assert(cons_sym == cons_sym2);  
}
END_TEST
START_TEST(test_add_symbol){
  sl_symbol *new_sym = XSYMBOL(Fgensym());
  new_sym->val = sl_5;
  sl_string *symbol_name = &(new_sym->name);
  add_symbol(thread_env->obarray, new_sym);
  sl_symbol *found_symbol = find_symbol_by_name(thread_env->obarray, 
                                                symbol_name);
  ck_assert(new_sym == found_symbol);
  ck_assert(found_symbol->val == sl_5);
}
END_TEST
START_TEST(test_intern){
  sl_obj new_sym = Fgensym();
  sl_obj found_sym = Fintern(new_sym, NIL);
  ck_assert(new_sym == found_sym);
  ck_assert(XSYMVAL(new_sym) == XSYMVAL(found_sym) &&
            XSYMVAL(new_sym) == Qunbound);
}
END_TEST
  
int main(int argc, char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 2, test_symbol_table_basic,
                                     test_gensym);
  TCase *tc_volume = gen_test_case("volume", 1, test_symbol_table_multi);
  TCase *tc_lookup_add = gen_test_case("lookup-add", 3, test_find_symbol,
                                       test_add_symbol, test_intern);
  Suite *s = gen_test_suite("symbol", 3, tc_sanity, tc_volume, tc_lookup_add);
  
  int num_failed = run_test_suite(s);

  return (num_failed == 0 ? 0 : 1); 
}
