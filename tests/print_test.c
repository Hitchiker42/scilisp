#include "scilisp_test.h"
#include "../src/print.h"
#include "../src/sl_string.h"
#include "../src/cons.h"
#include "../src/vector.h"
START_TEST(test_basic_print){
  sl_obj nil_control = make_string_const("nil");
  sl_obj t_control = make_string_const("t");
  sl_obj one_control = make_string_const("1");
  sl_obj num_control = make_string_const("1234567890");

  fprintf(stderr, "%s\n", c_sprint(NIL));
  ck_assert_sl_equal(nil_control, Fsprint(NIL));
  
  fprintf(stderr, "%s\n", c_sprint(SL_T));
  ck_assert_sl_equal(t_control, Fsprint(SL_T));
  
  fprintf(stderr, "%s\n", c_sprint(sl_1));
  sl_assert_equal(one_control, Fsprint(sl_1));
  ck_assert_sl_equal(one_control, Fsprint(sl_1));

  fprintf(stderr, "%s\n", c_sprint(make_int(1234567890)));
  ck_assert_sl_equal(num_control, Fsprint(make_int(1234567890)));
}
END_TEST
START_TEST(test_print_cons){
  sl_obj pair = Fcons(sl_1, sl_2);
  sl_obj list = make_list(sl_1,sl_2,sl_3,sl_4,sl_5);
  sl_obj improper_list = Fcons(sl_1, Fcons(sl_2, Fcons(sl_3, sl_4)));

  sl_obj pair_control = make_string_const("(1 . 2)");
  sl_obj list_control = make_string_const("(1 2 3 4 5)");
  sl_obj improper_list_control = make_string_const("(1 2 3 . 4)");

  mark_point();

  sl_obj list_test = Fsprint(list);
  sl_obj pair_test = Fsprint(pair);
  sl_obj improper_list_test = Fsprint(improper_list);
  sl_assert_equal(list_control, list_test);
  sl_assert_equal(pair_control, pair_test);
  ck_assert_sl_equal(improper_list_control, improper_list_test);
}
END_TEST
START_TEST(test_print_vector){
  sl_obj *vec_mem = sl_malloc(10*sizeof(sl_obj));
  int i;
  for(i=0;i<10;i++){
    vec_mem[i] = make_int(i);
  }
  sl_obj int_vec = make_svector_obj(vec_mem, 10);
  sl_obj int_vec_control = make_string_const("[0 1 2 3 4 5 6 7 8 9]");
  sl_string *int_vec_test =sl_sprint(int_vec);
  DEBUG_PRINT_STR(int_vec_test);
  sl_assert_equal(int_vec_control, PACK_STRING(int_vec_test));
  sl_assert_equal(int_vec_control, Fsprint(int_vec));
}
END_TEST
  
int main(int argc, char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 1, test_basic_print);
  TCase *tc_seq = gen_test_case("sequence", 2, test_print_cons, test_print_vector);
  Suite *s = gen_test_suite("print", 2, tc_sanity, tc_seq);
  int num_failed = run_test_suite(s);
  return (num_failed == 0 ? 0 : 1);
}
