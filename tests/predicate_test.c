#include "scilisp_test.h"
#include "../src/scilisp.h"
static sl_obj int_test_1a, int_test_1b;
static sl_obj char_test_1a, char_test_1b;
static sl_obj double_test_1a, double_test_1b;
static sl_obj string_test_1a, string_test_1b;
static sl_obj cons_test_1a, cons_test_1b;
static sl_obj vector_test_1a, vector_test_1b;
void init_test_objs(){
  int_test_1a = make_int(1234321);
  int_test_1b = make_int(1234321);
  char_test_1a = make_char('a');
  char_test_1b = make_char('a');
  double_test_1a = make_double(2.781828);
  double_test_1b = make_double(2.781828);
  string_test_1a = make_string_const("Hello, World!");
  string_test_1b = make_string_const("Hello, World!");
  cons_test_1a = make_list(sl_1, sl_2, sl_3, sl_4, sl_5);
  cons_test_1b = make_list(sl_1, sl_2, sl_3, sl_4, sl_5);
  vector_test_1a = build_vector(5, sl_1, sl_2, sl_3, sl_4, sl_5);
  vector_test_1b = build_vector(5, sl_1, sl_2, sl_3, sl_4, sl_5);
}
//tests for predicate functions
START_TEST(test_eq){
  init_test_objs();
  sl_assert_eq(int_test_1a,int_test_1b);
  sl_assert_eq(char_test_1a, char_test_1b);
  sl_assert_not_eq(double_test_1a, double_test_1b);
  sl_assert_not_eq(string_test_1a, string_test_1b);
  sl_assert_not_eq(cons_test_1a, cons_test_1b);
  sl_assert_not_eq(vector_test_1a, vector_test_1b);
  sl_assert_not_eq(vector_test_1a, cons_test_1a);
}
END_TEST
START_TEST(test_eqv){
  init_test_objs();
  sl_assert_eqv(int_test_1a,int_test_1b);
  sl_assert_eqv(char_test_1a, char_test_1b);
  sl_assert_not_eqv(double_test_1a, double_test_1b);
  sl_assert_not_eqv(string_test_1a, string_test_1b);
  sl_assert_not_eqv(cons_test_1a, cons_test_1b);
  sl_assert_not_eqv(vector_test_1a, vector_test_1b);
  sl_assert_not_eqv(vector_test_1a, cons_test_1a);
}
END_TEST
START_TEST(test_equal){
  init_test_objs();
  sl_assert_equal(int_test_1a,int_test_1b);
  sl_assert_equal(char_test_1a, char_test_1b);
  DEBUG_PRINTF("%#0lx\n%#0lx\n", double_test_1a, double_test_1b);
  sl_assert_equal(double_test_1a, double_test_1b);
  sl_assert_equal(string_test_1a, string_test_1b);
  sl_assert_equal(cons_test_1a, cons_test_1b);
  sl_assert_equal(vector_test_1a, vector_test_1b);
  sl_assert_not_equal(vector_test_1a, cons_test_1a);

}
END_TEST
START_TEST(test_equalp){
  init_test_objs();
  sl_assert_equalp(int_test_1a,int_test_1b);
  sl_assert_equalp(char_test_1a, char_test_1b);
  sl_assert_equalp(double_test_1a, double_test_1b);
  //  sl_assert_equalp(string_test_1a, string_test_1b);
  sl_assert_equalp(cons_test_1a, cons_test_1b);
  sl_assert_equalp(vector_test_1a, vector_test_1b);
  sl_assert_equalp(vector_test_1a, cons_test_1a);

}
END_TEST
START_TEST(test_type_predicates){
  init_test_objs();
  ck_assert(Fconsp(cons_test_1a));
  ck_assert(Farrayp(vector_test_1a));
  ck_assert(Farrayp(string_test_1a));
  ck_assert(Fvectorp(vector_test_1a));
  ck_assert(Fvectorp(string_test_1a));
  ck_assert(Fsvectorp(vector_test_1a));
  ck_assert(!Fsvectorp(string_test_1a));
  ck_assert(Fintegerp(int_test_1a));
  ck_assert(Ffloatp(double_test_1a));
}
END_TEST

int main(int argc, char *argv[]){
  scilisp_init();
  init_test_objs();
  TCase *tc_equality = gen_test_case("equality", 4, test_eq, test_eqv,
                                     test_equal, test_equalp);
  TCase *tc_typep = gen_test_case("typep", 1, test_type_predicates);
  Suite *s = gen_test_suite("predicate", 2, tc_equality, tc_typep);
  int num_failed = run_test_suite(s);
  return (num_failed == 0 ? 0 : 1);
}
