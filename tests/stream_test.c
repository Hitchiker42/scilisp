#include "scilisp_test.h"
#include "../src/stream.h"
#include "../src/sl_string.h"
START_TEST(test_make_stream){
  //the only way we know how to make streams is from files
  const char *filename = CHECK_DATA_DIR "/hello_world.txt";
  sl_stream *test_stream_1 = sl_open(filename, SL_read_only);
  ck_assert_non_null(test_stream_1);
  ck_assert_int_eq(test_stream_1->type, SL_mem_stream);
  mark_point();
  sl_stream *test_stream_2 =
    file_descriptor_to_stream(STDIN_FILENO, SL_read_only);
  ck_assert_non_null(test_stream_2);
  ck_assert_int_eq(test_stream_2->type, SL_file_stream);

  sl_string *str = make_string_const_unboxed("hello_world");
  sl_stream *test_stream_3 = string_to_stream(str);
  ck_assert_non_null(test_stream_3);
  ck_assert_int_eq(test_stream_3->type, SL_string_stream);
}
END_TEST
/*
  The next three tests are basically the same repeated with different stream types
  there's probably a better way to do this, but since it's just a test it's fine

  they should probably be broken into multiple tests though
*/
START_TEST(test_mem_stream_generic){
  const char *expected_output = "hello, world!";
  char output_buf[128];
  char *buf_ptr = output_buf;
  sl_stream *test_stream_1 = sl_open(CHECK_DATA_DIR "/hello_world.txt", SL_read_only);
  ck_assert_non_null(test_stream_1);
  ck_assert_int_eq(test_stream_1->type, SL_mem_stream);
  sl_char c;
  mark_point();
  while((c = stream_read_char(test_stream_1)) != EOF){    
    *buf_ptr++ = (char)c;
  }
  mark_point();
  *buf_ptr++ = '\0';
  ck_assert_str_eq(expected_output, output_buf);

  /*
    Check to make sure that the stream gives the same result if we read it again
   */
  stream_seek(test_stream_1, 0, SEEK_SET);//return to the start

  char *second_output = buf_ptr;
  while((c = stream_read_char(test_stream_1)) != EOF){
    *buf_ptr++ = (char)c;
  }
  *buf_ptr++ = '\0';
  ck_assert_str_eq(expected_output, second_output);
  ck_assert_str_eq(output_buf, second_output);
  stream_cleanup(test_stream_1);
} END_TEST

START_TEST(test_mem_stream_specific){
  sl_char c;
  //test ungetc and mem stream specific funcitons
  uint8_t test_input[] = {'a','b','c','d','e','\0',EOF};
  mem_stream *test_stream_2 = buffer_to_mem_stream(test_input, 7);
  ck_assert_non_null(test_stream_2);
  mem_stream_read_char(test_stream_2);//a
  mem_stream_read_char(test_stream_2);//b
  mem_stream_unread_char(test_stream_2);//put back b
  c = mem_stream_read_char(test_stream_2);
  ck_assert_int_eq(c,'b');
  c = mem_stream_read_char(test_stream_2);
  ck_assert_int_eq(c,'c');
} END_TEST
START_TEST(test_file_stream_generic){
  const char *expected_output = "hello, world!";
  char output_buf[128];
  char *buf_ptr = output_buf;
  FILE *c_stream = fopen(CHECK_DATA_DIR "/hello_world.txt","r");  
  sl_stream *test_stream_1 = FILE_to_stream(c_stream, SL_read_only);
  ck_assert_non_null(test_stream_1);
  ck_assert_int_eq(test_stream_1->type, SL_file_stream);
  sl_char c;
  while((c = stream_read_char(test_stream_1)) != EOF){
    *buf_ptr++ = (char)c;
    fputc(c, stderr);
  }
  *buf_ptr++ = '\0';
  ck_assert_str_eq(expected_output, output_buf);

  /*
    Check to make sure that the stream gives the same result if we read it again
   */
  stream_seek(test_stream_1, 0, SEEK_SET);//return to the start

  char *second_output = buf_ptr;
  while((c = stream_read_char(test_stream_1)) != EOF){
    *buf_ptr++ = (char)c;
  }
  *buf_ptr++ = '\0';
  ck_assert_str_eq(expected_output, second_output);
  ck_assert_str_eq(output_buf, second_output);
  stream_cleanup(test_stream_1);
} END_TEST
START_TEST(test_file_stream_specific){
  //test ungetc andstream specific funcitons
  sl_char c;
  char test_input[] = {'a','b','c','d','e','\0',EOF};
  FILE *c_str_stream = fmemopen(test_input, 7, "r");
  file_stream *test_stream_2 = file_stream_from_FILE(c_str_stream);  

  ck_assert_non_null(test_stream_2);
  file_stream_read_char(test_stream_2);//a
  file_stream_read_char(test_stream_2);//b
  file_stream_unread_char(test_stream_2);//put back b
  c = file_stream_read_char(test_stream_2);
  ck_assert_int_eq(c,'b');
  c = file_stream_read_char(test_stream_2);
  ck_assert_int_eq(c,'c');
  fclose(c_str_stream);
} END_TEST
START_TEST(test_string_stream_generic){
  const char *expected_output = "hello, world!";
  char output_buf[128];
  char *buf_ptr = output_buf;
  sl_stream *test_stream_1 = string_to_stream(make_string_const_unboxed("hello, world!"));
  ck_assert_int_eq(test_stream_1->type, SL_string_stream);
  sl_char c;
  while((c = stream_read_char(test_stream_1)) != EOF){
    *buf_ptr++ = (char)c;
  }
  *buf_ptr++ = '\0';
  ck_assert_str_eq(expected_output, output_buf);

  /*
    Check to make sure that the stream gives the same result if we read it again
    and that seeking works
   */
  stream_seek(test_stream_1, 0, SEEK_SET);//return to the start

  char *second_output = buf_ptr;
  while((c = stream_read_char(test_stream_1)) != EOF){
    *buf_ptr++ = (char)c;
  }
  *buf_ptr++ = '\0';
  ck_assert_str_eq(expected_output, second_output);
  ck_assert_str_eq(output_buf, second_output);
}
END_TEST
START_TEST(test_string_stream_specific){
  //test ungetc andstream specific funcitons
  sl_char c;
  char test_input[] = {'a','b','c','d','e','\0',EOF};
  sl_string *test_string = make_string_unboxed(test_input, 7, 0);
  string_stream *test_stream_2 = string_to_string_stream(test_string);
  ck_assert_non_null(test_stream_2);
  string_stream_read_char(test_stream_2);//a
  string_stream_read_char(test_stream_2);//b
  string_stream_unread_char(test_stream_2);//put back b
  c = string_stream_read_char(test_stream_2);
  ck_assert_int_eq(c,'b');
  c = string_stream_read_char(test_stream_2);
  ck_assert_int_eq(c,'c');
}
END_TEST

//START_TEST(test_utf8_opaque){
  
int main(int argc, char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 1, test_make_stream);
  TCase *tc_mem_stream = gen_test_case("mem_stream", 2,test_mem_stream_generic,
                                       test_mem_stream_specific);
  TCase *tc_file_stream = gen_test_case("file_stream", 2,test_file_stream_generic,
                                        test_file_stream_specific);
  TCase *tc_string_stream = gen_test_case("string_stream", 2,test_string_stream_generic,
                                          test_string_stream_specific);
  Suite *s = gen_test_suite("stream", 4, tc_sanity, tc_mem_stream,
                            tc_file_stream, tc_string_stream);
  int num_failed = run_test_suite(s);
  return (num_failed == 0 ? 0 : 1);
}
                                       
