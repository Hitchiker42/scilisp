#include "scilisp_test.h"
#include "../src/scilisp.h"
#include "math.h"
/*
   In order to fully test math operations we're going to need macros
   similiar to those used to generate math functions, which means this
   might be a bit messy
*/
sl_obj bigint_1, bigint_2, bigint_3;
sl_obj double_1, double_2, double_e, double_3, double_pi;
sl_obj bigfloat_1, bigfloat_2, bigfloat_e, bigfloat_3, bigfloat_pi;
sl_obj int_1, int_2, int_3;
void init_test_vars(void){
  int_1 = sl_1;
  int_2 = sl_2;
  int_3 = sl_3;
  
  bigint_1 = make_bigint_from_int(1);
  bigint_2 = make_bigint_from_int(2);
  bigint_3 = make_bigint_from_int(3);

  double_1 = make_double(1.0);
  double_2 = make_double(2.0);
  double_e = make_double(2.7182818284590452354);
  double_3 = make_double(3.0);
  double_pi = make_double(3.14159265358979323846);

  bigfloat_1 = make_bigfloat_from_float(1.0);
  bigfloat_2 = make_bigfloat_from_float(2.0);
  bigfloat_e = make_bigfloat_from_float(M_E);
  bigfloat_3 = make_bigfloat_from_float(3.0);
  bigfloat_pi = make_bigfloat_from_float(M_PI);
}
//we first need to test numeric equality, since we need to work
//for all the other tests.
//to make sure num_eq fails correctly, without relying on num_ne
#define sl_assert_not_num_eq(expr1, expr2)      \
  sl_assert_comparison(!sl_num_eq, expr1, expr2)
#define do_types(fn,type,num1,num2)                                     \
  fn(type##_##num1, int_##num2);                                        \
  fn(type##_##num1, bigint_##num2);                                     \
  fn(type##_##num1, double_##num2);                                     \
  fn(type##_##num1, bigfloat_##num2);
START_TEST(test_num_eq){
  do_types(sl_assert_num_eq, int, 1, 1);
  do_types(sl_assert_not_num_eq, int, 1, 2);

  do_types(sl_assert_num_eq, double, 1, 1);
  do_types(sl_assert_not_num_eq, double, 1, 2);

  do_types(sl_assert_num_eq, bigint, 1, 1);
  do_types(sl_assert_not_num_eq, bigint, 1, 2);

  do_types(sl_assert_num_eq, bigfloat, 1, 1);
  do_types(sl_assert_not_num_eq, bigfloat, 1, 2);

  //testing non integer floats
  sl_assert_num_eq(double_pi, bigfloat_pi);
  sl_assert_num_eq(bigfloat_pi, double_pi);
  sl_assert_num_eq(double_e, bigfloat_e);
  sl_assert_num_eq(bigfloat_e, double_e);
}
END_TEST
#undef do_types

//lets just bite the bullet and write a long test
START_TEST(test_sl_add){
  sl_assert_num_eq(sl_add(int_1,int_2), int_3);
  sl_assert_num_eq(sl_add(bigint_1,bigint_2), bigint_3);
  sl_assert_num_eq(sl_add(double_1,double_2), double_3);
  sl_assert_num_eq(sl_add(bigfloat_1,bigfloat_2), bigfloat_3);

  sl_assert_num_eq(sl_add(int_1,double_2), double_3);
  sl_assert_num_eq(sl_add(double_1,int_2), double_3);

  sl_assert_num_eq(sl_add(int_1,bigint_2), bigint_3);
  sl_assert_num_eq(sl_add(bigint_1,int_2), bigint_3);

  sl_assert_num_eq(sl_add(int_1,bigfloat_2), bigfloat_3);
  sl_assert_num_eq(sl_add(bigfloat_1,int_2), bigfloat_3);

  sl_assert_num_eq(sl_add(bigint_1, double_2), double_3);
  sl_assert_num_eq(sl_add(double_1, bigint_2), double_3);

  sl_assert_num_eq(sl_add(bigfloat_1, double_2), bigfloat_3);
  sl_assert_num_eq(sl_add(double_1, bigfloat_2), bigfloat_3);

  sl_assert_num_eq(sl_add(bigint_1, bigfloat_2), bigfloat_3);
  sl_assert_num_eq(sl_add(bigfloat_1, bigint_2), bigfloat_3);

  //check non-integer floating pt
  sl_obj bigfloat_pi_e = make_bigfloat_from_float(M_PI+M_E);
  sl_obj double_pi_e = sl_add(double_pi, double_e);
  sl_assert_num_eq(double_pi_e, make_double(XDOUBLE(double_pi) + XDOUBLE(double_e)));
  sl_assert_num_eq(sl_add(double_pi, bigfloat_e), bigfloat_pi_e);
  sl_assert_num_eq(sl_add(bigfloat_pi, double_e), bigfloat_pi_e);
  sl_assert_num_eq(sl_add(bigfloat_pi, bigfloat_e), bigfloat_pi_e);
}
END_TEST

int main(int argc, char *argv[]){
  scilisp_init();
  init_test_vars();
  TCase *tc_compare = gen_test_case("compare", 1, test_num_eq);
  TCase *tc_binops = gen_test_case("binops", 1, test_sl_add);
  Suite *s = gen_test_suite("math", 2, tc_compare, tc_binops);
  int num_failed = run_test_suite(s);
  return (num_failed == 0 ? 0 : 1);
}
