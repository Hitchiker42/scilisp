#include "scilisp_test.h"
#include "../src/sl_string.h"
#include "../src/sequence.h"
/*
  tests for strings and string buffers
*/
START_TEST(test_make_string){
  char *control = "hello, world";//size = 12 + 1, len = 12
  //make_string_const sets length to sizeof(str)-1, i.e the null byte isn't
  //included in the length
  sl_string *const_test = make_string_const_unboxed("hello, world");
  sl_string *const_test_copied = XSTRING(make_string(control, 12, 0));
  char temp[] = "hello, world";//makes a mutable copy on the stack
  sl_string *copied_test = XSTRING(make_string(temp, 12, 0));

  ck_assert_int_eq(const_test->len, 12);
  ck_assert_int_eq(strlen(const_test->str),12);
  ck_assert_arr_equal(control, const_test->str, 12);

  ck_assert_int_eq(const_test_copied->len, 12);
  ck_assert_arr_equal(control, const_test_copied->str, 12);

  ck_assert_int_eq(copied_test->len, 12);
  ck_assert_arr_equal(control, copied_test->str, 12);

  sl_assert_equal(PACK_STRING(const_test), PACK_STRING(copied_test));

  ck_assert_ptr_eq(const_test->str, control);
  ck_assert_ptr_ne(copied_test->str, temp);
}
END_TEST  

START_TEST(test_debug_print_str){
  //you just need to examine the test log for this
  sl_obj hello = make_string_const("hello, world");
  sl_obj goodbye = make_string_const("goodbye, world");

  DEBUG_PRINT_STR(XSTRING(hello));
  DEBUG_PRINT_STR(XSTRING(goodbye));
}
END_TEST

START_TEST(test_str_nuls){
  char control[] = {'1','\0','2','\0','3','\0'};
  sl_string *str = make_string_unboxed(control, 6, 0);
  sl_obj str_obj = PACK_STRING(str);
  ck_assert_int_eq(Flength(str_obj), sl_6);
  if(memcmp(control,str->str,6) != 0){
    ck_abort_msg("String with embedded nul's not equal to control string");
  }
  ck_assert_int_eq(STR_REF(str_obj,sl_3),0);
}
END_TEST 

int main(int argc, char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 2, test_make_string,
                                   test_debug_print_str);
  TCase *tc_nuls = gen_test_case("nuls", 1, test_str_nuls);
  Suite *s = gen_test_suite("string", 2, tc_sanity, tc_nuls);

  int num_failed = run_test_suite(s);

  return (num_failed == 0 ? 0 : 1);
}
  
