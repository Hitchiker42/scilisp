/* main file for running tests, doesn't contain any tests itself*/
#include "scilisp_test.h"

START_TEST(test_pass){
  ck_assert(1);
}
END_TEST
START_TEST(test_scilisp_init){
  scilisp_init();
}
END_TEST
/* gen_test_case_fn(sanity, 1, test_pass); */
/* gen_test_case_fn(fail, 1, test_fail); */
/* gen_test_suite_fn(core, 2, test_sanity, test_fail); */

int main(int argc, char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 1, test_pass);
  TCase *tc_init = gen_test_case("init", 1, test_scilisp_init);
  Suite *s = gen_test_suite("core", 2, tc_sanity, tc_init);
  
  int num_failed = run_test_suite(s);

  return (num_failed == 0 ? 0 : 1);
}
