#include "scilisp_test.h"
#include "../src/scilisp.h"
/*
  tests for vectors and strings
*/
START_TEST(test_vector_simple){
  sl_svector *test_vector = make_svector(10);
  int i;
  for(i=0;i<10;i++){
    test_vector->value[i] = make_int(i);
  }
  sl_obj test_vector_obj = PACK_VECTOR(test_vector);
  ck_assert(Fvectorp(test_vector_obj));
  ck_assert(Fsvectorp(test_vector_obj));
  ck_assert(Farrayp(test_vector_obj));
  ck_assert_int_eq(offsetof(sl_vector, value), offsetof(sl_svector, value));
  ck_assert_int_eq(offsetof(sl_vector, len), offsetof(sl_svector, len));
  ck_assert(XVECTOR(test_vector_obj)->value);
  ck_assert(XSVECTOR(test_vector_obj)->value);
  mark_point();
  sl_obj vref_test = Fvref(test_vector_obj, sl_4);
  sl_assert_equal(sl_4, vref_test);
  //  ck_assert_arr_equal(XVECTOR(test_vector_obj)->value, control,10);

  debug_print(test_vector_obj);
  mark_point();
  sl_obj build_test = build_vector(10, sl_0, sl_1, sl_2, sl_3, sl_4, sl_5,
                                   sl_6, sl_7, sl_8, sl_9);
  debug_print(build_test);
  sl_assert_equal(build_test, test_vector_obj);
}
END_TEST
START_TEST(test_sequence_fns){
  sl_obj test_vector = build_vector(10, sl_0, sl_1, sl_2, sl_3, sl_4, sl_5,
                                    sl_6, sl_7, sl_8, sl_9);
  sl_assert_eq(Felt(test_vector,sl_3),sl_3);
  sl_assert_eq(Flength(test_vector),make_int(10));
}
END_TEST
START_TEST(test_make_vector){
  sl_obj test_vector = Fmake_vector(PACK_INT(10), sl_0);
  int i;
  sl_obj index;
  for(i=0;i<10;i++){
    index = PACK_INT(i);
    sl_assert_eq(Fvref(test_vector, index), sl_0);
    Fvset(test_vector, index, index);
    sl_assert_eq(Fvref(test_vector, index), index);
  }
}
END_TEST
  

int main(int argc, const char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 2, test_vector_simple,
                                   test_make_vector);
  TCase *tc_seq = gen_test_case("seq", 1, test_sequence_fns);
  Suite *s = gen_test_suite("vector", 2, tc_sanity, tc_seq);
  int num_failed = run_test_suite(s);
  return (num_failed == 0 ? 0 : 1);
}
