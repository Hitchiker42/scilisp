#include "scilisp_test.h"
#define gen_utf8_char_test(nbytes, size)                    \
  START_TEST(test_utf8_char_##name){                                    \
    size_t used;                                                        \
    uint32_t decode_test = utf8_decode_char(nbytes##_byte_test_enc, &used); \
    utf8_char_str encode_test = utf8_encode_char(nbytes##_byte_test_cp); \
    ck_assert_int_eq(used, size);                                       \
    ck_assert_int_eq(nbytes##_byte_test_cp, decode_test);               \
    ck_assert_arr_equal(encode_test.str, nbytes##_byte_test_enc, size); \
  }                                                                     \
  END_TEST
  

/* 1 byte: 0x00-0x7F */
uint32_t one_byte_test_cp = 0x22; /*!*/
char one_byte_test_enc[] = {0x22, 0x00};
/* 2 bytes: 0x80-0x7FF */
uint32_t two_byte_test_cp = 0x3BB; /*λ*/
char two_byte_test_enc[] = {0xCE, 0xBB, 0x00};
/* 3 bytes: 0x800-0xFFFF */
uint32_t three_byte_test_cp = 0x3075; /*ふ*/
char three_byte_test_enc[] = {0xE3, 0x81, 0xB5, 0x00}
/* 4 bytes: 0x10000-0x1FFFFF */
uint32_t four_byte_test_cp = 0x1D11F; /*𝄟*/
char four_byte_test_enc[] = {0xF0, 0x9D, 0x84, 0x9F, 0x00};
/* Technically the current utf8 standard doesn't include
   characters longer than 4 bytes, and even if it did, none exist */
gen_utf8_char_test(one, 1);
gen_utf8_char_test(two, 2);
gen_utf8_char_test(three, 3);
gen_utf8_char_test(four, 4);
