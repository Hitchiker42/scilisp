/*Common header file for tests*/
#ifndef _SL_CHECK_H
#define _SL_CHECK_H
#include <check.h>
#include "../src/common.h"
#include "../src/print.h"
#include <alloca.h>
#include <stdarg.h>
typedef TCase Tcase;
/*
  define two sets of macros, sl_assert ... and ck_assert_sl_...
  the first set attempt to print expressions when things fail,
  the second don't
*/
#define sl_assert_comparison(fn, expr1, expr2)                          \
  mark_point();                                                         \
  ck_assert_msg(fn(expr1,expr2),                                        \
                "Assertion '%s(%s, %s)' failed\n%s == '%s', %s == '%s'", \
                #fn,#expr1,#expr2,#expr1,c_sprint(expr1), #expr2,c_sprint(expr2))

#define sl_assert_equal(expr1,expr2)            \
  sl_assert_comparison(Fequal,expr1,expr2)
#define sl_assert_not_equal(expr1,expr2)        \
  sl_assert_comparison(!Fequal,expr1,expr2)
#define sl_assert_eqv(expr1,expr2)              \
  sl_assert_comparison(Feqv,expr1,expr2)
#define sl_assert_not_eqv(expr1,expr2)          \
  sl_assert_comparison(!Feqv,expr1,expr2)
#define sl_assert_eq(expr1,expr2)               \
  sl_assert_comparison(Feq,expr1,expr2)
#define sl_assert_not_eq(expr1,expr2)           \
  sl_assert_comparison(!Feq,expr1,expr2)
#define sl_assert_equalp(expr1,expr2)           \
  sl_assert_comparison(Fequalp,expr1,expr2)
#define sl_assert_not_equalp(expr1,expr2)       \
  sl_assert_comparison(!Fequalp,expr1,expr2)
#define sl_assert_num_eq(expr1, expr2)                                  \
  sl_assert_comparison(sl_num_eq,expr1,expr2)
#define sl_assert_num_ne(expr1, expr2)                                  \
  sl_assert_comparison(sl_num_ne,expr1,expr2)
#define sl_assert_nil(expr)                                             \
  mark_point();                                                         \
  ck_assert_msg(expr == NIL,"Assertation %s == 'NIL' failed, %s == '%s'" \
                #expr, #expr, c_sprint(expr))
#define sl_assert_non_nil(expr)                                         \
  mark_point();                                                         \
  ck_assert_msg(expr != NIL,                                            \
                "Assertation failed, expected %s to be non-null but %s == NIL" \
                #expr, #expr)

#define ck_assert_sl_eq(expr1,expr2)            \
  mark_point();                                 \
  ck_assert(Feq(expr1,expr2))
#define ck_assert_sl_eqv(expr1,expr2)           \
  mark_point();                                 \
  ck_assert(Feqv(expr1,expr2))
#define ck_assert_sl_equal(expr1,expr2)         \
  mark_point();                                 \
  ck_assert(Fequal(expr1,expr2))
#define ck_assert_sl_not_equal(expr1,expr2)     \
  mark_point();                                 \
  ck_assert(!(Fequal(expr1,expr2)))
#define ck_assert_sl_nil(expr)                  \
  mark_point();                                 \
  ck_assert(expr == NIL)
#define ck_assert_non_null(expr)                                \
  ck_assert_msg(expr,"Assertation failed "#expr" != NULL")
#define ck_assert_arr_equal(arr1,arr2,len)                            \
  ({int i,retval=1;                                                     \
    for(i=0;i<len;i++){                                                 \
      ck_assert_msg(arr1[i]==arr2[i],                                   \
                    "Assertation '%s[%d] == %s[%d]' failed: "           \
                    "%s[%d]==%ld, %s[%d]==%ld",                         \
                    #arr1, i, #arr2, i, #arr1, i,                       \
                    arr1[i], #arr2, i, arr2[i]);                        \
        }                                                               \
    ;})
#define ck_assert_c_str_equal(str1,str2)                                \
  ck_assert_msg(strcmp(str1,str2) == 0,                                 \
                "Assertation '%s' failed: %s==%s, %s==%s", #str1" == "#str2, \
                #str1, str1, #str2, str2)
#define make_dummy_function(name,args)          \
  sl_obj name DEFUN_ARGS_## args ##_DUMMY {     \
    return NIL;                                 \
  }
#define START_UNUSED_TEST(__testname)                   \
  static void SL_UNUSED __testname(int _i SL_UNUSED);   \
  START_TEST(__testname)
static Suite* gen_test_suite(const char *name, int numargs, ...){
  Suite *s = suite_create(name);
  TCase *tc;
  va_list ap;
  va_start(ap, numargs);
  int i;
  for (i=0;i<numargs;i++) {
    tc = va_arg(ap, TCase*);
    suite_add_tcase(s, tc);
  }
  va_end(ap);
  return s;
}
static TCase* gen_test_case(const char* name, int numargs, ...){
  TCase *tc=tcase_create(name);
  TFun f;
  int i;
  va_list ap;
  va_start(ap, numargs);
  for (i=0;i<numargs;i++){
    f = va_arg(ap, TFun);
    tcase_add_test(tc, f);
  }
  va_end(ap);
  return tc;
}
#define gen_test_suite_fn(name,num_cases,cases...)      \
  Suite* name##_suite(void){                            \
    Suite *s;                                           \
    TCase tc[num_cases]={cases};                        \
    int i;                                              \
    s = suite_create(#name);                            \
    for(i=0;i<num_cases;i++){                           \
      suite_add_tcase(s, tc[i]);                        \
    }                                                   \
    return s;                                           \
  }
#define gen_test_case_fn(name,num_fns,fns...)   \
  TCase* name##_test_case(void){                \
    TCase *tc;                                  \
    TFun f[num_fns]={fns};                      \
    int i;                                      \
    tc = tcase_create(#name);                   \
    for(i=0;i<num_fns;i++){                     \
      tcase_add_test(tc, f[i]);                 \
    }                                           \
    return tc;                                  \
  }
static int run_test_suite(Suite *s){
  SRunner *sr = srunner_create(s);
  int num_failed = 0;
  srunner_run_all(sr, CK_VERBOSE);
  num_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return num_failed;
}
static int run_test_suite_nofork(Suite *s){
  SRunner *sr = srunner_create(s);
  int num_failed = 0;
  srunner_set_fork_status(sr, CK_NOFORK);
  srunner_run_all(sr, CK_VERBOSE);
  num_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return num_failed;
}
#ifdef DEBUG_PRINT_STR
#undef DEBUG_PRINT_STR
#endif
#define DEBUG_PRINT_STR(__str)                          \
  fwrite(__str->str, __str->len, sizeof(char), stderr); \
  fputs("\n", stderr);                                  \
  mark_point()
#endif
