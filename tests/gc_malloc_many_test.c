#include <stdio.h>
#include <stdint.h>
#include <gc.h>
#include <assert.h>
typedef struct cons {
  uint64_t car;
  uint64_t cdr;
} cons;
void check_obj_size(){
  fprintf(stderr,"cons size = %d\n",sizeof(cons));
  uint8_t *fl = GC_malloc_many(sizeof(cons));
  uint8_t *fl_start = fl;
  long i=1,j=1;
  fprintf(stderr,"Block Size = %lu\n",((uint64_t)fl) - ((uint64_t)GC_NEXT(fl)));
  do {
    cons *temp = fl+8;
    fl = GC_NEXT(fl);
    temp->car = i;
    temp->cdr = j;
    i+=j;
    j+=i;
  } while (fl && GC_NEXT(fl));
  fl = fl_start;
  fprintf(stderr,"printing free list:\n");
  do {
    uint64_t *mem = fl;
    fprintf(stderr,
            "*fl = %p, *(fl+8) = %d, *(fl+16) = %d, *(fl+24) = %d\n"
            "fl = %p, fl+8 = %p, fl+16 = %p, fl+24 = %p\n",
            mem[0],mem[1],mem[2],mem[3], mem, mem+1,mem+2,mem+3);
    GC_is_visible(mem+1);
  } while ((fl = GC_NEXT(fl)));
}
int main (){
  check_obj_size();
}
    
