#include "scilisp_test.h"
#include "../src/scilisp.h"
#include "gmp.h"
START_TEST(test_read_int_simple){
  sl_obj int_control = make_int(1234567890);
  sl_string *input = make_string_const_unboxed("1234567890");
  sl_stream *stream = string_to_stream(input);
  mark_point();
  sl_obj int_test = lread(stream);
  sl_assert_equal(int_control, int_test);
  stream_cleanup(stream);
}
END_TEST
START_TEST(test_read_bigint_simple){
  sl_bigint temp;
  mpz_init_set_str(&temp, "11223344556677889900998877665544332211",0);
  sl_obj bigint_control = make_bigint(&temp);
  sl_string *input = 
    make_string_const_unboxed("11223344556677889900998877665544332211");
  sl_stream *stream = string_to_stream(input);
  mark_point();
  sl_obj bigint_test = lread(stream);
  sl_assert_equal(bigint_control, bigint_test);
  stream_cleanup(stream);
}
END_TEST
START_TEST(test_read_double_simple){
  sl_obj double_control = make_double(2.781828);
  sl_string *input = make_string_const_unboxed("2.781828");
  sl_stream *stream = string_to_stream(input);
  mark_point();
  sl_obj double_test = lread(stream);
  sl_assert_equal(double_control, double_test);
  stream_cleanup(stream);
}
END_TEST
START_TEST(test_read_string_simple){
  sl_obj string_control = make_string_const("hello, world");
  sl_string *input = make_string_const_unboxed("\"hello, world\"");
  sl_stream *stream = string_to_stream(input);
  mark_point();
  sl_obj string_test = lread(stream);
  sl_assert_equal(string_control, string_test);
  stream_cleanup(stream);
}
END_TEST
START_TEST(test_read_cons_simple){
  sl_obj cons_control = make_list(sl_0,sl_1,sl_2,sl_3,sl_4,sl_5);
  sl_string *input = make_string_const_unboxed("(0 1 2 3 4 5)");
  sl_stream *stream = string_to_stream(input);
  mark_point();
  sl_obj cons_test = lread(stream);
  sl_assert_equal(cons_control, cons_test);
  stream_cleanup(stream);
}
END_TEST
START_TEST(test_read_char_simple){
  sl_obj char_control = make_char('!');
  sl_string *input = make_string_const_unboxed("?!");
  sl_stream *stream = string_to_stream(input);
  mark_point();
  sl_obj char_test = lread(stream);
  sl_assert_equal(char_control, char_test);
  stream_cleanup(stream);
}
END_TEST
START_TEST(test_read_symbol_simple){
  sl_string *input = make_string_const_unboxed("make_symbol");
  sl_obj symbol_control = make_symbol_from_string(input);
  sl_stream *stream = string_to_stream(input);
  mark_point();
  sl_obj symbol_test = lread(stream);
  sl_assert_equal(get_symbol_name(symbol_test), get_symbol_name(symbol_control));
}
END_TEST
START_TEST(test_read_backquote){
  sl_obj *temp = sl_malloc(sizeof(sl_obj) * 13);
  int i;
  for(i=0;i<14;i++){
    temp[i] = make_int(i+1);
  }
  sl_obj control = make_list2(Qquote, Flist_star(14, temp));
  sl_obj input = make_string_const(
                 "`(,'1 2 3 ,@(append '(4 5 6) '(7 8 9)) 10 ,@'(11 12 13) . 14)");
  sl_stream *stream = string_to_stream(XSTRING(input));
  sl_obj test = lread(stream);
  sl_assert_equal(control, test);
}
END_TEST
START_TEST(test_read_int_from_string){
  sl_obj int_control = make_int(1234567890);
  sl_obj input = make_string_const("1234567890");
  mark_point();
  sl_obj int_test = read_from_string(input);
  sl_assert_equal(int_control, int_test);
}
END_TEST

int main(int argc, char *argv[]){
  scilisp_init();
  TCase *tc_simple =
    gen_test_case("simple", 7, test_read_int_simple, test_read_bigint_simple,
                  test_read_double_simple, test_read_string_simple,
                  test_read_cons_simple, test_read_char_simple,
                  test_read_symbol_simple);
  TCase *tc_string =
    gen_test_case("string", 1, test_read_int_from_string);
  TCase *tc_backquote = gen_test_case("backquote", 1, test_read_backquote);
  Suite *s = gen_test_suite("read", 3, tc_simple, tc_string, tc_backquote);
  int num_failed = run_test_suite(s);
  return (num_failed == 0 ? 0 : 1);
}
