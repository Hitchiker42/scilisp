#include "scilisp_test.h"
#define STRING_BUF_MACROS
#include "../src/scilisp.h"
START_TEST(test_make_string_buf){
  sl_string_buf *test_buf = make_string_buf();
  sl_obj control_string = make_string_const("");
  sl_string *test_string = string_buf_to_string(test_buf);
  DEBUG_PRINT_STR(test_string);
  ck_assert_int_eq(test_string->len, 0);
  ck_assert_sl_equal(control_string, PACK_STRING(test_string));


  test_buf = string_buf_from_cstr("hello, world");
  control_string = make_string_const("hello, world");
  test_string = string_buf_to_string(test_buf);
  DEBUG_PRINT_STR(test_string);
  
  ck_assert_int_eq(test_string->len, 12);
  ck_assert_sl_equal(control_string, PACK_STRING(test_string));

  test_buf = string_buf_from_char('x');
  control_string = make_string_const("x");
  test_string = string_buf_to_string(test_buf);
  DEBUG_PRINT_STR(test_string);
  ck_assert_int_eq(test_string->len, 1);
  ck_assert_sl_equal(control_string, PACK_STRING(test_string));

  test_buf = string_buf_from_string(XSTRING(control_string));
  test_string = string_buf_to_string(test_buf);
  ck_assert_sl_equal(control_string, PACK_STRING(test_string));
}
END_TEST

START_TEST(test_build_from_chars){
  sl_string_buf *test_buf = make_string_buf();
  char *control_string = "hello, world";
  sl_obj control_sl_string = make_string(control_string, 12, 0);
  int i,j,k=0;
  for(i=0;i<12;i++){
    buf_append_char(test_buf, control_string[i]);
  }
  ck_assert_str_eq(control_string, string_buf_to_cstr(test_buf, NULL));
  sl_obj str_obj = string_buf_to_string_obj(test_buf);

  ck_assert_int_eq(VECTOR_LEN(control_sl_string),
                   VECTOR_LEN(str_obj));
  ck_assert_int_eq(VECTOR_LEN(control_sl_string),
                   VECTOR_LEN(string_buf_to_string_obj(test_buf)));
  
  ck_assert_sl_equal(control_sl_string, string_buf_to_string_obj(test_buf));

  int long_length = (26 * 26 * 2);
  char *test_long = sl_malloc_atomic(long_length+1);
  test_buf = make_string_buf();
  for(i='A';i<='Z';i++){
    for(j='Z';j>='A';j--){
      test_long[k++]=i;
      test_long[k++]=j;
      string_buf_append_char(test_buf, i);
      string_buf_append_char(test_buf, j);
    }
  }
  test_long[k] = '\0';
  ck_assert_str_eq(test_long, string_buf_to_cstr(test_buf, NULL));
  control_sl_string = make_string(test_long, long_length, 0);
  ck_assert_int_eq(VECTOR_LEN(control_sl_string), k);
  ck_assert_str_eq(test_long, XSTRING(control_sl_string)->str);
  ck_assert_sl_equal(control_sl_string, string_buf_to_string_obj(test_buf));
}
END_TEST

//there will be/is another test to test building from strings and chars
START_TEST(test_build_from_strings){
  sl_obj control_str1 = make_string_const("hello");
  sl_obj control_str2 = make_string_const(", ");
  sl_obj control_str3 = make_string_const("world");
  sl_obj control_str_full = make_string_const("hello, world");

  sl_string_buf *test_buf1 = make_string_buf();
  buf_append_str(test_buf1, XSTRING(control_str1));
  buf_append_str(test_buf1, XSTRING(control_str2));
  buf_append_str(test_buf1, XSTRING(control_str3));

  ck_assert_sl_equal(control_str_full, string_buf_to_string_obj(test_buf1));

  sl_string_buf *test_buf2 = string_buf_from_string(XSTRING(control_str1));
  buf_append_str_obj(test_buf2, control_str2);
  buf_append_str_obj(test_buf2, control_str3);

  sl_assert_equal(control_str_full, string_buf_to_string_obj(test_buf2));
}
END_TEST

/*
*/
START_TEST(test_build_from_both){
  sl_obj control_str1 = make_string_const("ABCDE");
  //FGHIJ
  int start_char1 = 'F', end_char1 = 'J';
  sl_obj control_str_alt1 = make_string_const("FGHIJ");
  sl_obj control_str2 = make_string_const("KLMNO");
    //PQRST
  int start_char2 = 'P', end_char2 = 'T';
  sl_obj control_str_alt2 = make_string_const("PQRST");

  sl_obj control_str3 = make_string_const("UVWXYZ");  
  sl_obj control_str_full = make_string_const("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  sl_string_buf *test_buf_strings = make_string_buf();
  sl_string_buf *test_buf_chars = make_string_buf();
  sl_string_buf *test_buf_both = make_string_buf();
  char control_check[27] = {0};
  int i,j;
  for(i='A',j=0;i<='Z';i++,j++){
    control_check[j] = i;
    string_buf_append_char(test_buf_chars, i);
  }
  DEBUG_PRINT_STR(string_buf_to_string(test_buf_strings));
  ck_assert_str_eq(control_check, XSTRING(control_str_full)->str);
  ck_assert_sl_equal(control_str_full, string_buf_to_string_obj(test_buf_chars));
  DEBUG_PRINT_STR(string_buf_to_string(test_buf_strings));  
  buf_append_str_obj(test_buf_strings, control_str1);
  DEBUG_PRINT_STR(string_buf_to_string(test_buf_strings));
  buf_append_str_obj(test_buf_strings, control_str_alt1);
  DEBUG_PRINT_STR(string_buf_to_string(test_buf_strings));
  buf_append_str_obj(test_buf_strings, control_str2);
  DEBUG_PRINT_STR(string_buf_to_string(test_buf_strings));
  buf_append_str_obj(test_buf_strings, control_str_alt2);
  DEBUG_PRINT_STR(string_buf_to_string(test_buf_strings));
  buf_append_str_obj(test_buf_strings, control_str3);

  sl_assert_equal(control_str_full, string_buf_to_string_obj(test_buf_strings));

  buf_append_obj(test_buf_both, control_str1);
  for(i=start_char1;i<=end_char1;i++){
    buf_append_char(test_buf_both, i);
  }
  buf_append_obj(test_buf_both, control_str2);
  for(i=start_char2;i<=end_char2;i++){
    buf_append_char(test_buf_both, i);
  }
  buf_append_obj(test_buf_both, control_str3);

  ck_assert_sl_equal(control_str_full, string_buf_to_string_obj(test_buf_both));
}
END_TEST  

int main(int argc, char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 1, test_make_string_buf);
  TCase *tc_build = gen_test_case("build", 3, test_build_from_chars,
                                  test_build_from_strings,
                                  test_build_from_both);
  Suite *s = gen_test_suite("string_buf", 2, tc_sanity, tc_build);

  int num_failed = run_test_suite(s);

  return (num_failed == 0 ? 0 : 1);
}
