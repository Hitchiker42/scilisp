#!/home/tucker/bin/emacs --script
(find-file "unicode-chars.txt")
(set-buffer-file-coding-system 'utf-8-unix)
(dotimes (i #xffff)
  (let ((c (encode-coding-char i 'utf-8-unix)))
    (if c (insert i))))
(set-buffer-file-coding-system 'utf-8-unix)
(save-buffer)
