;;this file is a means of testing the reader, it is not necessarlly
;;meant to be valid lisp code
(defvar double-test-1 0.0123456789)
(defvar double-test-2 1.1E+4)
(defvar double-test-3 -11.)

(defvar int-test-1 00)
(defvar int-test-2 +3399)
(defvar int-test-3 -4499)

(defvar char-test-1 ?a)
(defvar char-test-2 ?9)
;;(defvar char-test-3 ?\?)
;;(defvar char-test-4 ?\\)
;;(defvar char-test-5 ?\n)
;;char test 6 and 7 should describe the same character
;;(defvar char-test-6 ?\金）
;;(defvar char-test-7 ?\u91d1)

(defvar string-test-1
  "the quick red fox jumped over the lazy brown dog")

(defvar string-test-2
  "this string has 
an embedded newline in it")
;;(defvar string-test-3 "testing escape sequences \a \\ \" \x45 \n")
;;(defvar string-test-4 "testing unicode 金 \u91d1)

(defvar cons-test-1 '(1 . 2))
(defvar cons-test-2 '(1 2 3 4))
(defvar cons-test-3 '(1 2 3 . 4))
(defvar cons-test-4 '(1 (2 . 3) (4 5 6)))
;;Other types to be added as they are implemented in the reader
  
