#include "scilisp_test.h"
#include "../src/scilisp.h"
make_dummy_function(Freverse,1)
make_dummy_function(Fnreverse,1)
#define make_test_cons() make_list(sl_0,sl_1,sl_2,sl_3,sl_4,sl_5)
/*
  Sainity tests to insure basic functions used in testing actually work
*/
//insure the cons function works
START_TEST(test_cons){
  sl_obj control = make_cons(sl_1, sl_2);
  sl_obj test_cons = Fcons(sl_1, sl_2);
  ck_assert_sl_equal(control, test_cons);
}
END_TEST
//insure that make_list works properly
//as it makes later tests much easier to write
START_TEST(test_make_list){
  sl_obj test_cons = make_list(sl_5);
  sl_obj control = make_cons(sl_5, NIL);

  sl_assert_equal(control, test_cons);

  test_cons = make_list(sl_5, sl_m5);
  control = make_cons(sl_5, make_cons(sl_m5, NIL));

  sl_assert_equal(control, test_cons);
  control = make_cons(sl_1, make_cons(sl_2, make_cons(sl_3,
                            make_cons (sl_4, make_cons(sl_5, NIL)))));
  test_cons = make_list(sl_1, sl_2, sl_3, sl_4, sl_5);
  sl_assert_equal(control, test_cons);
}
END_TEST
START_TEST(test_length){
  sl_obj len_1 = make_list1(sl_1);
  sl_obj len_2 = make_list2(sl_1,sl_2);
  sl_obj len_5 = make_list(sl_1, sl_2, sl_3, sl_4, sl_5);
  sl_obj len_0 = NIL;
  uint64_t length_1 = XINT(Flength(len_1));
  ck_assert_uint_eq(length_1, 1);
  ck_assert_sl_eq(sl_1, Flength(len_1));
  ck_assert_sl_eq(sl_2, Flength(len_2));
  ck_assert_sl_eq(sl_5, Flength(len_5));
  ck_assert_sl_eq(sl_0, Flength(len_0));
}
END_TEST
START_TEST(test_cadrs){
  sl_obj test_cons = make_list(sl_1, sl_2, sl_3, sl_4, sl_5);
  sl_obj test_cons_cdr = make_list(sl_2, sl_3, sl_4, sl_5);
  ck_assert(Fconsp(test_cons));

  ck_assert_sl_eq(XCAR(test_cons), sl_1);
  ck_assert_sl_eq(Fsafe_car(test_cons), sl_1);
  ck_assert_sl_eq(Fcar(test_cons), sl_1);

  ck_assert(Fconsp(XCDR(test_cons)));
  sl_assert_equal(XCDR(test_cons), test_cons_cdr);
  sl_assert_equal(Fsafe_cdr(test_cons), test_cons_cdr);
  sl_assert_equal(Fcdr(test_cons), test_cons_cdr);

  ck_assert_sl_eq(Fcar(Fcdr(test_cons)), sl_2);
  sl_assert_equal(Fcddr(test_cons), make_list(sl_3, sl_4, sl_5));

  ck_assert_sl_nil(Fcar(NIL));
  ck_assert_sl_nil(Fcdr(NIL));
}
END_TEST
START_TEST(test_nth){
  sl_obj test_cons = make_list(sl_0,sl_1,sl_2,sl_3,sl_4);

  ck_assert_sl_eq(Fnth(test_cons,sl_2),sl_2);
  ck_assert_sl_nil(Fnth(test_cons,sl_9));
}
END_TEST
START_TEST(test_split){
  sl_obj control = make_list(sl_0,sl_1,sl_2,sl_3,sl_4);
  sl_obj test_cons = make_list(sl_0,sl_1,sl_2,sl_3,sl_4);
  sl_obj head_2 = make_list(sl_0,sl_1);
  sl_obj tail_3 = make_list(sl_2,sl_3,sl_4);
  //more through testing would be better but this should
  //handle any major bugs

  sl_obj test_head2 = Fhead(test_cons, sl_2);
  sl_assert_equal(head_2, test_head2);
  sl_assert_equal(control, test_cons);

  sl_obj test_tail3 = Ftail(test_cons, sl_3);
  sl_assert_equal(tail_3, test_tail3);

  sl_obj test_split = Fsplit(test_cons, sl_2);
  sl_assert_equal(Fcar(test_split), head_2);
  sl_assert_equal(Fcdr(test_split), tail_3);
}
END_TEST
/*
  Tests of the more complex functions that might actually not work
*/
START_UNUSED_TEST(test_reverse){
  sl_obj test_cons = make_list(sl_1, sl_2, sl_3, sl_4, sl_5);
  sl_obj snoc_tset = make_list(sl_5, sl_4, sl_3, sl_2, sl_1);
  //nondestructive reverse
  sl_assert_equal(Freverse(test_cons), snoc_tset);
  sl_assert_equal(Freverse(snoc_tset), test_cons);

  test_cons = Fnreverse(test_cons);

  sl_assert_equal(test_cons, snoc_tset);

  snoc_tset = Fnreverse(snoc_tset);

  sl_assert_equal(snoc_tset, Freverse(test_cons));
}
END_TEST

/*
   Append will need to be a function taking N args, but
   for now assume it takes 2 args, so that I can test
   the internals without having to implement varable argument
   functions.
*/
START_TEST(test_append){
  sl_obj test_cons = make_list(sl_1, sl_2, sl_3, sl_4, sl_5);
  sl_obj build_1 = make_list(sl_1, sl_2, sl_3);
  sl_obj build_2 = make_list(sl_4, sl_5);

  sl_obj args[] = {build_1, build_2};
  sl_assert_equal(Fappend(2, args), test_cons);

  build_1 = Fnappend(2, args);

  sl_assert_equal(build_1, test_cons);
}
END_TEST
START_TEST(test_alloc_cons){
  //we use an internal free list to allocate cons cells for efficency
  //this should test to make sure that free list gets refreshed
  //approptately
  sl_obj test_cons = NIL;
  int num_conses = 1000;
  while(num_conses-- > 0){
    test_cons = make_cons(sl_0, test_cons);
    sl_assert_non_nil(test_cons);
  }
  sl_assert_non_nil(test_cons);
}
END_TEST
START_TEST(test_alloc_cons_fail){
  //this should cause a pointer dereference at adress 1, which should segfault
  //this just ensures the cons free list is acutally getting used
  thread_env->cons_fl = (void*)1;
  sl_obj test_cons = make_cons(sl_0, NIL);
  ck_assert(test_cons);//make sure the above line doesn't get removed
}
END_TEST

/*
  TODO: alist tests, push/pop tests
*/
/*START_TEST(test_push_pop){
  }*/
int main(int arg, char *argv[]){
  scilisp_init();
  TCase *tc_sanity = gen_test_case("sanity", 3, test_cons,
                                   test_make_list, test_length);
  TCase *tc_access = gen_test_case("access", 1, test_cadrs);
  TCase *tc_split_nth = gen_test_case("split_nth", 2, test_split, test_nth);
  TCase *tc_rev_append = gen_test_case("rev_append", 1, test_append);
  TCase *tc_alloc = gen_test_case("alloc", 1,
                                  test_alloc_cons);
  tcase_add_test_raise_signal(tc_alloc, test_alloc_cons_fail, SIGSEGV);
  //  TCase *tc_reverse = gen_test_case("reverse", 1, test_reverse);
  //  TCase *tc_build = gen_test_case("build", 1, test_append);
  Suite *s = gen_test_suite("cons", 5, tc_sanity, tc_access, tc_split_nth,
                            tc_rev_append, tc_alloc);
  int num_failed = run_test_suite(s);
  return (num_failed==0?0:1);
}
