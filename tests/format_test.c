#include "scilisp_test.h"
#include "../src/scilisp.h"
#define generate_format_test(name, fmt_string, expected, args...)       \
  START_TEST(name){                                                     \
    sl_obj test = Fformat(make_string_const(fmt_string), ##args);       \
    sl_assert_equal(test, make_string_const(expected)));                \
  }
