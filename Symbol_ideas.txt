No matter what I do I'm going to need a lot of code that just
initializes all the symbols, that's just how it is.

I could do what emacs does and have a function in each file which
initializes the symbols from that file, which seems like a fine way of
doing it, but it feels kinda weird.

ecl seems to just have mounds of code that does everything by brute
force

I'll need to generate some code for sure, namely the list of initial
symbols as an array so I can loop over all of them and add them
to the initial obarray.

At startup I need to:
Initialize symbols:
  -initializing subrs entails creating a sl_misc object and
   assigning it to the symbol value. i.e

   Qsymbol->val = make_misc(&Ssymbol, SL_subr);

  -initializing variables entails creating a structure of
   the appropiate type and setting the symbol i.e
   Qstdin->val = make_misc(file_descriptor_to_stream(STDIN_FILENO),
   SL_stream);

  -constants/keywords shouldn't need any work

  -iterate across an array of the initial symbols and add them
           to the initial obarray
What I need at compile time is:
-All of the subr structs for the builtin subrs
-All of the Qsymbol objects, taking advantage of the
     fact that the sl_obj representation of a symbol
     is the same as the c representation, If this changes
     some things will need to be different
