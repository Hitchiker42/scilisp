A compiler and interpreter for the SciLisp lisp dialect written in C. SciLisp
borrows elements from common lisp, scheme and emacs lisp. SciLisp is generally
most similar to common lisp, the core feature from scheme is that SciLisp is a
lisp 1, that is it has only one namespace for variables and functions. An
attempt is made to replicate the ease of use of emacs lisp compared to common
lisp, without sacrificing the generality of common lisp. SciLisp is a general
purpose language but as the name implies there is a focus on numerical and
scientific computing. Many of the scientific and numerical features are
inspired by the julia language. The compiler uses the llvm compiler
framework, which can also be used for the interpreter but a standalone
interpreter also exists.

SciLisp is currently a work in progress. This is a rewrite of scilisp and is
attempting to have a more consistent and test driven development schedule. A
key element of this rewrite is to insure that for each new commit the test
suite passes, with exceptions made for minor commits noting that they are a
work in progress. The check unit testing library is used for unit tests in C
for each component of the language. A separate script is used for testing the
actual interpreter/compiler invocation and basic functionality. Further tests
will be written in SciLisp itself once a functional and stable interpreter
exists.

The llvm code will be written in C++ to be able to leverage the full power of
the llvm api, however this is a ways off and will not be worked on until a
working interpreter exists.
