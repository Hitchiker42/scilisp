#ifndef _SL_SYMBOL_H_
#define _SL_SYMBOL_H_
#include "common.h"
#include "hash.h" /* for sl_hash */
//historical name
typedef sl_symbol_table sl_obarray;
struct sl_symbol_table {
  //the table holds pointers to symbols so it can be atomic
  sl_symbol **table;
  uint32_t size;
  uint32_t entries;
  float load_factor;
  sem_t sem;
  volatile int rehash_pending;
  volatile int num_threads_using;
  /* ideas for implementing packages
     sl_symbol_table *uses; //for used packages
   */
};
static volatile uint64_t gensym_counter = 0;
//used by the reader to add/find symbols
sl_symbol *add_symbol(sl_symbol_table *ht, sl_symbol *sym);
//optional arguments, kinda, uses default values if given 0 as argument
sl_symbol_table *make_symbol_table(uint32_t size, float load_factor);
//internal use only (assumes sym isn't in ht, and doesn't check)
void add_new_symbol(sl_symbol_table *ht, sl_symbol *sym);
//mostly for testing, I don't see any real use for these
sl_symbol *find_symbol(sl_symbol_table *ht, sl_symbol *sym);
sl_symbol *find_symbol_by_name(sl_symbol_table *ht, sl_string *str);
int remove_symbol(sl_symbol_table *ht, sl_symbol *sym);
//find the value of the given symbol, in the given/default environment
//the types of the arguments are not checked, so that is up to the caller
sl_obj get_symval(sl_obj sym);
sl_obj get_lexical_binding(sl_obj sym);
sl_obj set_lexical_binding(sl_obj sym, sl_obj val);
//given a list of (var . value) pairs create a lexical environment where
//each var is bound to value (value is evaluated first).
sl_obj make_lexical_env(sl_obj bindings);
//internal use only, increments the gensym counter and returns it's value
//used to generate a unique symbol with a name different than the default gensym
uint64_t inc_gensym_counter(void);
//currently the table arguments arg ignored
sl_obj Fintern(sl_obj sym, sl_obj table);
sl_obj Funintern(sl_obj sym, sl_obj table);
sl_obj Fgensym(void);
sl_obj Fmake_symbol(sl_obj name);
#ifndef __cplusplus
SL_INLINE sl_obj make_symbol(char *lname, uint32_t name_len,
                             sl_obj value, uint32_t props,
                             SL_UNUSED char *doc){
  /*
    this would be a fitting place to use typed gc to allocate space for
    the symbol and it's name in one go, but still insure the space
    used to store the name isn't scanned for pointers.    e.g.
    //in SL_init
    //make a type descriptor for an object who's first field contains a pointer
    //but no other fields do
    symbol_gc_descr = GC_make_descriptor(0x1, 1);
    //here
    sl_symbol *sym = gc_malloc_explicitly_typed(sizeof(sl_symbol)+
    sizeof(sl_string)+name_len,symbol_gc_descr)
   */
  sl_symbol *sym = sl_malloc(sizeof(sl_symbol));
  sym->val = value;
  sym->sym_props = props;
  sl_string sym_name= {.tag = SL_vector, .type = SL_string,
                       .encoding = SL_UTF8, .len = name_len};
  sym->name = sym_name;
  sym->name.str = sl_malloc_atomic(name_len);
  assert(name_len == strlen(lname));
  memcpy((char*)sym->name.str, lname, name_len);
  sym->hv = sl_hash_str(&sym->name);
  return PACK_SYMBOL(sym);
}
//same as above but don't copy the name string
SL_INLINE sl_obj make_symbol_nocopy(char *lname, uint32_t name_len,
                                    sl_obj value, uint32_t props,
                                    SL_UNUSED char *doc){
  sl_symbol *sym = sl_malloc(sizeof(sl_symbol));
  sym->val = value;
  sym->sym_props = props;
  sl_string sym_name= {.tag = SL_vector, .type = SL_string, .encoding = SL_UTF8,
                       .len = name_len, .str = lname};
  sym->name = sym_name;
  sym->hv = sl_hash_str(&sym_name);
  return PACK_SYMBOL(sym);
}
//the above are used to make symbols during evaluation (or statically) this
//is used to make symbols while in the reader
SL_INLINE sl_obj make_symbol_from_string(sl_string *str){
  sl_symbol *sym = sl_malloc(sizeof(sl_symbol));
  sym->val = Qunbound;
  sym->name = *str;
  sym->hv = sl_hash_str(str);
  return PACK_SYMBOL(sym);
}
SL_INLINE sl_obj get_symbol_name(sl_obj sym){
  return PACK_STRING(XSYMNAME(sym));
}
#endif //__cplusplus
#endif
