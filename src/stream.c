#include "stream.h"
#include "sl_string.h"
#include "util.h"
/*
just for reference
struct sl_stream {
  void *stream;
  uint8_t type;
  uint8_t mode;
};
struct file_stream {
  FILE *file;
  sl_char last_char;
  uint32_t size;//can be 0, e.g. for stdin
};
struct string_stream {
  sl_string str;
  uint32_t size;//memory allocated for the string
  char *buf;//temporary buffer
};
struct mem_stream {
  uint8_t *buf;
  uint8_t *bufptr;//current location
  uint32_t size;
  uint32_t length;
};
 */
/*
  conversion/creation functions
*/
sl_stream *sl_open(const char *filename, int mode);
sl_stream *file_descriptor_to_stream(int fd, int mode){
  //read only is 0, so we can't detect it, so we have to detecte
  //that it's not open for writing instead
  if(regular_filep(fd) &&
     (!(mode & SL_write_only) && !(mode & SL_read_write))){
    size_t sz;
    void *buf = mmap_file(fd, 0, &sz);
    if(buf == NULL){
      raise_error_temp();
    }
    close(fd);
    mem_stream *stream = buffer_to_mem_stream(buf, sz);
    return make_stream(stream, SL_mem_stream, mode);
  } else {
    HERE();
    FILE *file = fdopen(fd, filemode_bits_to_string(mode));
    HERE();
    //don't close fd, fdopen doesn't dup it like mmap
    file_stream *stream = file_stream_from_FILE(file);
    return make_stream(stream, SL_file_stream, mode);
  }
}
sl_stream *FILE_to_stream(FILE *f, int mode){
  //we already have a C stream so just use that, it's lazy but eh
  file_stream *stream = file_stream_from_FILE(f);
  return make_stream(stream, SL_file_stream, mode);
}
sl_stream *buffer_to_stream(uint8_t *buf, uint32_t size, int mode){
  mem_stream *stream = sl_malloc(sizeof(mem_stream));
  stream->buf = buf;
  stream->bufptr = stream->buf;
  stream->size = stream->len = size;
  return make_stream(stream, SL_mem_stream, mode);
}
#define ALWAYS_STRING_STREAM_LIMIT PAGE_SIZE
#define PREFER_MEM_STREAM 0
sl_stream *string_to_stream(sl_string *str){
  //We can always turn a string into a string stream
  //but we need an extra byte after the end of the string (to mark as EOF)
  //to turn a string into a mem_stream. Testing for this is mlidly expensive
  //so only do this if the string is long enough for it to be worth it.
  //this is only possible if we're not using a custom allocator
#ifndef CUSTOM_ALLOC
  if(str->len > ALWAYS_STRING_STREAM_LIMIT &&
     GC_base((void*)str->str) == GC_base((void*)(str->str+str->len+1))){
    ((char*)str->str)[str->len+1] = EOF;
      mem_stream *stream = string_to_mem_stream(str);
      return make_stream(stream, SL_mem_stream, O_RDONLY);
  }
#endif
    string_stream *stream = string_to_string_stream(str);
    return make_stream(stream, SL_string_stream, O_RDONLY);
}
//sl_obj make_stream_obj(sl_stream *stream){
void mem_stream_cleanup(mem_stream *stream){
  if((uint64_t)stream->buf % PAGE_SIZE){
    //it's possible that the memory was allocated with mmap, even if it wasn't
    //there's no harm in calling munmap on an unmapped adress
    munmap(stream->buf, stream->size);
  }
  return;
}
void file_stream_cleanup(file_stream *stream){
  fclose(stream->file);
}
void string_stream_cleanup(string_stream *stream){
  return;
}
mem_stream *buffer_to_mem_stream(uint8_t *buf, uint32_t size){
  mem_stream *retval = sl_malloc(sizeof(mem_stream));
  retval->buf = buf;
  retval->bufptr = retval->buf;
  retval->size = retval->len = size;
  return retval;
}
mem_stream *string_to_mem_stream(sl_string *str){
  mem_stream *retval = sl_malloc(sizeof(mem_stream));
  retval->buf = (uint8_t*)str->str;
  retval->bufptr = retval->buf;
  //we assume the first byte past the end of the string is EOF
  retval->size = retval->len = str->len+1;
  return retval;
}
file_stream *file_stream_from_FILE(FILE *file){
  file_stream *stream = sl_malloc(sizeof(file_stream));
  stream->file = file;
  return stream;
}
string_stream *string_to_string_stream(sl_string *str){
  string_stream *stream = sl_malloc(sizeof(string_stream));
  stream->str = *str;
  stream->pos = 0;
  return stream;
}
/*
  positioning/length
*/
long mem_stream_get_pos(mem_stream *stream){
  return stream->bufptr - stream->buf;
}
long string_stream_get_pos(string_stream *stream){
  return stream->pos;
}
long file_stream_get_pos(file_stream *stream){
  return ftell(stream->file);
}
long mem_stream_length(mem_stream *stream){
  return stream->len;
}
long string_stream_length(string_stream *stream){
  return stream->str.len;
}
long file_stream_length(file_stream *stream){
  return file_len(stream->file);
}

long mem_stream_seek(mem_stream *stream, long offset, int whence){
  if(whence == SEEK_CUR){
    offset += (stream->bufptr - stream->buf);
  } else if (whence == SEEK_END){
    //a non-negitive offset only makes sense for a writable stream
    offset += (stream->size);
  } else if (whence == SEEK_SET) {
    //offset = offset;
  } else {
    /* currently there's no way to report different errors, once I add that
     I'll give better information on what the error was
    */
    return -1;
  }
  if(offset < 0 || offset > stream->size){
    return -1;
  }
  stream->bufptr = stream->buf + offset;
  return offset;
}
long string_stream_seek(string_stream *stream, long offset, int whence){
  if(whence == SEEK_CUR){
    offset += stream->pos;
  } else if (whence == SEEK_END) {
    offset += stream->str.len;
  } else if (whence == SEEK_SET) {
    //offset = offset;
  } else {
    return -1;
  }
  stream->pos = offset;
  return offset;
}
long file_stream_seek(file_stream *stream, long offset, int whence){
  int fseek_retval = fseek(stream->file, offset, whence);
  if(fseek_retval != 0){
    return fseek_retval;
  } else {
    return ftell(stream->file);
  }
}

#define check_mem_stream_size(stream)                          \
  ({int pos = stream->bufptr - stream->buf;                    \
    if(pos >= stream->len){                                    \
      uint32_t size = MAX(pos, stream->size*2);                \
      stream->buf = sl_realloc(stream->buf,size);              \
      stream->size = size;                                     \
  };})
/*
  output functions
*/
sl_char mem_stream_write_char(mem_stream *stream, sl_char c){
  check_mem_stream_size(stream);
  *stream->bufptr++ = c;
  stream->len++;
  return c;
}
int mem_stream_write_string(mem_stream *stream, sl_string *str){
  uint8_t *start = stream->bufptr;
  stream->bufptr += str->len;
  //update bufptr here so that we can call check_mem_stream_size instead of
  //rewriting the whole thing
  check_mem_stream_size(stream);
  memcpy(start, str->str, str->len);
  stream->len += str->len;
  return 1;
}
int mem_stream_write_line(mem_stream *stream, sl_string *str){
  mem_stream_write_string(stream, str);
  //a bit of cheating here, since write_string updates the position of the
  //stream before writing we already know that we can write a character
  //to the stream without checking again
  *stream->bufptr++='\n';
  stream->len++;
  return 1;
}
sl_char file_stream_write_char(file_stream *stream, sl_char c){
  stream->size++;
  return fputc(c, stream->file);
}
int file_stream_write_string(file_stream *stream, sl_string *str){
  stream->size+=str->len;
  size_t nbytes = fwrite(str->str, str->len, sizeof(uint8_t), stream->file);
  return (nbytes ? 1 : 0);
}
int file_stream_write_line(file_stream *stream, sl_string *str){
  int retval = file_stream_write_string(stream, str);
  if(retval){
    stream->size++;
    fputc('\n',stream->file);
  }
  return retval;
}
sl_char string_stream_write_char(string_stream *stream, sl_char c){
  raise_error(0,make_string_const("Unimplemented"));
}
int string_stream_write_string(string_stream *stream, sl_string *str){
  raise_error(0,make_string_const("Unimplemented"));
}
int string_stream_write_line(string_stream *stream, sl_string *str){
  raise_error(0,make_string_const("Unimplemented"));
}
/*
  reading multiple characters
*/
const char *string_stream_read_chars(string_stream *stream, int chars){
  if(stream->pos + chars > stream->str.len){
    //or perhaps return a sl_char vector terminated with EOF
    return NULL;
  }
  const char *retval = stream->str.str+stream->pos+chars;
  stream->pos += chars;
  return retval;
}
const char *mem_stream_read_chars(mem_stream *s, int chars){
  if ((s->bufptr - s->buf)+chars > s->len) {
    return NULL;
  }
  const char *retval = (char*)s->bufptr;
  s->bufptr+=chars;
  return retval;
}
const char *file_stream_read_chars(file_stream *s, int chars){
  if(s->size && (ftello(s->file)+chars)>s->size){
    return NULL;
  }
  char *buf = sl_malloc_atomic(sizeof(char)*chars);
  size_t chars_read = fread(buf, (size_t)chars, sizeof(char), s->file);
  if(chars_read < chars){
    //bit of an issue here, but this will only happen with things like stdin
    return NULL;
  }
  return (const char *)buf;
}
sl_string *string_stream_read_until_char(string_stream *s, sl_char c);

sl_stream *sl_open(const char *filename, int mode){
  int fd = open(filename, mode);
  if(fd == -1){
    perror("open");
    return NULL;
  }
  return file_descriptor_to_stream(fd, mode);
}

DEFUN("open",open,2,2,"(open path mode) return a file stream for the file"
      " located at path, in mode, mode. mode is a keyword and is one of "
      ":read, :write, :rw.")
  (sl_obj path, sl_obj mode){
  if(!STRINGP(path)){
    raise_error_temp();
  }
  if(!KEYWORDP(mode)){
    raise_error_temp();
  }
  if(mode == Kread || mode == Kinput){
      return make_stream_obj(sl_open(XSTRING(path)->str, SL_read_only));
  } else if (mode == Kwrite || mode == Koutput){
      return make_stream_obj(sl_open(XSTRING(path)->str, SL_write_only));
  } else if (mode == Kio || mode == Krw){
      return make_stream_obj(sl_open(XSTRING(path)->str, SL_read_write));
  } else {
    raise_error_temp();
  }
  SL_UNREACHABLE;
}



#define gen_stream_function(name, rettype)              \
  rettype name(sl_stream *stream){                      \
    enum SL_stream_type stream_type = stream->type;     \
    switch(stream_type){                                \
      case SL_mem_stream:                               \
        return mem_##name(stream->stream);              \
      case SL_file_stream:                              \
        return file_##name(stream->stream);             \
      case SL_string_stream:                            \
        return string_##name(stream->stream);           \
    }                                                   \
    SL_UNREACHABLE;                                     \
  }
//only works in a context where stream is a valid sl_stream
#define stream_switch(name,args...)                     \
  enum SL_stream_type stream_type = stream->type;       \
  switch(stream_type){                                  \
    case SL_mem_stream:                                 \
      return mem_##name(stream->stream,##args);         \
    case SL_file_stream:                                \
      return file_##name(stream->stream,##args);        \
    case SL_string_stream:                              \
      return string_##name(stream->stream,##args);      \
    }

gen_stream_function(stream_read_char, sl_char);
gen_stream_function(stream_peek_char, sl_char);
gen_stream_function(stream_unread_char, void);
gen_stream_function(stream_cleanup, void);
gen_stream_function(stream_length, long);
long stream_seek(sl_stream *stream, long offset, int whence){
  stream_switch(stream_seek, offset, whence);
  SL_UNREACHABLE;
}
sl_char stream_write_char(sl_stream *stream, sl_char c){
  if(!(stream->mode & SL_write_only) && !(stream->mode & SL_read_write)){
    raise_error_temp();
  }
  stream_switch(stream_write_char, c);
  SL_UNREACHABLE;
}
int stream_write_string(sl_stream *stream, sl_string *str){
  if(!(stream->mode & SL_write_only) && !(stream->mode & SL_read_write)){
    raise_error_temp();
  }
  stream_switch(stream_write_string, str);
  SL_UNREACHABLE;
}
int stream_write_line(sl_stream *stream, sl_string *str){
  if(!(stream->mode & SL_write_only) && !(stream->mode & SL_read_write)){
    raise_error_temp();
  }
  stream_switch(stream_write_line, str);
  SL_UNREACHABLE;
}

DEFUN("read-char", read_char, 0, 1, "(read-char (stream stdin)) read a character "
      " from stream")
  (sl_obj stream){
  if(NILP(stream)){
    return stream_read_char(XSTREAM(Qstandard_input));
  } else if (STREAMP(stream)){
    return stream_read_char(XSTREAM(stream));
  } else {
    raise_error_temp();
  }
}
DEFUN("peek-char", peek_char, 0, 1, "(peek-char (stream stdin)) get next "
      "character from stream, without removing it")
  (sl_obj stream){
  if(NILP(stream)){
    return stream_peek_char(XSTREAM(Qstandard_input));
  } else if (STREAMP(stream)){
    return stream_peek_char(XSTREAM(stream));
  } else {
    raise_error_temp();
  }
}
DEFUN("write-string", write_string, 1, 2, "(write-string string &optional stream) "
      "write string to stream, or stdout if stream is nil")
  (sl_obj string, sl_obj stream_obj){
  sl_stream *stream;
  if(!STRINGP(string)){
    raise_error_temp();
  }
  if(NILP(stream_obj)){
    stream = XSTREAM(Qstandard_output);
  } else if (STREAMP(stream_obj)){
    stream = XSTREAM(stream_obj);
  } else {
    raise_error_temp();
  }
  return PACK_INT(stream_write_string(stream, XSTRING(string)));
}
DEFUN("write-char", write_char, 1, 2, "(write-char char &optional stream) "
      "write char to stream, or stdout if char is nil")
  (sl_obj c, sl_obj stream_obj){
  sl_stream *stream;
  if(!CHARP(c)){
    raise_error_temp();
  }
  if(NILP(stream_obj)){
    stream = XSTREAM(Qstandard_output);
  } else if (STREAMP(stream_obj)){
    stream = XSTREAM(stream_obj);
  } else {
    raise_error_temp();
  }
  return PACK_INT(stream_write_char(stream, c));
}
DEFUN("newline", newline, 0, 1, "(newline &optional stream), write a newline"
      "to stream, or standard-output if stream is NIL")
  (sl_obj stream_obj){
  sl_stream *stream;
  if(NILP(stream_obj)){
    stream = XSTREAM(Qstandard_output);
  } else if (STREAMP(stream_obj)){
    stream = XSTREAM(stream_obj);
  } else {
    raise_error_temp();
  }
  return PACK_INT(stream_write_char(stream, make_char('\n')));
}
  
void init_stream_symbols(void){
  DEBUG_PRINTF("initializing variables\n");
  DEFVAR("*standard-input*", standard_input,
         make_stream_obj(FILE_to_stream(stdin, SL_read_only)),
         "Default stream used by read and other input functions");
  //  DEFVAR_ALIAS("*stdin*", stdin, standard_input);
  DEFVAR("*standard-output*", standard_output,
         make_stream_obj(FILE_to_stream(stdout, SL_write_only)),
         "Default stream used by print and other output functions");
  //  DEFVAR_ALIAS("*stdout*", stdout, standard_output);
  DEFVAR("*standard-err*", standard_error,
         make_stream_obj(FILE_to_stream(stderr, SL_write_only)),
         "Default stream used by print and other output functions");
  INIT_SUBR(read_char,1);
  INIT_SUBR(peek_char,1);
  INIT_SUBR(newline, 1);
  INIT_SUBR(write_string,2);
}
