#include "common.h"
#include "sl_llvm.h"
#include "llvm-c/Core.h"
#include "atomic.h"
struct sl_llvm_context {
  LLVMContext *context;
  LLVMModule *module;
  LLVMBuilder *builder;
  LLVMValueRef *function;
};
//types in llvm coorsponding to the different types of scilisp funcions
enum sl_llvm_function_type {
  llvm_fun_0, llvm_fun_1, llvm_fun_2, llvm_fun_3,
  llvm_fun_4, llvm_fun_5, llvm_fun_6, llvm_fun_7,
  llvm_fun_8, llvm_fun_many, llvm_fun_unevalled,
};
static LLVMType *sl_llvm_function_types[11];
static LLVMType *llvm_sl_obj_type;
static LLVMValue *llvm_nil;
/*
  TODO: figure out how to make builtin functions available to llvm.

  One way would be to compile the entire library into llvm bitcode and
  load that into memory at startup, but that's sort of riducilous, since
  It would add 2+ MB to the ammount of memory needed, and I would
  need to load the bitcode into a module every time.

  Preferably I'd just load symbols from the shared library as needed, but
  I'm not quite sure how to do that.
*/
void llvm_init(){
  sl_llvm_context *ctx = thread_env->llvm_context;
  int i;
  ctx->context = LLVMContextCreate();
  //I'm not sure how I should name modules
  ctx->modules = LLVMModuleCreateWithNameInContext("", ctx->context);
  ctx->builder = LLVMCreateBuilderInContext(ctx->context);
  /*
    Create types here
   */
  llvm_sl_obj_type = LLVMInt64TypeInContext(cxt->context);
  LLVMType *arg_types[8];//an array of 8 sl_obj types
  //two seperate loops might be faster
  for(i=0;i<8;i++){
    arg_types[i] = llvm_sl_obj_type;
    sl_llvm_function_types[i] = LLVMFunctionType(llvm_sl_obj_type,
                                                 arg_types, i, 0);
  }
  //this seems weird, but it should work as long as I do the proper
  //casting when I call things
  sl_llvm_function_types[i++] = sl_llvm_function_types[0];//void
  sl_llvm_function_types[i++] = sl_llvm_function_types[1];//unevalled
  sl_llvm_function_types[i++] = sl_llvm_function_types[2];//many
  llvm_nil = LLVMConstNull(llvm_sl_obj_type);
  return;
}
typedef LLVMValue*(codegen_fun)(sl_obj);
static LLVMValue *codegen_int(sl_obj);
static LLVMValue *codegen_pointer(sl_obj);
static LLVMValue *codegen_symbol(sl_obj);
static LLVMValue *codegen_cons(sl_obj);
static LLVMValue *codegen_number(sl_obj);
static LLVMValue *codegen_vector(sl_obj);
static LLVMValue *codegen_misc(sl_obj);
static LLVMValue *codegen_imm(sl_obj);
static codegen_fun* sl_codegen_dispatch[8] =
  {codegen_symbol, codegen_cons, codegen_int, codegen_number,
   codegen_vector, codegen_misc, codegen_int, codegen_imm};
static LLVMValue *codegen_int(sl_obj obj){
  //llvm_sl_obj_type is just an int64
  return LLVMConstInt(llvm_sl_obj_type, XINT(obj), 0);
}
static LLVMValue *codegen_pointer(sl_obj obj){
  return LLVMConstInt(llvm_sl_obj_type, XPTR(obj), 0);
}
static LLVMValue *codegen_number(sl_obj obj){
  /*
    Thinking about it, shouldn't I just return a
    pointer rather than extracting the underlying data, unless
    I write code to use the underlying types, llvm should only
    need to deal with boxed data.
   */
  return LLVMConstInt(llvm_sl_obj_type, obj, 0);
  /*  sl_llvm_context *ctx = thread_env->llvm_context;
  int type_key = SL_number_key(obj);
  if(type_key == SL_double_key){
    return LLVMConstReal(LLVMDoubleTypeInContext(ctx->context),
                         XDOUBLE(obj));
                         }*/
}
  
/*
  Special Forms
*/
/*
  Ok, lets try to write out an outline of how llvm stuff will work.
  -init llvm, create a context, module, and builder
  -possibly load all builtin functions into the module
  REPL:
  -read line of code, pass object to toplevel repl codegen routine
  -create anonymous thunk to run code
  -compile code, the difficulty here is ensuring changes are reflected
    in the toplevel environment
  Compile:
  -read entire file into list, macroexpand, pass to toplevel compile function
  -scan through list, collecting functions, to allow using a function
    before it is defined.
  -compile each function (figure out how to deal with functions that aren't
    yet defined), and evaluate other toplevel defines
  -compile toplevel code into anynomous thunk
  Special Forms:
  -Control: if, while, progn(I might not need this), exceptions
  -Variables/Functions: lambda, setq, define(possibly), let, progv
  -Funcall for lambdas and builtin functions
  Intrinsics:
  -Math driver
  -some cons functions
  -Thats proably all I need
*/
//generate code for expr, then a comparision to NIL, this gets us
//an llvm_bool to use in branches and such
SL_INLINE LLVMValue *codegen_bool(sl_obj expr){
  LLVMValue *val = sl_codegen(expr);
  LLVMValue *cmp = llvm_build(ICmp, LLVMIntEQ, expr, llvm_nil);
  return cmp;
}
SL_INLINE LLVMValue *codegen_funcall_lambda(sl_obj expr);
SL_INLINE LLVMValue *codegen_funcall_builtin(sl_obj expr);
LLVMValue *codegen_funcall(sl_obj expr){
  assert(SUBRP(XCAR(expr)));
  if(LAMBDAP(XCAR(expr))){
    return codegen_funcall_lambda(expr);
  } else {
    return codegen_funcall_builtin(expr);
  }
}
//SL_INLINE sl_obj codegen_funcall_builtin(sl_obj expr){

/*
  Generate a trampoline/closure/wrapper for a lambda to allow calling
  it as a C function
*/
static volatile uint64_t lambda_counter = 0;
LLVMValue *codegen_lambda(sl_obj lambda){
  LLVMValue *lambda_ptr = LLVMConstInt(llvm_sl_obj_type, XSUBR(lambda)->fLAMBDA, 0);
  sl_llvm_context *ctx = thread_env->llvm_context;
  //Do I really need to give this a name?
  uint64_t lambda_count = sl_atomic_inc(&lambda_counter);
  const char *name = XSTRING(sl_sprintf("#:llvm_lambda_%d", 0, lambda_count));
  //the function could takes it's arguments as a list of unevaluated sl_objs
  LLVMValue *llvm_lambda = LLVMAddFunction(ctx->module, name, sl_llvm_function_types[1]);
  LLVMBasicBlock *entry = LLVMAppendBasicBlockInContext(cxt->context, llvm_lambda, "");
  LLVMPositionBuilderAtEnd(ctx->builder, entry);
  LLVMValue *arglist = LLVMGetParam(llvm_lambda, 0);
  LLVMValue **llvm_arglist = sl_malloc(2 * sizeof(LLVMValue*));
  llvm_arglist[0] = lambda_ptr;
  llvm_arglist[1] = arglist;
  LLVMValue *llvm_funcall_lambda = somehow_get_this_as_an_llvm_function();
  LLVMValue *retval = llvm_build(Call, llvm_funcall_lambda, llvm_arglist, 2, "");
  llvm_build(Ret, retval);
  return llvm_lambda;
}
LLVMValue *codegen_progn(sl_obj progn_expr){
  //hrmm, what should we return, I think we can just build things
  //and return the last value of the progn
  sl_obj expr;
  LLVMValue *value;
  while(CONSP(progn_expr)){
    expr = POP(progn_expr);
    value = sl_codgen(expr);
  }
  return value;
  /*  LLVMValue *retval = llvm_build(ret, value);
      return retval;*/
}
LLVMValue *codegen_if(sl_obj if_expr){
  if(!CONSP(if_expr) || !CONSP(XCDR(if_expr))){
    raise_error_temp();
  }
  LLVMBasicBlock *then_block = llvm_append_bb("then");
  LLVMBasicBlock *else_block = llvm_append_bb("else");
  LLVMBasicBlock *merge_block = llvm_append_bb("merge");

  LLVMValue *cmp = codegen_bool(XCAR(if_expr));
  LLVMValue *br = llvm_build(CondBr, cmp, then_block, else_block);
  
  llvm_position_at_end(then_block);
  LLVMValue *then_expr = sl_codegen(XCADR(if_expr));
  llvm_build(Br, merge_block);

  llvm_position_at_end(else_block);
  LLVMValue *else_expr = codegen_progn(XCDDR(if_expr));//could be nil
  llvm_build(Br, merge_block);

  llvm_position_at_end(merge_block);
  LLVMValue *phi = llvm_build(Phi, llvm_sl_obj_type, "");
  LLVMValue *incoming_values[2] = {then_expr, else_expr};
  LLVMBasicBlock *incoming_blocks[2] = {then_block, else_block};
  LLVMAddIncoming(phi, incoming_blocks, incoming_blocks, 2);
  return phi;
}
/*
  Grr, this is confusing, do I need to explicitly insert instructions
  into the bulider, or do they get inserted implicity?
*/
LLVMValue *codegen_while(sl_obj while_expr){
  if(!CONSP(while_expr)){
    raise_error_temp();
  }
  //Make a block for just the test, since we need to branch to it
  LLVMBasicBlock *test_block = llvm_append_bb("test");
  LLVMBasicBlock *body_block = llvm_append_bb("body");
  LLVMBasicBlock *end_block = llvm_append_bb("end");

  llvm_build(Br, "test");
  llvm_position_at_end(test_block);
  LLVMValue *test_expr = codegen_bool(XCAR(while_expr));
  llvm_build(CondBr, test_expr, body_block, end_block);

  llvm_position_at_end(body_block);  
  LLVMValue *body_expr = codegen_progn(XCDR(while_expr));
  llvm_build(Br, test_block);

  //I'm not sure what else to do here other than position the builder
  //in an empty block and return
  llvm_position_at_end(end_block);
  return llvm_nil;
}
