#ifndef _SL_COMMON_H_
#error "do not include debug.h directly, use common.h"
#endif
//get __assert_fail, also a definition for assert when NDEBUG is defined
#include <assert.h>
#if (defined DEBUG) & !(defined NDEBUG)
#include <execinfo.h>
#define DEBUG_PRINTF(fmt,args...)               \
  mpfr_fprintf(stderr,fmt,##args)
#define HERE()                                  \
  mpfr_fprintf(stderr,"HERE in %s in %s at line %d\n",__func__,__FILE__,__LINE__)
#define DEBUG_MSG(fmt,args...)                                          \
  mpfr_fprintf(stderr,"%s:%s:%d: " fmt "\n", __func__,__FILE__, __LINE__, ##args)
#define DEBUG_PRINT_STR(__str)                    \
  fwrite(__str->str, __str->len, sizeof(char), stderr);\
  fputs("\n", stderr)
//#define DEBUG_PRINT_OBJ(
/* an alternate way to do the above is:
#define DEUBG_PRINT_STR(__str)
fprintf(stderr,"%.*s\n",__str->len,__str->str);
*/
#else
#define DEBUG_PRINTF
#define HERE
#define DEBUG_MSG
#define DEBUG_PRINT_STR
#endif
//the following make runtime checks,, they get compiled in as long
//as NDEBUG isn't defined, if it is they don't even get compiled
int runtime_debug;//flag toggling runtime debugging
#ifndef NDEBUG
static int debug_printf(const char *fmt, ...){
  if(runtime_debug){
    va_list ap;
    va_start(ap, fmt);
    return vfprintf(stderr, fmt, ap);
  } else {
    return -1;
  }
}
#undef assert
#define assert_simple(expr)                                     \
  ({if(!(expr)){                                                \
      __assert_fail(#expr, __FILE__, __LINE__, __func__);}})
#define assert_msg(expr, msg)                                   \
  ({if(!(expr)){                                                \
      fputs(msg, stderr);                                       \
      __assert_fail(#expr, __FILE__, __LINE__, __func__);}})
#define assert_fmt(expr, fmt, args...)                          \
  ({if(!(expr)){                                                \
      fprintf(stderr,fmt, ##args);                              \
      __assert_fail(#expr, __FILE__, __LINE__, __func__);}})
#define assert(...)                                                     \
  GET_MACRO(3, __VA_ARGS__, assert_fmt, assert_msg, assert_simple)(__VA_ARGS__)
#endif
    
 
