;;;; Control structures implemented as lisp macros
;;;; Since any control structures implemented as special forms
;;;; need to be implemented for both the interpreter and for llvm
;;;; we want to make as few special forms as possible

(defmacro prog1 (form-1 &rest forms)
  (let ((retval (gensym)))
    `(let ((,retval ,form-1))
       (progn ,@forms)
       retval)))
(defmacro until (test &rest forms)
  `(while (not ,test)
     ,@forms))
(defmacro when (test &rest forms)
  (if ,test
      (progn ,@forms)))
(defmacro unless (test &rest forms)
  (if (not ,test)
      (progn ,@forms)))
(defmacro and (&rest tests)
  (let ((retval (gensym)))
    `(let ((,retval t))
       ;;I think this should work
       (while (cons? tests)
         (set! ,retval (car ,tests))
         ;;if the car is nil set the rest of the list to nil to
         ;;break out of the loop, since I don't have the ability to
         ;;actually break out of loops yet
         (if (not ,retval)
             (set! ,tests nil)
             (set! ,tests (cdr ,tests))))
       ,retval)))
;;similar implementation as and, except we default to returning nil
;;and break out of the loop if we get something non-nil
(defmacro or (&rest tests)
  (let ((retval (gensym)))
    `(let ((,retval nil))
       (while (cons? tests)
         (set! ,retval (car ,tests))
         (if ,retval
             (set! ,tests nil)
             (set! ,tests (cdr ,tests))))
       ,retval)))
(defmacro cond (&rest cases)
  (let ((acc (gensym)))
    `(let ((acc (lambda (x)
                  ;;I'm not sure if this will work or if i need to put a
                  ;;progn somewhere in here
                  (if (cons? x)
                      (if (caar x)
                          (cdar x)
                          (acc (cdr x)));;recurse to the next case
                      nil))))
       (acc ,cases))))
(defmacro dotimes (spec &rest body)
  `(let ((,(car spec) 0))
     (while (< ,(car spec) ,(cadr spec))
       ,@body
       (set! ,(car spec) (1+ ,(car spec))))
     ,@(cddr spec)))
(defmacro dolist (spec &rest body)
  `(let ((,(car spec) ,(pop (cadr spec))))
     (while (cons? ,(cadr spec))
       ,@body
       (set! ,(car spec) ,(pop (cadr spec))))
     (set! ,(car spec) nil)
     ,@(cddr spec)))
;;I'm not sure if this will work, I'm pretty sure that it will
;;but manipulating code in macros is always a bit tricky
(defmacro do (spec test &rest body)
  `(let (,@(mapcar (lambda (x)
                     (if (cons? x) (list (car x) (cadr x)) x))
                   spec))
     (while (not ,(car test))
       ,@body
       ,@(mapcar (lambda (x)
                   (if (and (cons? x) (cons? (cdr x)))
                       (list 'set! (car x) (caddr x))))))
     ,(cdr test)))

;;or with named let
;; (defmacro cond (&rest cases)
;;   (let ((acc (gensym)))
;;     `(let acc ((x ,cases))
;;           ;;I'm not sure if this will work or if i need to put a
;;           ;;progn somewhere in here
;;           (if (cons? x)
;;               (if (caar x)
;;                   (cdar x)
;;                   (acc (cdr x)));;recurse to the next case
;;               nil))))
