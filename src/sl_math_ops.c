#include "sl_math_ops.h"
#include "bignum.h"
/*
  Not all mpz functions have a signed version, so declare macros for the ones
  that don't, to allow using the same macros to generate things
*/
#define mpz_add_si mpz_add_ui
#define mpz_sub_si mpz_sub_ui
/*
  There are a ton of mpz division functions, but these behave most
  similarly to the way c integer division works
*/
#undef mpz_div_ui
#undef mpz_div
#define mpz_div_ui(out,x,y) mpz_tdiv_q_ui(out,x,y)
#define mpz_ui_div(out,x,y) mpz_tdiv_q_ui(out,y,x)
#define mpz_div_si(out,x,y) mpz_tdiv_q_ui(out,x,y)
#define mpz_si_div(out,x,y) mpz_tdiv_q_ui(out,y,x)
#define mpz_div mpz_tdiv_q
//only non communitive mpfr/gmp functions have a version which
//takes the bignum second, these macros fix that, and so ease code generation
#define mpfr_d_add(out, x, y, rnd)              \
  mpfr_add_d(out, y, x, rnd)
#define mpfr_d_mul(out, x, y, rnd)              \
  mpfr_mul_d(out, y, x, rnd)
#define mpfr_d_div(out, x, y, rnd)              \
  mpfr_div_d(out, y, x, rnd)
#define mpfr_z_add(out, x, y, rnd)              \
  mpfr_add_z(out, y, x, rnd)
#define mpfr_z_mul(out, x, y, rnd)              \
  mpfr_mul_z(out, y, x, rnd)
#define mpfr_z_div(out, x, y, rnd)              \
  mpfr_div_z(out, y, x, rnd)
#define mpfr_ui_add(out, x, y, rnd)             \
  mpfr_add_ui(out, y, x, rnd)
#define mpfr_ui_mul(out, x, y, rnd)             \
  mpfr_mul_ui(out, y, x, rnd)
#define mpfr_ui_div(out, x, y, rnd)             \
  mpfr_div_ui(out, y, x, rnd)
#define mpfr_si_add(out, x, y, rnd)             \
  mpfr_add_si(out, y, x, rnd)
#define mpfr_si_mul(out, x, y, rnd)             \
  mpfr_mul_si(out, y, x, rnd)
#define mpfr_si_div(out, x, y, rnd)             \
  mpfr_div_si(out, y, x, rnd)

#define mpz_si_mul(out,x,y)                     \
  mpz_mul_si(out,y,x)
#define mpz_ui_mul(out,x,y)                     \
  mpz_mul_ui(out,y,x)
#define mpz_si_add(out,x,y)                     \
  mpz_add_si(out,y,x)
#define mpz_ui_add(out,x,y)                     \
  mpz_add_ui(out,y,x)
#define mpz_si_sub(out,x,y)                     \
  mpz_sub_si(out,y,x)


#define rounding_mode MPFR_RNDN

#define mpz_wrapper(fn, args...)                                \
  ({__mpz_struct *temp = sl_malloc(sizeof(__mpz_struct));       \
    mpz_init(temp);                                             \
    fn(temp,##args);                                            \
    temp;})
#define mpq_wrapper(fn, args...)                                \
  ({__mpq_struct *temp = sl_malloc(sizeof(__mpq_struct));       \
    mpq_init(temp);                                             \
    fn(temp,##args);                                            \
    temp;})
#define mpfr_wrapper(fn, args...)                               \
  ({__mpfr_struct *temp = sl_malloc(sizeof(__mpfr_struct));     \
    mpfr_init(temp);                                            \
    fn(temp,##args);                                            \
    temp;})
/*
  Alternate way of doing float x bigint ops which uses bigfloats if
  the integer is to big
    if(isinf(mpz_get_d(y))){I may use this
    return make_bigfloat(mpfr_wrapper(mpfr_##name##_z,
    float_to_bigfloat(x),y,rounding_mode));
    }
*/
/* Should add a pre/post process argument that can be used to modify the
   arguments and/or the result
 */
#define define_math_ops(name,op)                                        \
  sl_obj uint_uint_##name(sl_obj x, sl_obj y){                          \
    return PACK_INT(XUINT(x) op XUINT(y));                              \
  }                                                                     \
  sl_obj int_int_##name(sl_obj x, sl_obj y){                            \
    return PACK_INT(XINT(x) op XINT(y));                                \
  }                                                                     \
  sl_obj int_float_##name(sl_obj x, sl_obj y){                          \
    return make_double(XINT(x) op XDOUBLE(y));                          \
  }                                                                     \
  sl_obj uint_float_##name(sl_obj x, sl_obj y){                         \
    return make_double(XUINT(x) op XDOUBLE(y));                         \
  }                                                                     \
  sl_obj int_bigint_##name(sl_obj x, sl_obj y){                         \
    return make_bigint(mpz_wrapper(mpz_si_##name, XINT(x), XBIGINT(y))); \
  }                                                                     \
  sl_obj uint_bigint_##name(sl_obj x, sl_obj y){                        \
    return make_bigint(mpz_wrapper(mpz_ui_##name, XUINT(x), XBIGINT(y))); \
  }                                                                     \
  sl_obj int_bigfloat_##name(sl_obj x, sl_obj y){                       \
    return make_bigfloat(mpfr_wrapper(mpfr_si_##name, XINT(x),          \
                                      XBIGFLOAT(y), rounding_mode));    \
  }                                                                     \
  sl_obj uint_bigfloat_##name(sl_obj x, sl_obj y){                      \
    return make_bigfloat(mpfr_wrapper(mpfr_ui_##name, XUINT(x),         \
                                      XBIGFLOAT(y), rounding_mode));    \
  }                                                                     \
  sl_obj float_int_##name(sl_obj x, sl_obj y){                          \
    return make_double(XDOUBLE(x) op XINT(y));                          \
  }                                                                     \
  sl_obj float_uint_##name(sl_obj x, sl_obj y){                         \
    return make_double(XDOUBLE(x) op XUINT(y));                         \
  }                                                                     \
  sl_obj float_float_##name(sl_obj x, sl_obj y){                        \
    return make_double(XDOUBLE(x) op XDOUBLE(y));                       \
  }                                                                     \
  sl_obj float_bigint_##name(sl_obj x, sl_obj y){                       \
    return make_double(XDOUBLE(x) op mpz_get_d(XBIGINT(y)));            \
  }                                                                     \
  sl_obj float_bigfloat_##name(sl_obj x, sl_obj y){               \
    return make_bigfloat(mpfr_wrapper(mpfr_d_##name, XDOUBLE(x),        \
                                      XBIGFLOAT(y), rounding_mode));    \
  }                                                                     \
  sl_obj bigint_int_##name(sl_obj x, sl_obj y){                         \
    return make_bigint(mpz_wrapper(mpz_##name##_ui, XBIGINT(x), XINT(y))); \
  }                                                                     \
  sl_obj bigint_uint_##name(sl_obj x, sl_obj y){                        \
    return make_bigint(mpz_wrapper(mpz_##name##_ui, XBIGINT(x), XUINT(y))); \
  }                                                                     \
  sl_obj bigint_float_##name(sl_obj x, sl_obj y){                       \
    return make_double(mpz_get_d(XBIGINT(x)) op XDOUBLE(y));            \
  }                                                                     \
  sl_obj bigint_bigint_##name(sl_obj x, sl_obj y){                      \
    return make_bigint(mpz_wrapper(mpz_##name,XBIGINT(x), XBIGINT(y))); \
  }                                                                     \
  sl_obj bigint_bigfloat_##name(sl_obj x, sl_obj y){                    \
    return make_bigfloat(mpfr_wrapper(mpfr_z_##name, XBIGINT(x),        \
                                      XBIGFLOAT(y), rounding_mode));    \
  }                                                                     \
  sl_obj bigfloat_int_##name(sl_obj x, sl_obj y){                       \
    return make_bigfloat(mpfr_wrapper(mpfr_##name##_si, XBIGFLOAT(x),   \
                                      XINT(y), rounding_mode));         \
  }                                                                     \
  sl_obj bigfloat_uint_##name(sl_obj x, sl_obj y){                      \
    return make_bigfloat(mpfr_wrapper(mpfr_##name##_ui, XBIGFLOAT(x),   \
                                      XUINT(y), rounding_mode));        \
  }                                                                     \
  sl_obj bigfloat_float_##name(sl_obj x, sl_obj y){                     \
    return make_bigfloat(mpfr_wrapper(mpfr_##name##_d, XBIGFLOAT(x),    \
                                      XDOUBLE(y), rounding_mode));      \
  }                                                                     \
  sl_obj bigfloat_bigint_##name(sl_obj x, sl_obj y){                    \
    return make_bigfloat(mpfr_wrapper(mpfr_##name##_z, XBIGFLOAT(x),    \
                                      XBIGINT(y), rounding_mode));      \
  }                                                                     \
  sl_obj bigfloat_bigfloat_##name(sl_obj x, sl_obj y){                  \
    return make_bigfloat(mpfr_wrapper(mpfr_##name, XBIGFLOAT(x),        \
                                      XBIGFLOAT(y), rounding_mode));    \
  }

#define define_math_cmps(name,op)                               \
  sl_obj uint_uint_##name(sl_obj x, sl_obj y){                  \
    return SL_BOOLEAN(XUINT(x) op XUINT(y));                    \
  }                                                             \
  sl_obj int_int_##name(sl_obj x, sl_obj y){                    \
    return SL_BOOLEAN(XINT(x) op XINT(y));                      \
  }                                                             \
  sl_obj int_float_##name(sl_obj x, sl_obj y){                  \
    return SL_BOOLEAN(XINT(x) op XDOUBLE(y));                   \
  }                                                             \
  sl_obj uint_float_##name(sl_obj x, sl_obj y){                 \
    return SL_BOOLEAN(XUINT(x) op XDOUBLE(y));                  \
  }                                                             \
  sl_obj int_bigint_##name(sl_obj x, sl_obj y){                 \
    return is_##name(mpz_cmp_si(XBIGINT(y),XINT(x)));           \
  }                                                             \
  sl_obj uint_bigint_##name(sl_obj x, sl_obj y){                \
    return is_##name(mpz_cmp_ui(XBIGINT(y), XUINT(x)));         \
  }                                                             \
  sl_obj int_bigfloat_##name(sl_obj x, sl_obj y){               \
    return is_##name(mpfr_cmp_si(XBIGFLOAT(y), XINT(x)));       \
  }                                                             \
  sl_obj uint_bigfloat_##name(sl_obj x, sl_obj y){              \
    return is_##name(mpfr_cmp_ui(XBIGFLOAT(y), XUINT(x)));      \
  }                                                             \
  sl_obj float_int_##name(sl_obj x, sl_obj y){                  \
    return SL_BOOLEAN(XDOUBLE(x) op XINT(y));                   \
  }                                                             \
  sl_obj float_uint_##name(sl_obj x, sl_obj y){                 \
    return SL_BOOLEAN(XDOUBLE(x) op XUINT(y));                  \
  }                                                             \
  sl_obj float_float_##name(sl_obj x, sl_obj y){                \
    return SL_BOOLEAN(XDOUBLE(x) op XDOUBLE(y));                \
  }                                                             \
  sl_obj float_bigint_##name(sl_obj x, sl_obj y){               \
    return is_##name(mpz_cmp_d(XBIGINT(y),XDOUBLE(x)));         \
  }                                                             \
  sl_obj float_bigfloat_##name(sl_obj x, sl_obj y){             \
    return is_##name(mpfr_cmp_d(XBIGFLOAT(y), XDOUBLE(x)));     \
  }                                                             \
  sl_obj bigint_int_##name(sl_obj x, sl_obj y){                 \
    return is_##name(mpz_cmp_si(XBIGINT(x), XINT(y)));          \
  }                                                             \
  sl_obj bigint_uint_##name(sl_obj x, sl_obj y){                \
    return is_##name(mpz_cmp_ui(XBIGINT(x), XUINT(y)));         \
  }                                                             \
  sl_obj bigint_float_##name(sl_obj x, sl_obj y){               \
    return is_##name(mpz_cmp_d(XBIGINT(x), XDOUBLE(y)));        \
  }                                                             \
  sl_obj bigint_bigint_##name(sl_obj x, sl_obj y){              \
    return is_##name(mpz_cmp(XBIGINT(x), XBIGINT(y)));          \
  }                                                             \
  sl_obj bigint_bigfloat_##name(sl_obj x, sl_obj y){            \
    return is_##name(mpfr_cmp_z(XBIGFLOAT(y), XBIGINT(x)));     \
  }                                                             \
  sl_obj bigfloat_int_##name(sl_obj x, sl_obj y){               \
    return is_##name(mpfr_cmp_ui(XBIGFLOAT(x),XINT(y)));        \
  }                                                             \
  sl_obj bigfloat_uint_##name(sl_obj x, sl_obj y){              \
    return is_##name(mpfr_cmp_ui(XBIGFLOAT(x),XUINT(y)));       \
  }                                                             \
  sl_obj bigfloat_float_##name(sl_obj x, sl_obj y){             \
    return is_##name(mpfr_cmp_d(XBIGFLOAT(x),XDOUBLE(y)));      \
  }                                                             \
  sl_obj bigfloat_bigint_##name(sl_obj x, sl_obj y){            \
    return is_##name(mpfr_cmp_z(XBIGFLOAT(x),XBIGINT(y)));      \
  }                                                             \
  sl_obj bigfloat_bigfloat_##name(sl_obj x, sl_obj y){          \
    return is_##name(mpfr_cmp(XBIGFLOAT(x),XBIGFLOAT(y)));      \
  }

//these are for functions which return real numbers (e.g cos, exp, ln, etc...)
//thus even the integer functions return floating point numbers
#define define_math_funs(name)                                          \
  sl_obj uint_##name(sl_obj x){                                         \
    return make_double(name(XUINT(x)));                                 \
  }                                                                     \
  sl_obj int_##name(sl_obj x){                                          \
    return make_double(name(XINT(x)));                                  \
  }                                                                     \
  sl_obj float_##name(sl_obj x){                                        \
    return make_double(name(XDOUBLE(x)));                               \
  }                                                                     \
  sl_obj bigint_##name(sl_obj x){                                       \
    return make_bigfloat(mpfr_wrapper(mpfr_##name,                      \
                                      bigint_to_bigfloat(XBIGINT(x)),   \
                                      rounding_mode));                  \
  }                                                                     \
  sl_obj bigfloat_##name(sl_obj x){                                \
      return make_bigfloat(mpfr_wrapper(mpfr_##name,                    \
                                        XBIGFLOAT(x), rounding_mode));  \
  }
//I'm not totally sure how the shifting required to pack/extract and integer
//will effect bitwise operations, I'll need to test these to see what happens
#define define_math_bitops(name, op)                                    \
  sl_obj int_int_log##name(sl_obj x, sl_obj y){                         \
    return PACK_INT(XUINT(x) op XUINT(y));                              \
  }                                                                     \
  sl_obj bigint_int_log##name(sl_obj x, sl_obj y){                      \
    return make_bigint(mpz_wrapper(mpz_##name,                          \
                                   XBIGINT(x), int_to_bigint(XUINT(y)))); \
  }                                                                     \
  sl_obj int_bigint_log##name(sl_obj x,sl_obj x){                       \
    return make_bigint(mpz_wrapper(mpz_##name,                          \
                                   int_to_bigint(XUINT(x)), XBIGINT(y))); \
  }                                                                     \
  sl_obj bigint_bigint_log##name(sl_obj x, sl_obj y){                   \
    return make_bigint(mpz_wrapper(mpz_##name, XBIGINT(x), XBIGINT(y))); \
  }
//given an integer, returned from a gmp/mpfr comparisions function
//return SL_T if the named comparision is true otherwise return NIL
#define is_eq(ret) SL_BOOLEAN(ret == 0)
#define is_ne(ret) SL_BOOLEAN(ret != 0)
#define is_lt(ret) SL_BOOLEAN(ret < 0)
#define is_gt(ret) SL_BOOLEAN(ret > 0)
#define is_le(ret) SL_BOOLEAN(ret <= 0)
#define is_ge(ret) SL_BOOLEAN(ret >= 0)

//math ops
define_math_ops(add, +);
define_math_ops(sub, -);
define_math_ops(mul, *);
define_math_ops(div, /);
//math comparisions
define_math_cmps(eq, ==);
define_math_cmps(ne, !=);
define_math_cmps(lt, <);
define_math_cmps(gt, >);
define_math_cmps(le, <=);
define_math_cmps(ge, >=);

define_math_funs(sin);
define_math_funs(cos);
define_math_funs(tan);
define_math_funs(asin);
define_math_funs(acos);
define_math_funs(atan);
define_math_funs(sinh);
define_math_funs(cosh);
define_math_funs(tanh);
define_math_funs(asinh);
define_math_funs(acosh);
define_math_funs(atanh);
define_math_funs(log);
define_math_funs(log10);
define_math_funs(log2);
sl_bigint *bigint_shift(sl_bigint *bigint, int64_t count){
  if(count > 0){
    return mpz_wrapper(mpz_mul_2exp, bigint, count);
  } else if (count < 0){
    return mpz_wrapper(mpz_fdiv_q_2exp, bigint, count);
  } else {
    return bigint;
  }
}
uint64_t int_shift(uint64_t val, int64_t count){
  if(count > 0){
    return val << count;
  } else if(count < 0){
    return val >> labs(count);
  } else {
    return val;
  }
}
