#ifndef SL_FRAMES_H
#define SL_FRAMES_H
#include "common.h"
#ifdef __GNUC__
#define longjmp __builtin_longjmp
#define setjmp __builtin_setjmp
#endif
typedef struct sl_frame sl_frame;
//jmp_buf is an array type, so we define an alias for the underlying struct
//to use instead
typedef struct __jmp_buf_tag sl_jmp_buf;
/*
  This is not a type that's available from lisp, so it shouldn't ever
  be packed into an sl_obj (probably).
*/
struct sl_frame {
  //do I need a tag to differentate errors/labels
  union {
    sl_obj name;//label for return/goto
    sl_obj tag;//type of error for catch/unwind_protect
  };
  sl_obj value;//return value/error msg/etc
  //these values are to reset program state to the point the frame was defined
  sl_obj lexical_env;
  sl_obj thread_local_bindings;
  sl_obj *frame_ptr;
  //used to store any extra information, eg function names for a backtrace
  sl_obj *info;
  sl_jmp_buf c_env;
};
SL_INLINE sl_frame *make_frame(sl_obj name, sl_obj value){
  sl_frame *f = sl_malloc(sizeof(sl_frame));
  f->name = name;
  f->value = value;
}
SL_INLINE void set_frame_info(sl_frame *f, sl_obj *info){
  f->info = info;
}
/*
  We need to inline this to make sure it's not part of the call stack
*/
//this is basically a macro
SL_ALWAYS_INLINE void reset_env_frame(sl_frame *f){
  thread_env->lexical_env = f->lexical_env;
  thread_env->thread_local_bindings = f->thread_local_bindings;
  thread_env->frame_ptr = f->frame_ptr;
}
SL_ALWAYS_INLINE void get_env_frame(sl_frame *f){
  f->lexical_env = thread_env->lexical_env;
  f->thread_local_bindings = thread_env->thread_local_bindings;
  f->frame_ptr = thread_env->frame_ptr;
}
SL_ALWAYS_INLINE SL_ATTRIBUTE(returns_twice) sl_obj push_frame(sl_frame *f){
  if(setjmp(&f->c_env) != 0){
    set_env_frame(f);
    return f->value;
  } else {
    get_env_frame(f);
    frame_stack_push(f);
    return NIL;
  }
}
/*
  This creates the possible issue where (catch :unwind_protect body)
  is the same as (unwind_protect body). This is fine if it's documented
  and thus part of the design, but if I don't what that I need to change
  the way I handle unwind_protect. The best way to do this would be to
  define a c only unwind protect symbol, similar to what I do for 
  the unbound `symbol`.
*/
SL_INLINE SL_NORETURN void jmp_to_frame(sl_obj tag){
  //There should always be an unwind_protect at the bottom
  //of the frame stack, so we don't need to do bounds checking
  while(1){
    if(thread_env->frame_ptr == Kunwind_protect ||
       EQV(thread_env->frame_ptr->tag, tag)){
      longjmp(&thread_env->frame_ptr->c_env, 1);
    } else {
      thread_env->frame_ptr--;
    }
  }
  SL_UNREACHABLE;
}
#define push_new_frame(name, value)             \
  ({sl_frame *f = make_frame(name, value);      \
    push_frame(f);})
/*
  various special forms go here, I need to figure out what should be special
  forms and what should be macros first though.
  i.e throw, catch, unwind_protect. tagbody/go, return_from, etc...
*/
#endif
