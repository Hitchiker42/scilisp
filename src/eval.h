#ifndef _SL_EVAL_H_
#define _SL_EVAL_H_
/*
  Header for functions for evalation, compilation, calling functions, etc...
*/
#include "common.h"
sl_obj Feval(sl_obj obj);
sl_obj sl_eval_string(const char *str, uint32_t len);
#define sl_eval_cstr(str) sl_eval_string(str, strlen(str));
sl_obj sl_funcall(sl_obj f, sl_obj args);
//needed to make C closures for lambda functions
sl_obj funcall_lambda(sl_lambda *l, sl_obj args);
#endif
