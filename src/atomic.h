#ifndef _SL_ATOMIC_H_
#define _SL_ATOMIC_H_
#define SL_RW_BARRIER __ATOMIC_SEQ_CST
#define SL_RD_BARRIER __ATOMIC_SEQ_ACQUIRE
#define SL_WR_BARRIER __ATOMIC_SEQ_RELEASE
/*
  gcc provides polymorphic builtin functions for atomic operations,
  to keep this polymorphism atomic operations need to be defined 
  as macros.

  Typed versions will be provided for use in lisp.
*/
#define sl_atomic_load_n(ptr)                   \
  (__atomic_load_n(ptr, SL_RW_BARRIER))
#define sl_atomic_load(ptr, ret)                \
  (__atomic_load(ptr,ret,SL_RW_BARRIER))
#define sl_atomic_read(ptr) sl_atomic_load_n(ptr)
#define sl_atomic_store_n(ptr, val)             \
  (__atomic_store_n(ptr, val, SL_RW_BARRIER))
#define sl_atomic_store(ptr, val)             \
  (__atomic_store(ptr, val, SL_RW_BARRIER))
#define sl_atomic_write(ptr, val) sl_atomic_store_n(ptr,val)
#define sl_atomic_exchange_n(ptr,val)           \
  (__atomic_exchange_n(ptr,val,SL_RW_BARRIER))
#define sl_atomic_exchange(ptr,val)           \
  (__atomic_exchange(ptr,val,SL_RW_BARRIER))
#define sl_atomic_cmpxchg_n(ptr, expected, val)   \
  (__atomic_compare_exchange_n(ptr, expected, val, 0, \
                               SL_RW_BARRIER, SL_RW_BARRIER))
#define sl_atomic_cmpxchg(ptr, expected, val)                   \
  (__atomic_compare_exchange(ptr, expected, val, 0,             \
                             SL_RW_BARRIER, SL_RW_BARRIER))
#define sl_atomic_add(ptr,val)                  \
  (__atomic_add_fetch(ptr,val,SL_RW_BARRIER))
#define sl_atomic_inc(ptr)                      \
  (__atomic_add_fetch(ptr, 1, SL_RW_BARRIER))
#define sl_atomic_sub(ptr,val)                  \
  (__atomic_sub_fetch(ptr,val,SL_RW_BARRIER))
#define sl_atomic_dec(ptr)                      \
  (__atomic_sub_fetch(ptr, 1, SL_RW_BARRIER))
#define sl_atomic_and(ptr,val)                  \
  (__atomic_and_fetch(ptr,val,SL_RW_BARRIER))
#define sl_atomic_xor(ptr,val)                  \
  (__atomic_xor_fetch(ptr,val,SL_RW_BARRIER))
#define sl_atomic_or(ptr,val)                  \
  (__atomic_or_fetch(ptr,val,SL_RW_BARRIER))
//gcc doesn't provide an atomic not, which I'll write at some point

#define sl_atomic_xadd(ptr,val)                 \
  (__atomic_fetch_add(ptr,val))
#define sl_atomic_post_inc(ptr,val)                 \
  (__atomic_fetch_add(ptr,1))
#define sl_atomic_post_dec(ptr,val)                 \
  (__atomic_fetch_add(ptr,-1))
#ifdef __x86_64__
#define sl_spin_pause() __asm__ volatile ("pause")
#else
#define sl_spin_pasue() __asm__ volatile ("")
#endif
//all of the other atomic operations(xsub,xand,xxor,etc..) aren't
//really atomic (on x86_64), they use cmpxchg. I may
//add macros for them later
#endif
