#include "llvm_codegen.cpp"
#include "sl_llvm.h"
#include "gc_cpp.h"
//makes for cleaner code
#define thread_ctx unwrap(thread_env->llvm_context)
using namespace llvm;
//generic new should use gc malloc atomic
//new type; is the same as new (PointerFreeGC) type
inline void *new(size_t sz){
  return GC_MALLOC_ATOMIC(sz);
}
void init_llvm(void){
//god c++ is weird
  thread_env->llvm_context = wrap(new (PointerFreeGC) LLVMContext);
  return;
}
/*
  Best practice would be to use builtin C++ mechinisms for polymorphism.
  But since I've setup my own type system for SciLisp, this isn't really
  feasable, so we do things the same way as in C, dispatch tables.
*/
typedef Value*(codegen_fun)(sl_obj);
static Value *codegen_int(sl_obj);
static Value *codegen_symbol(sl_obj);
static Value *codegen_cons(sl_obj);
static Value *codegen_number(sl_obj);
static Value *codegen_vector(sl_obj);
static Value *codegen_misc(sl_obj);
static Value *codegen_imm(sl_obj);
static codegen_fun* sl_codegen_dispatch[8] =
  {codegen_symbol, codegen_cons, codegen_int, codegen_number,
   codegen_vector, codegen_misc, codegen_int, codegen_imm};
/*
  Return an llvm value representing the scilisp object obj.
*/
Value *sl_codegen(sl_obj obj){
  return sl_codegen_dispatch[XSL_TYPE(obj)](obj);
}

static Value *codegen_int(sl_obj sl_int){
  //specifically for immediate integers
  return ConstantInt::get(getInt64Ty(thread_ctx), XINT(sl_int));
}
static Value *codegen_symbol(sl_obj);
static Value *codegen_cons(sl_obj);
static Value *codegen_number(sl_obj);
static Value *codegen_vector(sl_obj);
static Value *codegen_misc(sl_obj);
static Value *codegen_imm(sl_obj);

static Value *codegen_double(sl_number *dbl){
  return ConstantFP::get(getDoubleTy(thread_ctx), dbl->float64);
}
