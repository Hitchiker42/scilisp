#include "format.h"
#include "util.h"
#include "bignum.h"
/*
  This file should contain my own hand written version of format. Currently
  I use a hacked together version using the builtin printf, plus some
  gnu C extensions. This verision should parse the format string itself, using
  sprintf to format the arguments themeselves is fine though.
*/
/*
  TODO: write code to parse format specifiers (I need to do this even if I
  continue to use printf to do the formatting since I need that information
  for some lisp specifiers), handle integers internally.
*/
static int format_bigint(sl_string_buf *buf, sl_bigint *val,
                         struct format_spec *spec);
static int format_bigfloat(sl_string_buf *buf, sl_bigfloat *val,
                           struct format_spec *spec);
//parse a format specification, str should point to a '%', return value is
//the number of characters in the spec. The specification itself is stored
//in the spec argument, since C can't do multiple return values
int parse_format_spec(char *str, int len, struct format_spec *spec){
  assert(*str == '%');
  int i;
  memset(spec, '\0', sizeof(struct format_spec));
  char *str_start = str;
  /*It's fun to use goto every once in a while, and besides some of
    the labels are actually necessary.
  */
 MODIFIERS:
  if(str - str_start > len){
    return -1;
  }
  switch(*str++){
    case '#':
      if(spec->alt == 0){
        spec->alt = 1;
      } else {
        return -2;
      }
      goto MODIFIERS;
    case '+':
      if(spec->sign == 0){
        spec->sign = 1;
      } else {
        return -2;
      }
      goto MODIFIERS;
    case '-':
      if(spec->left == 0){
        spec->left = 1;
      } else {
        return -2;
      }
      goto MODIFIERS;
    case '0':
      if(spec->zero == 0){
        spec->zero = 1;
      } else {
        return -2;
      }
      goto MODIFIERS;
    case '.':
      str--;
      goto PRECISION;
  }
 WIDTH:
  spec->width = strtol(str, &str, 10);
  if(*str != '.'){
    goto FORMAT_CHAR;
  }
 PRECISION:
  assert(*str == '.');
  str++;
  spec->prec = strtol(str, &str, 10);
 FORMAT_CHAR:
  if(fmt_control_chars[(uint8_t)*str] == 0){
    return -1;
  }
  spec->spec_char = *str++;
  return str_start - str;
}
/*
  Quick note on the usage of string bufs in this file. Since most things being
  appended to the buffer are strings, its better to avoid appending characters,
  since flushing the buffer is more expensive than appending a small string.
*/
DEFUN("format",format,1,MANY,"(format format-string &rest args)\n"
      "Format a string, inserting args into format-string based on control"
      "sequences beginning with '%', similar to the printf function in C")
  (uint64_t numargs, sl_obj *args){
  if(!STRINGP(args[0])){
    raise_error_temp();
  }
  if(numargs == 1){
    return args[0];
  }
  //copy format string to the stack, since we may need to modify it
  int fmt_len = XSTRING(args[0])->len;
  char *fmt_str = alloca(fmt_len + 1);
  memcpy(fmt_str, XSTRING(args[0])->str, fmt_len);
  char *fmt_spec_start;

  sl_string_buf *buf = make_string_buf();
  struct format_spec;
  int i = 1;
  while((fmt_spec_start = memchr(fmt_str,'%',fmt_len))){
    buf_append_cstrn(buf, fmt_str, fmt_spec_start - fmt_str);
    fmt_len -= fmt_spec_start - fmt_str;

    //this finds the first character after '%' that can't be a modifier character
    uint32_t control_index = memspn_table(fmt_str+1, MAX_CONTROL_LENGTH,
                                          fmt_control_modifiers) + 1;
    uint8_t control_char = fmt_str[control_index];
    if(fmt_control_chars[control_char] == 0){
      raise_error_fmt(0, "Error, undefined format control character %c\n",
                      control_char);
    }
    //maybe put a check here to insure we haven't gone past the end of the string
    fmt_str = fmt_spec_start + control_index + 1;
    fmt_len -= control_index + 1;
    if(i >= numargs && (control_char != '%' || control_char != '$')){
      raise_error_cstr(0, "Error, excess specifiers in format string\n");
    }
    switch(control_char){
      //lisp control characters, modifiers are currently ignored
      case 'a':
      case 'A':
        buf_append_str(buf, sl_sprinc(args[i++]));
        continue;
      case 's':
      case 'S':
        buf_append_str(buf, sl_sprint(args[i++]));
        continue;
      case '$':{
        //variable interpolation
        uint32_t symbol_len = 0;
        //compute the length of the symbol
        while(sl_is_symbol_char(fmt_str[symbol_len]) &&
              fmt_str[symbol_len] != '%' &&
              ++symbol_len < fmt_len);
        //make a symbol
        sl_obj sym =
          make_symbol_from_string(make_string_unboxed_nocopy(fmt_str,
                                                             symbol_len, 0));
        //intern it/get interned version
        sym = PACK_SYMBOL(find_symbol(thread_env->obarray,XSYMBOL(sym)));
        sl_obj val = get_symval(sym);
        buf_append_str(buf, sl_sprint(val));
        continue;
      }
      case 'd':
      case 'x':
      case 'X':{
        if(INTP(args[i])){
          int64_t x = XINT(args[i++]);
          /* I should use sl_iota, but I need to write code to parse
             modifiers first
             int base = (control_char == 'd' ? 10 : 0x10);
             int upcase = (control_char == 'X');*/
          //hack to deal with trailing null byte
          uint8_t temp = *fmt_str;
          *fmt_str = '\0';
          buf_append_cstr(buf, asprintf_alloca(fmt_str, x));
          *fmt_str = temp;
        } else if(BIGINTP(args[i])){
          sl_bigint *x = XBIGINT(args[i++]);
          char *temp_spec = alloca(control_index + 2);
          memcpy(temp_spec, fmt_str, control_index-1);
          temp_spec[control_index-1] = 'Z';
          temp_spec[control_index] = control_char;
          temp_spec[control_index+1] = '\0';
          //this calls mpfr_sprintf internally, optimize this later
          buf_append_str(buf, sl_sprintf(temp_spec, 0, x));
        } else {
          raise_error_fmt(0, "Format type error, expected an integer "
                          "for a(n) %c specifier", control_char);
        }
        continue;
      }
      case 'f':
      case 'g':
      case 'G':
      case 'e':
      case 'E':{
        if(DOUBLEP(args[i])){
          double x = XDOUBLE(args[i++]);
          uint8_t temp = *fmt_str;
          *fmt_str = '\0';
          buf_append_cstr(buf, asprintf_alloca(fmt_str, x));
          *fmt_str = temp;
        } else if(BIGFLOATP(args[i])){
          sl_bigfloat *x = XBIGFLOAT(args[i++]);
          char *temp_spec = alloca(control_index + 2);
          memcpy(temp_spec, fmt_str, control_index-1);
          temp_spec[control_index-1] = 'R';
          temp_spec[control_index] = control_char;
          temp_spec[control_index+1] = '\0';
          //this calls mpfr_sprintf internally, optimize this later
          buf_append_str(buf, sl_sprintf(temp_spec, 0,x));
          continue;
        } else {
          raise_error_fmt(0, "Format type error, expected a floating point "
                          "number for a(n) %c specifier", control_char);
        }
        continue;
      }
      default:
        SL_UNREACHABLE;//the switch covers all the format chars
    }
  }
  return string_buf_to_string_obj(buf);
}
//assume val is definately an integer
static int format_int(sl_string_buf *buf, sl_obj val,
                      struct format_spec *spec){
  size_t base = 0;
  int uc = 0;
  switch(spec->spec_char){
    case 'd':
      base = 10; break;
    case 'X':
      uc = 1;
    case 'x':
      base = 16;
      if(spec->alt){
        buf_append_str_lit(buf, uc ? "0X" : "0x");
      }
      break;
    case 'R':
      uc = 1;
    case 'r':
      base = spec->prec; break;
    default:
      SL_UNREACHABLE;
  }
  char *number = NULL;
  size_t nbytes = 0;
  if(INTP(val)){
    sl_string *temp = sl_itoa(XINT(val), base, uc, spec->sign);
    number = temp->str;
    nbytes = temp->len;
  } else {//bigint
    sl_bigint *x = XBIGINT(val);
    nbytes = mpz_sizeinbase(x, base)+2;//+2 = sign + null
    if(uc){
      base = -base;
    }
    void *temp = NULL;
    number = sl_malloc(nbytes);
    if(spec->sign && (mpz_sgn(x) >= 0)){
      *number = '+';
      temp = number + 1;
    } else {
      temp = number;
    }
    temp = mpz_get_str(temp, base, x);
    char *end = number + nbytes - 1;
    do {
      nbytes-=1;
    } while(*end-- != '\0');
  }
  if(nbytes >= spec->width){
    buf_append_cstrn(buf, number, nbytes);
  } else {
    int pad_amt = (spec->width - nbytes);
    char pad_char = (spec->zero && !spec->left) ? '0' : ' ';
    //could replace this with alloca, but would need to insure this function
    //gets inlined into format
    char *pad_str = sl_malloc(pad_amt);
    memset(pad_str, pad_char, pad_amt);
    if(spec->left){
      buf_append_cstrn(buf, pad_str, pad_amt);
      buf_append_cstrn(buf, number, nbytes);
    } else {
      buf_append_cstrn(buf, number, nbytes);
      buf_append_cstrn(buf, pad_str, pad_amt);
    }
  }
}
/* static int format_float(sl_string_buf *buf, sl_obj val, */
/*                         struct format_spec *spec){ */
/*   if(DOUBLEP(val)){ */
/*     int sign = value & (1 << 63); */
/*     value &= ~(1 << 63); //remove sign */
/*     uint64_t mant = (value << 12)>>12; */
/*     int32_t exp = (value >> 52) - 1023; */
    
/*   } */





/*
  TODO: Interpret format_char/pad_char as utf8 characters, not single bytes
int parse_format_spec_experimental(char *str, struct format_spec *spec){
  assert(str[0] == '%');
  char *str_start = str;
 MODIFIERS:
  switch(*str++){
    case '@':
      if(spec->alt_1 == 0){
        spec->alt_1 = 1;
      } else {
        return -1;
      }
      goto MODIFIERS;
    case ':':
      if(spec->alt_2 == 0){
        spec->alt_2 = 1;
      } else {
        return -1;
      }
      goto MODIFIERS;
    case '-':
      if(spec->left == 0){
        spec->left = 1;
      } else {
        return -1;
      }
      goto MODIFIERS;
    case '.':
      str--;
      goto PRECISION;
  }
 WIDTH:
  spec->width = strtol(str, &str, 10);
  if(*str != '.'){
    goto FORMAT_CHAR;
  }
 PRECISION:
  assert(*str == '.');
  str++;
  spec->prec = strtol(str, &str, 10);
  if(*str != '.'){
    goto FORMAT_CHAR;
  }
 PAD_CHAR:
  assert(*str == '.');
  str++;
  spec->pad_char = *str;
 FORMAT_CHAR:
  if(fmt_control_chars[(uint8_t)*str] == 0){
    return -1;
  }
  spec->spec = *str++;
  return str_start - str;
}
*/
