#include "sl_math.h"
/*
  //left rotate a 64 bit value
  uint64 Rot64(uint64 x, int k){
    return (x << k) | (x >> (64 - k));
  }
*/
DEFUN("ash", ash, 2, 2, "(ash integer count) return a copy of integer shifted "
      "to the left by count, or if count is negative shifted to the right.")
  (sl_obj integer, sl_obj count){
  if(!INTEGERP(integer) || !INTEGERP(count)){
    raise_error_temp();
  }
  int64_t sbits;//shift bits
  if(BIGINTP(count)){
    //if count is bigger than LONG_MAX integer wouldn't even fit into memory
    sbits = bigint_to_int(XBIGINT(count));
  } else {
    sbits = XFIXNUM(count);
  }
  if(BIGINTP(integer)){
    sl_bigint *retval = bigint_shift(XBIGINT(integer), sbits);
    return make_bignum(retval, SL_bigint);
  } else {
    //TODO: overflow check
    return PACK_INT(int_shift(XFIXNUM(integer), sbits));
  }
}
/*
  Special type casting rules
  int op int ->  int if result fits in 62 bits, bigint otherwise
  bigint op float -> float if bigint < some defined maximum, otherwise bigfloat
  float op float -> float, always, even if the result is inf or denormal
*/
#define gen_dispatch_table(name)                \
  {int_int_##name, int_float_##name, int_bigint_##name, int_bigfloat_##name, \
   float_int_##name, float_float_##name,                                \
   float_bigint_##name, float_bigfloat_##name,                          \
   bigint_int_##name, bigint_float_##name,                              \
   bigint_bigint_##name, bigint_bigfloat_##name,                        \
   bigfloat_int_##name, bigfloat_float_##name,                          \
   bigfloat_bigint_##name, bigfloat_bigfloat_##name}
#define dispatch_table_columns 4
#define math_dispatch(table, __type1, __type2, __x, __y)        \
  table[(__type1 * dispatch_table_columns) + __type2](__x,__y)
#define gen_math_op(name)                                               \
  static sl_obj(*name##_dispatch_table[])(sl_obj,sl_obj) =              \
    gen_dispatch_table(name);                                           \
  sl_obj sl_##name(sl_obj x, sl_obj y){                                 \
    TYPECHECK_2(NUMBER, x, y, 0, 0);                                    \
    uint32_t type1 = INTP(x) ? 0 : SL_number_key(x);                    \
    uint32_t type2 = INTP(y) ? 0 : SL_number_key(y);                    \
    if(type1 > 4 || type2 > 4){                                         \
      raise_error_temp();                                               \
    }                                                                   \
    return math_dispatch(name##_dispatch_table, type1, type2, x, y);    \
  }
 
#define gen_math_cmp(name)                                              \
  static sl_obj(*name##_dispatch_table[])(sl_obj,sl_obj) =              \
    gen_dispatch_table(name);                                           \
  sl_obj sl_num_##name(sl_obj x, sl_obj y){                             \
    TYPECHECK_2_TEMP(NUMBER, x, y);                                     \
    int type1 = INTP(x) ? 0 : SL_number_key(x);                         \
    int type2 = INTP(y) ? 0 : SL_number_key(y);                         \
    if(type1 > 4 || type2 > 4){                                         \
      raise_error_temp();                                               \
    }                                                                   \
    return math_dispatch(name##_dispatch_table, type1, type2, x, y);    \
  }
//a switch stmt generates basically the same code(actually probably worse code)
#define gen_fn_dispatch_table(name)                             \
  {int_##name, float_##name, bigint_##name, bigfloat_##name}
#define gen_math_fn(name)                                               \
  static sl_obj(*name##_dispatch_table[])(sl_obj) = gen_fn_dispatch_table(name); \
  sl_obj sl_##name(sl_obj x){                                           \
    TYPECHECK_1_TEMP(NUMBER,x);                                         \
    uint32_t type = INTP(x) ? 0 : SL_number_key(x);                     \
    if(type > 4){raise_error_temp();}                                   \
    return name##_dispatch_table[type](x);                              \
  }

gen_math_op(add);
gen_math_op(sub);
gen_math_op(mul);
gen_math_op(div);
//only difference with comparisons is the function name (num_eq vs just eq)
gen_math_cmp(eq);
gen_math_cmp(ne);
gen_math_cmp(lt);
gen_math_cmp(le);
gen_math_cmp(gt);
gen_math_cmp(ge);

gen_math_fn(sin);
gen_math_fn(cos);
gen_math_fn(tan);
gen_math_fn(asin);
gen_math_fn(acos);
gen_math_fn(atan);
gen_math_fn(sinh);
gen_math_fn(cosh);
gen_math_fn(tanh);
gen_math_fn(asinh);
gen_math_fn(acosh);
gen_math_fn(atanh);
//atan2 is tricky, since it takes two args
//gen_math_fn(atan2);
gen_math_fn(log);
gen_math_fn(log10);
gen_math_fn(log2);

sl_obj sl_neg(sl_obj x){
  //might update this later
  return sl_sub(sl_0, x);
}
sl_obj sl_recip(sl_obj x){
  //might update this later
  return sl_div(sl_1, x);
}
DEFUN("+",add,0,MANY,"(+ &rest args) return sum of args")
  (uint64_t numargs, sl_obj *args){
  //return 0 if no args given
  DEBUG_PRINTF("Calling add with %ld args\n", numargs);
  sl_obj acc = (numargs ? args[0] : 0);
  int i = 1;
  while(i<numargs){
    acc = sl_add(acc, args[i++]);
  }
  return acc;
}
DEFUN("-",sub,0,MANY,"(- &rest args) when given one arg negate it, with more"
      "than one arg, subtract all but the first from the first")
  (uint64_t numargs, sl_obj *args){
  if(numargs < 2){
    return (numargs ? sl_neg(args[0]) : 0);
  } else {
    sl_obj acc = args[0];
    int i = 1;
    while(i < numargs){
      acc = sl_sub(acc, args[i++]);
    }
    return acc;
  }
}
DEFUN("*",mul,0,MANY,"(* &rest args) return product of args")
  (uint64_t numargs, sl_obj *args){
  //return 0 if no args given
  sl_obj acc = (numargs ? args[0] : 0);
  int i = 1;
  while(i<numargs){
    acc = sl_mul(acc, args[i++]);
  }
  return acc;
}
DEFUN("/",div,0,MANY,"(/ &rest args) when given one arg return the reciprocal, "
      "with more than one arg, divide the first arg by the remaining args")
  (uint64_t numargs, sl_obj *args){
  if(numargs < 2){   //
    return (numargs ? sl_recip(args[0]) : 0);
  } else {
    sl_obj acc = args[0];
    int i = 1;
    while(i < numargs){
      acc = sl_div(acc, args[i++]);
    }
    return acc;
  }
}
DEFUN("1+",add1,1,1,"(1+ num) add 1 to number num")
     (sl_obj num){
  //kinda lazy
  return sl_add(num,sl_1);
}
DEFUN("1-",sub1,1,1,"(1- num) subtract 1 from number num")
     (sl_obj num){
  //kinda lazy
  return sl_sub(num,sl_1);
}
DEFUN("=",num_eq,1,MANY,"(= number &rest numbers) return t if number and all "
      "numbers are mathematically equivlent, regardless of type")
  (uint64_t numargs, sl_obj *args){
  sl_obj num = args[0];
  int i=1;
  while(i < numargs){
    if(!(sl_num_eq(num,args[i++]))){
      return NIL;
    }
  }
  return Qt;
}
DEFUN("/=",num_ne,1,MANY,"(/= number &rest numbers) return t if number and all "
      "numbers are mathematically different, regardless of type")
  (uint64_t numargs, sl_obj *args){
  sl_obj num = args[0];
  int i=1;
  while(i < numargs){
    if(!(sl_num_ne(num,args[i++]))){
      return NIL;
    }
  }
 return Qt;
}
DEFUN(">",num_gt,1,MANY,"(> number &rest numbers) return t if each number is "
      "greater than the previous number")
  (uint64_t numargs, sl_obj *args){
  sl_obj num = args[0];
  int i=1;
  while(i < numargs){
    if(!(sl_num_gt(num,args[i++]))){
      return NIL;
    }
  }
  return Qt;
}
DEFUN("<",num_lt,1,MANY,"(> number &rest numbers) return t if each number is "
      "less than the previous number")
  (uint64_t numargs, sl_obj *args){
  sl_obj num = args[0];
  int i=1;
  while(i < numargs){
    if(!(sl_num_lt(num,args[i++]))){
      return NIL;
    }
  }
  return Qt;
}
DEFUN(">=",num_ge,1,MANY,"(> number &rest numbers) return t if each number is "
      "greater than or equal to the previous number")
  (uint64_t numargs, sl_obj *args){
  sl_obj num = args[0];
  int i=1;
  while(i < numargs){
    if(!(sl_num_ge(num,args[i++]))){
      return NIL;
    }
  }
  return Qt;
}
DEFUN("<=",num_le,1,MANY,"(> number &rest numbers) return t if each number is "
      "less than or equal to the previous number")
  (uint64_t numargs, sl_obj *args){
  sl_obj num = args[0];
  int i=1;
  while(i < numargs){
    if(!(sl_num_le(num,args[i++]))){
      return NIL;
    }
  }
  return Qt;
}
#define defun_math_fun(lname, cname, doc)                               \
  DEFUN(lname, cname, 1, 1, "("lname" x) compute the "doc" of x")       \
  (sl_obj x){                                                           \
    TYPECHECK_1(NUMBER, x, 0 ,0);                                       \
    return sl_##cname(x);                                               \
  }
defun_math_fun("sin",sin,"sine");
defun_math_fun("cos",cos,"cosine");
defun_math_fun("tan",tan,"tangent");
defun_math_fun("asin",asin,"arc sine");
defun_math_fun("acos",acos,"arc cosine");
defun_math_fun("atan",atan,"arc tangent");
defun_math_fun("sinh",sinh,"hyperbolic sine");
defun_math_fun("cosh",cosh,"hyperbolic cosine");
defun_math_fun("tanh",tanh,"hyperbolic tangent");
defun_math_fun("asinh",asinh,"hyperbolic arc sine");
defun_math_fun("acosh",acosh,"hyperbolic arc cosine");
defun_math_fun("atanh",atanh,"hyperbolic arc tangent");
/*DEFUN("atan",atan,1,2,"(atan a &optional (b 1)) compute the arc tangent of a/b")
  (sl_obj a, sl_obj b){
  TYPECHECK_1(NUMBER, a, 0, 0);
  if(NILP(b)){
    return sl_atan(a);
  }
  TYPECHECK_1(NUMBER, b,0,0);
  return sl_atan2(a,b);
}*/
//should have a global static state
/*
DEFUN("random",random,0,2,"(random &optional limit random-state) return a"
      "random number less that limit, and of the same type as limit. if "
      "limit is not given return a random integer. The 'random-state' "
      "argument should be a random state returned by make-random-state."
      "If it is not given a static state is used, so random-state needs to"
      "be supplied to safely use random in multithreaded programs")
  (sl_obj limit, sl_obj state){
  //I'll need to look at the internals of gmp/mpfr to determine
  //how to generate those
  if(NILP(limit)){
    return rand_uint64();
  }
  if(!NUMBERP(limit)){
    raise_error_temp();
  }
  if(!NILP(state)){
    raise_error_temp();
  }
  if(FLOATP(limit)){
    return sl_mul(make_double(rand_double()),limit);
  }
}*/
void init_math_symbols(void){
  INIT_SUBR(add, MANY);
  INIT_SUBR(sub, MANY);
  INIT_SUBR(mul, MANY);
  INIT_SUBR(div, MANY);
  INIT_SUBR(num_eq, MANY);
  INIT_SUBR(num_gt, MANY);
  INIT_SUBR(num_lt, MANY);
  INIT_SUBR(num_ne, MANY);
  INIT_SUBR(num_le, MANY);
  INIT_SUBR(num_ge, MANY);
  INIT_SUBR(add1, 1);
  INIT_SUBR(sub1, 1);
}
