/*
  header for string functions, including internal string operations
*/
#ifndef _SL_STRING_H_
#define _SL_STRING_H_
#include "common.h"
/*
  SciLisp strings don't have a set representation, the internal structure
  of strings shouldn't be known outside of the implementation of the 
  core string functions. Currently SciLisp strings are essentially just
  character arrays with a length, and some information about the 
  encoding (i.e is it pure ascii or not)
*/

int sl_string_equal(sl_string* s1, sl_string* s2);
sl_char sl_string_to_char(sl_string *str);
sl_obj Fstring_to_char(sl_obj str);
sl_string* sl_concat_string_objs(uint64_t numargs, sl_obj *args);
sl_string* sl_concat_strings(uint64_t numargs, sl_string **args);
sl_string* sl_substr(sl_string* s, int32_t start, int32_t end);
sl_obj Fconcat(uint64_t numargs, sl_obj *args);
sl_obj Fvector_push(sl_obj vec, sl_obj elt);
/*
  To be added, at some point
*/
//convert a utf8 string to a vector of utf32 characters
sl_obj Fstring_to_vector(sl_obj str);
//convert a vector of characters into a utf8 string
sl_obj Fvector_to_string(sl_obj vec);
//find index of c in str
int sl_strchr(sl_string *s, sl_char c);
//same as above but in lisp
sl_obj Fstring_index(sl_obj str, sl_obj c);
sl_obj Fmake_string(sl_obj size, sl_obj initial_element);


static sl_string *sl_char_to_string(sl_char c);
sl_obj Fchar_to_string(sl_obj c);
#define make_string_core(__str,__value,__len,__ascii)   \
  __str->str = __value;                                 \
  __str->len = (__len);                                 \
  __str->ascii = (__ascii ? 1 : 0);                     \
  __str->tag = SL_string;

SL_INLINE sl_obj make_string(const char *value, uint64_t len, int ascii){
  sl_string *str = sl_malloc_atomic(sizeof(sl_string) + len*sizeof(char));
  char *str_mem = (char*)((void*)str + sizeof(sl_string));
  memcpy(str_mem, value, len * sizeof(char));
  make_string_core(str, str_mem, len, ascii);
  return make_sl_obj((sl_obj)str, SL_vector);
}
SL_INLINE sl_obj make_string_nocopy(const char *value,
                                    uint64_t len, int ascii){
  sl_string *str = sl_malloc(sizeof(sl_string));
  make_string_core(str, value, len, ascii);
  str->copy_on_write = 1;
  return make_sl_obj((sl_obj)str, SL_vector);
}
//If you want an empty string, you probably want it unboxed
SL_INLINE sl_string *make_empty_string(uint64_t len){
  sl_string *str = NULL;
  make_string_core(str, NULL, len, 1);
  //Optimize the case where we're just making a string header
  str->str = (len ?
              sl_malloc(sizeof(sl_string)*len*sizeof(char)) : NULL);
  return str;
}
/*
  Make a string and return a pointer to it, rather than an sl_obj
*/
SL_INLINE sl_string *make_string_unboxed(const char *value,
                                         uint64_t len, int ascii){
  sl_string *str = sl_malloc_atomic(sizeof(sl_string)+ len*sizeof(char));
  char *str_mem = (char*)((void*)str + sizeof(sl_string));
  memcpy(str_mem, value, len * sizeof(char));
  make_string_core(str, str_mem, len, ascii);
  return str;
}
SL_INLINE sl_string *make_string_unboxed_nocopy(const char *value,
                                                uint64_t len, int ascii){
  sl_string *str = sl_malloc(sizeof(sl_string));
  make_string_core(str, value, len, ascii);
  str->copy_on_write = 1;
  return str;
}
/*
  Convience function which assumes cstr isn't necessarly ascii, and
  uses strlen to find it's length
*/
SL_INLINE sl_string *sl_string_from_c_string(const char *cstr){
  sl_string *str = sl_malloc(sizeof(sl_string));
  make_string_core(str, cstr, strlen(cstr), 0);
  str->copy_on_write = 1;
  return str;
}
//this is to fix an undefined function error ,
//this isn't how this function should actually work (it should use utf8)
static sl_string* sl_char_to_string(sl_char c){
  return XSTRING(make_string((char*)&c, 1, 0));
}
//no optional arguments or anything like that
SL_INLINE sl_string *quick_substring(sl_string *s,
                                     int32_t start, int32_t end){
  uint32_t len = end-start;
  sl_string *sub = make_string_unboxed_nocopy(s->str+start,len,s->ascii);
  sub->copy_on_write = 1;
  return sub;  
}
SL_INLINE sl_string *quick_copy_string(sl_string *str){
  //make an empty string header
  sl_string *new_str = sl_malloc(sizeof(sl_string*));
  make_string_core(new_str, str->str, str->len, str->ascii);
  str->copy_on_write = new_str->copy_on_write = 1;
  return new_str;
}
  
//convience macro to make a sl_string from a c string constant
//assumes that the c string is made of only ascii characters
/*#define make_string_const(__str)              \
  make_string_nocopy(__str, sizeof(__str)-1, 1)*/
#define make_string_const_unboxed(__str)                                \
  make_string_unboxed_nocopy(__str, sizeof(__str)-1, 1)

#endif
