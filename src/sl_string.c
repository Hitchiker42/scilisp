#include "sl_string.h"
#include "vector.h"
int sl_string_equal(sl_string *str1, sl_string *str2){
  if(str1->len != str2->len){
    return 0;
  } else {
    return !memcmp(str1->str,str2->str, str1->len);
  }
}
sl_string *sl_concat_strings(uint64_t numargs, sl_string **args){
  /*Fairly simple, compute needed memory, allocate it, copy strings*/
  uint32_t i,len = 0;
  for(i=0;i<numargs;i++){
    len += args[i]->len;
  }
  char *new_str = sl_malloc_atomic(len*sizeof(char));
  char *strptr = new_str;
  for(i=0;i<numargs;i++){
    memcpy(strptr, args[i]->str, args[i]->len);
    strptr += args[i]->len;
  }
  sl_string *retval = make_string_unboxed_nocopy(new_str, len, 0);
  return retval;
}
DEFUN("copy-string",copy_string,1,1,"(copy-string str) return a copy of str")
  (sl_obj str){
  TYPECHECK_1(STRING, str, 0,
              make_string_const("Error, expected a string"));
  return PACK_STRING(quick_copy_string(XSTRING(str)));
}
DEFUN("substring",substring,1,3, "(substring str &optional start end) "
      "return a substring consisting of the characters str from start "
      "(inclusive) to end (exclusive). If end is not given the length of str "
      "is used as the end, if start and end aren't given just return str.")
  (sl_obj str_obj, sl_obj start, sl_obj end){
  if(NILP(start)){
    return Fcopy_string(str_obj);
  } else {
    TYPECHECK_1(STRING, str_obj, 0,
                make_string_const("Error, expected a string (in substring)"));
    TYPECHECK_1(INT, start, 0,
                make_string_const("Error, expected an integer"));
  }
  sl_string *str = XSTRING(str_obj);
  if(NILP(end)){
    end = str->len;
  } else {
    TYPECHECK_1(INT, end, 0,
                make_string_const("Error, expected an integer"));
  }
  int i,j;
  i = XINT(start);
  j = XINT(end);
  sl_string *new_str = quick_substring(str, i, (j-1));
  return PACK_STRING(new_str);
}
sl_obj subseq_search_string(sl_obj str, sl_obj substr){
  //Obviously there is a lot of complexity in doing substring searches
  //so leave the bulk of the work to libc, since it's going to be optimized
  void *haystack = XSTRING(str)->str;
  size_t haystack_len = XSTRING(str)->len;
  void *needle = XSTRING(substr)->str;
  size_t needle_len = XSTRING(substr)->len;

  //memmem is a gnu extension
  void *location = memmem(haystack, haystack_len, needle, needle_len);
  if(location != NULL){
    size_t pos = location - haystack;//position in bytes not characters
    return make_int(pos);
  } else {
    return NIL;
  }
}
sl_obj elt_string(sl_obj str){
  return utf8_nth_char(XSTRING(str)->str, XSTRING(str)->len);
}

/*
  Before I write these I should change substring to be more generic
*/
/*
  Need to add some support for this kind of string first

DEFUN("substring/shared",substring_shared,1,3,
      "(substring/shared str &optional start end) like substring but "
      "modifications in the new string show up in the old string, "
      "and vice/versa")
  (sl_obj str_obj, sl_obj start, sl_obj end){
  //not super fast, but much eaiser to write/maintain
  sl_obj new_str = Fsubstring(str_obj, start, end);
  XSTRING(new_str)->copy_on_write = 0;
*/
void init_string_symbols(void){
}
