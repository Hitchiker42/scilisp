	.file	"sl_math.c"
# GNU C (GCC) version 4.9.2 20141224 (prerelease) (x86_64-unknown-linux-gnu)
#	compiled by GNU C version 4.9.2 20141224 (prerelease), GMP version 6.0.0, MPFR version 3.1.2-p11, MPC version 1.0.2
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -D _GNU_SOURCE -D NDEBUG sl_math.c -mtune=generic
# -march=x86-64 -O2 -std=gnu99 -fverbose-asm
# options enabled:  -faggressive-loop-optimizations
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fcombine-stack-adjustments -fcommon -fcompare-elim
# -fcprop-registers -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffunction-cse -fgcse
# -fgcse-lm -fgnu-runtime -fgnu-unique -fguess-branch-probability
# -fhoist-adjacent-loads -fident -fif-conversion -fif-conversion2
# -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-cp
# -fipa-profile -fipa-pure-const -fipa-reference -fipa-sra
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -fmath-errno -fmerge-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-blocks-and-partition -freorder-functions -frerun-cse-after-loop
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-insns2 -fshow-column -fshrink-wrap
# -fsigned-zeros -fsplit-ivs-in-unroller -fsplit-wide-types
# -fstrict-aliasing -fstrict-overflow -fstrict-volatile-bitfields
# -fsync-libcalls -fthread-jumps -ftoplevel-reorder -ftrapping-math
# -ftree-bit-ccp -ftree-builtin-call-dce -ftree-ccp -ftree-ch
# -ftree-coalesce-vars -ftree-copy-prop -ftree-copyrename -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mtls-direct-seg-refs -mvzeroupper

	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
.LHOTB0:
	.type	sl_malloc.part.0, @function
sl_malloc.part.0:
.LFB137:
	.cfi_startproc
	pushq	%rax	#
	.cfi_def_cfa_offset 16
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE137:
	.size	sl_malloc.part.0, .-sl_malloc.part.0
.LCOLDE0:
.LHOTE0:
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.globl	Fash
	.type	Fash, @function
Fash:
.LFB105:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %rbp	# integer, D.12389
	movq	%rdi, %r12	# integer, D.12389
	andl	$3, %ebp	#, D.12389
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	andl	$7, %r12d	#, D.12389
	cmpq	$2, %rbp	#, D.12389
	movq	%rdi, %rbx	# integer, integer
	je	.L4	#,
	cmpq	$3, %r12	#, D.12389
	jne	.L5	#,
	movq	%rdi, %rax	# integer, D.12389
	andq	$-8, %rax	#, D.12389
	cmpw	$258, 8(%rax)	#, _15->type
	jne	.L5	#,
.L4:
	movq	%rsi, %rax	# count, D.12389
	movq	%rsi, %rdx	# count, D.12389
	andl	$3, %eax	#, D.12389
	andl	$7, %edx	#, D.12389
	cmpq	$2, %rax	#, D.12389
	jne	.L21	#,
	cmpq	$3, %rdx	#, D.12389
	je	.L22	#,
	sarq	$2, %rsi	#, sbits
.L10:
	cmpq	$3, %r12	#, D.12389
	je	.L23	#,
	cmpq	$2, %rbp	#, D.12389
	jne	.L12	#,
	movq	%rbx, %rdi	# integer, D.12388
	sarq	$2, %rdi	#, D.12388
.L15:
	call	int_shift	#
	leaq	2(,%rax,4), %rax	#, D.12387
.L14:
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	cmpq	$3, %rdx	#, D.12389
	jne	.L5	#,
	movq	%rsi, %rax	# count, D.12390
	andq	$-8, %rax	#, D.12390
	cmpw	$258, 8(%rax)	#, _24->type
	jne	.L5	#,
.L7:
	movq	(%rax), %rdi	# _28->D.9723.bigint,
	call	__gmpz_get_si	#
	movq	%rax, %rsi	#, sbits
	jmp	.L10	#
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rbx, %rax	# integer, D.12390
	andq	$-8, %rax	#, D.12390
	cmpw	$258, 8(%rax)	#, _39->type
	je	.L24	#,
.L12:
	shrq	$32, %rbx	#, D.12388
	movq	%rbx, %rdi	# D.12388, D.12388
	jmp	.L15	#
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%rax), %rdi	# _39->D.9723.bigint,
	call	bigint_shift	#
	movl	$16, %edi	#,
	movq	%rax, %rbx	#, retval
	call	GC_malloc	#
	testq	%rax, %rax	# tmp123
	je	.L25	#,
	movl	$258, %edx	#,
	movq	%rbx, (%rax)	# retval, new_number_65->D.9723.ptr
	movw	%dx, 8(%rax)	#, new_number_65->type
	orq	$3, %rax	#, D.12387
	jmp	.L14	#
.L5:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
.L22:
	movq	%rsi, %rax	# count, D.12390
	shrq	$32, %rsi	#, sbits
	andq	$-8, %rax	#, D.12390
	cmpw	$258, 8(%rax)	#, _46->type
	jne	.L10	#,
	jmp	.L7	#
.L25:
	call	sl_malloc.part.0	#
	.cfi_endproc
.LFE105:
	.size	Fash, .-Fash
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4,,15
	.globl	sl_add
	.type	sl_add, @function
sl_add:
.LFB106:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12399
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12399
	andl	$3, %eax	#, D.12399
	cmpq	$2, %rax	#, D.12399
	setne	%al	#, D.12398
	andl	$7, %edx	#, D.12399
	cmpq	$3, %rdx	#, D.12399
	je	.L36	#,
	testb	%al, %al	# D.12398
	jne	.L27	#,
.L36:
	movq	%rsi, %rdx	# y, D.12399
	movq	%rsi, %rcx	# y, D.12399
	andl	$3, %edx	#, D.12399
	cmpq	$2, %rdx	#, D.12399
	setne	%dl	#, D.12398
	andl	$7, %ecx	#, D.12399
	cmpq	$3, %rcx	#, D.12399
	je	.L29	#,
	testb	%dl, %dl	# D.12398
	jne	.L27	#,
.L29:
	testb	%al, %al	# D.12398
	je	.L30	#,
	movq	%rdi, %rax	# x, D.12399
	andq	$-8, %rax	#, D.12399
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	seta	%al	#, D.12398
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12398
	je	.L32	#,
.L33:
	movq	%rsi, %rdx	# y, D.12399
	andq	$-8, %rdx	#, D.12399
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	seta	%dl	#, D.12398
.L32:
	testb	%al, %al	# D.12398
	jne	.L27	#,
	testb	%dl, %dl	# D.12398
	jne	.L27	#,
	leal	(%rcx,%r8,4), %eax	#,
	movq	add_dispatch_table(,%rax,8), %rax	# add_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	testb	%dl, %dl	# D.12398
	jne	.L35	#,
	xorl	%eax, %eax	#
	movq	add_dispatch_table(,%rax,8), %rax	# add_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L33	#
.L27:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE106:
	.size	sl_add, .-sl_add
	.section	.text.unlikely
.LCOLDE2:
	.text
.LHOTE2:
	.section	.text.unlikely
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4,,15
	.globl	Fadd
	.type	Fadd, @function
Fadd:
.LFB124:
	.cfi_startproc
	xorl	%eax, %eax	# acc
	testq	%rdi, %rdi	# numargs
	je	.L55	#,
	cmpq	$1, %rdi	#, numargs
	movq	(%rsi), %rax	# *args_7(D), acc
	jbe	.L55	#,
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	movq	%rsi, %r12	# args, args
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %rbp	# numargs, numargs
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	$1, %ebx	#, ivtmp.115
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%r12,%rbx,8), %rsi	# MEM[base: args_7(D), index: ivtmp.115_22, step: 8, offset: 0B], MEM[base: args_7(D), index: ivtmp.115_22, step: 8, offset: 0B]
	movq	%rax, %rdi	# acc,
	addq	$1, %rbx	#, ivtmp.115
	call	sl_add	#
	cmpq	%rbp, %rbx	# numargs, ivtmp.115
	jne	.L49	#,
	popq	%rbx	#
	.cfi_restore 3
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_restore 6
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_restore 12
	.cfi_def_cfa_offset 8
.L55:
	rep ret
	.cfi_endproc
.LFE124:
	.size	Fadd, .-Fadd
	.section	.text.unlikely
.LCOLDE3:
	.text
.LHOTE3:
	.section	.text.unlikely
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4,,15
	.globl	Fadd1
	.type	Fadd1, @function
Fadd1:
.LFB128:
	.cfi_startproc
	movl	$6, %esi	#,
	jmp	sl_add	#
	.cfi_endproc
.LFE128:
	.size	Fadd1, .-Fadd1
	.section	.text.unlikely
.LCOLDE4:
	.text
.LHOTE4:
	.section	.text.unlikely
.LCOLDB5:
	.text
.LHOTB5:
	.p2align 4,,15
	.globl	sl_sub
	.type	sl_sub, @function
sl_sub:
.LFB107:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12424
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12424
	andl	$3, %eax	#, D.12424
	cmpq	$2, %rax	#, D.12424
	setne	%al	#, D.12423
	andl	$7, %edx	#, D.12424
	cmpq	$3, %rdx	#, D.12424
	je	.L67	#,
	testb	%al, %al	# D.12423
	jne	.L58	#,
.L67:
	movq	%rsi, %rdx	# y, D.12424
	movq	%rsi, %rcx	# y, D.12424
	andl	$3, %edx	#, D.12424
	cmpq	$2, %rdx	#, D.12424
	setne	%dl	#, D.12423
	andl	$7, %ecx	#, D.12424
	cmpq	$3, %rcx	#, D.12424
	je	.L60	#,
	testb	%dl, %dl	# D.12423
	jne	.L58	#,
.L60:
	testb	%al, %al	# D.12423
	je	.L61	#,
	movq	%rdi, %rax	# x, D.12424
	andq	$-8, %rax	#, D.12424
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	seta	%al	#, D.12423
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12423
	je	.L63	#,
.L64:
	movq	%rsi, %rdx	# y, D.12424
	andq	$-8, %rdx	#, D.12424
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	seta	%dl	#, D.12423
.L63:
	testb	%al, %al	# D.12423
	jne	.L58	#,
	testb	%dl, %dl	# D.12423
	jne	.L58	#,
	leal	(%rcx,%r8,4), %eax	#,
	movq	sub_dispatch_table(,%rax,8), %rax	# sub_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	testb	%dl, %dl	# D.12423
	jne	.L66	#,
	xorl	%eax, %eax	#
	movq	sub_dispatch_table(,%rax,8), %rax	# sub_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L64	#
.L58:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE107:
	.size	sl_sub, .-sl_sub
	.section	.text.unlikely
.LCOLDE5:
	.text
.LHOTE5:
	.section	.text.unlikely
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4,,15
	.globl	Fsub1
	.type	Fsub1, @function
Fsub1:
.LFB129:
	.cfi_startproc
	movl	$6, %esi	#,
	jmp	sl_sub	#
	.cfi_endproc
.LFE129:
	.size	Fsub1, .-Fsub1
	.section	.text.unlikely
.LCOLDE6:
	.text
.LHOTE6:
	.section	.text.unlikely
.LCOLDB7:
	.text
.LHOTB7:
	.p2align 4,,15
	.globl	Fsub
	.type	Fsub, @function
Fsub:
.LFB125:
	.cfi_startproc
	cmpq	$1, %rdi	#, numargs
	jbe	.L89	#,
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rsi, %r12	# args, args
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	(%rsi), %rax	# *args_8(D), acc
	movq	%rdi, %rbp	# numargs, numargs
	movl	$1, %ebx	#, ivtmp.133
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r12,%rbx,8), %rsi	# MEM[base: args_8(D), index: ivtmp.133_22, step: 8, offset: 0B], MEM[base: args_8(D), index: ivtmp.133_22, step: 8, offset: 0B]
	movq	%rax, %rdi	# acc,
	addq	$1, %rbx	#, ivtmp.133
	call	sl_sub	#
	cmpq	%rbp, %rbx	# numargs, ivtmp.133
	jne	.L82	#,
	popq	%rbx	#
	.cfi_restore 3
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_restore 6
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_restore 12
	.cfi_def_cfa_offset 8
.L88:
	rep ret
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%eax, %eax	# D.12450
	testq	%rdi, %rdi	# numargs
	je	.L88	#,
	movq	(%rsi), %rsi	# *args_8(D),
	movl	$2, %edi	#,
	jmp	sl_sub	#
	.cfi_endproc
.LFE125:
	.size	Fsub, .-Fsub
	.section	.text.unlikely
.LCOLDE7:
	.text
.LHOTE7:
	.section	.text.unlikely
.LCOLDB8:
	.text
.LHOTB8:
	.p2align 4,,15
	.globl	sl_mul
	.type	sl_mul, @function
sl_mul:
.LFB108:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12455
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12455
	andl	$3, %eax	#, D.12455
	cmpq	$2, %rax	#, D.12455
	setne	%al	#, D.12454
	andl	$7, %edx	#, D.12455
	cmpq	$3, %rdx	#, D.12455
	je	.L100	#,
	testb	%al, %al	# D.12454
	jne	.L91	#,
.L100:
	movq	%rsi, %rdx	# y, D.12455
	movq	%rsi, %rcx	# y, D.12455
	andl	$3, %edx	#, D.12455
	cmpq	$2, %rdx	#, D.12455
	setne	%dl	#, D.12454
	andl	$7, %ecx	#, D.12455
	cmpq	$3, %rcx	#, D.12455
	je	.L93	#,
	testb	%dl, %dl	# D.12454
	jne	.L91	#,
.L93:
	testb	%al, %al	# D.12454
	je	.L94	#,
	movq	%rdi, %rax	# x, D.12455
	andq	$-8, %rax	#, D.12455
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	seta	%al	#, D.12454
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12454
	je	.L96	#,
.L97:
	movq	%rsi, %rdx	# y, D.12455
	andq	$-8, %rdx	#, D.12455
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	seta	%dl	#, D.12454
.L96:
	testb	%al, %al	# D.12454
	jne	.L91	#,
	testb	%dl, %dl	# D.12454
	jne	.L91	#,
	leal	(%rcx,%r8,4), %eax	#,
	movq	mul_dispatch_table(,%rax,8), %rax	# mul_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	testb	%dl, %dl	# D.12454
	jne	.L99	#,
	xorl	%eax, %eax	#
	movq	mul_dispatch_table(,%rax,8), %rax	# mul_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L97	#
.L91:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE108:
	.size	sl_mul, .-sl_mul
	.section	.text.unlikely
.LCOLDE8:
	.text
.LHOTE8:
	.section	.text.unlikely
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4,,15
	.globl	Fmul
	.type	Fmul, @function
Fmul:
.LFB126:
	.cfi_startproc
	xorl	%eax, %eax	# acc
	testq	%rdi, %rdi	# numargs
	je	.L119	#,
	cmpq	$1, %rdi	#, numargs
	movq	(%rsi), %rax	# *args_7(D), acc
	jbe	.L119	#,
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	movq	%rsi, %r12	# args, args
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %rbp	# numargs, numargs
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	$1, %ebx	#, ivtmp.146
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%r12,%rbx,8), %rsi	# MEM[base: args_7(D), index: ivtmp.146_22, step: 8, offset: 0B], MEM[base: args_7(D), index: ivtmp.146_22, step: 8, offset: 0B]
	movq	%rax, %rdi	# acc,
	addq	$1, %rbx	#, ivtmp.146
	call	sl_mul	#
	cmpq	%rbp, %rbx	# numargs, ivtmp.146
	jne	.L113	#,
	popq	%rbx	#
	.cfi_restore 3
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_restore 6
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_restore 12
	.cfi_def_cfa_offset 8
.L119:
	rep ret
	.cfi_endproc
.LFE126:
	.size	Fmul, .-Fmul
	.section	.text.unlikely
.LCOLDE9:
	.text
.LHOTE9:
	.section	.text.unlikely
.LCOLDB10:
	.text
.LHOTB10:
	.p2align 4,,15
	.globl	sl_div
	.type	sl_div, @function
sl_div:
.LFB109:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12476
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12476
	andl	$3, %eax	#, D.12476
	cmpq	$2, %rax	#, D.12476
	setne	%al	#, D.12475
	andl	$7, %edx	#, D.12476
	cmpq	$3, %rdx	#, D.12476
	je	.L130	#,
	testb	%al, %al	# D.12475
	jne	.L121	#,
.L130:
	movq	%rsi, %rdx	# y, D.12476
	movq	%rsi, %rcx	# y, D.12476
	andl	$3, %edx	#, D.12476
	cmpq	$2, %rdx	#, D.12476
	setne	%dl	#, D.12475
	andl	$7, %ecx	#, D.12476
	cmpq	$3, %rcx	#, D.12476
	je	.L123	#,
	testb	%dl, %dl	# D.12475
	jne	.L121	#,
.L123:
	testb	%al, %al	# D.12475
	je	.L124	#,
	movq	%rdi, %rax	# x, D.12476
	andq	$-8, %rax	#, D.12476
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	seta	%al	#, D.12475
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12475
	je	.L126	#,
.L127:
	movq	%rsi, %rdx	# y, D.12476
	andq	$-8, %rdx	#, D.12476
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	seta	%dl	#, D.12475
.L126:
	testb	%al, %al	# D.12475
	jne	.L121	#,
	testb	%dl, %dl	# D.12475
	jne	.L121	#,
	leal	(%rcx,%r8,4), %eax	#,
	movq	div_dispatch_table(,%rax,8), %rax	# div_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	testb	%dl, %dl	# D.12475
	jne	.L129	#,
	xorl	%eax, %eax	#
	movq	div_dispatch_table(,%rax,8), %rax	# div_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L127	#
.L121:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE109:
	.size	sl_div, .-sl_div
	.section	.text.unlikely
.LCOLDE10:
	.text
.LHOTE10:
	.section	.text.unlikely
.LCOLDB11:
	.text
.LHOTB11:
	.p2align 4,,15
	.globl	Fdiv
	.type	Fdiv, @function
Fdiv:
.LFB127:
	.cfi_startproc
	cmpq	$1, %rdi	#, numargs
	jbe	.L151	#,
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rsi, %r12	# args, args
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	(%rsi), %rax	# *args_8(D), acc
	movq	%rdi, %rbp	# numargs, numargs
	movl	$1, %ebx	#, ivtmp.159
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%r12,%rbx,8), %rsi	# MEM[base: args_8(D), index: ivtmp.159_22, step: 8, offset: 0B], MEM[base: args_8(D), index: ivtmp.159_22, step: 8, offset: 0B]
	movq	%rax, %rdi	# acc,
	addq	$1, %rbx	#, ivtmp.159
	call	sl_div	#
	cmpq	%rbp, %rbx	# numargs, ivtmp.159
	jne	.L144	#,
	popq	%rbx	#
	.cfi_restore 3
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_restore 6
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_restore 12
	.cfi_def_cfa_offset 8
.L150:
	rep ret
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%eax, %eax	# D.12499
	testq	%rdi, %rdi	# numargs
	je	.L150	#,
	movq	(%rsi), %rsi	# *args_8(D),
	movl	$6, %edi	#,
	jmp	sl_div	#
	.cfi_endproc
.LFE127:
	.size	Fdiv, .-Fdiv
	.section	.text.unlikely
.LCOLDE11:
	.text
.LHOTE11:
	.section	.text.unlikely
.LCOLDB12:
	.text
.LHOTB12:
	.p2align 4,,15
	.globl	sl_num_eq
	.type	sl_num_eq, @function
sl_num_eq:
.LFB110:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12504
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12504
	andl	$3, %eax	#, D.12504
	cmpq	$2, %rax	#, D.12504
	setne	%al	#, D.12503
	andl	$7, %edx	#, D.12504
	cmpq	$3, %rdx	#, D.12504
	je	.L162	#,
	testb	%al, %al	# D.12503
	jne	.L153	#,
.L162:
	movq	%rsi, %rdx	# y, D.12504
	movq	%rsi, %rcx	# y, D.12504
	andl	$3, %edx	#, D.12504
	cmpq	$2, %rdx	#, D.12504
	setne	%dl	#, D.12503
	andl	$7, %ecx	#, D.12504
	cmpq	$3, %rcx	#, D.12504
	je	.L155	#,
	testb	%dl, %dl	# D.12503
	jne	.L153	#,
.L155:
	testb	%al, %al	# D.12503
	je	.L156	#,
	movq	%rdi, %rax	# x, D.12504
	andq	$-8, %rax	#, D.12504
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	setg	%al	#, D.12503
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12503
	je	.L158	#,
.L159:
	movq	%rsi, %rdx	# y, D.12504
	andq	$-8, %rdx	#, D.12504
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	setg	%dl	#, D.12503
.L158:
	testb	%al, %al	# D.12503
	jne	.L153	#,
	testb	%dl, %dl	# D.12503
	jne	.L153	#,
	leal	(%rcx,%r8,4), %eax	#, D.12509
	cltq
	movq	eq_dispatch_table(,%rax,8), %rax	# eq_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	testb	%dl, %dl	# D.12503
	jne	.L161	#,
	xorl	%eax, %eax	# D.12509
	cltq
	movq	eq_dispatch_table(,%rax,8), %rax	# eq_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L159	#
.L153:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE110:
	.size	sl_num_eq, .-sl_num_eq
	.section	.text.unlikely
.LCOLDE12:
	.text
.LHOTE12:
	.section	.text.unlikely
.LCOLDB13:
	.text
.LHOTB13:
	.p2align 4,,15
	.globl	Fnum_eq
	.type	Fnum_eq, @function
Fnum_eq:
.LFB130:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12	# numargs, numargs
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp	# args, args
	movl	$1, %ebx	#, ivtmp.171
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
	movq	(%rsi), %r13	# *args_6(D), num
	jmp	.L174	#
	.p2align 4,,10
	.p2align 3
.L176:
	movq	0(%rbp,%rbx,8), %rsi	# MEM[base: args_6(D), index: ivtmp.171_19, step: 8, offset: 0B], MEM[base: args_6(D), index: ivtmp.171_19, step: 8, offset: 0B]
	movq	%r13, %rdi	# num,
	addq	$1, %rbx	#, ivtmp.171
	call	sl_num_eq	#
	testq	%rax, %rax	# D.12521
	je	.L179	#,
.L174:
	cmpq	%rbx, %r12	# ivtmp.171, numargs
	ja	.L176	#,
	movq	Qt(%rip), %rax	# Qt, D.12520
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	Qnil(%rip), %rax	# Qnil, D.12520
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE130:
	.size	Fnum_eq, .-Fnum_eq
	.section	.text.unlikely
.LCOLDE13:
	.text
.LHOTE13:
	.section	.text.unlikely
.LCOLDB14:
	.text
.LHOTB14:
	.p2align 4,,15
	.globl	sl_num_ne
	.type	sl_num_ne, @function
sl_num_ne:
.LFB111:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12525
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12525
	andl	$3, %eax	#, D.12525
	cmpq	$2, %rax	#, D.12525
	setne	%al	#, D.12524
	andl	$7, %edx	#, D.12525
	cmpq	$3, %rdx	#, D.12525
	je	.L190	#,
	testb	%al, %al	# D.12524
	jne	.L181	#,
.L190:
	movq	%rsi, %rdx	# y, D.12525
	movq	%rsi, %rcx	# y, D.12525
	andl	$3, %edx	#, D.12525
	cmpq	$2, %rdx	#, D.12525
	setne	%dl	#, D.12524
	andl	$7, %ecx	#, D.12525
	cmpq	$3, %rcx	#, D.12525
	je	.L183	#,
	testb	%dl, %dl	# D.12524
	jne	.L181	#,
.L183:
	testb	%al, %al	# D.12524
	je	.L184	#,
	movq	%rdi, %rax	# x, D.12525
	andq	$-8, %rax	#, D.12525
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	setg	%al	#, D.12524
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12524
	je	.L186	#,
.L187:
	movq	%rsi, %rdx	# y, D.12525
	andq	$-8, %rdx	#, D.12525
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	setg	%dl	#, D.12524
.L186:
	testb	%al, %al	# D.12524
	jne	.L181	#,
	testb	%dl, %dl	# D.12524
	jne	.L181	#,
	leal	(%rcx,%r8,4), %eax	#, D.12530
	cltq
	movq	ne_dispatch_table(,%rax,8), %rax	# ne_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	testb	%dl, %dl	# D.12524
	jne	.L189	#,
	xorl	%eax, %eax	# D.12530
	cltq
	movq	ne_dispatch_table(,%rax,8), %rax	# ne_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L187	#
.L181:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE111:
	.size	sl_num_ne, .-sl_num_ne
	.section	.text.unlikely
.LCOLDE14:
	.text
.LHOTE14:
	.section	.text.unlikely
.LCOLDB15:
	.text
.LHOTB15:
	.p2align 4,,15
	.globl	sl_num_lt
	.type	sl_num_lt, @function
sl_num_lt:
.LFB112:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12534
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12534
	andl	$3, %eax	#, D.12534
	cmpq	$2, %rax	#, D.12534
	setne	%al	#, D.12533
	andl	$7, %edx	#, D.12534
	cmpq	$3, %rdx	#, D.12534
	je	.L211	#,
	testb	%al, %al	# D.12533
	jne	.L202	#,
.L211:
	movq	%rsi, %rdx	# y, D.12534
	movq	%rsi, %rcx	# y, D.12534
	andl	$3, %edx	#, D.12534
	cmpq	$2, %rdx	#, D.12534
	setne	%dl	#, D.12533
	andl	$7, %ecx	#, D.12534
	cmpq	$3, %rcx	#, D.12534
	je	.L204	#,
	testb	%dl, %dl	# D.12533
	jne	.L202	#,
.L204:
	testb	%al, %al	# D.12533
	je	.L205	#,
	movq	%rdi, %rax	# x, D.12534
	andq	$-8, %rax	#, D.12534
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	setg	%al	#, D.12533
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12533
	je	.L207	#,
.L208:
	movq	%rsi, %rdx	# y, D.12534
	andq	$-8, %rdx	#, D.12534
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	setg	%dl	#, D.12533
.L207:
	testb	%al, %al	# D.12533
	jne	.L202	#,
	testb	%dl, %dl	# D.12533
	jne	.L202	#,
	leal	(%rcx,%r8,4), %eax	#, D.12539
	cltq
	movq	lt_dispatch_table(,%rax,8), %rax	# lt_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	testb	%dl, %dl	# D.12533
	jne	.L210	#,
	xorl	%eax, %eax	# D.12539
	cltq
	movq	lt_dispatch_table(,%rax,8), %rax	# lt_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L208	#
.L202:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE112:
	.size	sl_num_lt, .-sl_num_lt
	.section	.text.unlikely
.LCOLDE15:
	.text
.LHOTE15:
	.section	.text.unlikely
.LCOLDB16:
	.text
.LHOTB16:
	.p2align 4,,15
	.globl	sl_num_le
	.type	sl_num_le, @function
sl_num_le:
.LFB113:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12543
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12543
	andl	$3, %eax	#, D.12543
	cmpq	$2, %rax	#, D.12543
	setne	%al	#, D.12542
	andl	$7, %edx	#, D.12543
	cmpq	$3, %rdx	#, D.12543
	je	.L232	#,
	testb	%al, %al	# D.12542
	jne	.L223	#,
.L232:
	movq	%rsi, %rdx	# y, D.12543
	movq	%rsi, %rcx	# y, D.12543
	andl	$3, %edx	#, D.12543
	cmpq	$2, %rdx	#, D.12543
	setne	%dl	#, D.12542
	andl	$7, %ecx	#, D.12543
	cmpq	$3, %rcx	#, D.12543
	je	.L225	#,
	testb	%dl, %dl	# D.12542
	jne	.L223	#,
.L225:
	testb	%al, %al	# D.12542
	je	.L226	#,
	movq	%rdi, %rax	# x, D.12543
	andq	$-8, %rax	#, D.12543
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	setg	%al	#, D.12542
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12542
	je	.L228	#,
.L229:
	movq	%rsi, %rdx	# y, D.12543
	andq	$-8, %rdx	#, D.12543
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	setg	%dl	#, D.12542
.L228:
	testb	%al, %al	# D.12542
	jne	.L223	#,
	testb	%dl, %dl	# D.12542
	jne	.L223	#,
	leal	(%rcx,%r8,4), %eax	#, D.12548
	cltq
	movq	le_dispatch_table(,%rax,8), %rax	# le_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	testb	%dl, %dl	# D.12542
	jne	.L231	#,
	xorl	%eax, %eax	# D.12548
	cltq
	movq	le_dispatch_table(,%rax,8), %rax	# le_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L229	#
.L223:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE113:
	.size	sl_num_le, .-sl_num_le
	.section	.text.unlikely
.LCOLDE16:
	.text
.LHOTE16:
	.section	.text.unlikely
.LCOLDB17:
	.text
.LHOTB17:
	.p2align 4,,15
	.globl	sl_num_gt
	.type	sl_num_gt, @function
sl_num_gt:
.LFB114:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12552
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12552
	andl	$3, %eax	#, D.12552
	cmpq	$2, %rax	#, D.12552
	setne	%al	#, D.12551
	andl	$7, %edx	#, D.12552
	cmpq	$3, %rdx	#, D.12552
	je	.L253	#,
	testb	%al, %al	# D.12551
	jne	.L244	#,
.L253:
	movq	%rsi, %rdx	# y, D.12552
	movq	%rsi, %rcx	# y, D.12552
	andl	$3, %edx	#, D.12552
	cmpq	$2, %rdx	#, D.12552
	setne	%dl	#, D.12551
	andl	$7, %ecx	#, D.12552
	cmpq	$3, %rcx	#, D.12552
	je	.L246	#,
	testb	%dl, %dl	# D.12551
	jne	.L244	#,
.L246:
	testb	%al, %al	# D.12551
	je	.L247	#,
	movq	%rdi, %rax	# x, D.12552
	andq	$-8, %rax	#, D.12552
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	setg	%al	#, D.12551
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12551
	je	.L249	#,
.L250:
	movq	%rsi, %rdx	# y, D.12552
	andq	$-8, %rdx	#, D.12552
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	setg	%dl	#, D.12551
.L249:
	testb	%al, %al	# D.12551
	jne	.L244	#,
	testb	%dl, %dl	# D.12551
	jne	.L244	#,
	leal	(%rcx,%r8,4), %eax	#, D.12557
	cltq
	movq	gt_dispatch_table(,%rax,8), %rax	# gt_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	testb	%dl, %dl	# D.12551
	jne	.L252	#,
	xorl	%eax, %eax	# D.12557
	cltq
	movq	gt_dispatch_table(,%rax,8), %rax	# gt_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L250	#
.L244:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE114:
	.size	sl_num_gt, .-sl_num_gt
	.section	.text.unlikely
.LCOLDE17:
	.text
.LHOTE17:
	.section	.text.unlikely
.LCOLDB18:
	.text
.LHOTB18:
	.p2align 4,,15
	.globl	sl_num_ge
	.type	sl_num_ge, @function
sl_num_ge:
.LFB115:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12561
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	movq	%rdi, %rdx	# x, D.12561
	andl	$3, %eax	#, D.12561
	cmpq	$2, %rax	#, D.12561
	setne	%al	#, D.12560
	andl	$7, %edx	#, D.12561
	cmpq	$3, %rdx	#, D.12561
	je	.L274	#,
	testb	%al, %al	# D.12560
	jne	.L265	#,
.L274:
	movq	%rsi, %rdx	# y, D.12561
	movq	%rsi, %rcx	# y, D.12561
	andl	$3, %edx	#, D.12561
	cmpq	$2, %rdx	#, D.12561
	setne	%dl	#, D.12560
	andl	$7, %ecx	#, D.12561
	cmpq	$3, %rcx	#, D.12561
	je	.L267	#,
	testb	%dl, %dl	# D.12560
	jne	.L265	#,
.L267:
	testb	%al, %al	# D.12560
	je	.L268	#,
	movq	%rdi, %rax	# x, D.12561
	andq	$-8, %rax	#, D.12561
	movzwl	8(%rax), %r8d	# _14->type, _14->type
	andl	$15, %r8d	#, type1
	cmpl	$4, %r8d	#, type1
	setg	%al	#, D.12560
	xorl	%ecx, %ecx	# type2
	testb	%dl, %dl	# D.12560
	je	.L270	#,
.L271:
	movq	%rsi, %rdx	# y, D.12561
	andq	$-8, %rdx	#, D.12561
	movzwl	8(%rdx), %ecx	# _19->type, _19->type
	andl	$15, %ecx	#, type2
	cmpl	$4, %ecx	#, type2
	setg	%dl	#, D.12560
.L270:
	testb	%al, %al	# D.12560
	jne	.L265	#,
	testb	%dl, %dl	# D.12560
	jne	.L265	#,
	leal	(%rcx,%r8,4), %eax	#, D.12566
	cltq
	movq	ge_dispatch_table(,%rax,8), %rax	# ge_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	testb	%dl, %dl	# D.12560
	jne	.L273	#,
	xorl	%eax, %eax	# D.12566
	cltq
	movq	ge_dispatch_table(,%rax,8), %rax	# ge_dispatch_table, tmp133
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp133
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	xorl	%r8d, %r8d	# type1
	jmp	.L271	#
.L265:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.cfi_endproc
.LFE115:
	.size	sl_num_ge, .-sl_num_ge
	.section	.text.unlikely
.LCOLDE18:
	.text
.LHOTE18:
	.section	.text.unlikely
.LCOLDB19:
	.text
.LHOTB19:
	.p2align 4,,15
	.globl	sl_sin
	.type	sl_sin, @function
sl_sin:
.LFB116:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12569
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	andl	$3, %eax	#, D.12569
	cmpq	$2, %rax	#, D.12569
	je	.L289	#,
	movq	%rdi, %rax	# x, D.12569
	andl	$7, %eax	#, D.12569
	cmpq	$3, %rax	#, D.12569
	jne	.L288	#,
	movq	%rdi, %rax	# x, D.12569
	andq	$-8, %rax	#, D.12569
	movzwl	8(%rax), %edx	# _10->type, _10->type
	andl	$15, %edx	#, D.12571
	cmpw	$4, %dx	#, D.12571
	movzwl	%dx, %eax	# D.12571,
	jbe	.L286	#,
.L288:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.p2align 4,,10
	.p2align 3
.L289:
	xorl	%eax, %eax	#
.L286:
	movq	sin_dispatch_table(,%rax,8), %rax	# sin_dispatch_table, tmp100
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp100
	.cfi_endproc
.LFE116:
	.size	sl_sin, .-sl_sin
	.section	.text.unlikely
.LCOLDE19:
	.text
.LHOTE19:
	.section	.text.unlikely
.LCOLDB20:
	.text
.LHOTB20:
	.p2align 4,,15
	.globl	sl_cos
	.type	sl_cos, @function
sl_cos:
.LFB117:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12576
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	andl	$3, %eax	#, D.12576
	cmpq	$2, %rax	#, D.12576
	je	.L295	#,
	movq	%rdi, %rax	# x, D.12576
	andl	$7, %eax	#, D.12576
	cmpq	$3, %rax	#, D.12576
	jne	.L294	#,
	movq	%rdi, %rax	# x, D.12576
	andq	$-8, %rax	#, D.12576
	movzwl	8(%rax), %edx	# _10->type, _10->type
	andl	$15, %edx	#, D.12578
	cmpw	$4, %dx	#, D.12578
	movzwl	%dx, %eax	# D.12578,
	jbe	.L292	#,
.L294:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.p2align 4,,10
	.p2align 3
.L295:
	xorl	%eax, %eax	#
.L292:
	movq	cos_dispatch_table(,%rax,8), %rax	# cos_dispatch_table, tmp100
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp100
	.cfi_endproc
.LFE117:
	.size	sl_cos, .-sl_cos
	.section	.text.unlikely
.LCOLDE20:
	.text
.LHOTE20:
	.section	.text.unlikely
.LCOLDB21:
	.text
.LHOTB21:
	.p2align 4,,15
	.globl	sl_tan
	.type	sl_tan, @function
sl_tan:
.LFB118:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12583
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	andl	$3, %eax	#, D.12583
	cmpq	$2, %rax	#, D.12583
	je	.L301	#,
	movq	%rdi, %rax	# x, D.12583
	andl	$7, %eax	#, D.12583
	cmpq	$3, %rax	#, D.12583
	jne	.L300	#,
	movq	%rdi, %rax	# x, D.12583
	andq	$-8, %rax	#, D.12583
	movzwl	8(%rax), %edx	# _10->type, _10->type
	andl	$15, %edx	#, D.12585
	cmpw	$4, %dx	#, D.12585
	movzwl	%dx, %eax	# D.12585,
	jbe	.L298	#,
.L300:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.p2align 4,,10
	.p2align 3
.L301:
	xorl	%eax, %eax	#
.L298:
	movq	tan_dispatch_table(,%rax,8), %rax	# tan_dispatch_table, tmp100
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp100
	.cfi_endproc
.LFE118:
	.size	sl_tan, .-sl_tan
	.section	.text.unlikely
.LCOLDE21:
	.text
.LHOTE21:
	.section	.text.unlikely
.LCOLDB22:
	.text
.LHOTB22:
	.p2align 4,,15
	.globl	sl_log
	.type	sl_log, @function
sl_log:
.LFB119:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12590
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	andl	$3, %eax	#, D.12590
	cmpq	$2, %rax	#, D.12590
	je	.L307	#,
	movq	%rdi, %rax	# x, D.12590
	andl	$7, %eax	#, D.12590
	cmpq	$3, %rax	#, D.12590
	jne	.L306	#,
	movq	%rdi, %rax	# x, D.12590
	andq	$-8, %rax	#, D.12590
	movzwl	8(%rax), %edx	# _10->type, _10->type
	andl	$15, %edx	#, D.12592
	cmpw	$4, %dx	#, D.12592
	movzwl	%dx, %eax	# D.12592,
	jbe	.L304	#,
.L306:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.p2align 4,,10
	.p2align 3
.L307:
	xorl	%eax, %eax	#
.L304:
	movq	log_dispatch_table(,%rax,8), %rax	# log_dispatch_table, tmp100
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp100
	.cfi_endproc
.LFE119:
	.size	sl_log, .-sl_log
	.section	.text.unlikely
.LCOLDE22:
	.text
.LHOTE22:
	.section	.text.unlikely
.LCOLDB23:
	.text
.LHOTB23:
	.p2align 4,,15
	.globl	sl_log10
	.type	sl_log10, @function
sl_log10:
.LFB120:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12597
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	andl	$3, %eax	#, D.12597
	cmpq	$2, %rax	#, D.12597
	je	.L313	#,
	movq	%rdi, %rax	# x, D.12597
	andl	$7, %eax	#, D.12597
	cmpq	$3, %rax	#, D.12597
	jne	.L312	#,
	movq	%rdi, %rax	# x, D.12597
	andq	$-8, %rax	#, D.12597
	movzwl	8(%rax), %edx	# _10->type, _10->type
	andl	$15, %edx	#, D.12599
	cmpw	$4, %dx	#, D.12599
	movzwl	%dx, %eax	# D.12599,
	jbe	.L310	#,
.L312:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.p2align 4,,10
	.p2align 3
.L313:
	xorl	%eax, %eax	#
.L310:
	movq	log10_dispatch_table(,%rax,8), %rax	# log10_dispatch_table, tmp100
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp100
	.cfi_endproc
.LFE120:
	.size	sl_log10, .-sl_log10
	.section	.text.unlikely
.LCOLDE23:
	.text
.LHOTE23:
	.section	.text.unlikely
.LCOLDB24:
	.text
.LHOTB24:
	.p2align 4,,15
	.globl	sl_log2
	.type	sl_log2, @function
sl_log2:
.LFB121:
	.cfi_startproc
	movq	%rdi, %rax	# x, D.12604
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
	andl	$3, %eax	#, D.12604
	cmpq	$2, %rax	#, D.12604
	je	.L319	#,
	movq	%rdi, %rax	# x, D.12604
	andl	$7, %eax	#, D.12604
	cmpq	$3, %rax	#, D.12604
	jne	.L318	#,
	movq	%rdi, %rax	# x, D.12604
	andq	$-8, %rax	#, D.12604
	movzwl	8(%rax), %edx	# _10->type, _10->type
	andl	$15, %edx	#, D.12606
	cmpw	$4, %dx	#, D.12606
	movzwl	%dx, %eax	# D.12606,
	jbe	.L316	#,
.L318:
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	call	raise_error	#
	.p2align 4,,10
	.p2align 3
.L319:
	xorl	%eax, %eax	#
.L316:
	movq	log2_dispatch_table(,%rax,8), %rax	# log2_dispatch_table, tmp100
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 8
	jmp	*%rax	# tmp100
	.cfi_endproc
.LFE121:
	.size	sl_log2, .-sl_log2
	.section	.text.unlikely
.LCOLDE24:
	.text
.LHOTE24:
	.section	.text.unlikely
.LCOLDB25:
	.text
.LHOTB25:
	.p2align 4,,15
	.globl	sl_neg
	.type	sl_neg, @function
sl_neg:
.LFB122:
	.cfi_startproc
	movq	%rdi, %rsi	# x, x
	movl	$2, %edi	#,
	jmp	sl_sub	#
	.cfi_endproc
.LFE122:
	.size	sl_neg, .-sl_neg
	.section	.text.unlikely
.LCOLDE25:
	.text
.LHOTE25:
	.section	.text.unlikely
.LCOLDB26:
	.text
.LHOTB26:
	.p2align 4,,15
	.globl	sl_recip
	.type	sl_recip, @function
sl_recip:
.LFB123:
	.cfi_startproc
	movq	%rdi, %rsi	# x, x
	movl	$6, %edi	#,
	jmp	sl_div	#
	.cfi_endproc
.LFE123:
	.size	sl_recip, .-sl_recip
	.section	.text.unlikely
.LCOLDE26:
	.text
.LHOTE26:
	.section	.text.unlikely
.LCOLDB27:
	.text
.LHOTB27:
	.p2align 4,,15
	.globl	Fnum_ne
	.type	Fnum_ne, @function
Fnum_ne:
.LFB131:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12	# numargs, numargs
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp	# args, args
	movl	$1, %ebx	#, ivtmp.207
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
	movq	(%rsi), %r13	# *args_6(D), num
	jmp	.L324	#
	.p2align 4,,10
	.p2align 3
.L326:
	movq	0(%rbp,%rbx,8), %rsi	# MEM[base: args_6(D), index: ivtmp.207_19, step: 8, offset: 0B], MEM[base: args_6(D), index: ivtmp.207_19, step: 8, offset: 0B]
	movq	%r13, %rdi	# num,
	addq	$1, %rbx	#, ivtmp.207
	call	sl_num_ne	#
	testq	%rax, %rax	# D.12626
	je	.L329	#,
.L324:
	cmpq	%rbx, %r12	# ivtmp.207, numargs
	ja	.L326	#,
	movq	Qt(%rip), %rax	# Qt, D.12625
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movq	Qnil(%rip), %rax	# Qnil, D.12625
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE131:
	.size	Fnum_ne, .-Fnum_ne
	.section	.text.unlikely
.LCOLDE27:
	.text
.LHOTE27:
	.section	.text.unlikely
.LCOLDB28:
	.text
.LHOTB28:
	.p2align 4,,15
	.globl	Fnum_gt
	.type	Fnum_gt, @function
Fnum_gt:
.LFB132:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12	# numargs, numargs
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp	# args, args
	movl	$1, %ebx	#, ivtmp.217
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
	movq	(%rsi), %r13	# *args_6(D), num
	jmp	.L331	#
	.p2align 4,,10
	.p2align 3
.L333:
	movq	0(%rbp,%rbx,8), %rsi	# MEM[base: args_6(D), index: ivtmp.217_19, step: 8, offset: 0B], MEM[base: args_6(D), index: ivtmp.217_19, step: 8, offset: 0B]
	movq	%r13, %rdi	# num,
	addq	$1, %rbx	#, ivtmp.217
	call	sl_num_gt	#
	testq	%rax, %rax	# D.12638
	je	.L336	#,
.L331:
	cmpq	%rbx, %r12	# ivtmp.217, numargs
	ja	.L333	#,
	movq	Qt(%rip), %rax	# Qt, D.12637
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movq	Qnil(%rip), %rax	# Qnil, D.12637
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE132:
	.size	Fnum_gt, .-Fnum_gt
	.section	.text.unlikely
.LCOLDE28:
	.text
.LHOTE28:
	.section	.text.unlikely
.LCOLDB29:
	.text
.LHOTB29:
	.p2align 4,,15
	.globl	Fnum_lt
	.type	Fnum_lt, @function
Fnum_lt:
.LFB133:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12	# numargs, numargs
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp	# args, args
	movl	$1, %ebx	#, ivtmp.227
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
	movq	(%rsi), %r13	# *args_6(D), num
	jmp	.L338	#
	.p2align 4,,10
	.p2align 3
.L340:
	movq	0(%rbp,%rbx,8), %rsi	# MEM[base: args_6(D), index: ivtmp.227_19, step: 8, offset: 0B], MEM[base: args_6(D), index: ivtmp.227_19, step: 8, offset: 0B]
	movq	%r13, %rdi	# num,
	addq	$1, %rbx	#, ivtmp.227
	call	sl_num_lt	#
	testq	%rax, %rax	# D.12650
	je	.L343	#,
.L338:
	cmpq	%rbx, %r12	# ivtmp.227, numargs
	ja	.L340	#,
	movq	Qt(%rip), %rax	# Qt, D.12649
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movq	Qnil(%rip), %rax	# Qnil, D.12649
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE133:
	.size	Fnum_lt, .-Fnum_lt
	.section	.text.unlikely
.LCOLDE29:
	.text
.LHOTE29:
	.section	.text.unlikely
.LCOLDB30:
	.text
.LHOTB30:
	.p2align 4,,15
	.globl	Fnum_ge
	.type	Fnum_ge, @function
Fnum_ge:
.LFB134:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12	# numargs, numargs
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp	# args, args
	movl	$1, %ebx	#, ivtmp.237
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
	movq	(%rsi), %r13	# *args_6(D), num
	jmp	.L345	#
	.p2align 4,,10
	.p2align 3
.L347:
	movq	0(%rbp,%rbx,8), %rsi	# MEM[base: args_6(D), index: ivtmp.237_19, step: 8, offset: 0B], MEM[base: args_6(D), index: ivtmp.237_19, step: 8, offset: 0B]
	movq	%r13, %rdi	# num,
	addq	$1, %rbx	#, ivtmp.237
	call	sl_num_ge	#
	testq	%rax, %rax	# D.12662
	je	.L350	#,
.L345:
	cmpq	%rbx, %r12	# ivtmp.237, numargs
	ja	.L347	#,
	movq	Qt(%rip), %rax	# Qt, D.12661
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movq	Qnil(%rip), %rax	# Qnil, D.12661
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE134:
	.size	Fnum_ge, .-Fnum_ge
	.section	.text.unlikely
.LCOLDE30:
	.text
.LHOTE30:
	.section	.text.unlikely
.LCOLDB31:
	.text
.LHOTB31:
	.p2align 4,,15
	.globl	Fnum_le
	.type	Fnum_le, @function
Fnum_le:
.LFB135:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12	# numargs, numargs
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp	# args, args
	movl	$1, %ebx	#, ivtmp.247
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
	movq	(%rsi), %r13	# *args_6(D), num
	jmp	.L352	#
	.p2align 4,,10
	.p2align 3
.L354:
	movq	0(%rbp,%rbx,8), %rsi	# MEM[base: args_6(D), index: ivtmp.247_19, step: 8, offset: 0B], MEM[base: args_6(D), index: ivtmp.247_19, step: 8, offset: 0B]
	movq	%r13, %rdi	# num,
	addq	$1, %rbx	#, ivtmp.247
	call	sl_num_le	#
	testq	%rax, %rax	# D.12674
	je	.L357	#,
.L352:
	cmpq	%rbx, %r12	# ivtmp.247, numargs
	ja	.L354	#,
	movq	Qt(%rip), %rax	# Qt, D.12673
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movq	Qnil(%rip), %rax	# Qnil, D.12673
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE135:
	.size	Fnum_le, .-Fnum_le
	.section	.text.unlikely
.LCOLDE31:
	.text
.LHOTE31:
	.section	.text.unlikely
.LCOLDB32:
	.text
.LHOTB32:
	.p2align 4,,15
	.globl	init_math_symbols
	.type	init_math_symbols, @function
init_math_symbols:
.LFB136:
	.cfi_startproc
	pushq	%rbx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movl	$16, %edi	#,
	movl	$__Qadd, %ebx	#, D.12740
	movq	$__Qadd, Qadd(%rip)	#, Qadd
	andq	$-8, %rbx	#, D.12741
	call	GC_malloc	#
	testq	%rax, %rax	# tmp263
	je	.L362	#,
	movq	Qadd(%rip), %r8	# Qadd, D.12741
	movb	$0, 8(%rax)	#, misc_198->type
	movq	$__Sadd, (%rax)	#, misc_198->value
	orq	$5, %rax	#, tmp264
	movq	%rax, (%rbx)	# tmp264, _5->val
	andq	$-8, %r8	#, D.12741
	movl	12(%r8), %edx	# _10->name.len, D.12742
	movq	16(%r8), %rax	# _10->name.str, D.12744
	testl	%edx, %edx	# D.12742
	jle	.L375	#,
	subl	$1, %edx	#, D.12749
	movabsq	$1099511628211, %rsi	#, tmp325
	leaq	1(%rax,%rdx), %rdi	#, D.12748
	movabsq	$-3750763034362895579, %rdx	#, hash
	.p2align 4,,10
	.p2align 3
.L361:
	movzbl	(%rax), %ecx	# MEM[base: _109, offset: 0B], D.12740
	addq	$1, %rax	#, ivtmp.280
	xorq	%rcx, %rdx	# D.12740, D.12740
	imulq	%rsi, %rdx	# tmp325, hash
	cmpq	%rdi, %rax	# D.12748, ivtmp.280
	jne	.L361	#,
.L360:
	movq	%rdx, 32(%r8)	# hash, _10->hv
	movl	$__Qsub, %ebx	#, D.12740
	movl	$16, %edi	#,
	movq	$__Qsub, Qsub(%rip)	#, Qsub
	andq	$-8, %rbx	#, D.12741
	call	GC_malloc	#
	testq	%rax, %rax	# tmp271
	je	.L362	#,
	movq	Qsub(%rip), %r8	# Qsub, D.12741
	movb	$0, 8(%rax)	#, misc_177->type
	movq	$__Ssub, (%rax)	#, misc_177->value
	orq	$5, %rax	#, tmp272
	movq	%rax, (%rbx)	# tmp272, _18->val
	andq	$-8, %r8	#, D.12741
	movl	12(%r8), %edx	# _23->name.len, D.12742
	movq	16(%r8), %rax	# _23->name.str, D.12744
	testl	%edx, %edx	# D.12742
	jle	.L376	#,
	subl	$1, %edx	#, D.12749
	movabsq	$1099511628211, %rsi	#, tmp324
	leaq	1(%rax,%rdx), %rdi	#, D.12748
	movabsq	$-3750763034362895579, %rdx	#, hash
	.p2align 4,,10
	.p2align 3
.L364:
	movzbl	(%rax), %ecx	# MEM[base: _121, offset: 0B], D.12740
	addq	$1, %rax	#, ivtmp.276
	xorq	%rcx, %rdx	# D.12740, D.12740
	imulq	%rsi, %rdx	# tmp324, hash
	cmpq	%rdi, %rax	# D.12748, ivtmp.276
	jne	.L364	#,
.L363:
	movl	$__Qmul, %eax	#, D.12740
	movq	%rdx, 32(%r8)	# hash, _23->hv
	movl	$16, %edi	#,
	andq	$-8, %rax	#, D.12741
	movq	$__Qmul, Qmul(%rip)	#, Qmul
	movq	%rax, %rbx	# D.12741, D.12741
	call	GC_malloc	#
	testq	%rax, %rax	# tmp279
	je	.L362	#,
	movq	Qmul(%rip), %r8	# Qmul, D.12741
	movb	$0, 8(%rax)	#, misc_180->type
	movq	$__Smul, (%rax)	#, misc_180->value
	orq	$5, %rax	#, tmp280
	movq	%rax, (%rbx)	# tmp280, _31->val
	andq	$-8, %r8	#, D.12741
	movl	12(%r8), %edx	# _36->name.len, D.12742
	movq	16(%r8), %rax	# _36->name.str, D.12744
	testl	%edx, %edx	# D.12742
	jle	.L377	#,
	subl	$1, %edx	#, D.12749
	movabsq	$1099511628211, %rsi	#, tmp323
	leaq	1(%rax,%rdx), %rdi	#, D.12748
	movabsq	$-3750763034362895579, %rdx	#, hash
	.p2align 4,,10
	.p2align 3
.L366:
	movzbl	(%rax), %ecx	# MEM[base: _133, offset: 0B], D.12740
	addq	$1, %rax	#, ivtmp.272
	xorq	%rcx, %rdx	# D.12740, D.12740
	imulq	%rsi, %rdx	# tmp323, hash
	cmpq	%rdi, %rax	# D.12748, ivtmp.272
	jne	.L366	#,
.L365:
	movq	%rdx, 32(%r8)	# hash, _36->hv
	movl	$__Qdiv, %ebx	#, D.12740
	movl	$16, %edi	#,
	movq	$__Qdiv, Qdiv(%rip)	#, Qdiv
	andq	$-8, %rbx	#, D.12741
	call	GC_malloc	#
	testq	%rax, %rax	# tmp287
	je	.L362	#,
	movq	Qdiv(%rip), %r8	# Qdiv, D.12741
	movb	$0, 8(%rax)	#, misc_183->type
	movq	$__Sdiv, (%rax)	#, misc_183->value
	orq	$5, %rax	#, tmp288
	movq	%rax, (%rbx)	# tmp288, _44->val
	andq	$-8, %r8	#, D.12741
	movl	12(%r8), %edx	# _49->name.len, D.12742
	movq	16(%r8), %rax	# _49->name.str, D.12744
	testl	%edx, %edx	# D.12742
	jle	.L378	#,
	subl	$1, %edx	#, D.12749
	movabsq	$1099511628211, %rsi	#, tmp322
	leaq	1(%rax,%rdx), %rdi	#, D.12748
	movabsq	$-3750763034362895579, %rdx	#, hash
	.p2align 4,,10
	.p2align 3
.L368:
	movzbl	(%rax), %ecx	# MEM[base: _145, offset: 0B], D.12740
	addq	$1, %rax	#, ivtmp.268
	xorq	%rcx, %rdx	# D.12740, D.12740
	imulq	%rsi, %rdx	# tmp322, hash
	cmpq	%rdi, %rax	# D.12748, ivtmp.268
	jne	.L368	#,
.L367:
	movq	%rdx, 32(%r8)	# hash, _49->hv
	movl	$__Qnum_eq, %ebx	#, D.12740
	movl	$16, %edi	#,
	movq	$__Qnum_eq, Qnum_eq(%rip)	#, Qnum_eq
	andq	$-8, %rbx	#, D.12741
	call	GC_malloc	#
	testq	%rax, %rax	# tmp295
	je	.L362	#,
	movq	Qnum_eq(%rip), %r8	# Qnum_eq, D.12741
	movb	$0, 8(%rax)	#, misc_186->type
	movq	$__Snum_eq, (%rax)	#, misc_186->value
	orq	$5, %rax	#, tmp296
	movq	%rax, (%rbx)	# tmp296, _57->val
	andq	$-8, %r8	#, D.12741
	movl	12(%r8), %edx	# _62->name.len, D.12742
	movq	16(%r8), %rax	# _62->name.str, D.12744
	testl	%edx, %edx	# D.12742
	jle	.L379	#,
	subl	$1, %edx	#, D.12749
	movabsq	$1099511628211, %rsi	#, tmp321
	leaq	1(%rax,%rdx), %rdi	#, D.12748
	movabsq	$-3750763034362895579, %rdx	#, hash
	.p2align 4,,10
	.p2align 3
.L370:
	movzbl	(%rax), %ecx	# MEM[base: _157, offset: 0B], D.12740
	addq	$1, %rax	#, ivtmp.264
	xorq	%rcx, %rdx	# D.12740, D.12740
	imulq	%rsi, %rdx	# tmp321, hash
	cmpq	%rdi, %rax	# D.12748, ivtmp.264
	jne	.L370	#,
.L369:
	movq	%rdx, 32(%r8)	# hash, _62->hv
	movl	$__Qadd1, %ebx	#, D.12740
	movl	$16, %edi	#,
	movq	$__Qadd1, Qadd1(%rip)	#, Qadd1
	andq	$-8, %rbx	#, D.12741
	call	GC_malloc	#
	testq	%rax, %rax	# tmp303
	je	.L362	#,
	movq	Qadd1(%rip), %r8	# Qadd1, D.12741
	movb	$0, 8(%rax)	#, misc_189->type
	movq	$__Sadd1, (%rax)	#, misc_189->value
	orq	$5, %rax	#, tmp304
	movq	%rax, (%rbx)	# tmp304, _70->val
	andq	$-8, %r8	#, D.12741
	movl	12(%r8), %edx	# _75->name.len, D.12742
	movq	16(%r8), %rax	# _75->name.str, D.12744
	testl	%edx, %edx	# D.12742
	jle	.L380	#,
	subl	$1, %edx	#, D.12749
	movabsq	$1099511628211, %rsi	#, tmp320
	leaq	1(%rax,%rdx), %rdi	#, D.12748
	movabsq	$-3750763034362895579, %rdx	#, hash
	.p2align 4,,10
	.p2align 3
.L372:
	movzbl	(%rax), %ecx	# MEM[base: _169, offset: 0B], D.12740
	addq	$1, %rax	#, ivtmp.260
	xorq	%rcx, %rdx	# D.12740, D.12740
	imulq	%rsi, %rdx	# tmp320, hash
	cmpq	%rdi, %rax	# D.12748, ivtmp.260
	jne	.L372	#,
.L371:
	movq	%rdx, 32(%r8)	# hash, _75->hv
	movl	$__Qsub1, %ebx	#, D.12740
	movl	$16, %edi	#,
	movq	$__Qsub1, Qsub1(%rip)	#, Qsub1
	andq	$-8, %rbx	#, D.12741
	call	GC_malloc	#
	testq	%rax, %rax	# tmp311
	je	.L362	#,
	movq	Qsub1(%rip), %r8	# Qsub1, D.12741
	movb	$0, 8(%rax)	#, misc_192->type
	movq	$__Ssub1, (%rax)	#, misc_192->value
	orq	$5, %rax	#, tmp312
	movq	%rax, (%rbx)	# tmp312, _83->val
	andq	$-8, %r8	#, D.12741
	movl	12(%r8), %edx	# _88->name.len, D.12742
	movq	16(%r8), %rax	# _88->name.str, D.12744
	testl	%edx, %edx	# D.12742
	jle	.L381	#,
	subl	$1, %edx	#, D.12749
	movabsq	$1099511628211, %rsi	#, tmp319
	leaq	1(%rax,%rdx), %rdi	#, D.12748
	movabsq	$-3750763034362895579, %rdx	#, hash
	.p2align 4,,10
	.p2align 3
.L374:
	movzbl	(%rax), %ecx	# MEM[base: _249, offset: 0B], D.12740
	addq	$1, %rax	#, ivtmp.256
	xorq	%rcx, %rdx	# D.12740, D.12740
	imulq	%rsi, %rdx	# tmp319, hash
	cmpq	%rdi, %rax	# D.12748, ivtmp.256
	jne	.L374	#,
.L373:
	movq	%rdx, 32(%r8)	# hash, _88->hv
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L375:
	.cfi_restore_state
	movabsq	$-3750763034362895579, %rdx	#, hash
	jmp	.L360	#
.L376:
	movabsq	$-3750763034362895579, %rdx	#, hash
	jmp	.L363	#
.L377:
	movabsq	$-3750763034362895579, %rdx	#, hash
	jmp	.L365	#
.L378:
	movabsq	$-3750763034362895579, %rdx	#, hash
	jmp	.L367	#
.L379:
	movabsq	$-3750763034362895579, %rdx	#, hash
	jmp	.L369	#
.L380:
	movabsq	$-3750763034362895579, %rdx	#, hash
	jmp	.L371	#
.L381:
	movabsq	$-3750763034362895579, %rdx	#, hash
	jmp	.L373	#
.L362:
	call	sl_malloc.part.0	#
	.cfi_endproc
.LFE136:
	.size	init_math_symbols, .-init_math_symbols
	.section	.text.unlikely
.LCOLDE32:
	.text
.LHOTE32:
	.comm	Qnum_le,8,8
	.comm	Qnum_ge,8,8
	.comm	Qnum_lt,8,8
	.comm	Qnum_gt,8,8
	.comm	Qnum_ne,8,8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC33:
	.string	"="
	.data
	.align 32
	.type	__Qnum_eq, @object
	.size	__Qnum_eq, 48
__Qnum_eq:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC33
# sym_props:
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"(= number &rest numbers) return t if number and all numbers are mathematically equivlent, regardless of type"
	.data
	.align 32
	.type	__Snum_eq, @object
	.size	__Snum_eq, 56
__Snum_eq:
# <anonymous>:
# fMANY:
	.quad	Fnum_eq
	.zero	16
# symbol_name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC33
# minargs:
	.long	1
# maxargs:
	.long	-1
# doc:
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"1-"
	.data
	.align 32
	.type	__Qsub1, @object
	.size	__Qsub1, 48
__Qsub1:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	2
# str:
	.quad	.LC35
# sym_props:
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"(1- num) subtract 1 from number num"
	.data
	.align 32
	.type	__Ssub1, @object
	.size	__Ssub1, 56
__Ssub1:
# <anonymous>:
# f1:
	.quad	Fsub1
	.zero	16
# symbol_name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	2
# str:
	.quad	.LC35
# minargs:
	.long	1
# maxargs:
	.long	1
# doc:
	.quad	.LC36
	.section	.rodata.str1.1
.LC37:
	.string	"1+"
	.data
	.align 32
	.type	__Qadd1, @object
	.size	__Qadd1, 48
__Qadd1:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	2
# str:
	.quad	.LC37
# sym_props:
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.1
.LC38:
	.string	"(1+ num) add 1 to number num"
	.data
	.align 32
	.type	__Sadd1, @object
	.size	__Sadd1, 56
__Sadd1:
# <anonymous>:
# f1:
	.quad	Fadd1
	.zero	16
# symbol_name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	2
# str:
	.quad	.LC37
# minargs:
	.long	1
# maxargs:
	.long	1
# doc:
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"/"
	.data
	.align 32
	.type	__Qdiv, @object
	.size	__Qdiv, 48
__Qdiv:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC39
# sym_props:
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"(/ &rest args) when given one arg return the reciprocal, with more than one arg, divide the first arg by the remaining args"
	.data
	.align 32
	.type	__Sdiv, @object
	.size	__Sdiv, 56
__Sdiv:
# <anonymous>:
# fMANY:
	.quad	Fdiv
	.zero	16
# symbol_name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC39
# minargs:
	.long	0
# maxargs:
	.long	-1
# doc:
	.quad	.LC40
	.section	.rodata.str1.1
.LC41:
	.string	"*"
	.data
	.align 32
	.type	__Qmul, @object
	.size	__Qmul, 48
__Qmul:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC41
# sym_props:
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"(* &rest args) return product of args"
	.data
	.align 32
	.type	__Smul, @object
	.size	__Smul, 56
__Smul:
# <anonymous>:
# fMANY:
	.quad	Fmul
	.zero	16
# symbol_name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC41
# minargs:
	.long	0
# maxargs:
	.long	-1
# doc:
	.quad	.LC42
	.section	.rodata.str1.1
.LC43:
	.string	"-"
	.data
	.align 32
	.type	__Qsub, @object
	.size	__Qsub, 48
__Qsub:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC43
# sym_props:
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"(- &rest args) when given one arg negate it, with morethan one arg, subtract all but the first from the first"
	.data
	.align 32
	.type	__Ssub, @object
	.size	__Ssub, 56
__Ssub:
# <anonymous>:
# fMANY:
	.quad	Fsub
	.zero	16
# symbol_name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC43
# minargs:
	.long	0
# maxargs:
	.long	-1
# doc:
	.quad	.LC44
	.section	.rodata.str1.1
.LC45:
	.string	"+"
	.data
	.align 32
	.type	__Qadd, @object
	.size	__Qadd, 48
__Qadd:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC45
# sym_props:
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"(+ &rest args) return sum of args"
	.data
	.align 32
	.type	__Sadd, @object
	.size	__Sadd, 56
__Sadd:
# <anonymous>:
# fMANY:
	.quad	Fadd
	.zero	16
# symbol_name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC45
# minargs:
	.long	0
# maxargs:
	.long	-1
# doc:
	.quad	.LC46
	.section	.rodata
	.align 32
	.type	log2_dispatch_table, @object
	.size	log2_dispatch_table, 32
log2_dispatch_table:
	.quad	int_log2
	.quad	float_log2
	.quad	bigint_log2
	.quad	bigfloat_log2
	.align 32
	.type	log10_dispatch_table, @object
	.size	log10_dispatch_table, 32
log10_dispatch_table:
	.quad	int_log10
	.quad	float_log10
	.quad	bigint_log10
	.quad	bigfloat_log10
	.align 32
	.type	log_dispatch_table, @object
	.size	log_dispatch_table, 32
log_dispatch_table:
	.quad	int_log
	.quad	float_log
	.quad	bigint_log
	.quad	bigfloat_log
	.align 32
	.type	tan_dispatch_table, @object
	.size	tan_dispatch_table, 32
tan_dispatch_table:
	.quad	int_tan
	.quad	float_tan
	.quad	bigint_tan
	.quad	bigfloat_tan
	.align 32
	.type	cos_dispatch_table, @object
	.size	cos_dispatch_table, 32
cos_dispatch_table:
	.quad	int_cos
	.quad	float_cos
	.quad	bigint_cos
	.quad	bigfloat_cos
	.align 32
	.type	sin_dispatch_table, @object
	.size	sin_dispatch_table, 32
sin_dispatch_table:
	.quad	int_sin
	.quad	float_sin
	.quad	bigint_sin
	.quad	bigfloat_sin
	.align 64
	.type	ge_dispatch_table, @object
	.size	ge_dispatch_table, 128
ge_dispatch_table:
	.quad	int_int_ge
	.quad	int_float_ge
	.quad	int_bigint_ge
	.quad	int_bigfloat_ge
	.quad	float_int_ge
	.quad	float_float_ge
	.quad	float_bigint_ge
	.quad	float_bigfloat_ge
	.quad	bigint_int_ge
	.quad	bigint_float_ge
	.quad	bigint_bigint_ge
	.quad	bigint_bigfloat_ge
	.quad	bigfloat_int_ge
	.quad	bigfloat_float_ge
	.quad	bigfloat_bigint_ge
	.quad	bigfloat_bigfloat_ge
	.align 64
	.type	gt_dispatch_table, @object
	.size	gt_dispatch_table, 128
gt_dispatch_table:
	.quad	int_int_gt
	.quad	int_float_gt
	.quad	int_bigint_gt
	.quad	int_bigfloat_gt
	.quad	float_int_gt
	.quad	float_float_gt
	.quad	float_bigint_gt
	.quad	float_bigfloat_gt
	.quad	bigint_int_gt
	.quad	bigint_float_gt
	.quad	bigint_bigint_gt
	.quad	bigint_bigfloat_gt
	.quad	bigfloat_int_gt
	.quad	bigfloat_float_gt
	.quad	bigfloat_bigint_gt
	.quad	bigfloat_bigfloat_gt
	.align 64
	.type	le_dispatch_table, @object
	.size	le_dispatch_table, 128
le_dispatch_table:
	.quad	int_int_le
	.quad	int_float_le
	.quad	int_bigint_le
	.quad	int_bigfloat_le
	.quad	float_int_le
	.quad	float_float_le
	.quad	float_bigint_le
	.quad	float_bigfloat_le
	.quad	bigint_int_le
	.quad	bigint_float_le
	.quad	bigint_bigint_le
	.quad	bigint_bigfloat_le
	.quad	bigfloat_int_le
	.quad	bigfloat_float_le
	.quad	bigfloat_bigint_le
	.quad	bigfloat_bigfloat_le
	.align 64
	.type	lt_dispatch_table, @object
	.size	lt_dispatch_table, 128
lt_dispatch_table:
	.quad	int_int_lt
	.quad	int_float_lt
	.quad	int_bigint_lt
	.quad	int_bigfloat_lt
	.quad	float_int_lt
	.quad	float_float_lt
	.quad	float_bigint_lt
	.quad	float_bigfloat_lt
	.quad	bigint_int_lt
	.quad	bigint_float_lt
	.quad	bigint_bigint_lt
	.quad	bigint_bigfloat_lt
	.quad	bigfloat_int_lt
	.quad	bigfloat_float_lt
	.quad	bigfloat_bigint_lt
	.quad	bigfloat_bigfloat_lt
	.align 64
	.type	ne_dispatch_table, @object
	.size	ne_dispatch_table, 128
ne_dispatch_table:
	.quad	int_int_ne
	.quad	int_float_ne
	.quad	int_bigint_ne
	.quad	int_bigfloat_ne
	.quad	float_int_ne
	.quad	float_float_ne
	.quad	float_bigint_ne
	.quad	float_bigfloat_ne
	.quad	bigint_int_ne
	.quad	bigint_float_ne
	.quad	bigint_bigint_ne
	.quad	bigint_bigfloat_ne
	.quad	bigfloat_int_ne
	.quad	bigfloat_float_ne
	.quad	bigfloat_bigint_ne
	.quad	bigfloat_bigfloat_ne
	.align 64
	.type	eq_dispatch_table, @object
	.size	eq_dispatch_table, 128
eq_dispatch_table:
	.quad	int_int_eq
	.quad	int_float_eq
	.quad	int_bigint_eq
	.quad	int_bigfloat_eq
	.quad	float_int_eq
	.quad	float_float_eq
	.quad	float_bigint_eq
	.quad	float_bigfloat_eq
	.quad	bigint_int_eq
	.quad	bigint_float_eq
	.quad	bigint_bigint_eq
	.quad	bigint_bigfloat_eq
	.quad	bigfloat_int_eq
	.quad	bigfloat_float_eq
	.quad	bigfloat_bigint_eq
	.quad	bigfloat_bigfloat_eq
	.align 64
	.type	div_dispatch_table, @object
	.size	div_dispatch_table, 128
div_dispatch_table:
	.quad	int_int_div
	.quad	int_float_div
	.quad	int_bigint_div
	.quad	int_bigfloat_div
	.quad	float_int_div
	.quad	float_float_div
	.quad	float_bigint_div
	.quad	float_bigfloat_div
	.quad	bigint_int_div
	.quad	bigint_float_div
	.quad	bigint_bigint_div
	.quad	bigint_bigfloat_div
	.quad	bigfloat_int_div
	.quad	bigfloat_float_div
	.quad	bigfloat_bigint_div
	.quad	bigfloat_bigfloat_div
	.align 64
	.type	mul_dispatch_table, @object
	.size	mul_dispatch_table, 128
mul_dispatch_table:
	.quad	int_int_mul
	.quad	int_float_mul
	.quad	int_bigint_mul
	.quad	int_bigfloat_mul
	.quad	float_int_mul
	.quad	float_float_mul
	.quad	float_bigint_mul
	.quad	float_bigfloat_mul
	.quad	bigint_int_mul
	.quad	bigint_float_mul
	.quad	bigint_bigint_mul
	.quad	bigint_bigfloat_mul
	.quad	bigfloat_int_mul
	.quad	bigfloat_float_mul
	.quad	bigfloat_bigint_mul
	.quad	bigfloat_bigfloat_mul
	.align 64
	.type	sub_dispatch_table, @object
	.size	sub_dispatch_table, 128
sub_dispatch_table:
	.quad	int_int_sub
	.quad	int_float_sub
	.quad	int_bigint_sub
	.quad	int_bigfloat_sub
	.quad	float_int_sub
	.quad	float_float_sub
	.quad	float_bigint_sub
	.quad	float_bigfloat_sub
	.quad	bigint_int_sub
	.quad	bigint_float_sub
	.quad	bigint_bigint_sub
	.quad	bigint_bigfloat_sub
	.quad	bigfloat_int_sub
	.quad	bigfloat_float_sub
	.quad	bigfloat_bigint_sub
	.quad	bigfloat_bigfloat_sub
	.align 64
	.type	add_dispatch_table, @object
	.size	add_dispatch_table, 128
add_dispatch_table:
	.quad	int_int_add
	.quad	int_float_add
	.quad	int_bigint_add
	.quad	int_bigfloat_add
	.quad	float_int_add
	.quad	float_float_add
	.quad	float_bigint_add
	.quad	float_bigfloat_add
	.quad	bigint_int_add
	.quad	bigint_float_add
	.quad	bigint_bigint_add
	.quad	bigint_bigfloat_add
	.quad	bigfloat_int_add
	.quad	bigfloat_float_add
	.quad	bigfloat_bigint_add
	.quad	bigfloat_bigfloat_add
	.comm	Qash,8,8
	.local	gensym_counter
	.comm	gensym_counter,8,8
	.align 8
	.type	Qnil, @object
	.size	Qnil, 8
Qnil:
	.quad	__Qnil
	.section	.rodata.str1.1
.LC47:
	.string	"nil"
	.section	.rodata
	.align 32
	.type	__Qnil, @object
	.size	__Qnil, 48
__Qnil:
# val:
	.quad	0
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	3
# str:
	.quad	.LC47
# sym_props:
	.zero	17
	.byte	1
	.zero	6
	.align 8
	.type	Qt, @object
	.size	Qt, 8
Qt:
	.quad	__Qt
	.section	.rodata.str1.1
.LC48:
	.string	"t"
	.section	.rodata
	.align 32
	.type	__Qt, @object
	.size	__Qt, 48
__Qt:
# val:
	.quad	6
# name:
# tag:
	.byte	2
# ascii:
	.zero	2
# len:
	.byte	1
	.long	1
# str:
	.quad	.LC48
# sym_props:
	.zero	17
	.byte	1
	.zero	6
	.comm	Qunbound,8,8
	.comm	Qcddddr,8,8
	.comm	Qcdddar,8,8
	.comm	Qcddadr,8,8
	.comm	Qcddaar,8,8
	.comm	Qcdaddr,8,8
	.comm	Qcdadar,8,8
	.comm	Qcdaadr,8,8
	.comm	Qcdaaar,8,8
	.comm	Qcadddr,8,8
	.comm	Qcaddar,8,8
	.comm	Qcadadr,8,8
	.comm	Qcadaar,8,8
	.comm	Qcaaddr,8,8
	.comm	Qcaadar,8,8
	.comm	Qcaaadr,8,8
	.comm	Qcaaaar,8,8
	.comm	Qcdddr,8,8
	.comm	Qcddar,8,8
	.comm	Qcdadr,8,8
	.comm	Qcdaar,8,8
	.comm	Qcaddr,8,8
	.comm	Qcadar,8,8
	.comm	Qcaadr,8,8
	.comm	Qcaaar,8,8
	.comm	Qcddr,8,8
	.comm	Qcdar,8,8
	.comm	Qcadr,8,8
	.comm	Qcaar,8,8
	.comm	Qlist_star,8,8
	.comm	Qlist,8,8
	.comm	Qpush,8,8
	.comm	Qpop,8,8
	.comm	Qsafe_cdr,8,8
	.comm	Qsafe_car,8,8
	.comm	Qcdr,8,8
	.comm	Qcar,8,8
	.comm	Qrassq,8,8
	.comm	Qassq,8,8
	.comm	Qrassoc,8,8
	.comm	Qassoc,8,8
	.comm	Qsplit,8,8
	.comm	Qtail,8,8
	.comm	Qhead,8,8
	.comm	Qnth,8,8
	.comm	Qnappend,8,8
	.comm	Qappend,8,8
	.comm	Qcons,8,8
	.comm	Qquote,8,8
	.comm	Qread,8,8
	.comm	Qsub1,8,8
	.comm	Qadd1,8,8
	.comm	Qnum_eq,8,8
	.comm	Qdiv,8,8
	.comm	Qmul,8,8
	.comm	Qsub,8,8
	.comm	Qadd,8,8
	.comm	Qwrite_string,8,8
	.comm	Qnewline,8,8
	.comm	Qpeek_char,8,8
	.comm	Qread_char,8,8
	.comm	Qstandard_error,8,8
	.comm	Qstandard_output,8,8
	.comm	Qstandard_input,8,8
	.comm	Krw,8,8
	.comm	Qrw_key,8,8
	.comm	Kwrite,8,8
	.comm	Qwrite_key,8,8
	.comm	Kread,8,8
	.comm	Qread_key,8,8
	.comm	Kio,8,8
	.comm	Qio_key,8,8
	.comm	Koutput,8,8
	.comm	Qoutput_key,8,8
	.comm	Kinput,8,8
	.comm	Qinput_key,8,8
	.comm	Qsetq,8,8
	.comm	Qgensym,8,8
	.comm	Qequalp,8,8
	.comm	Qequal,8,8
	.comm	Qeqv,8,8
	.comm	Qeq,8,8
	.comm	Qfloatp,8,8
	.comm	Qintegerp,8,8
	.comm	Qsvectorp,8,8
	.comm	Qvectorp,8,8
	.comm	Qarrayp,8,8
	.comm	Qconsp,8,8
	.comm	Qmake_vector,8,8
	.comm	Qvset,8,8
	.comm	Qvref,8,8
	.comm	Qnreverse,8,8
	.comm	Qreverse,8,8
	.comm	Qlength,8,8
	.comm	Qelt,8,8
	.comm	Qsprint,8,8
	.comm	Qprin1,8,8
	.comm	Qprint,8,8
	.comm	runtime_debug,4,4
	.ident	"GCC: (GNU) 4.9.2 20141224 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
