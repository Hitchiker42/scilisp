#ifdef _SL_UNICODE_H_
#define _SL_UNICODE_H_
#include <uniname.h>
#define ascii_upcase(c) (c &  (~0x20))
#define ascii_downcase(c) (c | 0x20)
/*
  This is just a temporary place to put this
*/
DEFUN("character->name", Fcharacter_to_name, 1,1, "(character->name char)"
      " return the unicode character name of char")
  (sl_obj c){
  TYPECHECK_1(CHAR, c, 0, 0);
  char buf[UNINAME_MAX];
  buf = unicode_character_name(XCHAR(c), buf);
  if(!buf){
    return NULL;
  }
  //the unicode character name is always ascii
  return make_string(buf, strlen(buf), 1);
}
DEFUN("name->character", Fname_to_character, 1, 1, "(name->character name)"
      " return the unicode character named 'name'")
  (sl_obj name){
  TYPECHECK_1(STRING, name, 0, 0);
  //should probably have a function to get a c string from a sl_string
  char *buf = sl_string_to_cstr(XSTRING(name));
  sl_char c = unicode_name_character(buf);
  if(c == UNINAME_INVALID){
    return NIL;
  } else {
    return make_char(c);
  }
}
#endif
