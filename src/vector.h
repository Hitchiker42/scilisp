/*
  Header for array and vector functions
*/
#ifndef _VECTOR_H_
#define _VECTOR_H_
#include "common.h"
/*
  this will need to be modified when I add utf8 support
*/
#define STR_REF(obj, ind) (XSTRING(obj)->str[(ind)])
//this isn't an lvalue
#define TYPED_VREF(obj,ind) (typed_vref(XSVECTOR(obj),ind))
//for compatibility with common lisp svref should alias to vref 
sl_obj Fvref(sl_obj obj, sl_obj index);
sl_obj Fvset(sl_obj vec, sl_obj index, sl_obj val);
//needs to take an arbitary number of args because arrays
//can have arbitary dimensionns
sl_obj Faref(uint64_t numargs, sl_obj *args);
sl_obj Fset(uint64_t numargs, sl_obj *args);
//this should ultimately take keyword args
sl_obj Fmake_vector(sl_obj length, sl_obj inital_element);
/*sl_obj Fvector_push(sl_obj elt, sl_obj vec);
  sl_obj Fvector_pop(sl_obj elt, sl_obj vec);*/
//Freverse is a sequence function
sl_obj reverse_vector(sl_obj obj);
sl_obj nreverse_vector(sl_obj obj);
sl_obj map_vector(sl_obj fn, sl_obj vec);
sl_obj reduce_vector(sl_obj fn, sl_obj vec, sl_obj start);
SL_INLINE sl_svector *make_svector(uint64_t len){
  sl_svector *vec = sl_malloc(sizeof(sl_svector));
  vec->len = len;
  vec->tag = SL_untyped_svector;
  vec->value = sl_malloc(sizeof(sl_obj)*len);
  return vec;
}
SL_INLINE sl_obj make_new_svector_obj(uint64_t len){
  sl_svector *vec = make_svector(len);
  return make_sl_obj((sl_obj)vec, SL_vector);
}
SL_INLINE sl_obj make_svector_obj(sl_obj *vec, uint64_t len){
  sl_svector *new_vector = sl_malloc(sizeof(sl_vector));
  new_vector->tag = SL_untyped_svector;
  new_vector->len = len;
  new_vector->value = vec;
  return make_sl_obj((sl_obj)new_vector,SL_vector);
}
SL_INLINE sl_obj make_typed_svector(sl_obj *vec, uint64_t len, SL_number_type type){
  sl_svector *new_vector = sl_malloc(sizeof(sl_vector));
  new_vector->tag = SL_typed_svector;
  new_vector->type = type;
  new_vector->len = len;
  new_vector->value = vec;
  return make_sl_obj((sl_obj)new_vector,SL_vector);
}
SL_INLINE sl_obj copy_vector(sl_obj vec){
  sl_svector *copy = make_svector(VECTOR_LEN(vec));
  memcpy(copy->value, XSVECTOR(vec)->value, VECTOR_LEN(vec));
  return make_sl_obj((sl_obj)copy, SL_vector);
}
//A (hopefully) simple macro to create a dynamically allocated array
//from the given arguments
//using variable length arrays and designated initializers we create
//a temporary array on the stack and copy that to the actual array
//making this a macro makes it significantly faster than a va_args function
//which I actually tested to make sure, and yes, even gcc at -O3 can't
//optimize a call to a va_args function into the same code as a call
//to the macro
#define build_vector(_len,_args...)                     \
  ({sl_obj *vec = sl_malloc(_len *sizeof(sl_obj));      \
    sl_obj temp[]={_args};                              \
    memcpy(vec, temp, _len*sizeof(sl_obj));             \
    make_svector_obj(vec,_len);})
/*
  extract the ind'th element from a typed svector as an sl object
  don't actually call this function user the TYPED_SVREF macro, or
  
*/
SL_INLINE sl_obj typed_vref(sl_svector *svec, int ind){
  switch(svec->type){
    case (SL_double):
      return make_double(svec->value[ind]);      
    default:
      raise_error(0,0);
  }
}
#endif
