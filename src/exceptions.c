#include "common.h"
#include "print.h"
//temporary defination untill I get a better one
//the prototype should stay the same however
void SL_NORETURN raise_error(sl_obj tag, sl_obj data){
  if(!NILP(data)){
    fprintf(stderr, "Error raised:\n %s\n", c_sprint(data));
  }
  raise(SIGUSR1);
  SL_UNREACHABLE;
}
DEFUN("assert",assert,1,MANY,"(assert form &optional fmt-str &rest fmt-args)\n"
      "Evaluate form, if form returns nil raise an exception.\n"
      "If fmt-str/fmt-args are provided they are passed to format"
      "and the resultant string is used as the value of the exception.")
     (uint64_t numargs, sl_obj *args){
  sl_obj test = Feval(args[0]);
  if(NILP(test)){
    if(numargs > 1){
      sl_obj fmt_str = Fformat(numargs-1, args + 1);
      raise_error(0, fmt_str);
    } else {
      raise_error(0, make_string_const("Assertion failure"));
    }
  }
  return NIL;
}
