#ifndef _MURMURHASH3_H_
#define _MURMURHASH3_H_
#include <stdint.h>

uint64_t murmur_hash3_64_seeded(const void * key, const int len, 
                                const uint32_t seed);
uint32_t murmur_hash3_32_seeded(const void * key, const int len, 
                                const uint32_t seed);
//the 32 bit fnv prime, because why not 
static const uint32_t murmur3_default_seed = 16777619U;
static inline uint32_t murmur_hash3_32(const void *key, int len){
  return murmur_hash3_32_seeded(key, len, murmur3_default_seed);
}
static inline uint64_t murmur_hash3_64(const void *key, int len){
  return murmur_hash3_64_seeded(key, len, murmur3_default_seed);
}
#ifdef _GNUC_
uint128 murmur_hash3_128_seeded(const void *key, const int len,
                                const uint32_t seed);
static inline __uint128 murmur_hash3_128(const void *key, int len){
  return murmur_hash3_128_seeded(key, len, murmur3_default_seed);
}
#endif  
#endif // _MURMURHASH3_H_
