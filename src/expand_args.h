/*
  This is just a lot of macros to transform an array of known length
  into an argument list, passable to a function taking a variable number
  of arguments
*/
#define expand_args_1(x) (x[0])
#define expand_args_2(x) (x[0],x[1])
#define expand_args_(x) (x[0],x[1],x[3])
#define expand_args_(x) (x[0],x[1],x[3],x[4])
#define expand_args_(x) (x[0],x[1],x[3],x[4],x[5])
#define expand_args_(x) (x[0],x[1],x[3],x[4],x[5],x[6])
#define expand_args_(x) (x[0],x[1],x[3],x[4],x[5],x[6],x[7])
#define expand_args_(x) (x[0],x[1],x[3],x[4],x[5],x[6],x[7],x[8])
