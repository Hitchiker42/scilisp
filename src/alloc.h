/*
  Allocation routines, memory allocation can be customized by
  defining CUSTOM_ALLOC and defining SL_MALLOC to the new allocation
  function. Other macros can be defined for realloc, strdup, etc..
*/
/*
  There are two ways I can improve performance with the bohem gc. the eaisest
  way is to implemnt allocation of cons cells using gc_malloc_many. this
  returns a list of allocated objects of a given size and would be used
  in combination with a thread local free list to greatly decrease gc lock
  contention as well as the number of times memory needs to be allocated for
  conses.

  The other way is to write a custom mark routine which takes advantage of
  the fact I have tagged pointers to eliminate most accidental retentions
*/
#ifndef _SL_COMMON_H_
#error "Don't include alloc.h directly, use common.h"
#endif
#ifndef _SL_ALLOC_H_
#define _SL_ALLOC_H_

extern void raise_error(sl_obj tag, sl_obj data);
//Allocation Macros
#ifdef CUSTOM_ALLOC
//the predifened macros for custom allocation aren't very
//safe so if using a custom allocator it's best to define
//all the SL_ALLOC macros
//SL_MALLOC should return a pointer to zero filled memory
//of the requested size
#ifndef SL_MALLOC
#error "CUSTOM_ALLOC defined, but SL_MALLOC undefined"
#endif
#ifndef SL_FREE
//define to a no-op
#define SL_FREE ;
#endif
#ifndef SL_REALLOC
#define SL_REALLOC(ptr, size)\
  ({__typeof ptr temp = SL_MALLOC(size);        \
  memcpy(ptr, temp, size);                      \
  SL_FREE(ptr);                                 \
  ptr=temp;})
#endif
#ifndef SL_MALLOC_ATOMIC
#define SL_MALLOC_ATOMIC SL_MALLOC
#endif
#ifndef SL_MALLOC_UNCOLLECTABLE
#define SL_MALLOC_UNCOLLECTABLE SL_MALLOC
#endif
#ifndef SL_MALLOC_ATOMIC_UNCOLLECTABLE
#define SL_MALLOC_ATOMIC_UNCOLLECTABLE SL_MALLOC
#endif
#ifndef SL_STRDUP
#define SL_STRDUP(str)                          \
  ({int len = strlen(str);                      \
    char *new_str = SL_MALLOC_ATOMIC((len+1)*sizeof(char));     \
    memcpy(new_str, str, len+1);                                \
    new_str;})
#endif
#ifndef SL_STRNDUP
#define SL_STRNDUP(str, n)                      \
  ({char *new_str = SL_MALLOC_ATOMIC((n+1)*sizeof(char));       \
  memcpy(new_str, str, n+1);                                    \
  new_str;})
#endif
#ifndef SL_MALLOC_BIG
#define SL_MALLOC_BIG SL_MALLOC
#endif
#else /* ! CUSTOM_ALLOC*/
#define SL_MALLOC GC_MALLOC
#define SL_FREE GC_FREE
#define SL_REALLOC GC_REALLOC
#define SL_MALLOC_ATOMIC GC_MALLOC_ATOMIC
#define SL_MALLOC_UNCOLLECTABLE GC_MALLOC_UNCOLLECTABLE
#define SL_MALLOC_ATOMIC_UNCOLLECTABLE GC_MALLOC_ATOMIC_UNCOLLECTABLE
#define SL_STRDUP GC_STRDUP
#define SL_STRNDUP GC_STRNDUP
#define SL_MALLOC_BIG GC_MALLOC_IGNORE_OFF_PAGE
#endif
/*
   the SL_MALLOC and friends macros are just raw wrappers around
   the basic memory allocation routines, the sl_malloc and friends
   functions check the return value of the malloc functions and
   raise an error if they return NULL;

   _MALLOC_CHECK is a macro with a return in it, but it's only used
   here, and is really just to save some typing
*/
#define _MALLOC_CHECK(macro,args...)               \
  void* temp = macro(args);                        \
  if(!temp && sz){                                 \
    raise_error(0,0);                              \
  }                                                \
  return temp;
SL_INLINE void* sl_malloc(size_t sz){
  _MALLOC_CHECK(SL_MALLOC,sz);
}
SL_INLINE void* sl_realloc(void *ptr, size_t sz){
  _MALLOC_CHECK(SL_REALLOC,ptr,sz);
}
SL_INLINE void* sl_malloc_atomic(size_t sz){
  _MALLOC_CHECK(SL_MALLOC_ATOMIC,sz);
}
SL_INLINE void* sl_malloc_uncollectable(size_t sz){
  _MALLOC_CHECK(SL_MALLOC_UNCOLLECTABLE,sz);
}
SL_INLINE void* sl_malloc_atomic_uncollectable(size_t sz){
  _MALLOC_CHECK(SL_MALLOC_ATOMIC_UNCOLLECTABLE,sz);
}
SL_INLINE void* sl_malloc_big(size_t sz){
  _MALLOC_CHECK(SL_MALLOC_BIG,sz);
}
#define _STRDUP_CHECK(macro,args...)            \
  void *temp = macro(args);                     \
  if(!temp){                                    \
    raise_error(0,0);                           \
  }                                             \
  return temp;
SL_INLINE char* sl_cstrdup(const char *str){
  _STRDUP_CHECK(SL_STRDUP, str);
}
SL_INLINE char* sl_cstrndup(const char *str, size_t n){
  _STRDUP_CHECK(SL_STRNDUP, str, n);
}
//only use for big objects
SL_INLINE void sl_free(void *obj){
  SL_FREE(obj);
}
SL_INLINE void *sl_memdup(const void *mem, size_t sz){
  void *temp = sl_malloc(sz);
  memcpy(temp, mem, sz);
  return temp;
}
SL_INLINE void *sl_memdup_atomic(const void *mem, size_t sz){
  void *temp = sl_malloc_atomic(sz);
  memcpy(temp, mem, sz);
  return temp;
}

/* gmp has odd prototypes for it's realloc and free functions, so
   we need these functions in order to tell gmp/mpfr to use our memory
   allocation routines */
static void *gmp_malloc_func(size_t sz){
  return sl_malloc(sz);
}
static void *gmp_realloc_func(void *ptr, size_t old, size_t new){
  return sl_realloc(ptr, new);
}
static void gmp_free_func(void *ptr, size_t sz){
  //bignums can get pretty large, so it might be worth doing a gc_free
  //but for now I'm leaving this as a no-op
#ifdef CUSTOM_ALLOC
  SL_FREE(ptr);
#endif
  return;
}
#endif
