#include "common.h"
#include "env.h"
#include "symbol.h"
#include "sl_string.h"
#include "vector.h"
SL_THREAD_LOCAL struct sl_environment *thread_env;
sl_symbol_table *global_obarray;
#if(defined DEBUG) && !(defined NDEBUG)
static SL_NORETURN void signal_backtrace(int sig){
  //these aren't signal safe functions, I don't know if that matters
  fprintf(stderr,"Caught signal %d(%s).\n", sig,strsignal(sig));
  //We don't need the call stack any more, even if we aren't terminating
  //the program at the very least we're jumping back to the top level
  int stack_entries = backtrace((void**)&thread_env->call_stack, CALL_STACK_SIZE);
  //just write the backtrace straight to stderr
  backtrace_symbols_fd((void*)thread_env->call_stack, stack_entries, STDERR_FILENO);
  //the SA_RESETHAND flag needs to be set in the sigaction struct
  //for this to work. Later I might write code to save the old signal handler
  //set that here and then raise the signal
  raise(sig);
  SL_UNREACHABLE;
}
static struct sigaction backtrace_sigaction = {.sa_handler = signal_backtrace,
                                               .sa_flags = SA_RESETHAND};
//SIGINT is specifically NOT included here, I might change that later
static const int backtrace_signals[] = {SIGSEGV, SIGFPE, SIGBUS, SIGTERM};
//I could actually just use sizeof(backtrace_signals) but eh;
static const int num_backtrace_signals = 4;
#endif

sl_environment* make_environment(){
  sl_environment *env = sl_malloc(sizeof(sl_environment));
  env->obarray = global_obarray;
  sl_obj *temp = sl_malloc(2*sizeof(sl_obj));
  temp[0] = temp[1] = NIL;
  env->thread_local_bindings = make_svector_obj(temp,2);
  //make a dummy environment to avoid null pointer dereferences
  //this should be temporary, since it adds an extra layer of
  //recursion to every variable look up.
  env->lexical_env = make_svector_obj(temp, 2);
  //this might not always be correct, but it should be more often than not
  env->current_thread = pthread_self();
  env->backquote_flag = 0;
  env->stack_ptr = env->call_stack;
  /*
    Possibly initialize an alternate stack for signals here
    stack_t = {void *ss_sp; int ss_flags; size_t ss_size;}
    SIGSTKSZ = 8192, MINSIGSTKSZ = 2048, (meaning 6148 available for me)
  */
  env->signal_stack.ss_sp = sl_malloc_atomic_uncollectable(SIGSTKSZ);
  env->signal_stack.ss_size = SIGSTKSZ;
  env->signal_stack.ss_flags = 0;
  if(sigaltstack(&env->signal_stack,NULL) < 0){
    perror("sigaltstack");
  }
#ifdef FAST_ALLOC_CONS
  env->cons_fl = GC_malloc_many(sizeof(sl_cons)-8);
#endif
#ifdef DEBUG
  int i;
  sigemptyset(&backtrace_sigaction.sa_mask);
  for(i=0;i<num_backtrace_signals;i++){
    sigaction(backtrace_signals[i], &backtrace_sigaction, NULL);
  }
#endif
  return env;
}
/*
  Lexical Environment code, should probably be moved to another file
*/
//named let could eaisly be done as a macro, but since normal let
//has to be a special form so does named let.
/*
  named let (ala scheme)
    format: (let var varlist body...)
    var is bound to a lambda function taking the variables in varlist
    as arguments, varlist has the same format as in a normal let,
    with body being the body of the function.
    i.e (define factorial (val)
           (let fac ((acc 1)
                     (n val))
                     (if (> n 1) (fac (* n acc) (1- n)) acc)))
    is the same as:
        (define factorial (val)
          (let ((fac (lambda (acc n)
                       (if (> n 1) (fac (* n acc) (1-n )) acc))))
             (fac 1 val)))
*/
static sl_obj internal_named_let(sl_obj args){
  //minimum number of args to a named let is 2, vs 1 for a normal let 
  if(!(CONSP(XCDR(args))) || !LISTP(XCADR(args))){
    raise_error_temp();
  }
  sl_obj var = XCAR(args);//Guaranteed to be a symbol
  sl_obj bindings = XCADR(args);//Has to be a list because of above check
  sl_obj body = XCDDR(args);//might be nil
  //there are a couple ways to do this, but I think this is the most elegent
  //if not necessarly the most efficent
  sl_obj parameters = NIL, arguments = NIL;
  int nargs = 0;
  //This could eaisly be faster by building the lists in reverse
  //and avoiding the calls to nreverse
  while(CONSP(bindings)){
    nargs++;
    if(CONSP(XCAR(bindings))){
      PUSH(XCAAR(bindings), parameters);
      PUSH(Feval(XCADR(bindings)), arguments);
    } else if(SYMBOLP(XCAR(bindings))){
      PUSH(XCAR(bindings), parameters);
      PUSH(NIL, arguments);
    } else {
      raise_error_temp();
    }
  }
  parameters = nreverse_cons(parameters);
  arguments = nreverse_cons(arguments);
  sl_obj proc = Flambda(make_cons(parameters, body));
  //this might not work, I may need to make 2 lexical environments
  //one to bind var to proc and one to bind the parameters to the args
  sl_obj env = make_new_svector_obj(nargs + 3);
  //We need to bind var to a lambda to allow for recursive calls
  VSET(env, 3, make_cons(var, proc));
  int i;
  //this emulates calling proc with parameters bound to arguments
  //but without the overhead of funcall
  for(i=4;i<nargs+4;i++){
    VSET(env, i, make_cons(POP(parameters),Feval(POP(arguments))));
  }
  push_lexical_env(env);
  sl_obj retval = Feval(body);
  pop_lexical_env();
  return retval;
}
static sl_obj internal_let(sl_obj args){
  sl_obj bindings_list = XCAR(args);//Guaranteed to be a list
  sl_obj bindings_vec = make_lexical_env(bindings_list);
  push_lexical_env(bindings_vec);
  sl_obj retval = Fprogn(XCDR(args));
  pop_lexical_env();
  return retval;
}
DEFUN("let",let,1,UNEVALED,"(let varlist body...), or (let var varlist body..)"
      "In the first form evaluate body with variables lexically bound according "
      "to varlist.\n Each element of varlist is either a symbol, which is bound "
      "to nil, or a list (symbol value), which binds symbol to value.\n"
      "In the second form bind var to a procedure whose formal arguments are "
      "the varaibles in varlist, and whose body is 'body'\nWhen the let form is "
      "evaluated it is akin to calling var with arguments according to varlist")
  (sl_obj args){
  sl_obj head = XCAR(args);
  if(LISTP(head)){
    return internal_let(args);
  } else if(SYMBOLP(head)){
    return internal_named_let(args);
  } else {
    raise_error_cstr(0,
                     "Error, first argument to let must be a list or a symbol");
  }
}
DEFUN("let*",let_star,1,UNEVALED,"(let* varlist body...),"
      "Evaluate body with variables lexically bound according to varlist.\n"
      "Each element of varlist is either a symbol, which is bound to nil,"
      "or a list (symbol value), which binds symbol to value."
      "Bindings are done sequentially.")
  (sl_obj args){
  sl_obj bindings_list = XCAR(args);
  if(!LISTP(bindings_list)){
    raise_error_cstr(0, "Error, first argument to let* must be a list");
  }
  //the bindings list should be short enough that computing the length is fast
  int num_bindings = CONS_LENGTH(bindings_list);
  sl_obj bindings_vec = make_new_svector_obj(num_bindings + 2);
  sl_obj binding;
  int i = 1;
  push_lexical_env(bindings_vec);
  while((binding = POP(bindings_list))){
    XSVECTOR(bindings_vec)->len = i;
    VSET(bindings_vec, i++, make_cons(XCAR(binding), Feval(XCDR(binding))));
  }
  XSVECTOR(bindings_vec)->len = num_bindings + 2;
  sl_obj retval = Fprogn(XCDR(args));
  pop_lexical_env();
  return retval;
}
//should ultimately take an arbitary even number of arguments
DEFUN("set!",setq,2,UNEVALED,"(setq var val) set the value of var in the"
      "current lexical scope to val")
  (sl_obj args){
  sl_obj var = XCAR(args);
  if(!SYMBOLP(var)){
    raise_error_temp();
  }
  sl_obj val = Feval(XCADR(args));
  return set_lexical_binding(var, val);
}

/*
  progv is different than in common-lisp, the declaration of progv in cl is:
  (progv symbols values &rest body). Clearly this is different than let,
  which, in my opinion, is just confusing. So in scilisp progv takes the same
  form as let, i.e (progv varlist body), it should be trivial to make a macro
  to use the CL version, i.e:
  (defmacro cl-progv (symbols values &rest body)
    `(progv ,@(merge symbols values) ,@body))
*/
/*
DEFUN("progv",let,1,UNEVALED,"(progv varlist body...),"
      "Evaluate body with variables dynamically bound according to varlist.\n"
      "varlist takes the same form as in let, bindings are done in parallel.")
  (sl_obj args){
  sl_obj bindings_list = XCAR(args);
  if(!LISTP(bindings_list)){
    raise_error_cstr(0, "Error, first argument to progv must be a list");
  }
  //the bindings list should be short enough that computing the length is fast
  int num_bindings = CONS_LENGTH(bindings_list);
  sl_obj bindings_vec = make_svector_obj(num_bindings + 2);
  VSET(bindings_vec, 0, thread_env->lexical_env);
  sl_obj binding;
  int i = 1;
  while(binding = POP(bindings_list)){
    VSET(bindings_vec, i++, Feval(binding));
  }
  //set last element of previous bindings vector to new vector
  VSET(thread_env->lexical_env,
       VECTOR_LENGTH(thread_env->lexical_env), bindings_vec);
  thread_env->lexical_env = bindings_vec;
  sl_obj retval = Fprogn(XCDR(args));
  thread_env->lexical_env = VREF(thread_env->lexical_env, 0);
  VSET(thread_env->lexical_env,
       VECTOR_LENGTH(thread_env->lexical_env), NIL);
  return retval;
  }*/
void init_env_symbols(void){
  INIT_SUBR(let, UNEVALED);
  INIT_SUBR(let_star, UNEVALED);
  INIT_SUBR(setq, UNEVALED);
}
