#ifndef _SL_COMMON_H_
#error "do not include predicates.h directly, include common.h instead"
#endif
#ifndef _SL_PRED_H_
#define _SL_PRED_H_
/*
  declarations of predicate functions as well as predicate macros,
  also macros to extract values from types other than sl_obj.
*/

/* Macros to check the type of an sl_obj */
/*the 2 least significant bits of an int must be 0*/
#define INTP(obj) (sl_type_bits(obj) == SL_INT0 || sl_type_bits(obj) == SL_INT1)
#define CONSP(obj) (sl_type_bits(obj) == SL_cons)
#define LISTP(obj) (CONSP(obj) || NILP(obj))
#define VECTORP(obj) (sl_type_bits(obj) == SL_vector)
#define IMMP(obj) (sl_type_bits(obj) == SL_imm)
#define SYMBOLP(obj) (sl_type_bits(obj) == SL_symbol)
#define MISCP(obj) (sl_type_bits(obj) == SL_misc)
//Numbers
/* indirect number (i.e non immediate number */
#define IND_NUMBERP(obj) (sl_type_bits(obj) == SL_number)
/* A lot of the following are not super relevent anymore since 
   I've sortof gotten rid of not 62 bit integer immediate numbers */
/* Macros to check the type of a non integer immediate */
/* non integer immediate that is a number */
#define IMM_INTP(obj) (IMMP(obj) && (XIMM(obj).tag & SL_number_int_bit))
#define IMM_NUMBERP(obj) (IMMP(obj) && (XIMM(obj).tag != SL_char))
#define FIXNUMP(obj) (INTP(obj) || IMM_INTP(obj))
//just return the uint32 from an immediate since any smaller
//typed value will be equal to it, also this currently ignores
//int64's stored in an sl_number
#define XFIXNUM(obj) (INTP(obj) ? XINT(obj) : XIMM(obj).uint32)
// || (IMM_NUMBERP(obj)))
#define NUMBERP(obj) (INTP(obj) || (IND_NUMBERP(obj)))
#define INTEGERP(obj) (INTP(obj) || BIGINTP(obj))
#define INT64P(obj) (IND_NUMBERP(obj) && XNUMBER(obj)->type == SL_int64)
//floating point numbers
#define DOUBLEP(obj) (IND_NUMBERP(obj) && XNUMBER(obj)->type == SL_double)
#define XDOUBLE(obj) (XNUMBER(obj)->float64)
#define FLOAT64P(obj) (DOUBLEP(obj))
#define XFLOAT64(obj) (XDOUBLE(obj))
#define FLOAT32P(obj) (XIMM(obj).tag == SL_float32)
#define XFLOAT32(obj) (XIMM(obj).float32)
#define FLOATP(x) (IMM_NUMBERP(x) ? FLOAT32P(x) :       \
                   IND_NUMBERP(x) ? FLOAT64P(x) : NIL)
#define XFLOAT(x) (IMM_NUMBERP(x) ? XFLOAT32(x) : XFLOAT64(x))
//bignums
#define BIGNUMP(obj) (IND_NUMBERP(obj) && SL_number_is_bignum(XNUMBER(obj)))
#define BIGFLOATP(obj) (IND_NUMBERP(obj) && XNUMBER(obj)->type == SL_bigfloat)
#define BIGINTP(obj) (IND_NUMBERP(obj) && XNUMBER(obj)->type == SL_bigint)
#define XBIGFLOAT(obj) (XNUMBER(obj)->bigfloat)
#define XBIGINT(obj) (XNUMBER(obj)->bigint)
#define CHARP(obj) (IMMP(obj) && (((sl_imm)obj).tag == SL_char))
#define XCHAR(obj) (((sl_imm)obj).char32)
#define NILP(obj) (obj == NIL)
#define STRINGP(x) (VECTORP(x) && VECTOR_TYPE(x) == SL_string)
#define SVECTORP(x) (VECTORP(x) && (VECTOR_TYPE(x) == SL_untyped_svector || \
                                    (VECTOR_TYPE(x) == SL_typed_svector)))
//the compiler should simplify this so that it only calls XVECTOR once
#define SVECTOR_OR_STRINGP(x) (SVECTORP(x) || STRINGP(x))
#define ARRAYP(x) (VECTORP(x) && (VECTOR_TYPE(x) == SL_untyped_array ||  \
                                  (VECTOR_TYPE(x) == SL_typed_array))
#define SEQUENCEP(x) (CONSP(x) || SVECTOR_OR_STRINGP(x))

#define SUBRP(obj) (MISCP(obj) && XMISC(obj)->type == SL_subr)
#define STREAMP(obj) (MISCP(obj) && XMISC_TYPE(obj) == SL_stream)
#define KEYWORDP(obj) (SYMBOLP(obj) && (XSYMPROPS(obj) & SL_SYMBOL_KEYWORD))

#define XSTREAM(obj) ((sl_stream*)XMISC(obj)->value)
#define XSUBR(obj) ((sl_subr*)XMISC(obj)->value)
#define SUBR_MAXARGS(obj) (((sl_subr*)XMISC(obj)->value)->maxargs)
#define SUBR_MINARGS(obj) (((sl_subr*)XMISC(obj)->value)->maxargs)
#define XENV(obj) ((sl_env*)XMISC(obj)->value)
//for now ignore bigints and floats
#define ZEROP(obj) (NUMBERP(obj) && XINT(obj) == 0)
#define FALSEP(obj) (NILP(obj) || ZEROP(obj))
#define TRUEP(obj) (!(FALSEP(obj)))
sl_obj Feq(sl_obj obj1, sl_obj obj2);
sl_obj Feqv(sl_obj obj1, sl_obj obj2);
sl_obj Fequal(sl_obj obj1, sl_obj obj2);
sl_obj Fequalp(sl_obj obj1, sl_obj obj2);
sl_obj Fconsp(sl_obj obj);
sl_obj Farrayp(sl_obj obj);
sl_obj Fvectorp(sl_obj obj);
sl_obj Fsvectorp(sl_obj obj);
sl_obj Fintegerp(sl_obj obj);
sl_obj Ffloatp(sl_obj obj);

//I'd like to inline this, but I'd also rather not include math_ops.h
//in everything, so for now I'm putting the definaton in predicates.c
sl_obj integer_equal(sl_obj x, sl_obj y);
/* equality predicate macros */
#define EQ(obj1,obj2) (obj1 == obj2)
//I'll change this to use a statement expresesion if necessary
#define EQV(obj1,obj2)\
  (EQ(obj1,obj2) ? 1 :                          \
   INTEGERP(obj1) && INTEGERP(obj2) ?           \
   integer_equal(obj1,obj2) : 0)
#define EQL(obj1,obj2) EQV(obj1, obj2)
//thees are just aliases for the functions
#define EQUAL(obj1,obj2) Fequal(obj1,obj2)
#define EQUALP(obj1,obj2) Fequalp(obj1,obj2)
#endif
