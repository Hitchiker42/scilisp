	.file	"cons.c"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4,,15
	.globl	Fsafe_car
	.type	Fsafe_car, @function
Fsafe_car:
.LFB110:
	.cfi_startproc
	movq	%rdi, %rdx
	xorl	%eax, %eax
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	andq	$-8, %rdi
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE110:
	.size	Fsafe_car, .-Fsafe_car
	.section	.text.unlikely
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.globl	Fsafe_cdr
	.type	Fsafe_cdr, @function
Fsafe_cdr:
.LFB112:
	.cfi_startproc
	movq	%rdi, %rdx
	xorl	%eax, %eax
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE112:
	.size	Fsafe_cdr, .-Fsafe_cdr
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"cons.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Error in function %s, in file %s, at line %d"
	.section	.text.unlikely
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4,,15
	.globl	Fnth
	.type	Fnth, @function
Fnth:
.LFB116:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L11
	testq	%rdi, %rdi
	jne	.L13
.L11:
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$2, %rax
	je	.L12
	movq	%rsi, %rax
	andl	$7, %eax
	cmpq	$7, %rax
	jne	.L13
	testl	$16777216, %esi
	je	.L13
	shrq	$32, %rsi
	testq	%rdi, %rdi
	je	.L19
.L17:
	cmpl	$0, %esi
	jg	.L47
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L48:
	subl	$1, %esi
	je	.L23
.L47:
	andq	$-8, %rdi
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L48
.L19:
	xorl	%eax, %eax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	shrq	$2, %rsi
	testq	%rdi, %rdi
	jne	.L17
	jmp	.L19
.L18:
	jne	.L19
	.p2align 4,,10
	.p2align 3
.L23:
	andq	$-8, %rdi
	movq	(%rdi), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L13:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$116, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.9919, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
	.cfi_endproc
.LFE116:
	.size	Fnth, .-Fnth
	.section	.text.unlikely
.LCOLDE4:
	.text
.LHOTE4:
	.section	.text.unlikely
.LCOLDB5:
	.text
.LHOTB5:
	.p2align 4,,15
	.globl	Ftail
	.type	Ftail, @function
Ftail:
.LFB119:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L50
	testq	%rdi, %rdi
	jne	.L52
.L50:
	movq	%rsi, %rcx
	andl	$3, %ecx
	cmpq	$2, %rcx
	je	.L51
	movq	%rsi, %rax
	andl	$7, %eax
	cmpq	$7, %rax
	jne	.L52
	testl	$16777216, %esi
	je	.L52
.L51:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L53
	movq	%rdi, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L54:
	andq	$-8, %rax
	addq	$1, %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L54
	movq	%rsi, %rax
	shrq	$32, %rsi
	shrq	$2, %rax
	cmpq	$2, %rcx
	cmove	%rax, %rsi
	movq	%rdi, %rax
	cmpl	%edx, %esi
	jge	.L53
	subl	%esi, %edx
	testl	%edx, %edx
	leal	-1(%rdx), %ecx
	je	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	subl	$1, %ecx
	andq	$-8, %rdi
	cmpl	$-1, %ecx
	movq	8(%rdi), %rdi
	jne	.L57
	movq	%rdi, %rax
.L53:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L52:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$154, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.9962, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
	.cfi_endproc
.LFE119:
	.size	Ftail, .-Ftail
	.section	.text.unlikely
.LCOLDE5:
	.text
.LHOTE5:
	.section	.text.unlikely
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4,,15
	.type	Fcar.part.0, @function
Fcar.part.0:
.LFB162:
	.cfi_startproc
	xorl	%esi, %esi
	movl	$.LC3, %edi
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$66, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.9860, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
	.cfi_endproc
.LFE162:
	.size	Fcar.part.0, .-Fcar.part.0
	.section	.text.unlikely
.LCOLDE6:
	.text
.LHOTE6:
	.section	.text.unlikely
.LCOLDB7:
	.text
.LHOTB7:
	.p2align 4,,15
	.globl	Fcar
	.type	Fcar, @function
Fcar:
.LFB111:
	.cfi_startproc
	movq	%rdi, %rax
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L84
	testq	%rdi, %rdi
	jne	.L85
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	andq	$-8, %rdi
	movq	(%rdi), %rax
	ret
.L85:
	pushq	%rax
	.cfi_def_cfa_offset 16
	call	Fcar.part.0
	.cfi_endproc
.LFE111:
	.size	Fcar, .-Fcar
	.section	.text.unlikely
.LCOLDE7:
	.text
.LHOTE7:
	.section	.text.unlikely
.LCOLDB8:
	.text
.LHOTB8:
	.p2align 4,,15
	.globl	Fcaar
	.type	Fcaar, @function
Fcaar:
.LFB132:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L96
	testq	%rdi, %rdi
	jne	.L91
	xorl	%eax, %eax
.L90:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L97
	testq	%rax, %rax
	je	.L90
.L91:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L97:
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE132:
	.size	Fcaar, .-Fcaar
	.section	.text.unlikely
.LCOLDE8:
	.text
.LHOTE8:
	.section	.text.unlikely
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4,,15
	.globl	Fcaaar
	.type	Fcaaar, @function
Fcaaar:
.LFB136:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L115
	testq	%rdi, %rdi
	jne	.L105
	xorl	%eax, %eax
.L102:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L116
.L111:
	testq	%rax, %rax
	je	.L102
.L105:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L116:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L111
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE136:
	.size	Fcaaar, .-Fcaaar
	.section	.text.unlikely
.LCOLDE9:
	.text
.LHOTE9:
	.section	.text.unlikely
.LCOLDB10:
	.text
.LHOTB10:
	.p2align 4,,15
	.globl	Fcaaaar
	.type	Fcaaaar, @function
Fcaaaar:
.LFB144:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L141
	testq	%rdi, %rdi
	jne	.L124
	xorl	%eax, %eax
.L121:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L142
.L136:
	testq	%rax, %rax
	je	.L121
.L124:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L142:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L136
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L136
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE144:
	.size	Fcaaaar, .-Fcaaaar
	.section	.text.unlikely
.LCOLDE10:
	.text
.LHOTE10:
	.section	.text.unlikely
.LCOLDB11:
	.text
.LHOTB11:
	.p2align 4,,15
	.type	Fcdr.part.1, @function
Fcdr.part.1:
.LFB163:
	.cfi_startproc
	xorl	%esi, %esi
	movl	$.LC3, %edi
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$84, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.9878, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
	.cfi_endproc
.LFE163:
	.size	Fcdr.part.1, .-Fcdr.part.1
	.section	.text.unlikely
.LCOLDE11:
	.text
.LHOTE11:
	.section	.text.unlikely
.LCOLDB12:
	.text
.LHOTB12:
	.p2align 4,,15
	.globl	Fcdr
	.type	Fcdr, @function
Fcdr:
.LFB113:
	.cfi_startproc
	movq	%rdi, %rax
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L150
	testq	%rdi, %rdi
	jne	.L151
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	ret
.L151:
	pushq	%rax
	.cfi_def_cfa_offset 16
	call	Fcdr.part.1
	.cfi_endproc
.LFE113:
	.size	Fcdr, .-Fcdr
	.section	.text.unlikely
.LCOLDE12:
	.text
.LHOTE12:
	.section	.text.unlikely
.LCOLDB13:
	.text
.LHOTB13:
	.p2align 4,,15
	.globl	Fcadr
	.type	Fcadr, @function
Fcadr:
.LFB133:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L164
	testq	%rdi, %rdi
	jne	.L165
	xorl	%eax, %eax
.L156:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L166
	testq	%rax, %rax
	je	.L156
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L166:
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L165:
	.cfi_restore_state
	call	Fcdr.part.1
	.cfi_endproc
.LFE133:
	.size	Fcadr, .-Fcadr
	.section	.text.unlikely
.LCOLDE13:
	.text
.LHOTE13:
	.section	.text.unlikely
.LCOLDB14:
	.text
.LHOTB14:
	.p2align 4,,15
	.globl	Fcdar
	.type	Fcdar, @function
Fcdar:
.LFB134:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L179
	testq	%rdi, %rdi
	jne	.L180
	xorl	%eax, %eax
.L171:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L181
	testq	%rax, %rax
	je	.L171
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L181:
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L180:
	.cfi_restore_state
	call	Fcar.part.0
	.cfi_endproc
.LFE134:
	.size	Fcdar, .-Fcdar
	.section	.text.unlikely
.LCOLDE14:
	.text
.LHOTE14:
	.section	.text.unlikely
.LCOLDB15:
	.text
.LHOTB15:
	.p2align 4,,15
	.globl	Fcddr
	.type	Fcddr, @function
Fcddr:
.LFB135:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L192
	testq	%rdi, %rdi
	jne	.L187
	xorl	%eax, %eax
.L186:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L193
	testq	%rax, %rax
	je	.L186
.L187:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L193:
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE135:
	.size	Fcddr, .-Fcddr
	.section	.text.unlikely
.LCOLDE15:
	.text
.LHOTE15:
	.section	.text.unlikely
.LCOLDB16:
	.text
.LHOTB16:
	.p2align 4,,15
	.globl	Fcaadr
	.type	Fcaadr, @function
Fcaadr:
.LFB137:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L212
	testq	%rdi, %rdi
	jne	.L213
	xorl	%eax, %eax
.L198:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L214
.L206:
	testq	%rax, %rax
	je	.L198
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L214:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L206
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L213:
	.cfi_restore_state
	call	Fcdr.part.1
	.cfi_endproc
.LFE137:
	.size	Fcaadr, .-Fcaadr
	.section	.text.unlikely
.LCOLDE16:
	.text
.LHOTE16:
	.section	.text.unlikely
.LCOLDB17:
	.text
.LHOTB17:
	.p2align 4,,15
	.globl	Fcadar
	.type	Fcadar, @function
Fcadar:
.LFB138:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L232
	testq	%rdi, %rdi
	jne	.L222
	xorl	%eax, %eax
.L219:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L233
	testq	%rax, %rax
	je	.L219
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L233:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L234
	testq	%rax, %rax
	je	.L219
.L222:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L234:
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE138:
	.size	Fcadar, .-Fcadar
	.section	.text.unlikely
.LCOLDE17:
	.text
.LHOTE17:
	.section	.text.unlikely
.LCOLDB18:
	.text
.LHOTB18:
	.p2align 4,,15
	.globl	Fcaddr
	.type	Fcaddr, @function
Fcaddr:
.LFB139:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L255
	testq	%rdi, %rdi
	jne	.L242
	xorl	%eax, %eax
.L239:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L256
	testq	%rax, %rax
	je	.L239
.L242:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L256:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L257
	testq	%rax, %rax
	je	.L239
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L257:
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE139:
	.size	Fcaddr, .-Fcaddr
	.section	.text.unlikely
.LCOLDE18:
	.text
.LHOTE18:
	.section	.text.unlikely
.LCOLDB19:
	.text
.LHOTB19:
	.p2align 4,,15
	.globl	Fcdaar
	.type	Fcdaar, @function
Fcdaar:
.LFB140:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L278
	testq	%rdi, %rdi
	jne	.L265
	xorl	%eax, %eax
.L262:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L279
	testq	%rax, %rax
	je	.L262
.L265:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L279:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L280
	testq	%rax, %rax
	je	.L262
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L280:
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE140:
	.size	Fcdaar, .-Fcdaar
	.section	.text.unlikely
.LCOLDE19:
	.text
.LHOTE19:
	.section	.text.unlikely
.LCOLDB20:
	.text
.LHOTB20:
	.p2align 4,,15
	.globl	Fcdadr
	.type	Fcdadr, @function
Fcdadr:
.LFB141:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L298
	testq	%rdi, %rdi
	jne	.L288
	xorl	%eax, %eax
.L285:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L299
	testq	%rax, %rax
	je	.L285
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L299:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L300
	testq	%rax, %rax
	je	.L285
.L288:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L300:
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE141:
	.size	Fcdadr, .-Fcdadr
	.section	.text.unlikely
.LCOLDE20:
	.text
.LHOTE20:
	.section	.text.unlikely
.LCOLDB21:
	.text
.LHOTB21:
	.p2align 4,,15
	.globl	Fcddar
	.type	Fcddar, @function
Fcddar:
.LFB142:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L319
	testq	%rdi, %rdi
	jne	.L320
	xorl	%eax, %eax
.L305:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L321
.L313:
	testq	%rax, %rax
	je	.L305
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L321:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L313
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L320:
	.cfi_restore_state
	call	Fcar.part.0
	.cfi_endproc
.LFE142:
	.size	Fcddar, .-Fcddar
	.section	.text.unlikely
.LCOLDE21:
	.text
.LHOTE21:
	.section	.text.unlikely
.LCOLDB22:
	.text
.LHOTB22:
	.p2align 4,,15
	.globl	Fcdddr
	.type	Fcdddr, @function
Fcdddr:
.LFB143:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L339
	testq	%rdi, %rdi
	jne	.L329
	xorl	%eax, %eax
.L326:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L340
.L335:
	testq	%rax, %rax
	je	.L326
.L329:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L340:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L335
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE143:
	.size	Fcdddr, .-Fcdddr
	.section	.text.unlikely
.LCOLDE22:
	.text
.LHOTE22:
	.section	.text.unlikely
.LCOLDB23:
	.text
.LHOTB23:
	.p2align 4,,15
	.globl	Fcaaadr
	.type	Fcaaadr, @function
Fcaaadr:
.LFB145:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L366
	testq	%rdi, %rdi
	jne	.L367
	xorl	%eax, %eax
.L345:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L368
.L358:
	testq	%rax, %rax
	je	.L345
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L368:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L358
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L358
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L367:
	.cfi_restore_state
	call	Fcdr.part.1
	.cfi_endproc
.LFE145:
	.size	Fcaaadr, .-Fcaaadr
	.section	.text.unlikely
.LCOLDE23:
	.text
.LHOTE23:
	.section	.text.unlikely
.LCOLDB24:
	.text
.LHOTB24:
	.p2align 4,,15
	.globl	Fcaadar
	.type	Fcaadar, @function
Fcaadar:
.LFB146:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L393
	testq	%rdi, %rdi
	jne	.L378
	xorl	%eax, %eax
.L373:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L394
	testq	%rax, %rax
	je	.L373
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L394:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L395
.L388:
	testq	%rax, %rax
	je	.L373
.L378:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L395:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L388
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE146:
	.size	Fcaadar, .-Fcaadar
	.section	.text.unlikely
.LCOLDE24:
	.text
.LHOTE24:
	.section	.text.unlikely
.LCOLDB25:
	.text
.LHOTB25:
	.p2align 4,,15
	.globl	Fcaaddr
	.type	Fcaaddr, @function
Fcaaddr:
.LFB147:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L422
	testq	%rdi, %rdi
	jne	.L403
	xorl	%eax, %eax
.L400:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L423
	testq	%rax, %rax
	je	.L400
.L403:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L423:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L424
.L415:
	testq	%rax, %rax
	je	.L400
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L424:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L415
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE147:
	.size	Fcaaddr, .-Fcaaddr
	.section	.text.unlikely
.LCOLDE25:
	.text
.LHOTE25:
	.section	.text.unlikely
.LCOLDB26:
	.text
.LHOTB26:
	.p2align 4,,15
	.globl	Fcadaar
	.type	Fcadaar, @function
Fcadaar:
.LFB148:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L449
	testq	%rdi, %rdi
	jne	.L432
	xorl	%eax, %eax
.L429:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L450
.L444:
	testq	%rax, %rax
	je	.L429
.L432:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L450:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L451
	testq	%rax, %rax
	je	.L429
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L451:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L444
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE148:
	.size	Fcadaar, .-Fcadaar
	.section	.text.unlikely
.LCOLDE26:
	.text
.LHOTE26:
	.section	.text.unlikely
.LCOLDB27:
	.text
.LHOTB27:
	.p2align 4,,15
	.globl	Fcadadr
	.type	Fcadadr, @function
Fcadadr:
.LFB149:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L478
	testq	%rdi, %rdi
	jne	.L461
	xorl	%eax, %eax
.L456:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L479
.L470:
	testq	%rax, %rax
	je	.L456
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L479:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L480
	testq	%rax, %rax
	je	.L456
.L461:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L480:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L470
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE149:
	.size	Fcadadr, .-Fcadadr
	.section	.text.unlikely
.LCOLDE27:
	.text
.LHOTE27:
	.section	.text.unlikely
.LCOLDB28:
	.text
.LHOTB28:
	.p2align 4,,15
	.globl	Fcaddar
	.type	Fcaddar, @function
Fcaddar:
.LFB150:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L506
	testq	%rdi, %rdi
	jne	.L491
	xorl	%eax, %eax
.L485:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L507
.L499:
	testq	%rax, %rax
	je	.L485
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L507:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L499
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L508
	testq	%rax, %rax
	je	.L485
.L491:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L508:
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE150:
	.size	Fcaddar, .-Fcaddar
	.section	.text.unlikely
.LCOLDE28:
	.text
.LHOTE28:
	.section	.text.unlikely
.LCOLDB29:
	.text
.LHOTB29:
	.p2align 4,,15
	.globl	Fcadddr
	.type	Fcadddr, @function
Fcadddr:
.LFB151:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L536
	testq	%rdi, %rdi
	jne	.L516
	xorl	%eax, %eax
.L513:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L537
.L530:
	testq	%rax, %rax
	je	.L513
.L516:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L537:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L530
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L538
	testq	%rax, %rax
	je	.L513
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L538:
	andq	$-8, %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE151:
	.size	Fcadddr, .-Fcadddr
	.section	.text.unlikely
.LCOLDE29:
	.text
.LHOTE29:
	.section	.text.unlikely
.LCOLDB30:
	.text
.LHOTB30:
	.p2align 4,,15
	.globl	Fcdaaar
	.type	Fcdaaar, @function
Fcdaaar:
.LFB152:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L566
	testq	%rdi, %rdi
	jne	.L546
	xorl	%eax, %eax
.L543:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L567
.L560:
	testq	%rax, %rax
	je	.L543
.L546:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L567:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L560
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L568
	testq	%rax, %rax
	je	.L543
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L568:
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE152:
	.size	Fcdaaar, .-Fcdaaar
	.section	.text.unlikely
.LCOLDE30:
	.text
.LHOTE30:
	.section	.text.unlikely
.LCOLDB31:
	.text
.LHOTB31:
	.p2align 4,,15
	.globl	Fcdaadr
	.type	Fcdaadr, @function
Fcdaadr:
.LFB153:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L594
	testq	%rdi, %rdi
	jne	.L579
	xorl	%eax, %eax
.L573:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L595
.L587:
	testq	%rax, %rax
	je	.L573
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L595:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L587
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L596
	testq	%rax, %rax
	je	.L573
.L579:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L596:
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE153:
	.size	Fcdaadr, .-Fcdaadr
	.section	.text.unlikely
.LCOLDE31:
	.text
.LHOTE31:
	.section	.text.unlikely
.LCOLDB32:
	.text
.LHOTB32:
	.p2align 4,,15
	.globl	Fcdadar
	.type	Fcdadar, @function
Fcdadar:
.LFB154:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L623
	testq	%rdi, %rdi
	jne	.L606
	xorl	%eax, %eax
.L601:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L624
.L615:
	testq	%rax, %rax
	je	.L601
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L624:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L625
	testq	%rax, %rax
	je	.L601
.L606:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L625:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L615
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE154:
	.size	Fcdadar, .-Fcdadar
	.section	.text.unlikely
.LCOLDE32:
	.text
.LHOTE32:
	.section	.text.unlikely
.LCOLDB33:
	.text
.LHOTB33:
	.p2align 4,,15
	.globl	Fcdaddr
	.type	Fcdaddr, @function
Fcdaddr:
.LFB155:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L650
	testq	%rdi, %rdi
	jne	.L633
	xorl	%eax, %eax
.L630:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L651
.L645:
	testq	%rax, %rax
	je	.L630
.L633:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L651:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L652
	testq	%rax, %rax
	je	.L630
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L652:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L645
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE155:
	.size	Fcdaddr, .-Fcdaddr
	.section	.text.unlikely
.LCOLDE33:
	.text
.LHOTE33:
	.section	.text.unlikely
.LCOLDB34:
	.text
.LHOTB34:
	.p2align 4,,15
	.globl	Fcddaar
	.type	Fcddaar, @function
Fcddaar:
.LFB156:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L679
	testq	%rdi, %rdi
	jne	.L660
	xorl	%eax, %eax
.L657:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L680
	testq	%rax, %rax
	je	.L657
.L660:
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L680:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L681
.L672:
	testq	%rax, %rax
	je	.L657
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L681:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L672
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE156:
	.size	Fcddaar, .-Fcddaar
	.section	.text.unlikely
.LCOLDE34:
	.text
.LHOTE34:
	.section	.text.unlikely
.LCOLDB35:
	.text
.LHOTB35:
	.p2align 4,,15
	.globl	Fcddadr
	.type	Fcddadr, @function
Fcddadr:
.LFB157:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L706
	testq	%rdi, %rdi
	jne	.L691
	xorl	%eax, %eax
.L686:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L707
	testq	%rax, %rax
	je	.L686
	call	Fcar.part.0
	.p2align 4,,10
	.p2align 3
.L707:
	andq	$-8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L708
.L701:
	testq	%rax, %rax
	je	.L686
.L691:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L708:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L701
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE157:
	.size	Fcddadr, .-Fcddadr
	.section	.text.unlikely
.LCOLDE35:
	.text
.LHOTE35:
	.section	.text.unlikely
.LCOLDB36:
	.text
.LHOTB36:
	.p2align 4,,15
	.globl	Fcdddar
	.type	Fcdddar, @function
Fcdddar:
.LFB158:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L734
	testq	%rdi, %rdi
	jne	.L735
	xorl	%eax, %eax
.L713:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L734:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L736
.L726:
	testq	%rax, %rax
	je	.L713
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L736:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L726
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L726
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L735:
	.cfi_restore_state
	call	Fcar.part.0
	.cfi_endproc
.LFE158:
	.size	Fcdddar, .-Fcdddar
	.section	.text.unlikely
.LCOLDE36:
	.text
.LHOTE36:
	.section	.text.unlikely
.LCOLDB37:
	.text
.LHOTB37:
	.p2align 4,,15
	.globl	Fcddddr
	.type	Fcddddr, @function
Fcddddr:
.LFB159:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L761
	testq	%rdi, %rdi
	jne	.L744
	xorl	%eax, %eax
.L741:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	andq	$-8, %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L762
.L756:
	testq	%rax, %rax
	je	.L741
.L744:
	call	Fcdr.part.1
	.p2align 4,,10
	.p2align 3
.L762:
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L756
	andq	$-8, %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L756
	andq	$-8, %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE159:
	.size	Fcddddr, .-Fcddddr
	.section	.text.unlikely
.LCOLDE37:
	.text
.LHOTE37:
	.section	.text.unlikely
.LCOLDB38:
	.text
.LHOTB38:
	.p2align 4,,15
	.globl	Fpop
	.type	Fpop, @function
Fpop:
.LFB124:
	.cfi_startproc
	andq	$-8, %rdi
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	jne	.L770
	movq	(%rdi), %rdx
	andq	$-8, %rdx
	movq	(%rdx), %rax
	movq	%rax, %rsi
	andl	$7, %esi
	cmpq	$1, %rsi
	je	.L765
	testq	%rax, %rax
	jne	.L771
	xorl	%eax, %eax
	movq	%rcx, (%rdx)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	andq	$-8, %rax
	movq	%rax, %rcx
	movq	(%rax), %rax
	movq	8(%rcx), %rcx
	movq	%rcx, (%rdx)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L770:
	.cfi_restore_state
	movl	$224, %r8d
.L769:
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$.LC2, %ecx
	movl	$__func__.10013, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L771:
	movl	$229, %r8d
	jmp	.L769
	.cfi_endproc
.LFE124:
	.size	Fpop, .-Fpop
	.section	.text.unlikely
.LCOLDE38:
	.text
.LHOTE38:
	.section	.text.unlikely
.LCOLDB39:
	.text
.LHOTB39:
	.p2align 4,,15
	.globl	Fassoc
	.type	Fassoc, @function
Fassoc:
.LFB128:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	andl	$7, %edx
	movq	%rdi, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	cmpq	$1, %rdx
	je	.L779
	testq	%rsi, %rsi
	jne	.L774
.L780:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L776:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L787
	movq	%rbp, %rsi
	call	Fequal
	testq	%rax, %rax
	jne	.L788
.L778:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L780
.L779:
	movq	%rsi, %rbx
	andq	$-8, %rbx
	movq	(%rbx), %rdi
	movq	%rdi, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L776
	movq	%rdi, %rax
	movq	%rbp, %rsi
	andq	$-8, %rax
	movq	(%rax), %rdi
	call	Fequal
	testq	%rax, %rax
	je	.L778
.L788:
	movq	(%rbx), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L774:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$350, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.10096, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L787:
	call	Fcar.part.0
	.cfi_endproc
.LFE128:
	.size	Fassoc, .-Fassoc
	.section	.text.unlikely
.LCOLDE39:
	.text
.LHOTE39:
	.section	.text.unlikely
.LCOLDB40:
	.text
.LHOTB40:
	.p2align 4,,15
	.globl	Frassoc
	.type	Frassoc, @function
Frassoc:
.LFB129:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	andl	$7, %edx
	movq	%rdi, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	cmpq	$1, %rdx
	je	.L796
	testq	%rsi, %rsi
	jne	.L791
.L797:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L804
	movq	%rbp, %rsi
	call	Fequal
	testq	%rax, %rax
	jne	.L805
.L795:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L797
.L796:
	movq	%rsi, %rbx
	andq	$-8, %rbx
	movq	(%rbx), %rdi
	movq	%rdi, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L793
	movq	%rdi, %rax
	movq	%rbp, %rsi
	andq	$-8, %rax
	movq	(%rax), %rdi
	call	Fequal
	testq	%rax, %rax
	je	.L795
.L805:
	movq	(%rbx), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L791:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$355, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.10111, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L804:
	call	Fcar.part.0
	.cfi_endproc
.LFE129:
	.size	Frassoc, .-Frassoc
	.section	.text.unlikely
.LCOLDE40:
	.text
.LHOTE40:
	.section	.text.unlikely
.LCOLDB41:
	.text
.LHOTB41:
	.p2align 4,,15
	.globl	Fassq
	.type	Fassq, @function
Fassq:
.LFB130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	andl	$7, %edx
	movq	%rdi, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	cmpq	$1, %rdx
	je	.L813
	testq	%rsi, %rsi
	jne	.L808
.L814:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L821
	movq	%rbp, %rsi
	call	Feq
	testq	%rax, %rax
	jne	.L822
.L812:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L814
.L813:
	movq	%rsi, %rbx
	andq	$-8, %rbx
	movq	(%rbx), %rdi
	movq	%rdi, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L810
	movq	%rdi, %rax
	movq	%rbp, %rsi
	andq	$-8, %rax
	movq	(%rax), %rdi
	call	Feq
	testq	%rax, %rax
	je	.L812
.L822:
	movq	(%rbx), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L808:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$360, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.10126, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L821:
	call	Fcar.part.0
	.cfi_endproc
.LFE130:
	.size	Fassq, .-Fassq
	.section	.text.unlikely
.LCOLDE41:
	.text
.LHOTE41:
	.section	.text.unlikely
.LCOLDB42:
	.text
.LHOTB42:
	.p2align 4,,15
	.globl	Frassq
	.type	Frassq, @function
Frassq:
.LFB131:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	andl	$7, %edx
	movq	%rdi, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	cmpq	$1, %rdx
	je	.L830
	testq	%rsi, %rsi
	jne	.L825
.L831:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L838
	movq	%rbp, %rsi
	call	Feq
	testq	%rax, %rax
	jne	.L839
.L829:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L831
.L830:
	movq	%rsi, %rbx
	andq	$-8, %rbx
	movq	(%rbx), %rdi
	movq	%rdi, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	jne	.L827
	movq	%rdi, %rax
	movq	%rbp, %rsi
	andq	$-8, %rax
	movq	8(%rax), %rdi
	call	Feq
	testq	%rax, %rax
	je	.L829
.L839:
	movq	(%rbx), %rax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L825:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$365, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.10141, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L838:
	call	Fcdr.part.1
	.cfi_endproc
.LFE131:
	.size	Frassq, .-Frassq
	.section	.text.unlikely
.LCOLDE42:
	.text
.LHOTE42:
	.section	.text.unlikely
.LCOLDB43:
.LHOTB43:
	.type	sl_malloc.part.6, @function
sl_malloc.part.6:
.LFB168:
	.cfi_startproc
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE168:
	.size	sl_malloc.part.6, .-sl_malloc.part.6
.LCOLDE43:
.LHOTE43:
.LCOLDB44:
.LHOTB44:
	.type	sl_malloc_atomic.part.7, @function
sl_malloc_atomic.part.7:
.LFB169:
	.cfi_startproc
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE169:
	.size	sl_malloc_atomic.part.7, .-sl_malloc_atomic.part.7
.LCOLDE44:
.LHOTE44:
.LCOLDB45:
	.text
.LHOTB45:
	.p2align 4,,15
	.globl	Fcons
	.type	Fcons, @function
Fcons:
.LFB109:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movl	$16, %edi
	movq	%rsi, %rbx
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	je	.L847
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$1, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L847:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE109:
	.size	Fcons, .-Fcons
	.section	.text.unlikely
.LCOLDE45:
	.text
.LHOTE45:
	.section	.text.unlikely
.LCOLDB46:
	.text
.LHOTB46:
	.p2align 4,,15
	.globl	Flist
	.type	Flist, @function
Flist:
.LFB114:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	testq	%rdi, %rdi
	je	.L852
	movq	%rsi, %r12
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L851:
	movl	$16, %edi
	movq	-8(%r12,%rbx,8), %r13
	call	GC_malloc
	testq	%rax, %rax
	je	.L855
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	orq	$1, %rax
	subq	$1, %rbx
	movq	%rax, %rbp
	jne	.L851
.L849:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	movq	%rbp, %rax
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
.L852:
	.cfi_restore_state
	xorl	%ebp, %ebp
	jmp	.L849
.L855:
	call	sl_malloc.part.6
	.cfi_endproc
.LFE114:
	.size	Flist, .-Flist
	.section	.text.unlikely
.LCOLDE46:
	.text
.LHOTE46:
	.section	.text.unlikely
.LCOLDB47:
	.text
.LHOTB47:
	.p2align 4,,15
	.globl	Flist_star
	.type	Flist_star, @function
Flist_star:
.LFB115:
	.cfi_startproc
	cmpq	$1, %rdi
	je	.L863
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	leaq	-2(%rdi), %rbx
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	movq	-8(%rsi,%rdi,8), %rbp
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%rax, %rbx
.L860:
	movl	$16, %edi
	movq	(%r12,%rbx,8), %r13
	call	GC_malloc
	testq	%rax, %rax
	je	.L864
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	orq	$1, %rax
	testq	%rbx, %rbx
	movq	%rax, %rbp
	leaq	-1(%rbx), %rax
	jne	.L861
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	movq	%rbp, %rax
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L863:
	movq	(%rsi), %rax
	ret
.L864:
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -40
	.cfi_offset 6, -32
	.cfi_offset 12, -24
	.cfi_offset 13, -16
	call	sl_malloc.part.6
	.cfi_endproc
.LFE115:
	.size	Flist_star, .-Flist_star
	.section	.text.unlikely
.LCOLDE47:
	.text
.LHOTE47:
	.section	.text.unlikely
.LCOLDB48:
	.text
.LHOTB48:
	.p2align 4,,15
	.type	reverse_acc, @function
reverse_acc:
.LFB121:
	.cfi_startproc
	testq	%rsi, %rsi
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %rbp
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	je	.L866
	movl	$16, %edi
	movq	(%rsi), %r12
	call	GC_malloc
	testq	%rax, %rax
	je	.L873
	movq	%rax, %rdx
	movq	%r12, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rdx
	movq	%rdx, 8(%rbp)
	movq	8(%rbx), %rdx
	movq	%rdx, %rcx
	andl	$7, %ecx
	cmpq	$1, %rcx
	je	.L868
	movq	%rdx, 8(%rbp)
.L866:
	movq	%rbp, %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L873:
	.cfi_restore_state
	call	sl_malloc.part.6
.L868:
	movq	%rdx, %rsi
	andq	$-8, %rax
	andq	$-8, %rsi
	movq	%rax, %rdi
	call	reverse_acc
	.cfi_endproc
.LFE121:
	.size	reverse_acc, .-reverse_acc
	.section	.text.unlikely
.LCOLDE48:
	.text
.LHOTE48:
	.section	.text.unlikely
.LCOLDB49:
	.text
.LHOTB49:
	.p2align 4,,15
	.globl	Fpush
	.type	Fpush, @function
Fpush:
.LFB125:
	.cfi_startproc
	andq	$-8, %rdi
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	andq	$-8, %rax
	cmpq	$0, 8(%rax)
	jne	.L878
	movq	(%rax), %rbx
	movq	(%rdi), %r12
	movl	$16, %edi
	andq	$-8, %rbx
	movq	(%rbx), %rbp
	call	GC_malloc
	testq	%rax, %rax
	je	.L879
	movq	%r12, (%rax)
	movq	%rbp, 8(%rax)
	orq	$1, %rax
	movq	%rax, (%rbx)
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L878:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$239, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.10027, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L879:
	call	sl_malloc.part.6
	.cfi_endproc
.LFE125:
	.size	Fpush, .-Fpush
	.section	.text.unlikely
.LCOLDE49:
	.text
.LHOTE49:
	.section	.text.unlikely
.LCOLDB50:
	.text
.LHOTB50:
	.p2align 4,,15
	.globl	Fnappend
	.type	Fnappend, @function
Fnappend:
.LFB127:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L890
	cmpq	$1, %rdi
	je	.L905
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	movq	(%rsi), %rbx
	testq	%rbx, %rbx
	jne	.L883
	movl	$1, %r13d
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L891:
	movq	%rax, %r13
.L885:
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L906
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	leaq	1(%r13), %rax
	movslq	%r13d, %r8
	movq	-8(%rbp,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.L891
	subq	$1, %r12
	cmpq	%r12, %r13
	jae	.L892
	.p2align 4,,10
	.p2align 3
.L888:
	movq	%rdx, %rax
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L887
	testq	%rdx, %rdx
	jne	.L907
	.p2align 4,,10
	.p2align 3
.L887:
	movq	%rdx, %rcx
	andq	$-8, %rcx
	movq	8(%rcx), %rdx
	movq	%rdx, %rdi
	andl	$7, %edi
	cmpq	$1, %rdi
	je	.L887
	movq	8(%rbp,%r8,8), %rdx
	addq	$1, %r8
	cmpq	%r12, %r8
	movq	%rdx, 8(%rcx)
	jb	.L888
	movq	%rbx, %rax
.L902:
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 8
	ret
.L890:
	xorl	%eax, %eax
	ret
.L905:
	movq	(%rsi), %rax
	ret
.L883:
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -40
	.cfi_offset 6, -32
	.cfi_offset 12, -24
	.cfi_offset 13, -16
	movq	%rbx, %rdx
	xorl	%r8d, %r8d
	subq	$1, %r12
	jmp	.L888
.L892:
	xorl	%eax, %eax
	jmp	.L902
.L907:
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$312, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.10078, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L906:
	call	sl_malloc.part.6
	.cfi_endproc
.LFE127:
	.size	Fnappend, .-Fnappend
	.section	.text.unlikely
.LCOLDE50:
	.text
.LHOTE50:
	.section	.text.unlikely
.LCOLDB51:
	.text
.LHOTB51:
	.p2align 4,,15
	.globl	Fappend
	.type	Fappend, @function
Fappend:
.LFB126:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L925
	cmpq	$1, %rdi
	je	.L962
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdi, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	movq	(%rsi), %r15
	movq	%r15, %rax
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L963
	testq	%r15, %r15
	jne	.L964
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L917
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	movq	%rax, 8(%rsp)
	subq	$1, %r13
.L923:
	movq	8(%rsp), %rax
	xorl	%r12d, %r12d
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L919:
	addq	$1, %r12
	cmpq	%r13, %r12
	movq	(%r14,%r12,8), %r15
	je	.L965
.L920:
	movq	%r15, %rcx
	andl	$7, %ecx
	cmpq	$1, %rcx
	je	.L918
	testl	%r12d, %r12d
	jle	.L916
	testq	%r15, %r15
	jne	.L966
.L916:
	testq	%r15, %r15
	je	.L919
	andq	$-8, %rax
	movl	$16, %edi
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L917
	addq	$1, %r12
	movq	%r15, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	cmpq	%r13, %r12
	movq	%rax, 8(%rbx)
	movq	(%r14,%r12,8), %r15
	jne	.L920
.L965:
	movq	%r15, %rcx
	andl	$7, %ecx
	cmpq	$1, %rcx
	je	.L922
	andq	$-8, %rax
	movq	%r15, 8(%rax)
	movq	8(%rsp), %rax
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_restore 14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_restore 15
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L967:
	.cfi_restore_state
	movq	%r15, %rcx
	movq	%rbp, (%rax)
	movq	$0, 8(%rax)
	andl	$7, %ecx
	orq	$1, %rax
	cmpq	$1, %rcx
	movq	%rax, 8(%rbx)
	jne	.L916
.L918:
	movq	%r15, %rdx
	andq	$-8, %rax
	movl	$16, %edi
	andq	$-8, %rdx
	movq	%rax, %rbx
	movq	(%rdx), %rbp
	movq	8(%rdx), %r15
	call	GC_malloc
	testq	%rax, %rax
	jne	.L967
.L917:
	call	sl_malloc.part.6
	.p2align 4,,10
	.p2align 3
.L922:
	movq	%r15, %rdx
	andq	$-8, %rax
	movl	$16, %edi
	andq	$-8, %rdx
	movq	%rax, %rbx
	movq	(%rdx), %rbp
	movq	8(%rdx), %r15
	call	GC_malloc
	testq	%rax, %rax
	je	.L917
	movq	%r15, %rcx
	movq	%rbp, (%rax)
	movq	$0, 8(%rax)
	andl	$7, %ecx
	orq	$1, %rax
	cmpq	$1, %rcx
	movq	%rax, 8(%rbx)
	je	.L922
	movq	8(%rsp), %rax
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_restore 14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_restore 15
	.cfi_def_cfa_offset 8
	ret
.L925:
	xorl	%eax, %eax
	ret
.L963:
	.cfi_def_cfa_offset 80
	.cfi_offset 3, -56
	.cfi_offset 6, -48
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	.cfi_offset 15, -16
	movq	%r15, %rbx
	movl	$16, %edi
	andq	$-8, %rbx
	movq	(%rbx), %rbp
	call	GC_malloc
	testq	%rax, %rax
	je	.L917
	movq	$0, 8(%rax)
	movq	%rbp, (%rax)
	orq	$1, %rax
	movq	%rax, 8(%rsp)
	movq	8(%rbx), %r15
	subq	$1, %r13
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_def_cfa_offset 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	(%rsi), %rax
	ret
.L964:
	.cfi_def_cfa_offset 80
	.cfi_offset 3, -56
	.cfi_offset 6, -48
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	.cfi_offset 15, -16
	call	Fcar.part.0
.L966:
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$266, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.10045, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
	.cfi_endproc
.LFE126:
	.size	Fappend, .-Fappend
	.section	.text.unlikely
.LCOLDE51:
	.text
.LHOTE51:
	.section	.text.unlikely
.LCOLDB52:
	.text
.LHOTB52:
	.p2align 4,,15
	.type	__bswap_32, @function
__bswap_32:
.LFB11:
	.cfi_startproc
	movl	%edi, %eax
	bswap	%eax
	ret
	.cfi_endproc
.LFE11:
	.size	__bswap_32, .-__bswap_32
	.section	.text.unlikely
.LCOLDE52:
	.text
.LHOTE52:
	.section	.text.unlikely
.LCOLDB53:
	.text
.LHOTB53:
	.p2align 4,,15
	.type	__bswap_64, @function
__bswap_64:
.LFB12:
	.cfi_startproc
	movq	%rdi, %rax
	bswap	%rax
	ret
	.cfi_endproc
.LFE12:
	.size	__bswap_64, .-__bswap_64
	.section	.text.unlikely
.LCOLDE53:
	.text
.LHOTE53:
	.section	.text.unlikely
.LCOLDB54:
	.text
.LHOTB54:
	.p2align 4,,15
	.type	sl_malloc, @function
sl_malloc:
.LFB60:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	call	GC_malloc
	testq	%rax, %rax
	jne	.L971
	testq	%rbx, %rbx
	jne	.L979
.L971:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L979:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE60:
	.size	sl_malloc, .-sl_malloc
	.section	.text.unlikely
.LCOLDE54:
	.text
.LHOTE54:
	.section	.text.unlikely
.LCOLDB55:
	.text
.LHOTB55:
	.p2align 4,,15
	.type	sl_realloc, @function
sl_realloc:
.LFB61:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rsi, %rbx
	call	GC_realloc
	testq	%rax, %rax
	jne	.L981
	testq	%rbx, %rbx
	jne	.L989
.L981:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L989:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE61:
	.size	sl_realloc, .-sl_realloc
	.section	.text.unlikely
.LCOLDE55:
	.text
.LHOTE55:
	.section	.text.unlikely
.LCOLDB56:
	.text
.LHOTB56:
	.p2align 4,,15
	.type	sl_malloc_atomic, @function
sl_malloc_atomic:
.LFB62:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	call	GC_malloc_atomic
	testq	%rax, %rax
	jne	.L991
	testq	%rbx, %rbx
	jne	.L999
.L991:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L999:
	.cfi_restore_state
	call	sl_malloc_atomic.part.7
	.cfi_endproc
.LFE62:
	.size	sl_malloc_atomic, .-sl_malloc_atomic
	.section	.text.unlikely
.LCOLDE56:
	.text
.LHOTE56:
	.section	.text.unlikely
.LCOLDB57:
	.text
.LHOTB57:
	.p2align 4,,15
	.type	sl_malloc_uncollectable, @function
sl_malloc_uncollectable:
.LFB63:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	call	GC_malloc_uncollectable
	testq	%rax, %rax
	jne	.L1001
	testq	%rbx, %rbx
	jne	.L1009
.L1001:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1009:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE63:
	.size	sl_malloc_uncollectable, .-sl_malloc_uncollectable
	.section	.text.unlikely
.LCOLDE57:
	.text
.LHOTE57:
	.section	.text.unlikely
.LCOLDB58:
	.text
.LHOTB58:
	.p2align 4,,15
	.type	sl_malloc_atomic_uncollectable, @function
sl_malloc_atomic_uncollectable:
.LFB64:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	call	GC_malloc_atomic_uncollectable
	testq	%rax, %rax
	jne	.L1011
	testq	%rbx, %rbx
	jne	.L1019
.L1011:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1019:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE64:
	.size	sl_malloc_atomic_uncollectable, .-sl_malloc_atomic_uncollectable
	.section	.text.unlikely
.LCOLDE58:
	.text
.LHOTE58:
	.section	.text.unlikely
.LCOLDB59:
	.text
.LHOTB59:
	.p2align 4,,15
	.type	sl_malloc_big, @function
sl_malloc_big:
.LFB65:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	call	GC_malloc_ignore_off_page
	testq	%rax, %rax
	jne	.L1021
	testq	%rbx, %rbx
	jne	.L1029
.L1021:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1029:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE65:
	.size	sl_malloc_big, .-sl_malloc_big
	.section	.text.unlikely
.LCOLDE59:
	.text
.LHOTE59:
	.section	.text.unlikely
.LCOLDB60:
	.text
.LHOTB60:
	.p2align 4,,15
	.type	sl_cstrdup, @function
sl_cstrdup:
.LFB66:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	call	GC_strdup
	testq	%rax, %rax
	je	.L1033
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1033:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE66:
	.size	sl_cstrdup, .-sl_cstrdup
	.section	.text.unlikely
.LCOLDE60:
	.text
.LHOTE60:
	.section	.text.unlikely
.LCOLDB61:
	.text
.LHOTB61:
	.p2align 4,,15
	.type	sl_cstrndup, @function
sl_cstrndup:
.LFB67:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	call	GC_strndup
	testq	%rax, %rax
	je	.L1037
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1037:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	raise_error
	.cfi_endproc
.LFE67:
	.size	sl_cstrndup, .-sl_cstrndup
	.section	.text.unlikely
.LCOLDE61:
	.text
.LHOTE61:
	.section	.text.unlikely
.LCOLDB62:
	.text
.LHOTB62:
	.p2align 4,,15
	.type	sl_free, @function
sl_free:
.LFB68:
	.cfi_startproc
	jmp	GC_free
	.cfi_endproc
.LFE68:
	.size	sl_free, .-sl_free
	.section	.text.unlikely
.LCOLDE62:
	.text
.LHOTE62:
	.section	.text.unlikely
.LCOLDB63:
	.text
.LHOTB63:
	.p2align 4,,15
	.type	sl_memdup, @function
sl_memdup:
.LFB69:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L1040
	testq	%rbx, %rbx
	jne	.L1048
.L1040:
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%rcx, %rdi
	call	memcpy
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1048:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE69:
	.size	sl_memdup, .-sl_memdup
	.section	.text.unlikely
.LCOLDE63:
	.text
.LHOTE63:
	.section	.text.unlikely
.LCOLDB64:
	.text
.LHOTB64:
	.p2align 4,,15
	.type	sl_memdup_atomic, @function
sl_memdup_atomic:
.LFB70:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc_atomic
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L1050
	testq	%rbx, %rbx
	jne	.L1058
.L1050:
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%rcx, %rdi
	call	memcpy
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1058:
	.cfi_restore_state
	call	sl_malloc_atomic.part.7
	.cfi_endproc
.LFE70:
	.size	sl_memdup_atomic, .-sl_memdup_atomic
	.section	.text.unlikely
.LCOLDE64:
	.text
.LHOTE64:
	.section	.text.unlikely
.LCOLDB65:
	.text
.LHOTB65:
	.p2align 4,,15
	.type	make_sl_obj, @function
make_sl_obj:
.LFB74:
	.cfi_startproc
	movl	%esi, %eax
	orq	%rdi, %rax
	ret
	.cfi_endproc
.LFE74:
	.size	make_sl_obj, .-make_sl_obj
	.section	.text.unlikely
.LCOLDE65:
	.text
.LHOTE65:
	.section	.text.unlikely
.LCOLDB66:
	.text
.LHOTB66:
	.p2align 4,,15
	.type	make_int, @function
make_int:
.LFB75:
	.cfi_startproc
	leaq	2(,%rdi,4), %rax
	ret
	.cfi_endproc
.LFE75:
	.size	make_int, .-make_int
	.section	.text.unlikely
.LCOLDE66:
	.text
.LHOTE66:
	.section	.text.unlikely
.LCOLDB67:
	.text
.LHOTB67:
	.p2align 4,,15
	.type	make_float, @function
make_float:
.LFB76:
	.cfi_startproc
	movd	%xmm0, %eax
	salq	$32, %rax
	orq	$1913126919, %rax
	ret
	.cfi_endproc
.LFE76:
	.size	make_float, .-make_float
	.section	.text.unlikely
.LCOLDE67:
	.text
.LHOTE67:
	.section	.text.unlikely
.LCOLDB68:
	.text
.LHOTB68:
	.p2align 4,,15
	.type	make_char, @function
make_char:
.LFB77:
	.cfi_startproc
	movq	%rdi, %rax
	salq	$32, %rax
	orq	$589831, %rax
	ret
	.cfi_endproc
.LFE77:
	.size	make_char, .-make_char
	.section	.text.unlikely
.LCOLDE68:
	.text
.LHOTE68:
	.section	.text.unlikely
.LCOLDB69:
	.text
.LHOTB69:
	.p2align 4,,15
	.type	make_immediate, @function
make_immediate:
.LFB78:
	.cfi_startproc
	movzbl	%sil, %esi
	salq	$32, %rdi
	salq	$16, %rsi
	orq	$7, %rsi
	movq	%rsi, %rax
	orq	%rdi, %rax
	ret
	.cfi_endproc
.LFE78:
	.size	make_immediate, .-make_immediate
	.section	.text.unlikely
.LCOLDE69:
	.text
.LHOTE69:
	.section	.text.unlikely
.LCOLDB70:
	.text
.LHOTB70:
	.p2align 4,,15
	.type	make_bignum, @function
make_bignum:
.LFB79:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movl	$16, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	je	.L1067
	movq	%rbp, (%rax)
	movw	%bx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$3, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1067:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE79:
	.size	make_bignum, .-make_bignum
	.section	.text.unlikely
.LCOLDE70:
	.text
.LHOTE70:
	.section	.text.unlikely
.LCOLDB71:
	.text
.LHOTB71:
	.p2align 4,,15
	.type	make_number, @function
make_number:
.LFB80:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movl	$16, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	je	.L1071
	movq	%rbp, (%rax)
	movw	%bx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$3, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1071:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE80:
	.size	make_number, .-make_number
	.section	.text.unlikely
.LCOLDE71:
	.text
.LHOTE71:
	.section	.text.unlikely
.LCOLDB72:
	.text
.LHOTB72:
	.p2align 4,,15
	.type	make_double, @function
make_double:
.LFB81:
	.cfi_startproc
	subq	$24, %rsp
	.cfi_def_cfa_offset 32
	movl	$16, %edi
	movsd	%xmm0, 8(%rsp)
	call	GC_malloc
	testq	%rax, %rax
	movsd	8(%rsp), %xmm0
	je	.L1075
	movl	$12801, %edx
	movsd	%xmm0, (%rax)
	movw	%dx, 8(%rax)
	orq	$3, %rax
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1075:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE81:
	.size	make_double, .-make_double
	.section	.text.unlikely
.LCOLDE72:
	.text
.LHOTE72:
	.section	.text.unlikely
.LCOLDB73:
	.text
.LHOTB73:
	.p2align 4,,15
	.type	make_misc, @function
make_misc:
.LFB82:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$16, %edi
	movl	%esi, %ebp
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	je	.L1079
	movb	%bpl, 8(%rax)
	movq	%rbx, (%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$5, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1079:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE82:
	.size	make_misc, .-make_misc
	.section	.text.unlikely
.LCOLDE73:
	.text
.LHOTE73:
	.section	.text.unlikely
.LCOLDB74:
	.text
.LHOTB74:
	.p2align 4,,15
	.type	make_subr, @function
make_subr:
.LFB83:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rdi, %r15
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movl	$56, %edi
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rsi, %rbp
	movq	%rdx, %r14
	movq	%rcx, %r13
	movq	%r8, %r12
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	call	GC_malloc
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1083
	movq	%r15, (%rax)
	movq	%rbp, %rdi
	call	strlen
	salq	$32, %rax
	movq	%rbp, 32(%rbx)
	movl	%r14d, 40(%rbx)
	orq	$516, %rax
	movl	%r13d, 44(%rbx)
	movq	%r12, 48(%rbx)
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	movq	%rbx, %rax
	orq	$5, %rax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1083:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE83:
	.size	make_subr, .-make_subr
	.section	.text.unlikely
.LCOLDE74:
	.text
.LHOTE74:
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"Error, lisp stack overflow, terminating program"
	.section	.text.unlikely
.LCOLDB76:
	.text
.LHOTB76:
	.p2align 4,,15
	.type	env_stack_push, @function
env_stack_push:
.LFB84:
	.cfi_startproc
	movq	thread_env@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	1048640(%rax), %rdx
	leaq	1048640(%rax), %rcx
	cmpq	%rcx, %rdx
	ja	.L1087
	leaq	8(%rdx), %rcx
	movq	%rcx, 1048640(%rax)
	movq	%rdi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L1087:
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.LC75, %edi
	movl	$47, %edx
	movl	$1, %esi
	call	fwrite
	movl	$11, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	raise
	.cfi_endproc
.LFE84:
	.size	env_stack_push, .-env_stack_push
	.section	.text.unlikely
.LCOLDE76:
	.text
.LHOTE76:
	.section	.rodata.str1.1
.LC77:
	.string	"Lisp stack underflow"
	.section	.text.unlikely
.LCOLDB78:
	.text
.LHOTB78:
	.p2align 4,,15
	.type	env_stack_pop, @function
env_stack_pop:
.LFB85:
	.cfi_startproc
	movq	thread_env@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	1048640(%rax), %rdx
	leaq	64(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L1092
	leaq	-8(%rdx), %rcx
	movq	%rcx, 1048640(%rax)
	movq	-8(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1092:
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L1093
	movq	$0, (%rax)
	movq	$.LC77, 8(%rax)
	xorl	%edi, %edi
	movb	$2, (%rax)
	movb	$1, 3(%rax)
	movl	$20, 4(%rax)
	orq	$4, %rax
	movq	%rax, %rsi
	call	raise_error
.L1093:
	call	sl_malloc.part.6
	.cfi_endproc
.LFE85:
	.size	env_stack_pop, .-env_stack_pop
	.section	.text.unlikely
.LCOLDE78:
	.text
.LHOTE78:
	.section	.text.unlikely
.LCOLDB79:
	.text
.LHOTB79:
	.p2align 4,,15
	.type	fnv_hash, @function
fnv_hash:
.LFB86:
	.cfi_startproc
	testl	%esi, %esi
	jle	.L1097
	subl	$1, %esi
	movabsq	$-3750763034362895579, %rax
	movabsq	$1099511628211, %rcx
	leaq	1(%rdi,%rsi), %rsi
	.p2align 4,,10
	.p2align 3
.L1096:
	movzbl	(%rdi), %edx
	addq	$1, %rdi
	xorq	%rdx, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rdi
	jne	.L1096
	rep ret
.L1097:
	movabsq	$-3750763034362895579, %rax
	ret
	.cfi_endproc
.LFE86:
	.size	fnv_hash, .-fnv_hash
	.section	.text.unlikely
.LCOLDE79:
	.text
.LHOTE79:
	.section	.text.unlikely
.LCOLDB80:
	.text
.LHOTB80:
	.p2align 4,,15
	.type	murmur_hash3_32, @function
murmur_hash3_32:
.LFB87:
	.cfi_startproc
	movl	$16777619, %edx
	jmp	murmur_hash3_32_seeded
	.cfi_endproc
.LFE87:
	.size	murmur_hash3_32, .-murmur_hash3_32
	.section	.text.unlikely
.LCOLDE80:
	.text
.LHOTE80:
	.section	.text.unlikely
.LCOLDB81:
	.text
.LHOTB81:
	.p2align 4,,15
	.type	murmur_hash3_64, @function
murmur_hash3_64:
.LFB88:
	.cfi_startproc
	movl	$16777619, %edx
	jmp	murmur_hash3_64_seeded
	.cfi_endproc
.LFE88:
	.size	murmur_hash3_64, .-murmur_hash3_64
	.section	.text.unlikely
.LCOLDE81:
	.text
.LHOTE81:
	.section	.rodata.str1.1
.LC82:
	.string	"symbol.h"
.LC83:
	.string	"name_len == strlen(lname)"
	.section	.text.unlikely
.LCOLDB84:
	.text
.LHOTB84:
	.p2align 4,,15
	.type	make_symbol, @function
make_symbol:
.LFB89:
	.cfi_startproc
	pushq	%r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	pushq	%rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	movl	$48, %edi
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	movl	%esi, %ebp
	movq	%rdx, %r14
	movl	%ecx, %r12d
	call	GC_malloc
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1112
	movb	%r12b, 41(%rax)
	movq	%r14, (%rax)
	movq	%rbp, %rax
	salq	$32, %rax
	movq	%rbp, %rdi
	movq	$0, 16(%rbx)
	orq	$516, %rax
	movq	%rax, 8(%rbx)
	call	GC_malloc_atomic
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L1103
	testq	%rbp, %rbp
	jne	.L1113
.L1103:
	movq	%r13, %rdi
	movq	%r12, 16(%rbx)
	call	strlen
	cmpq	%rax, %rbp
	jne	.L1114
	movq	%rbp, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	memcpy
	movl	12(%rbx), %esi
	movq	%r12, %rdi
	movl	$16777619, %edx
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbp
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r13
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	ret
.L1113:
	.cfi_restore_state
	call	sl_malloc_atomic.part.7
.L1112:
	call	sl_malloc.part.6
.L1114:
	movl	$__PRETTY_FUNCTION__.9542, %ecx
	movl	$64, %edx
	movl	$.LC82, %esi
	movl	$.LC83, %edi
	call	__assert_fail
	.cfi_endproc
.LFE89:
	.size	make_symbol, .-make_symbol
	.section	.text.unlikely
.LCOLDE84:
	.text
.LHOTE84:
	.section	.text.unlikely
.LCOLDB85:
	.text
.LHOTB85:
	.p2align 4,,15
	.type	make_symbol_nocopy, @function
make_symbol_nocopy:
.LFB90:
	.cfi_startproc
	pushq	%r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	movq	%rdx, %r14
	pushq	%r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	pushq	%rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	movq	%rdi, %rbp
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	movl	$48, %edi
	movl	%esi, %r12d
	movl	%ecx, %r13d
	call	GC_malloc
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1118
	movq	%r14, (%rax)
	movb	%r13b, 41(%rax)
	movq	%r12, %rax
	salq	$32, %rax
	movl	%r12d, %esi
	movq	%rbp, %rdi
	orq	$516, %rax
	movl	$16777619, %edx
	movq	%rbp, 16(%rbx)
	movq	%rax, 8(%rbx)
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbp
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r13
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	ret
.L1118:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE90:
	.size	make_symbol_nocopy, .-make_symbol_nocopy
	.section	.text.unlikely
.LCOLDE85:
	.text
.LHOTE85:
	.section	.text.unlikely
.LCOLDB86:
	.text
.LHOTB86:
	.p2align 4,,15
	.type	make_symbol_from_string, @function
make_symbol_from_string:
.LFB91:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movl	$48, %edi
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1122
	movq	Qunbound(%rip), %rax
	movq	8(%rbp), %rdx
	movl	4(%rbp), %esi
	movq	8(%rbp), %rdi
	movq	%rax, (%rbx)
	movq	0(%rbp), %rax
	movq	%rdx, 16(%rbx)
	movl	$16777619, %edx
	movq	%rax, 8(%rbx)
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	movq	%rbx, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1122:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE91:
	.size	make_symbol_from_string, .-make_symbol_from_string
	.section	.text.unlikely
.LCOLDE86:
	.text
.LHOTE86:
	.section	.text.unlikely
.LCOLDB87:
	.text
.LHOTB87:
	.p2align 4,,15
	.type	get_symbol_name, @function
get_symbol_name:
.LFB92:
	.cfi_startproc
	movq	%rdi, %rax
	andq	$-8, %rax
	addq	$8, %rax
	orq	$4, %rax
	ret
	.cfi_endproc
.LFE92:
	.size	get_symbol_name, .-get_symbol_name
	.section	.text.unlikely
.LCOLDE87:
	.text
.LHOTE87:
	.section	.text.unlikely
.LCOLDB88:
	.text
.LHOTB88:
	.p2align 4,,15
	.type	make_cons, @function
make_cons:
.LFB93:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movl	$16, %edi
	movq	%rsi, %rbx
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	je	.L1127
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$1, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1127:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE93:
	.size	make_cons, .-make_cons
	.section	.text.unlikely
.LCOLDE88:
	.text
.LHOTE88:
	.section	.text.unlikely
.LCOLDB89:
	.text
.LHOTB89:
	.p2align 4,,15
	.type	make_list_1, @function
make_list_1:
.LFB94:
	.cfi_startproc
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE94:
	.size	make_list_1, .-make_list_1
	.section	.text.unlikely
.LCOLDE89:
	.text
.LHOTE89:
	.section	.text.unlikely
.LCOLDB90:
	.text
.LHOTB90:
	.p2align 4,,15
	.type	make_list_2, @function
make_list_2:
.LFB95:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rsi, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	je	.L1132
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$1, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1132:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE95:
	.size	make_list_2, .-make_list_2
	.section	.text.unlikely
.LCOLDE90:
	.text
.LHOTE90:
	.section	.text.unlikely
.LCOLDB91:
	.text
.LHOTB91:
	.p2align 4,,15
	.type	make_list_3, @function
make_list_3:
.LFB96:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rsi, %rbp
	call	GC_malloc
	testq	%rax, %rax
	je	.L1135
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1135
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	orq	$1, %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L1135:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE96:
	.size	make_list_3, .-make_list_3
	.section	.text.unlikely
.LCOLDE91:
	.text
.LHOTE91:
	.section	.text.unlikely
.LCOLDB92:
	.text
.LHOTB92:
	.p2align 4,,15
	.type	make_list_4, @function
make_list_4:
.LFB97:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rcx, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rsi, %rbp
	movq	%rdx, %r12
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	call	GC_malloc
	testq	%rax, %rax
	je	.L1142
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1142
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1142
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	orq	$1, %rax
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
.L1142:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE97:
	.size	make_list_4, .-make_list_4
	.section	.text.unlikely
.LCOLDE92:
	.text
.LHOTE92:
	.section	.text.unlikely
.LCOLDB93:
	.text
.LHOTB93:
	.p2align 4,,15
	.type	make_list_5, @function
make_list_5:
.LFB98:
	.cfi_startproc
	pushq	%r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	movq	%r8, %r14
	pushq	%r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	pushq	%rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rsi, %rbp
	movq	%rcx, %r13
	call	GC_malloc
	testq	%rax, %rax
	je	.L1152
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1152
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1152
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1152
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	orq	$1, %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbp
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r13
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	ret
.L1152:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE98:
	.size	make_list_5, .-make_list_5
	.section	.text.unlikely
.LCOLDE93:
	.text
.LHOTE93:
	.section	.text.unlikely
.LCOLDB94:
	.text
.LHOTB94:
	.p2align 4,,15
	.type	make_list_6, @function
make_list_6:
.LFB99:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%r9, %r15
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdx, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rsi, %r12
	movq	%rcx, %r14
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	movq	%r8, %rbp
	call	GC_malloc
	testq	%rax, %rax
	je	.L1165
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1165
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1165
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1165
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1165
	movq	%r12, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	orq	$1, %rax
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1165:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE99:
	.size	make_list_6, .-make_list_6
	.section	.text.unlikely
.LCOLDE94:
	.text
.LHOTE94:
	.section	.text.unlikely
.LCOLDB95:
	.text
.LHOTB95:
	.p2align 4,,15
	.type	make_list_7, @function
make_list_7:
.LFB100:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rsi, %r15
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rcx, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rdx, %r12
	movq	%r8, %r14
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	movq	%r9, %rbp
	call	GC_malloc
	testq	%rax, %rax
	je	.L1181
	movq	64(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1181
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1181
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1181
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1181
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1181
	movq	%r15, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	orq	$1, %rax
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1181:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE100:
	.size	make_list_7, .-make_list_7
	.section	.text.unlikely
.LCOLDE95:
	.text
.LHOTE95:
	.section	.text.unlikely
.LCOLDB96:
	.text
.LHOTB96:
	.p2align 4,,15
	.type	make_list_8, @function
make_list_8:
.LFB101:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rdx, %r15
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rsi, %r14
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rcx, %rbp
	movq	%r8, %r12
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	movq	%r9, %r13
	call	GC_malloc
	testq	%rax, %rax
	je	.L1200
	movq	72(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1200
	movq	64(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1200
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1200
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1200
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1200
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1200
	movq	%r14, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	orq	$1, %rax
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1200:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE101:
	.size	make_list_8, .-make_list_8
	.section	.text.unlikely
.LCOLDE96:
	.text
.LHOTE96:
	.section	.text.unlikely
.LCOLDB97:
	.text
.LHOTB97:
	.p2align 4,,15
	.type	make_list_9, @function
make_list_9:
.LFB102:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rsi, %r15
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdx, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rcx, %r14
	movq	%r8, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	movq	%r9, %r12
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	80(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	72(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	64(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1222
	movq	%r15, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	orq	$1, %rax
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1222:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE102:
	.size	make_list_9, .-make_list_9
	.section	.text.unlikely
.LCOLDE97:
	.text
.LHOTE97:
	.section	.text.unlikely
.LCOLDB98:
	.text
.LHOTB98:
	.p2align 4,,15
	.type	make_list_10, @function
make_list_10:
.LFB103:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rdx, %r15
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rsi, %r14
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rcx, %r12
	movq	%r8, %r13
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	movq	%r9, %rbp
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	88(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	80(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	72(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	64(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1247
	movq	%r14, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	orq	$1, %rax
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1247:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE103:
	.size	make_list_10, .-make_list_10
	.section	.text.unlikely
.LCOLDE98:
	.text
.LHOTE98:
	.section	.text.unlikely
.LCOLDB99:
	.text
.LHOTB99:
	.p2align 4,,15
	.type	make_list_11, @function
make_list_11:
.LFB104:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rcx, %r15
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rsi, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rdx, %r14
	movq	%r8, %rbp
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	movq	%r9, %r12
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	96(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	88(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	80(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	72(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	64(%rsp), %rdx
	movq	%rbx, 8(%rax)
	movl	$16, %edi
	movq	%rdx, (%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1275
	movq	%r13, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	orq	$1, %rax
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1275:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE104:
	.size	make_list_11, .-make_list_11
	.section	.text.unlikely
.LCOLDE99:
	.text
.LHOTE99:
	.section	.text.unlikely
.LCOLDB100:
	.text
.LHOTB100:
	.p2align 4,,15
	.globl	make_list_va
	.type	make_list_va, @function
make_list_va:
.LFB105:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$80, %rsp
	.cfi_def_cfa_offset 112
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	call	GC_malloc
	testq	%rax, %rax
	je	.L1308
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	movq	%rax, %rbx
	leaq	112(%rsp), %rax
	movl	$16, 8(%rsp)
	movq	40(%rsp), %rbp
	movq	%rbx, %rdx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1318:
	movl	%ecx, %esi
	addq	24(%rsp), %rsi
	addl	$8, %ecx
	movl	%ecx, 8(%rsp)
	movq	(%rsi), %rbp
	testq	%rbp, %rbp
	je	.L1317
.L1311:
	andq	$-8, %rdx
	movl	$16, %edi
	movq	%rdx, %r12
	call	GC_malloc
	testq	%rax, %rax
	je	.L1308
	movl	8(%rsp), %ecx
	movq	%rbp, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	movq	%rax, %rdx
	movq	%rax, 8(%r12)
	cmpl	$48, %ecx
	jb	.L1318
	movq	16(%rsp), %rsi
	movq	(%rsi), %rbp
	leaq	8(%rsi), %rax
	movq	%rax, 16(%rsp)
	testq	%rbp, %rbp
	jne	.L1311
.L1317:
	addq	$80, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	movq	%rbx, %rax
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L1308:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE105:
	.size	make_list_va, .-make_list_va
	.section	.text.unlikely
.LCOLDE100:
	.text
.LHOTE100:
	.section	.text.unlikely
.LCOLDB101:
	.text
.LHOTB101:
	.p2align 4,,15
	.globl	make_int_list
	.type	make_int_list, @function
make_int_list:
.LFB106:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$80, %rsp
	.cfi_def_cfa_offset 112
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	call	GC_malloc
	testq	%rax, %rax
	je	.L1321
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	movq	%rax, %rbp
	leaq	112(%rsp), %rax
	movl	$8, 8(%rsp)
	movq	%rbp, %rdx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1331:
	movl	%ecx, %eax
	addq	24(%rsp), %rax
	addl	$8, %ecx
	movl	%ecx, 8(%rsp)
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1330
.L1324:
	andq	$-8, %rdx
	movl	$16, %edi
	leaq	2(,%rbx,4), %rbx
	movq	%rdx, %r12
	call	GC_malloc
	testq	%rax, %rax
	je	.L1321
	movl	8(%rsp), %ecx
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	movq	%rax, %rdx
	movq	%rax, 8(%r12)
	cmpl	$48, %ecx
	jb	.L1331
	movq	16(%rsp), %rax
	movq	(%rax), %rbx
	leaq	8(%rax), %rcx
	movq	%rcx, 16(%rsp)
	testq	%rbx, %rbx
	jne	.L1324
.L1330:
	addq	$80, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	movq	%rbp, %rax
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L1321:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE106:
	.size	make_int_list, .-make_int_list
	.section	.text.unlikely
.LCOLDE101:
	.text
.LHOTE101:
	.section	.text.unlikely
.LCOLDB102:
	.text
.LHOTB102:
	.p2align 4,,15
	.globl	make_list1
	.type	make_list1, @function
make_list1:
.LFB107:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L1335
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1335:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE107:
	.size	make_list1, .-make_list1
	.section	.text.unlikely
.LCOLDE102:
	.text
.LHOTE102:
	.section	.text.unlikely
.LCOLDB103:
	.text
.LHOTB103:
	.p2align 4,,15
	.globl	make_list2
	.type	make_list2, @function
make_list2:
.LFB108:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp
	movl	$16, %edi
	movq	%rsi, %rbx
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	GC_malloc
	testq	%rax, %rax
	je	.L1338
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1338
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$1, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1338:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE108:
	.size	make_list2, .-make_list2
	.section	.text.unlikely
.LCOLDE103:
	.text
.LHOTE103:
	.section	.text.unlikely
.LCOLDB104:
	.text
.LHOTB104:
	.p2align 4,,15
	.globl	internal_head
	.type	internal_head, @function
internal_head:
.LFB117:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	andq	$-8, %rdi
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdx, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	.cfi_def_cfa_offset 64
	movq	(%rdi), %rbp
	movq	8(%rdi), %r15
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L1347
	subl	$1, %ebx
	movq	%rbp, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	testl	%ebx, %ebx
	movq	%rax, %r12
	jle	.L1345
	movq	%rax, %rcx
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1357:
	andq	$-8, %rcx
	andq	$-8, %r15
	movl	$16, %edi
	movq	(%r15), %r14
	movq	%rcx, %rbp
	movq	8(%r15), %r15
	call	GC_malloc
	testq	%rax, %rax
	je	.L1347
	movq	%r14, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	subl	$1, %ebx
	movq	%rax, %rcx
	movq	%rax, 8(%rbp)
	je	.L1345
.L1364:
	testq	%r15, %r15
	jne	.L1357
	xorl	%r15d, %r15d
.L1345:
	testq	%r13, %r13
	je	.L1350
	movq	%r15, 0(%r13)
.L1350:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	movq	%r12, %rax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L1347:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE117:
	.size	internal_head, .-internal_head
	.section	.text.unlikely
.LCOLDE104:
	.text
.LHOTE104:
	.section	.text.unlikely
.LCOLDB105:
	.text
.LHOTB105:
	.p2align 4,,15
	.globl	Fhead
	.type	Fhead, @function
Fhead:
.LFB118:
	.cfi_startproc
	movq	%rdi, %rax
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L1366
	testq	%rdi, %rdi
	jne	.L1368
.L1366:
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$2, %rax
	je	.L1367
	movq	%rsi, %rax
	andl	$7, %eax
	cmpq	$7, %rax
	jne	.L1368
	testl	$16777216, %esi
	je	.L1368
	shrq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L1372
.L1379:
	xorl	%eax, %eax
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L1367:
	.cfi_restore_state
	shrq	$2, %rsi
	testq	%rdi, %rdi
	je	.L1379
.L1372:
	xorl	%edx, %edx
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	jmp	internal_head
.L1368:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$144, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.9949, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
	.cfi_endproc
.LFE118:
	.size	Fhead, .-Fhead
	.section	.text.unlikely
.LCOLDE105:
	.text
.LHOTE105:
	.section	.text.unlikely
.LCOLDB106:
	.text
.LHOTB106:
	.p2align 4,,15
	.globl	Fsplit
	.type	Fsplit, @function
Fsplit:
.LFB120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	andl	$7, %eax
	subq	$24, %rsp
	.cfi_def_cfa_offset 48
	cmpq	$1, %rax
	je	.L1392
	testq	%rdi, %rdi
	jne	.L1394
.L1392:
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$2, %rax
	jne	.L1422
	shrq	$2, %rsi
	testq	%rdi, %rdi
	je	.L1401
.L1399:
	leaq	8(%rsp), %rdx
	call	internal_head
	movl	$16, %edi
	movq	%rax, %rbp
	movq	8(%rsp), %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1400
	movq	%rbp, (%rax)
	movq	%rbx, 8(%rax)
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$1, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L1422:
	.cfi_restore_state
	movq	%rsi, %rax
	andl	$7, %eax
	cmpq	$7, %rax
	jne	.L1394
	testl	$16777216, %esi
	je	.L1394
	shrq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L1399
.L1401:
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L1400
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	orq	$1, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1394:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$.LC3, %edi
	movl	$174, %r8d
	movl	$.LC2, %ecx
	movl	$__func__.9987, %edx
	xorl	%eax, %eax
	call	sl_sprintf
	orq	$4, %rax
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	raise_error
.L1400:
	call	sl_malloc.part.6
	.cfi_endproc
.LFE120:
	.size	Fsplit, .-Fsplit
	.section	.text.unlikely
.LCOLDE106:
	.text
.LHOTE106:
	.section	.text.unlikely
.LCOLDB107:
	.text
.LHOTB107:
	.p2align 4,,15
	.globl	reverse_cons
	.type	reverse_cons, @function
reverse_cons:
.LFB122:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	andq	$-8, %rbx
	movl	$16, %edi
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	movq	(%rbx), %rbp
	call	GC_malloc
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L1428
	movq	8(%rbx), %rsi
	movq	%rbp, (%rax)
	movq	%rax, %rbp
	movq	$0, 8(%rax)
	orq	$1, %rbp
	movq	%rsi, %rax
	andl	$7, %eax
	cmpq	$1, %rax
	je	.L1425
	andq	$-8, %rdx
	movq	%rbp, %rax
	movq	%rsi, 8(%rdx)
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L1425:
	.cfi_restore_state
	movq	%rdx, %rdi
	andq	$-8, %rsi
	andq	$-8, %rdi
	call	reverse_acc
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	movq	%rbp, %rax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1428:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE122:
	.size	reverse_cons, .-reverse_cons
	.section	.text.unlikely
.LCOLDE107:
	.text
.LHOTE107:
	.section	.text.unlikely
.LCOLDB108:
	.text
.LHOTB108:
	.p2align 4,,15
	.globl	nreverse_cons
	.type	nreverse_cons, @function
nreverse_cons:
.LFB123:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	andl	$7, %eax
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	cmpq	$1, %rax
	je	.L1443
	testq	%rdi, %rdi
	jne	.L1444
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L1433
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
.L1441:
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L1443:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$16, %edi
	andq	$-8, %rbx
	movq	(%rbx), %rbp
	call	GC_malloc
	testq	%rax, %rax
	je	.L1433
	movq	8(%rbx), %rdx
	movq	%rbp, (%rax)
	movq	$0, 8(%rax)
	orq	$1, %rax
	movq	%rdx, %rcx
	andl	$7, %ecx
	cmpq	$1, %rcx
	jne	.L1441
	movq	%rax, %rsi
	movq	%rdx, %rax
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	%rcx, %rax
.L1435:
	movq	%rax, %rdx
	andq	$-8, %rdx
	movq	8(%rdx), %rcx
	movq	%rsi, 8(%rdx)
	movq	%rax, %rsi
	movq	%rcx, %rdx
	andl	$7, %edx
	cmpq	$1, %rdx
	je	.L1437
	addq	$8, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L1433:
	.cfi_restore_state
	call	sl_malloc.part.6
.L1444:
	call	Fcar.part.0
	.cfi_endproc
.LFE123:
	.size	nreverse_cons, .-nreverse_cons
	.section	.text.unlikely
.LCOLDE108:
	.text
.LHOTE108:
	.section	.text.unlikely
.LCOLDB109:
	.text
.LHOTB109:
	.p2align 4,,15
	.globl	test_make_list
	.type	test_make_list, @function
test_make_list:
.LFB160:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movl	$16, %edi
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	$38, (%rax)
	movq	$0, 8(%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$34, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$30, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$26, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$22, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$18, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$14, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$10, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	%rbx, 8(%rax)
	movq	$6, (%rax)
	movl	$16, %edi
	orq	$1, %rax
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1447
	movq	$2, (%rax)
	movq	%rbx, 8(%rax)
	orq	$1, %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L1447:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE160:
	.size	test_make_list, .-test_make_list
	.section	.text.unlikely
.LCOLDE109:
	.text
.LHOTE109:
	.section	.text.unlikely
.LCOLDB110:
	.text
.LHOTB110:
	.p2align 4,,15
	.globl	init_cons_symbols
	.type	init_cons_symbols, @function
init_cons_symbols:
.LFB161:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movl	$__Qcons, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcons, Qcons(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scons, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcons(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qappend, %ebx
	movq	$__Qappend, Qappend(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Sappend, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qappend(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qnappend, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qnappend, Qnappend(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Snappend, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qnappend(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qnth, %ebx
	movq	$__Qnth, Qnth(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Snth, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qnth(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qhead, %ebx
	movq	$__Qhead, Qhead(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Shead, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qhead(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qtail, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qtail, Qtail(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Stail, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qtail(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qsplit, %ebx
	movq	$__Qsplit, Qsplit(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Ssplit, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qsplit(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qassoc, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qassoc, Qassoc(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Sassoc, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qassoc(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qrassoc, %ebx
	movq	$__Qrassoc, Qrassoc(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Srassoc, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qrassoc(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qassq, %ebx
	movq	$__Qassq, Qassq(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Sassq, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qassq(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qrassq, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qrassq, Qrassq(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Srassq, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qrassq(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcar, %ebx
	movq	$__Qcar, Qcar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcdr, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcdr, Qcdr(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qsafe_car, %ebx
	movq	$__Qsafe_car, Qsafe_car(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Ssafe_car, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qsafe_car(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qsafe_cdr, %ebx
	movq	$__Qsafe_cdr, Qsafe_cdr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Ssafe_cdr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qsafe_cdr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qpop, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qpop, Qpop(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Spop, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qpop(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qpush, %ebx
	movq	$__Qpush, Qpush(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Spush, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qpush(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qlist, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qlist, Qlist(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Slist, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qlist(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qlist_star, %ebx
	movq	$__Qlist_star, Qlist_star(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Slist_star, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qlist_star(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcaar, %ebx
	movq	$__Qcaar, Qcaar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcadr, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcadr, Qcadr(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scadr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcadr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcdar, %ebx
	movq	$__Qcdar, Qcdar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcddr, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcddr, Qcddr(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scddr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcddr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcaaar, %ebx
	movq	$__Qcaaar, Qcaaar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaaar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaaar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcaadr, %ebx
	movq	$__Qcaadr, Qcaadr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaadr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaadr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcadar, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcadar, Qcadar(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scadar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcadar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcaddr, %ebx
	movq	$__Qcaddr, Qcaddr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaddr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaddr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcdaar, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcdaar, Qcdaar(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdaar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdaar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcdadr, %ebx
	movq	$__Qcdadr, Qcdadr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdadr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdadr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcddar, %ebx
	movq	$__Qcddar, Qcddar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scddar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcddar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcdddr, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcdddr, Qcdddr(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdddr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdddr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcaaaar, %ebx
	movq	$__Qcaaaar, Qcaaaar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaaaar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaaaar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcaaadr, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcaaadr, Qcaaadr(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaaadr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaaadr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcaadar, %ebx
	movq	$__Qcaadar, Qcaadar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaadar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaadar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcaaddr, %ebx
	movq	$__Qcaaddr, Qcaaddr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaaddr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaaddr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcadaar, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcadaar, Qcadaar(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scadaar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcadaar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcadadr, %ebx
	movq	$__Qcadadr, Qcadadr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scadadr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcadadr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcaddar, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcaddar, Qcaddar(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scaddar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcaddar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcadddr, %ebx
	movq	$__Qcadddr, Qcadddr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scadddr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcadddr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcdaaar, %ebx
	movq	$__Qcdaaar, Qcdaaar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdaaar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdaaar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcdaadr, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcdaadr, Qcdaadr(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdaadr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdaadr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcdadar, %ebx
	movq	$__Qcdadar, Qcdadar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdadar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdadar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcdaddr, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcdaddr, Qcdaddr(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdaddr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdaddr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcddaar, %ebx
	movq	$__Qcddaar, Qcddaar(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scddaar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcddaar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcddadr, %ebx
	movq	$__Qcddadr, Qcddadr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scddadr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcddadr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	movl	$__Qcdddar, %eax
	movl	$16, %edi
	andq	$-8, %rax
	movq	$__Qcdddar, Qcdddar(%rip)
	movq	%rax, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scdddar, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcdddar(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movl	$16, %edi
	movq	%rax, 32(%rbx)
	movl	$__Qcddddr, %ebx
	movq	$__Qcddddr, Qcddddr(%rip)
	andq	$-8, %rbx
	call	GC_malloc
	testq	%rax, %rax
	je	.L1478
	movb	$0, 8(%rax)
	movq	$__Scddddr, (%rax)
	orq	$5, %rax
	movq	%rax, (%rbx)
	movq	Qcddddr(%rip), %rbx
	movl	$16777619, %edx
	andq	$-8, %rbx
	movl	12(%rbx), %esi
	movq	16(%rbx), %rdi
	call	murmur_hash3_64_seeded
	movq	%rax, 32(%rbx)
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L1478:
	.cfi_restore_state
	call	sl_malloc.part.6
	.cfi_endproc
.LFE161:
	.size	init_cons_symbols, .-init_cons_symbols
	.section	.text.unlikely
.LCOLDE110:
	.text
.LHOTE110:
	.section	.rodata
	.type	__func__.10141, @object
	.size	__func__.10141, 7
__func__.10141:
	.string	"Frassq"
	.type	__func__.10126, @object
	.size	__func__.10126, 6
__func__.10126:
	.string	"Fassq"
	.type	__func__.10111, @object
	.size	__func__.10111, 8
__func__.10111:
	.string	"Frassoc"
	.type	__func__.10096, @object
	.size	__func__.10096, 7
__func__.10096:
	.string	"Fassoc"
	.type	__func__.10078, @object
	.size	__func__.10078, 9
__func__.10078:
	.string	"Fnappend"
	.type	__func__.10045, @object
	.size	__func__.10045, 8
__func__.10045:
	.string	"Fappend"
	.type	__func__.10027, @object
	.size	__func__.10027, 6
__func__.10027:
	.string	"Fpush"
	.type	__func__.10013, @object
	.size	__func__.10013, 5
__func__.10013:
	.string	"Fpop"
	.type	__func__.9987, @object
	.size	__func__.9987, 7
__func__.9987:
	.string	"Fsplit"
	.type	__func__.9962, @object
	.size	__func__.9962, 6
__func__.9962:
	.string	"Ftail"
	.type	__func__.9949, @object
	.size	__func__.9949, 6
__func__.9949:
	.string	"Fhead"
	.type	__func__.9919, @object
	.size	__func__.9919, 5
__func__.9919:
	.string	"Fnth"
	.type	__func__.9878, @object
	.size	__func__.9878, 5
__func__.9878:
	.string	"Fcdr"
	.type	__func__.9860, @object
	.size	__func__.9860, 5
__func__.9860:
	.string	"Fcar"
	.type	__PRETTY_FUNCTION__.9542, @object
	.size	__PRETTY_FUNCTION__.9542, 12
__PRETTY_FUNCTION__.9542:
	.string	"make_symbol"
	.section	.rodata.str1.1
.LC111:
	.string	"cddddr"
	.data
	.align 32
	.type	__Qcddddr, @object
	.size	__Qcddddr, 48
__Qcddddr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC111
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"return the cddddr of the given cons cell"
	.data
	.align 32
	.type	__Scddddr, @object
	.size	__Scddddr, 56
__Scddddr:
	.quad	Fcddddr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC111
	.long	1
	.long	1
	.quad	.LC112
	.section	.rodata.str1.1
.LC113:
	.string	"cdddar"
	.data
	.align 32
	.type	__Qcdddar, @object
	.size	__Qcdddar, 48
__Qcdddar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC113
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"return the cdddar of the given cons cell"
	.data
	.align 32
	.type	__Scdddar, @object
	.size	__Scdddar, 56
__Scdddar:
	.quad	Fcdddar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC113
	.long	1
	.long	1
	.quad	.LC114
	.section	.rodata.str1.1
.LC115:
	.string	"cddadr"
	.data
	.align 32
	.type	__Qcddadr, @object
	.size	__Qcddadr, 48
__Qcddadr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC115
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"return the cddadr of the given cons cell"
	.data
	.align 32
	.type	__Scddadr, @object
	.size	__Scddadr, 56
__Scddadr:
	.quad	Fcddadr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC115
	.long	1
	.long	1
	.quad	.LC116
	.section	.rodata.str1.1
.LC117:
	.string	"cddaar"
	.data
	.align 32
	.type	__Qcddaar, @object
	.size	__Qcddaar, 48
__Qcddaar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC117
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"return the cddaar of the given cons cell"
	.data
	.align 32
	.type	__Scddaar, @object
	.size	__Scddaar, 56
__Scddaar:
	.quad	Fcddaar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC117
	.long	1
	.long	1
	.quad	.LC118
	.section	.rodata.str1.1
.LC119:
	.string	"cdaddr"
	.data
	.align 32
	.type	__Qcdaddr, @object
	.size	__Qcdaddr, 48
__Qcdaddr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC119
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"return the cdaddr of the given cons cell"
	.data
	.align 32
	.type	__Scdaddr, @object
	.size	__Scdaddr, 56
__Scdaddr:
	.quad	Fcdaddr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC119
	.long	1
	.long	1
	.quad	.LC120
	.section	.rodata.str1.1
.LC121:
	.string	"cdadar"
	.data
	.align 32
	.type	__Qcdadar, @object
	.size	__Qcdadar, 48
__Qcdadar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC121
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"return the cdadar of the given cons cell"
	.data
	.align 32
	.type	__Scdadar, @object
	.size	__Scdadar, 56
__Scdadar:
	.quad	Fcdadar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC121
	.long	1
	.long	1
	.quad	.LC122
	.section	.rodata.str1.1
.LC123:
	.string	"cdaadr"
	.data
	.align 32
	.type	__Qcdaadr, @object
	.size	__Qcdaadr, 48
__Qcdaadr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC123
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"return the cdaadr of the given cons cell"
	.data
	.align 32
	.type	__Scdaadr, @object
	.size	__Scdaadr, 56
__Scdaadr:
	.quad	Fcdaadr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC123
	.long	1
	.long	1
	.quad	.LC124
	.section	.rodata.str1.1
.LC125:
	.string	"cdaaar"
	.data
	.align 32
	.type	__Qcdaaar, @object
	.size	__Qcdaaar, 48
__Qcdaaar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC125
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"return the cdaaar of the given cons cell"
	.data
	.align 32
	.type	__Scdaaar, @object
	.size	__Scdaaar, 56
__Scdaaar:
	.quad	Fcdaaar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC125
	.long	1
	.long	1
	.quad	.LC126
	.section	.rodata.str1.1
.LC127:
	.string	"cadddr"
	.data
	.align 32
	.type	__Qcadddr, @object
	.size	__Qcadddr, 48
__Qcadddr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC127
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"return the cadddr of the given cons cell"
	.data
	.align 32
	.type	__Scadddr, @object
	.size	__Scadddr, 56
__Scadddr:
	.quad	Fcadddr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC127
	.long	1
	.long	1
	.quad	.LC128
	.section	.rodata.str1.1
.LC129:
	.string	"caddar"
	.data
	.align 32
	.type	__Qcaddar, @object
	.size	__Qcaddar, 48
__Qcaddar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC129
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"return the caddar of the given cons cell"
	.data
	.align 32
	.type	__Scaddar, @object
	.size	__Scaddar, 56
__Scaddar:
	.quad	Fcaddar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC129
	.long	1
	.long	1
	.quad	.LC130
	.section	.rodata.str1.1
.LC131:
	.string	"cadadr"
	.data
	.align 32
	.type	__Qcadadr, @object
	.size	__Qcadadr, 48
__Qcadadr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC131
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"return the cadadr of the given cons cell"
	.data
	.align 32
	.type	__Scadadr, @object
	.size	__Scadadr, 56
__Scadadr:
	.quad	Fcadadr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC131
	.long	1
	.long	1
	.quad	.LC132
	.section	.rodata.str1.1
.LC133:
	.string	"cadaar"
	.data
	.align 32
	.type	__Qcadaar, @object
	.size	__Qcadaar, 48
__Qcadaar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC133
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"return the cadaar of the given cons cell"
	.data
	.align 32
	.type	__Scadaar, @object
	.size	__Scadaar, 56
__Scadaar:
	.quad	Fcadaar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC133
	.long	1
	.long	1
	.quad	.LC134
	.section	.rodata.str1.1
.LC135:
	.string	"caaddr"
	.data
	.align 32
	.type	__Qcaaddr, @object
	.size	__Qcaaddr, 48
__Qcaaddr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC135
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"return the caaddr of the given cons cell"
	.data
	.align 32
	.type	__Scaaddr, @object
	.size	__Scaaddr, 56
__Scaaddr:
	.quad	Fcaaddr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC135
	.long	1
	.long	1
	.quad	.LC136
	.section	.rodata.str1.1
.LC137:
	.string	"caadar"
	.data
	.align 32
	.type	__Qcaadar, @object
	.size	__Qcaadar, 48
__Qcaadar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC137
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"return the caadar of the given cons cell"
	.data
	.align 32
	.type	__Scaadar, @object
	.size	__Scaadar, 56
__Scaadar:
	.quad	Fcaadar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC137
	.long	1
	.long	1
	.quad	.LC138
	.section	.rodata.str1.1
.LC139:
	.string	"caaadr"
	.data
	.align 32
	.type	__Qcaaadr, @object
	.size	__Qcaaadr, 48
__Qcaaadr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC139
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC140:
	.string	"return the caaadr of the given cons cell"
	.data
	.align 32
	.type	__Scaaadr, @object
	.size	__Scaaadr, 56
__Scaaadr:
	.quad	Fcaaadr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC139
	.long	1
	.long	1
	.quad	.LC140
	.section	.rodata.str1.1
.LC141:
	.string	"caaaar"
	.data
	.align 32
	.type	__Qcaaaar, @object
	.size	__Qcaaaar, 48
__Qcaaaar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC141
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"return the caaaar of the given cons cell"
	.data
	.align 32
	.type	__Scaaaar, @object
	.size	__Scaaaar, 56
__Scaaaar:
	.quad	Fcaaaar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC141
	.long	1
	.long	1
	.quad	.LC142
	.section	.rodata.str1.1
.LC143:
	.string	"cdddr"
	.data
	.align 32
	.type	__Qcdddr, @object
	.size	__Qcdddr, 48
__Qcdddr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC143
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC144:
	.string	"return the cdddr of the given cons cell"
	.data
	.align 32
	.type	__Scdddr, @object
	.size	__Scdddr, 56
__Scdddr:
	.quad	Fcdddr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC143
	.long	1
	.long	1
	.quad	.LC144
	.section	.rodata.str1.1
.LC145:
	.string	"cddar"
	.data
	.align 32
	.type	__Qcddar, @object
	.size	__Qcddar, 48
__Qcddar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC145
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"return the cddar of the given cons cell"
	.data
	.align 32
	.type	__Scddar, @object
	.size	__Scddar, 56
__Scddar:
	.quad	Fcddar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC145
	.long	1
	.long	1
	.quad	.LC146
	.section	.rodata.str1.1
.LC147:
	.string	"cdadr"
	.data
	.align 32
	.type	__Qcdadr, @object
	.size	__Qcdadr, 48
__Qcdadr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC147
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC148:
	.string	"return the cdadr of the given cons cell"
	.data
	.align 32
	.type	__Scdadr, @object
	.size	__Scdadr, 56
__Scdadr:
	.quad	Fcdadr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC147
	.long	1
	.long	1
	.quad	.LC148
	.section	.rodata.str1.1
.LC149:
	.string	"cdaar"
	.data
	.align 32
	.type	__Qcdaar, @object
	.size	__Qcdaar, 48
__Qcdaar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC149
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"return the cdaar of the given cons cell"
	.data
	.align 32
	.type	__Scdaar, @object
	.size	__Scdaar, 56
__Scdaar:
	.quad	Fcdaar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC149
	.long	1
	.long	1
	.quad	.LC150
	.section	.rodata.str1.1
.LC151:
	.string	"caddr"
	.data
	.align 32
	.type	__Qcaddr, @object
	.size	__Qcaddr, 48
__Qcaddr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC151
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"return the caddr of the given cons cell"
	.data
	.align 32
	.type	__Scaddr, @object
	.size	__Scaddr, 56
__Scaddr:
	.quad	Fcaddr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC151
	.long	1
	.long	1
	.quad	.LC152
	.section	.rodata.str1.1
.LC153:
	.string	"cadar"
	.data
	.align 32
	.type	__Qcadar, @object
	.size	__Qcadar, 48
__Qcadar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC153
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"return the cadar of the given cons cell"
	.data
	.align 32
	.type	__Scadar, @object
	.size	__Scadar, 56
__Scadar:
	.quad	Fcadar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC153
	.long	1
	.long	1
	.quad	.LC154
	.section	.rodata.str1.1
.LC155:
	.string	"caadr"
	.data
	.align 32
	.type	__Qcaadr, @object
	.size	__Qcaadr, 48
__Qcaadr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC155
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC156:
	.string	"return the caadr of the given cons cell"
	.data
	.align 32
	.type	__Scaadr, @object
	.size	__Scaadr, 56
__Scaadr:
	.quad	Fcaadr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC155
	.long	1
	.long	1
	.quad	.LC156
	.section	.rodata.str1.1
.LC157:
	.string	"caaar"
	.data
	.align 32
	.type	__Qcaaar, @object
	.size	__Qcaaar, 48
__Qcaaar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC157
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC158:
	.string	"return the caaar of the given cons cell"
	.data
	.align 32
	.type	__Scaaar, @object
	.size	__Scaaar, 56
__Scaaar:
	.quad	Fcaaar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC157
	.long	1
	.long	1
	.quad	.LC158
	.section	.rodata.str1.1
.LC159:
	.string	"cddr"
	.data
	.align 32
	.type	__Qcddr, @object
	.size	__Qcddr, 48
__Qcddr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC159
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"return the cddr of the given cons cell"
	.data
	.align 32
	.type	__Scddr, @object
	.size	__Scddr, 56
__Scddr:
	.quad	Fcddr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC159
	.long	1
	.long	1
	.quad	.LC160
	.section	.rodata.str1.1
.LC161:
	.string	"cdar"
	.data
	.align 32
	.type	__Qcdar, @object
	.size	__Qcdar, 48
__Qcdar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC161
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC162:
	.string	"return the cdar of the given cons cell"
	.data
	.align 32
	.type	__Scdar, @object
	.size	__Scdar, 56
__Scdar:
	.quad	Fcdar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC161
	.long	1
	.long	1
	.quad	.LC162
	.section	.rodata.str1.1
.LC163:
	.string	"cadr"
	.data
	.align 32
	.type	__Qcadr, @object
	.size	__Qcadr, 48
__Qcadr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC163
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"return the cadr of the given cons cell"
	.data
	.align 32
	.type	__Scadr, @object
	.size	__Scadr, 56
__Scadr:
	.quad	Fcadr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC163
	.long	1
	.long	1
	.quad	.LC164
	.section	.rodata.str1.1
.LC165:
	.string	"caar"
	.data
	.align 32
	.type	__Qcaar, @object
	.size	__Qcaar, 48
__Qcaar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC165
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC166:
	.string	"return the caar of the given cons cell"
	.data
	.align 32
	.type	__Scaar, @object
	.size	__Scaar, 56
__Scaar:
	.quad	Fcaar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC165
	.long	1
	.long	1
	.quad	.LC166
	.section	.rodata.str1.1
.LC167:
	.string	"rassq"
	.data
	.align 32
	.type	__Qrassq, @object
	.size	__Qrassq, 48
__Qrassq:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC167
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC168:
	.string	"(rassq key list) returns the first cons cell in list who's cdr is eq to key, or nil if none are found"
	.data
	.align 32
	.type	__Srassq, @object
	.size	__Srassq, 56
__Srassq:
	.quad	Frassq
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC167
	.long	2
	.long	2
	.quad	.LC168
	.section	.rodata.str1.1
.LC169:
	.string	"assq"
	.data
	.align 32
	.type	__Qassq, @object
	.size	__Qassq, 48
__Qassq:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC169
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC170:
	.string	"(assq key list) returns the first cons cell in list who's car is eq to key, or nil if none are found"
	.data
	.align 32
	.type	__Sassq, @object
	.size	__Sassq, 56
__Sassq:
	.quad	Fassq
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC169
	.long	2
	.long	2
	.quad	.LC170
	.section	.rodata.str1.1
.LC171:
	.string	"rassoc"
	.data
	.align 32
	.type	__Qrassoc, @object
	.size	__Qrassoc, 48
__Qrassoc:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC171
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"(rassoc key list) returns the first cons cell in list who's cdr is equal to key, or nil if none are found"
	.data
	.align 32
	.type	__Srassoc, @object
	.size	__Srassoc, 56
__Srassoc:
	.quad	Frassoc
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC171
	.long	2
	.long	2
	.quad	.LC172
	.section	.rodata.str1.1
.LC173:
	.string	"assoc"
	.data
	.align 32
	.type	__Qassoc, @object
	.size	__Qassoc, 48
__Qassoc:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC173
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC174:
	.string	"(assoc key list) returns the first cons cell in list who's car is equal to key, or nil if none are found"
	.data
	.align 32
	.type	__Sassoc, @object
	.size	__Sassoc, 56
__Sassoc:
	.quad	Fassoc
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC173
	.long	2
	.long	2
	.quad	.LC174
	.section	.rodata.str1.1
.LC175:
	.string	"nappend"
	.data
	.align 32
	.type	__Qnappend, @object
	.size	__Qnappend, 48
__Qnappend:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	7
	.quad	.LC175
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC176:
	.string	"(append &rest args) return list createdfrom concatenating args, all but the last argument must be lists, and all but the last argument may be modified"
	.data
	.align 32
	.type	__Snappend, @object
	.size	__Snappend, 56
__Snappend:
	.quad	Fnappend
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	7
	.quad	.LC175
	.long	0
	.long	-1
	.quad	.LC176
	.section	.rodata.str1.1
.LC177:
	.string	"append"
	.data
	.align 32
	.type	__Qappend, @object
	.size	__Qappend, 48
__Qappend:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC177
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"(append &rest args) return new list createdfrom concatenating args, all but the last argument must be lists"
	.data
	.align 32
	.type	__Sappend, @object
	.size	__Sappend, 56
__Sappend:
	.quad	Fappend
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	6
	.quad	.LC177
	.long	0
	.long	-1
	.quad	.LC178
	.section	.rodata.str1.1
.LC179:
	.string	"push"
	.data
	.align 32
	.type	__Qpush, @object
	.size	__Qpush, 48
__Qpush:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC179
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"(push elt place) set place to (cons elt place)"
	.data
	.align 32
	.type	__Spush, @object
	.size	__Spush, 56
__Spush:
	.quad	Fpush
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC179
	.long	2
	.long	-2
	.quad	.LC180
	.section	.rodata.str1.1
.LC181:
	.string	"pop"
	.data
	.align 32
	.type	__Qpop, @object
	.size	__Qpop, 48
__Qpop:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC181
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC182:
	.string	"(pop ls) set ls to (cdr ls) and return (car ls)"
	.data
	.align 32
	.type	__Spop, @object
	.size	__Spop, 56
__Spop:
	.quad	Fpop
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC181
	.long	1
	.long	-2
	.quad	.LC182
	.section	.rodata.str1.1
.LC183:
	.string	"split"
	.data
	.align 32
	.type	__Qsplit, @object
	.size	__Qsplit, 48
__Qsplit:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC183
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"(split lst len) split list into two sublists wherethe first list is of length len and the second list is the remaniningelements"
	.data
	.align 32
	.type	__Ssplit, @object
	.size	__Ssplit, 56
__Ssplit:
	.quad	Fsplit
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC183
	.long	2
	.long	2
	.quad	.LC184
	.section	.rodata.str1.1
.LC185:
	.string	"tail"
	.data
	.align 32
	.type	__Qtail, @object
	.size	__Qtail, 48
__Qtail:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC185
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC186:
	.string	"(tail list n) return the last n elements of listthe returned list shares structure with the original list"
	.data
	.align 32
	.type	__Stail, @object
	.size	__Stail, 56
__Stail:
	.quad	Ftail
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC185
	.long	2
	.long	2
	.quad	.LC186
	.section	.rodata.str1.1
.LC187:
	.string	"head"
	.data
	.align 32
	.type	__Qhead, @object
	.size	__Qhead, 48
__Qhead:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC187
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"(head list n) return a copy of the firstn elements of list"
	.data
	.align 32
	.type	__Shead, @object
	.size	__Shead, 56
__Shead:
	.quad	Fhead
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC187
	.long	2
	.long	2
	.quad	.LC188
	.section	.rodata.str1.1
.LC189:
	.string	"nth"
	.data
	.align 32
	.type	__Qnth, @object
	.size	__Qnth, 48
__Qnth:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC189
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC190:
	.string	"(nth list n) return the nth value of the given list"
	.data
	.align 32
	.type	__Snth, @object
	.size	__Snth, 56
__Snth:
	.quad	Fnth
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC189
	.long	2
	.long	2
	.quad	.LC190
	.section	.rodata.str1.1
.LC191:
	.string	"list*"
	.data
	.align 32
	.type	__Qlist_star, @object
	.size	__Qlist_star, 48
__Qlist_star:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC191
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC192:
	.string	"(list* first &rest list) like list, exceptthe last argument is the tail of the new list"
	.data
	.align 32
	.type	__Slist_star, @object
	.size	__Slist_star, 56
__Slist_star:
	.quad	Flist_star
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	5
	.quad	.LC191
	.long	1
	.long	-1
	.quad	.LC192
	.section	.rodata.str1.1
.LC193:
	.string	"list"
	.data
	.align 32
	.type	__Qlist, @object
	.size	__Qlist, 48
__Qlist:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC193
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC194:
	.string	"(list &rest args) return a list with thegiven argument as elements"
	.data
	.align 32
	.type	__Slist, @object
	.size	__Slist, 56
__Slist:
	.quad	Flist
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC193
	.long	0
	.long	-1
	.quad	.LC194
	.section	.rodata.str1.1
.LC195:
	.string	"cdr"
	.data
	.align 32
	.type	__Qcdr, @object
	.size	__Qcdr, 48
__Qcdr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC195
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"return the cdr of the given cons cell"
	.data
	.align 32
	.type	__Scdr, @object
	.size	__Scdr, 56
__Scdr:
	.quad	Fcdr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC195
	.long	1
	.long	1
	.quad	.LC196
	.section	.rodata.str1.1
.LC197:
	.string	"safe-cdr"
	.data
	.align 32
	.type	__Qsafe_cdr, @object
	.size	__Qsafe_cdr, 48
__Qsafe_cdr:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	8
	.quad	.LC197
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC198:
	.string	"return the cdr of the given cons cell, or nil if not given a cons cell"
	.data
	.align 32
	.type	__Ssafe_cdr, @object
	.size	__Ssafe_cdr, 56
__Ssafe_cdr:
	.quad	Fsafe_cdr
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	8
	.quad	.LC197
	.long	1
	.long	1
	.quad	.LC198
	.section	.rodata.str1.1
.LC199:
	.string	"car"
	.data
	.align 32
	.type	__Qcar, @object
	.size	__Qcar, 48
__Qcar:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC199
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC200:
	.string	"return the car of the given cons cell"
	.data
	.align 32
	.type	__Scar, @object
	.size	__Scar, 56
__Scar:
	.quad	Fcar
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	3
	.quad	.LC199
	.long	1
	.long	1
	.quad	.LC200
	.section	.rodata.str1.1
.LC201:
	.string	"safe-car"
	.data
	.align 32
	.type	__Qsafe_car, @object
	.size	__Qsafe_car, 48
__Qsafe_car:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	8
	.quad	.LC201
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC202:
	.string	"return the car of the given cons cell, or nil if not given a cons cell"
	.data
	.align 32
	.type	__Ssafe_car, @object
	.size	__Ssafe_car, 56
__Ssafe_car:
	.quad	Fsafe_car
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	8
	.quad	.LC201
	.long	1
	.long	1
	.quad	.LC202
	.section	.rodata.str1.1
.LC203:
	.string	"cons"
	.data
	.align 32
	.type	__Qcons, @object
	.size	__Qcons, 48
__Qcons:
	.quad	0
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC203
	.zero	17
	.byte	2
	.zero	6
	.section	.rodata.str1.8
	.align 8
.LC204:
	.string	"create a new cons cell with `car` and `cdr` as the\ncar and cdr respectively"
	.data
	.align 32
	.type	__Scons, @object
	.size	__Scons, 56
__Scons:
	.quad	Fcons
	.zero	16
	.byte	2
	.zero	2
	.byte	1
	.long	4
	.quad	.LC203
	.long	2
	.long	2
	.quad	.LC204
	.local	gensym_counter
	.comm	gensym_counter,8,8
	.comm	Qunbound,8,8
	.comm	Qnreverse,8,8
	.comm	Qreverse,8,8
	.comm	Qlength,8,8
	.comm	Qelt,8,8
	.comm	Qsub1,8,8
	.comm	Qadd1,8,8
	.comm	Qnum_eq,8,8
	.comm	Qdiv,8,8
	.comm	Qmul,8,8
	.comm	Qsub,8,8
	.comm	Qadd,8,8
	.comm	Krw,8,8
	.comm	Qrw_key,8,8
	.comm	Kwrite,8,8
	.comm	Qwrite_key,8,8
	.comm	Kread,8,8
	.comm	Qread_key,8,8
	.comm	Kio,8,8
	.comm	Qio_key,8,8
	.comm	Koutput,8,8
	.comm	Qoutput_key,8,8
	.comm	Kinput,8,8
	.comm	Qinput_key,8,8
	.comm	Qwrite_string,8,8
	.comm	Qnewline,8,8
	.comm	Qpeek_char,8,8
	.comm	Qread_char,8,8
	.comm	Qstandard_error,8,8
	.comm	Qstandard_output,8,8
	.comm	Qstandard_input,8,8
	.comm	Qquote,8,8
	.comm	Qread,8,8
	.comm	Qequalp,8,8
	.comm	Qequal,8,8
	.comm	Qeqv,8,8
	.comm	Qeq,8,8
	.comm	Qfloatp,8,8
	.comm	Qintegerp,8,8
	.comm	Qsvectorp,8,8
	.comm	Qvectorp,8,8
	.comm	Qarrayp,8,8
	.comm	Qconsp,8,8
	.comm	Qsetq,8,8
	.comm	Qgensym,8,8
	.comm	Qsprint,8,8
	.comm	Qprin1,8,8
	.comm	Qprint,8,8
	.comm	Qcddddr,8,8
	.comm	Qcdddar,8,8
	.comm	Qcddadr,8,8
	.comm	Qcddaar,8,8
	.comm	Qcdaddr,8,8
	.comm	Qcdadar,8,8
	.comm	Qcdaadr,8,8
	.comm	Qcdaaar,8,8
	.comm	Qcadddr,8,8
	.comm	Qcaddar,8,8
	.comm	Qcadadr,8,8
	.comm	Qcadaar,8,8
	.comm	Qcaaddr,8,8
	.comm	Qcaadar,8,8
	.comm	Qcaaadr,8,8
	.comm	Qcaaaar,8,8
	.comm	Qcdddr,8,8
	.comm	Qcddar,8,8
	.comm	Qcdadr,8,8
	.comm	Qcdaar,8,8
	.comm	Qcaddr,8,8
	.comm	Qcadar,8,8
	.comm	Qcaadr,8,8
	.comm	Qcaaar,8,8
	.comm	Qcddr,8,8
	.comm	Qcdar,8,8
	.comm	Qcadr,8,8
	.comm	Qcaar,8,8
	.comm	Qlist_star,8,8
	.comm	Qlist,8,8
	.comm	Qpush,8,8
	.comm	Qpop,8,8
	.comm	Qsafe_cdr,8,8
	.comm	Qsafe_car,8,8
	.comm	Qcdr,8,8
	.comm	Qcar,8,8
	.comm	Qrassq,8,8
	.comm	Qassq,8,8
	.comm	Qrassoc,8,8
	.comm	Qassoc,8,8
	.comm	Qsplit,8,8
	.comm	Qtail,8,8
	.comm	Qhead,8,8
	.comm	Qnth,8,8
	.comm	Qnappend,8,8
	.comm	Qappend,8,8
	.comm	Qcons,8,8
	.comm	Qmake_vector,8,8
	.comm	Qvset,8,8
	.comm	Qvref,8,8
	.comm	Qprogn,8,8
	.comm	Qwhile,8,8
	.comm	Qif,8,8
	.comm	runtime_debug,4,4
	.ident	"GCC: (GNU) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
