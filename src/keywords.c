#include "common.h"
/*
  This file just contains a function to initialize the default keyword symbols,
  its just so all of the keywords interned by default are in one place.
*/
void init_keyword_symbols(void){
  /* Keywords for IO functions*/
  DEFKEY("input", input);
  DEFKEY("output", output);
  DEFKEY("io", io);
  DEFKEY("read", read);
  DEFKEY("write", write);
  DEFKEY("rw", rw);
  /* Keywords for function definations*/
  DEFKEY("optional",optional);
  DEFKEY("rest",rest);
  /* Types */
  DEFKEY("number", number);
    DEFKEY("integer",integer);
    DEFKEY("float",float);
  DEFKEY("sequence",sequence);
    DEFKEY("cons",cons);
    DEFKEY("string",string);
    DEFKEY("vector",vector);
  DEFKEY("symbol",symbol);
  DEFKEY("character",character);
  DEFKEY("function",function);
  DEFKEY("stream",stream);
    DEFKEY("string-stream",string_stream);
    DEFKEY("file-stream",file_stream);
  /* keywords for variable definitions*/
  DEFKEY("constant",constant);
  DEFKEY("dynamic",dynamic);
  DEFKEY("define-once",define_once);
  DEFKEY("documentation",documentation);
  DEFKEY("hash-table",hash_table);
  DEFKEY("random-state",random_state);
  /* Exceptions */
  DEFKEY("unwind-protect", unwind_protect);
  DEFKEY("type-error",type_error);
  DEFKEY("read-error",read_error);
  DEFKEY("range-error",range_error);
  /* everything else */
  DEFKEY(",", backquote_splice);
  DEFKEY(",@", backquote_unquote);
}
