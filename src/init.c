#include "common.h"
/*
  Code to initialize scilisp, no corrsponding header since it only exports one
  function, scilisp_init
*/

/* These functions aren't needed anywhere but here,
   Though I could put them in symbol_list.h, so I could autogenerate them.
 */
typedef void(*sl_init_function)(void);
void init_cons_symbols(void);
void init_read_symbols(void);
void init_math_symbols(void);
void init_vector_symbols(void);
void init_print_symbols(void);
void init_predicate_symbols(void);
void init_stream_symbols(void);
void init_string_symbols(void);
void init_sequence_symbols(void);
void init_symbol_symbols(void);
void init_control_symbols(void);
void init_interpreter_symbols(void);
void init_env_symbols(void);
void init_lambda_symbols(void);
void init_keyword_symbols(void);
static const sl_init_function init_functions[] =
  {init_cons_symbols, init_read_symbols, init_math_symbols,
   init_vector_symbols, init_print_symbols, init_predicate_symbols,
   init_stream_symbols, init_string_symbols, init_sequence_symbols,
   init_symbol_symbols, init_control_symbols, init_interpreter_symbols,
   init_env_symbols, init_lambda_symbols, init_keyword_symbols};
static const int num_init_functions = sizeof(init_functions)/sizeof(sl_init_function);
static void default_user_init_fun(void){
  return;
}
SL_ATTRIBUTE(weak) void (*sl_user_init_hook)(void) = default_user_init_fun;
/*
  perform any global initializations and setup the environment
  for the initial thread.
*/
static void scilisp_init_core(void){
  //make gc allocated memory executable, needed for lambda trampolines
  GC_set_pages_executable(1);//This needs to go /before/ GC_INIT
  GC_INIT();
  //tell gmp/mpfr to use gc malloc functions
  mp_set_memory_functions(gmp_malloc_func, gmp_realloc_func, gmp_free_func);
  /* Initialize symbols */
  int i;
  for(i=0;i<num_init_functions;i++){
    init_functions[i]();
  }
  /* assume we have 2 variables initial_symbols, and
     num_initial_symbols with the symbols to add to the initial
     obarray and the number of those symbols
   */
  int symbol_table_size = NEAREST_POW_OF_2(num_initial_symbols);
  global_obarray = make_symbol_table(symbol_table_size, 0.8);
  assert(global_obarray);
  for(i=0;i<num_initial_symbols;i++){
    //DEBUG_PRINTF("%d\n",i);//use this to find what symbol is failing
    assert(initial_symbols[i] && XSYMBOL(*initial_symbols[i]));
/*    DEBUG_PRINTF("Adding symbol %.*s, hv = %#0lx\n",
                 XSYMNAME(*initial_symbols[i])->len,
                 XSYMNAME(*initial_symbols[i])->str, XSYMBOL(*initial_symbols[i])->hv);*/
    add_new_symbol(global_obarray,XSYMBOL(*initial_symbols[i]));
  }
  /* add special symbols */
  SETQ(Qt, Qt);
  add_new_symbol(global_obarray,XSYMBOL(Qt));
  add_new_symbol(global_obarray,XSYMBOL(Qnil));
  /*
    Initialize global variables here
   */
  assert(global_obarray);
//  DEBUG_PRINTF("setting thread_env\n");
  thread_env = make_environment();
  assert(thread_env);
//  DEBUG_PRINTF("thread env == %p\n",thread_env);
  //I'll be using SFMT for my random number generator, so this is just
  //a placeholder
  srand48(time(NULL));
  //SFMT init
  //Should make a global variable holding the current random state
  //sfmt_init_static();//initialize static random state

  //user init function, called after all system initialization
  sl_user_init_hook();
}
void scilisp_init(void){
  pthread_once(&init_control, scilisp_init_core);
  //scilisp_init_core();
  assert(thread_env);
//  DEBUG_PRINTF("thread env == %p\n",thread_env);
}
