
#include "common.h"
typedef struct sl_rational sl_rational;
struct sl_rational {
  int64_t numerator;
  uint64_t denominator;
};
#define ctz(a,b) __builtin_ctzl(a,b)
/* Borrowed from myself*/
uint64_t binary_gcd(uint64_t a,uint64_t b){
  //assume a > 0 and b > 0
  //find the common factors of two (using ctz)
  uint64_t common_factor_of_2 = ctz(a|b);
  a >>= ctz(a);//shift away common factors of 2
  //now a is always odd
  //loop until a == b or a == 1, insuring a is always the min of a and b
  while(1){
    b>>=ctz(b);//now b is odd
    if(a == b){//we're done
      break;
    }
    if(a > b){//make sure a is always the lesser of the two
      SWAP(a,b);
    }
    if(a == 1){
      break;
    }
    b-=a;
  }
  return a << common_factor_of_2;
}
//Slow version of the above
uint64_t euclid_gcd(uint64_t a,uint64_t b){
  if(b=0){
    return a;
  } else {
    euclid_gcd_acc(b,a%b);
  }
}
