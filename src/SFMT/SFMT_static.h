#ifndef _SFMT_STATIC_H
#define _SFMT_STATIC_H
//simple wrapper functions around the sfmt functions
//using static internal arrays to hold state
//easy to use but not thread safe
#include "SFMT_lib.h"
#include "SFMT_r.h"
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
static sfmt_state sfmt_static;
static int64_t sfmt_init_static(uint32_t seed){
  return sfmt_init_r(&sfmt_static, seed);
}
static void sfmt_init_stable_static(){
  sfmt_init_stable_r(&sfmt_static);
}
//lets use naming conventions from the rand48 functions
static inline uint32_t sfmt_rand_uint32(){
  return sfmt_rand_uint32_r(&sfmt_static);
}
static inline int32_t sfmt_rand_int32(){
  return (int32_t) sfmt_rand_uint32();
}
static inline uint64_t sfmt_rand_uint64(){
  return sfmt_rand_uint64_r(&sfmt_static)
}
static inline int64_t sfmt_rand_int64(){
  return (int64_t) sfmt_rand_uint64();
}
static double sfmt_rand_float64(){
  return sfmt_rand_float64(&sfmt_static);
}
#define sfmt_rand_double sfmt_rand_float64
#endif
