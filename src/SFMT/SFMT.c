#include "SFMT_r.h"
#include "SFMT_static.h"
#include <sys/time.h>
union uintN_t {
  uint32_t uint32[2];
  uint64_t uint64;
};
/*
  xor the seconds and useconds to prevent using the same seed if called
  twice in a second
*/
#define time_seed()                                             \
  ({struct timeval tp;                                          \
    gettimeofday(&tp,NULL);\
    (uint32_t)(tp.tv_sec^tp.tv_usec);})
static inline void sfmt_fill_buf(sfmt_state *sfmt){
  sfmt_fill_array32(sfmt->sfmt,sfmt->buf,BUF_LEN);
  sfmt->buf_index=0;
}
//Initialize the random state and buffer and fill the buffer
//seed is optional, the return value is the whatever actual seed
//was used
int64_t sfmt_init_r(sfmt_state *sfmt, uint32_t seed){
  if(!seed){
    seed = time_seed();
  }
  sfmt_init_gen_rand(sfmt->sfmt, seed);
  sfmt_fill_buf(sfmt);
  return seed;
}
void sfmt_init_stable_r(sfmt_state *sfmt){
  uint32_t init_array[SFMT_N32];
  union {
    long X_i;
    unsigned short state[3];
  } lcg_init;
  lcg_init.X_i=time_seed();
  int i;
  for(i=0;i<SFMT_N32;i++){
    init_array[i]=nrand48(lcg_init.state);
  }
  sfmt_init_by_array(sfmt,init_array,SFMT_N32);
  sfmt_fill_buf(sfmt);
}

uint32_t sfmt_rand_uint32_r(sfmt_state *sfmt){
  if(sfmt->buf_index>=BUF_LEN){
    sfmt_fill_buf(sfmt);
  }
  return sfmt->buf[sfmt->buf_index++];
}
uint64_t sfmt_rand_uint64_r(sfmt_state *sfmt){
  if(sfmt->buf_index+1>=BUF_LEN){
    sfmt_fill_buf(sfmt);
  }/*
  union uintN_t retval;
  retval.uint32[0]=sfmt->buf[sfmt->buf_index++];
  retval.uint32[1]=sfmt->buf[sfmt->buf_index++];
  return retval.uint64;*/
  //this should be the same as the commented code above
  uint64_t retval = *(uint64_t)(sfmt->buf + sfmt->buf_index);
  sfmt->buf_index += 2;
  return retval;
}
