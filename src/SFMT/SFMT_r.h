#ifndef SFMT_R_H
#define SFMT_R_H
#include "SFMT_lib.h"
#include <time.h>
#include <stdlib.h>
#ifndef BUF_LEN
#define BUF_LEN 4096
#endif
typedef struct sfmt_state sfmt_state;
static void *(*sfmt_malloc)(size_t)=malloc;
static void (*sfmt_free)(void*)=free;
//re-entrant wrapper functions around sfmt functions
//require the user to pass the random state for each call
//We keep an internal buffer of random numbers since it's much faster to 
//generate a large ammount of random numbers once than to repeatedly generate
//a small ammount of numbers
struct sfmt_state {
//the buffer needs to be 16 byte aligned b/c of simd stuff
  uint32_t buf[BUF_LEN] __attribute__((aligned(16)));
  sfmt_t *sfmt;
  uint32_t buf_index;
};
//seed is an optional argument, this means it's not actually possible
//to provide a seed of 0, which isn't a huge issue, but worth mentioning
int64_t smft_init_r(sfmt_state *sfmt, uint32_t seed);
//initialize the rng using an array of random numbers generated using
//a linear congruental generator
void sfmt_init_stable_r(sfmt_state *sfmt);

uint32_t sfmt_rand_uint32(sfmt_stace *sfmt);
uint64_t sfmt_rand_uint64(sfmt_state *sfmt);
  
//lets use naming conventions from the rand48 functions
static inline int32_t sfmt_rand_int32_r(sfmt_state *sfmt){
  return (int32_t) sfmt_rand_uint32_r(sfmt->sfmt,sfmt->buf);
}
static inline int64_t sfmt_rand_int64_r(sfmt_state *sfmt){
  return (int64_t) sfmt_rand_uint64_r(sfmt,buf);
}
/*
  There's another library to generate only random floats, but
  I doubt the conversion is a huge time sink unless you're generating
  a ton of doubles
*/
//magic number to convert a 64 bit integer to a double in the range [0,1)
//it's 1/(2^64) aka 1/(UINT64_MAX+1).
#define UINT_TO_DOUBLE_MAGIC (1.0/18446744073709551616.0)
#define INT_TO_DOUBLE_MAGIC (1.0/9223372036854775808.0)
static double sfmt_rand_float64_r(sfmt_state *sfmt){
  uint64_t r = sfmt_rand_uint64_r(sfmt);
  /*
    What's happening is r is converted into a double (which will be close to
    but not necessarly exacly the same as the integer value, since a double
    can only hold up to a 53 bit integer exactly), it is then divided by the
    maximum possible value it could have had + 1.
   */
  return r * UINT_TO_DOUBLE_MAGIC
}
//return a double in the range (-1,1) ... hopefully
static double sfmt_rand_signed_float64(sfmt_state *sfmt){
  int64_t r = (int64_t)sfmt_rand_uint64_r(sfmt);
  return r * INT_TO_DOUBLE_MAGIC;
}
#define sfmt_rand_double_r sfmt_rand_float64_r
#endif
