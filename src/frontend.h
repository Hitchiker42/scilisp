#ifndef _SCILISP_FRONTEND_H_
#define _SCILISP_FRONTEND_H_
#include "common.h"
#include <readline/readline.h>
#include <readline/history.h>
#include <getopt.h>
#include <setjmp.h>
//static char* sl_readline(char *prompt);
void usage();
int scilisp_getopt(int argc, char **argv);
static const char *toplevel_prompt = "> ";
static const char *nested_prompt = " ";
/*Fairly simple, but kinda cool ascii art banner*/
static const char *banner =
  "   ____    _ __   _        \n"
  "  / __/___(_) /  (_)__ ___ \n"
  " _\\ \\/ __/ / /__/ (_-</ _ \n"
  "/___/\\__/_/____/_/___/ .__/\n"
  "                    /_/    \n";
//temp hack
extern jmp_buf toplevel_repl;
void SL_NORETURN read_eval_print_loop();
void init_repl(void);
sl_string *read_sexp(void);
#endif
