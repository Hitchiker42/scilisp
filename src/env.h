#ifndef _SL_ENV_H_
#define _SL_ENV_H_
#include "common.h"
#define CALL_STACK_SIZE ((1<<20)/sizeof(sl_obj))
struct sl_environment {
  void *cons_fl; //free list of cons cells
  //this will likely become something like current_package
  //if/when I add support for packages
  sl_symbol_table *obarray;
  //system stuff
  pthread_t current_thread;
  stack_t signal_stack;
  /*
    the next two fields are local bindings, thread_local_bindings
    is for dynamic variables and lexical_env is obviously for
    lexical bindings
  */
  /*
    I'm not sure how to do dynamic bindings, I could eaisly do it the same
    way as with lexical bindings, but I could also just have a flat vector,
    since you can't make a closure with dynamic bindings, so there's no need
    to have levels of bindings.
  */
  sl_obj thread_local_bindings;
  /*
    The lexical environment is a doubly linked list of vectors, with the
    last element being the most deeply nested lexical scope and the first
    element being the top level scope (effectively).
    The first element of each vector points to the previous env, and the
    last element points to the next env, or NIL if either doesn't exist.
    The rest of the elements of the vector contain the actual lexical bindings,
    which are cons cells in the form (symbol . binding).

    The top level lexical environment is not actually implemented this way, for
    efficency. The top level bindings stored in the global obarray. Because
    of this the top level lexical env is just a dummy value.
  */
  sl_obj lexical_env;

  //this will change when I add non-local exits and such
  //but it's what I'm using now to call functions
  sl_obj call_stack[CALL_STACK_SIZE];//1MB
  sl_obj *stack_ptr;
  //llvm thread local context
  //explicit struct declaration to avoid including the entire llvm C api
  struct sl_llvm_context *llvm_context;
  //this always needs to be thread local, shouldn't be accessed by
  //the programmer, and I don't know where else to put it
  int backquote_flag;
};
#define env_stack_top (thread_env->call_stack + CALL_STACK_SIZE)
SL_INLINE void env_stack_push(sl_obj obj){
  if(SL_UNLIKELY(thread_env->stack_ptr > env_stack_top)){
    fprintf(stderr, "Error, lisp stack overflow, terminating program");
    raise(SIGSEGV);
  } else {
    *thread_env->stack_ptr++ = obj;
  }
}
SL_INLINE sl_obj env_stack_pop(void){
  if(SL_UNLIKELY(thread_env->stack_ptr <= thread_env->call_stack)){
    raise_error(0,make_string_const("Lisp stack underflow"));
  } else {
    return *--thread_env->stack_ptr;
  }
  SL_UNREACHABLE;
}
SL_INLINE sl_obj push_lexical_env(sl_obj env){
  //set last element of current env to point to new env
  VSET(thread_env->lexical_env, VECTOR_LENGTH(thread_env->lexical_env)-1, env);
  VSET(env, 0, thread_env->lexical_env);
  thread_env->lexical_env = env;
  return NIL;
}
SL_INLINE sl_obj pop_lexical_env(void){
  sl_obj env = thread_env->lexical_env;
  thread_env->lexical_env = VREF(env, 0);
  VSET(thread_env->lexical_env, VECTOR_LENGTH(thread_env->lexical_env)-1, NIL);
  return env;
}
  
sl_environment* make_environment();
extern sl_symbol_table *global_obarray;
static pthread_once_t init_control = PTHREAD_ONCE_INIT;
#endif
