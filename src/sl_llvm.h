#ifndef __SL_LLVM_H__
#define __SL_LLVM_H__
#include "common.h"
#define llvm_build(thing, args...)              \
  LLVMBuild##thing(thread_env->llvm_context->builder, ##args)
#define llvm_insert(val)                        \
  LLVMInsertIntoBuilder(thread_env->llvm_context->builder, val)
#define llvm_position_at_end(block)             \
  LLVMPositionBuilderAtEnd(thread_env->llvm_context->builder, block);
#define llvm_position_before(block)             \
  LLVMPositionBuilderBefore(thread_env->llvm_context->builder, block);
#define llvm_append_bb(name)                    \
  LLVMAppendBasicBlock(thread_env->llvm_context->function, name)
typedef struct sl_llvm_context sl_llvm_context;
void init_llvm(void);//initialize llvm context in thread env
//drop in replacement for normal eval
sl_obj llvm_eval(sl_obj expr);
sl_obj llvm_compile_lambda(sl_obj expr);
sl_obj llvm_compile_file(sl_obj expr);
LLVMValue *sl_codegen(sl_obj);//generic codegen function
//I'll add more stuff as I figure things out
typedef struct LLVMOpaqueContext LLVMContext;
typedef struct LLVMOpaqueModule LLVMModule;
typedef struct LLVMOpaqueType LLVMType;
typedef struct LLVMOpaqueValue LLVMValue;
typedef struct LLVMOpaqueBasicBlock LLVMBasicBlock;
typedef struct LLVMOpaqueBuilder LLVMBuilder;
#endif
