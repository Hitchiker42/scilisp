#include "print.h"
#define STRING_BUF_MACROS
#include "string_buf.h"
#include "sl_string.h"
#include "vector.h"
#include "cons.h"
#include "stream.h"
#include <printf.h>
/*
  TODO: Maybe reimplement print using dispatch tables, but maybe not
*/
/*
  I'll hopefully change this in the future, but for now I'm going to
  extend printf using gnu c extensions.
*/
int printf_lisp_obj(FILE *stream, const struct printf_info *info,
                    const void *const *args){
  sl_obj obj = *((const sl_obj *)(args[0]));
  sl_string *str = sl_sprint(obj);
  int padding = (info->left ? -info->width : info->width);
  int len = fprintf(stream, "%*.*s", padding,
                    str->len, str->str);
  return len;
}
//I probably won't use this but eh, it doesn't hurt
int printf_lisp_obj_arginfo(const struct printf_info *info,
                            size_t n, int *argtypes,
                            int *size){
  if(n>0){
    argtypes[0] = PA_POINTER;
    size[0] = sizeof(sl_obj*);
  }
  return 1;
}
//a wrapper around snprintf akin to asprintf, it allocates a large
//enough buffer to store the resultent string. it also
//returns a sl_string instead of a char*. sz is an initial guess
//of how much space the resulting string will take
//ultimately I'll change this to use mpfr's printf, and make
//%a and %s print lisp objects
//basically I'll parse the format string and use sprintf to actually
//do the hard part
sl_string* sl_sprintf(char *fmt, size_t sz,...){
  va_list ap,ap2;
  va_start(ap, sz);
  //I can either do a va_copy to get a duplicate va_list
  //or call va_end, and reinitalize with va_start if I need to call
  //vsnprintf twice, the first seems the safer way
  va_copy(ap2,ap);
  sz = sz ? sz : 128;
  char *str = sl_malloc_atomic(sz);
  int space_needed = mpfr_vsnprintf(str,sz,fmt,ap) + 1;
  va_end(ap);
  if(space_needed > sz){
    sl_realloc(str,space_needed);
    mpfr_vsnprintf(str,space_needed,fmt,ap2);
  }
  va_end(ap);
  va_end(ap2);
  sl_string *retval = make_string_unboxed_nocopy(str,space_needed-1,0);
  return retval;
}

sl_string* print_cons(sl_obj cons){
  sl_string_buf *buf = string_buf_from_char('(');
  while(CONSP(XCDR(cons))){
    buf_append_str(buf, sl_sprint(XCAR(cons)));
    buf_append_char(buf, ' ');
    cons = XCDR(cons);
  }
  buf_append_str(buf, sl_sprint(XCAR(cons)));
  if(XCDR(cons)){//improper list / single cons cell
    buf_append_cstr(buf, " . ");
    buf_append_str(buf, sl_sprint(XCDR(cons)));
    buf_append_char(buf, ')');
  } else {
    buf_append_char(buf,')');
  }
  return string_buf_to_string(buf);
}

sl_string* print_vector(sl_obj vect){
  sl_string_buf *buf = make_string_buf();
  string_buf_append_char(buf,'[');
  int i;
  for(i=0;i<VECTOR_LEN(vect)-1;i++){
    sl_string *entry = sl_sprint(VREF(vect,i));
    buf_append_str(buf, entry);
    buf_append_char(buf,' ');
  }
  buf_append_str(buf, sl_sprint(VREF(vect,i)));
  buf_append_char(buf,']');
  return string_buf_to_string(buf);
}
sl_string *print_imm(sl_obj obj){
  if(obj == SL_T){
    return make_string_const_unboxed("t");
  }
  sl_imm imm = XIMM(obj);
  if(imm.tag & SL_number_int_bit){
    return sl_itoa(imm.uint32, 10, 0, 1);
  }
  if(imm.tag & SL_number_float_bit){
    return sl_sprintf("%g",64, imm.float32);
  }
  if(imm.tag == SL_char){
    return sl_char_to_string(imm.char32);
  }
  //some kind of check or something should go here, or
  //wherever this gets called from
  return NULL;
}
sl_string *print_subr(sl_subr *sub){
  if(sub->maxargs != LAMBDA){
    return &(sub->symbol_name);
  } else {
    return make_string_const_unboxed("#<lambda>");
  }
}
sl_string *print_stream(sl_stream *stream){
  sl_string_buf *buf = make_string_buf();
  char *type, *mode;
  switch(stream->type){
    case SL_mem_stream:
      type = "mem stream";
      break;
    case SL_file_stream:
      type = "file stream";
      break;
    case SL_string_stream:
      type = "string stream";
      break;
    default:
      type = "unknown stream";
  }
  switch(stream->mode){
    case SL_read_only:
      mode = " output";
      break;
    case SL_write_only:
      mode = " input";
      break;
    case SL_read_write:
      mode = " input/output";
      break;
    default:
      mode = " unknown";
  }
  string_buf_append_char(buf, '#');
  string_buf_append_char(buf, '<');
  string_buf_append_cstr(buf, mode);
  string_buf_append_cstr(buf, type);
  string_buf_append_char(buf, '>');
  return string_buf_to_string(buf);
}
sl_string *print_env(sl_env *env){
  return make_string_const_unboxed("#<environment>");
}
sl_string *print_misc(sl_obj obj){
  switch(XMISC(obj)->type){
    case SL_subr:
      return print_subr(XSUBR(obj));
    case SL_stream:
      return print_stream(XSTREAM(obj));
    case SL_env:
      return print_env(XENV(obj));
    default:
      return NULL;
  }
}
sl_string *print_number(sl_obj obj){
  sl_number *num = XNUMBER(obj);
  if(num->type == SL_float64){
    return sl_sprintf("%g", 0, num->float64);
  } else if(num->type == SL_bigint){
    DEBUG_PRINTF("Printing bigint %Zd\n", num->bigint);
    uint32_t len = mpz_sizeinbase(num->bigint, 10)+2;
    char *str = sl_malloc(len);
    str = mpz_get_str(str, 10, num->bigint);
    while(str[len-1] != '\0'){
      len--;
    }    
    return make_string_unboxed_nocopy(str, len, 1);
  } else if(num->type == SL_bigfloat){
    return sl_sprintf("%R", 0, num->bigfloat);
  } else {
    return make_string_const_unboxed("#<Unknown number type>");
  }
}
//this is mostly just a fancy dispatch table
sl_string *sl_sprint(sl_obj obj){
  //this quiets a warningspecia
  enum SL_obj_tag obj_type = sl_type_bits(obj);
  switch(obj_type){
    case SL_int0:
    case SL_int1:{
      return sl_itoa(XINT(obj), 10, 0, 0);
      //      return sl_sprintf("%d", 32, XINT(obj));
    }
    case SL_cons: {
      return print_cons(obj);
    }
    case SL_vector: {
      if(STRINGP(obj)){
        return XSTRING(obj);
      }
      return print_vector(obj);
    }
    case SL_symbol: {
      return (obj != NIL ? &XSYMBOL(obj)->name : make_string_const_unboxed("nil"));
    }
    case SL_imm: {
      return print_imm(obj);
    }
    case SL_number: {
      return print_number(obj);
    }
    case SL_misc: {
      return print_misc(obj);
    }
    default:
      raise_error_temp();
  }
  SL_UNREACHABLE;
}
char *c_sprint(sl_obj obj){
  sl_string *str = sl_sprint(obj);
  //currently I don't ever null terminate strings unless they are made from
  //a string constant, so always make a new string for now
  char *retval = sl_malloc_atomic(str->len + 1);
  memcpy(retval, str->str, str->len);
  retval[str->len] = '\0';
  return retval;
}
DEFUN("sprint",sprint,1,1,"(sprint obj) print object to a string, readably")
  (sl_obj obj){
  return PACK_STRING(sl_sprint(obj));
}
DEFUN("print",print,1,2,"(print obj &optional stream) print object to stream,"
      " or stdout if stream is nil")
  (sl_obj obj, sl_obj stream){
  if(NILP(stream)){
    stream = XSYMVAL(Qstandard_output);
  }
  if(!STREAMP(stream)){
    fprintf(stderr,"stream == %#0lx\n", stream);
    raise_error_cstr(0,"Type error in print, expected a stream or nil");
  }
  //TODO: is there a better way to do this, without creating a string?
  sl_obj obj_str = PACK_STRING(sl_sprint(obj));
  Fwrite_string(obj_str, stream);
  stream_write_char(XSTREAM(stream), '\n');
  return NIL;
}
DEFUN("prin1",prin1,1,2,"(prin1 obj &optional stream) print object followed by "
      "a newline to stream, or stdout if stream is nil")
  (sl_obj obj, sl_obj stream){
  if(NILP(stream)){
    stream = XSYMVAL(Qstandard_output);
  }
  if(!STREAMP(stream)){
    fprintf(stderr,"stream == %#0lx\n", stream);
    raise_error_cstr(0,"Type error in print, expected a stream or nil");
  }
  //TODO: is there a better way to do this, without creating a string?
  sl_obj obj_str = PACK_STRING(sl_sprint(obj));
  Fwrite_string(obj_str, stream);
  return NIL;
}
/*
  functions to convert an unsigned integer to a string
*/
static const char _itoa_upper_digits[36] =
  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
static const char _itoa_lower_digits[36] =
  "0123456789abcdefghijklmnopqrstuvwxyz";
//for a 64 bit integer the maximum length of the string representation
//in base n is ceil(64/(lg n)).
static int max_length_per_base[32] =
  {64, 41, 32, 28, 25, 23, 22, 21, 20, 19, 18, 18,
   17, 17, 16, 16, 16, 16, 15, 15, 15, 15, 14, 14,
   14, 14 , 14, 14, 14, 13, 13};
sl_string *sl_itoa(int64_t value, uint32_t base, int uc, int sign){
  //we could probably compute the exact number of bytes needed
  //if we tried, but it shouldn't be a big deal
  //we could also use a temp buf with 64 bytes and copy it
  //after we know the string
  int max_length = max_length_per_base[base];
  char *buf = sl_malloc_atomic(max_length);
  char *buflim = buf + max_length;
//set signed to the sign of value
  if(value < 0){
    //we only get here if value is negitive
    value = NEG(value);
    sign = -1;
  }
  const char *digits = (uc ? _itoa_upper_digits : _itoa_lower_digits);
  if(!base) {base = 10;} //just in case
  switch (base) {
//this lets the compilier optimize specific cases
//it probably wouldn't hurt to write code to do base 2 bytewise
#define SPECIAL(Base)                           \
    case Base:                                  \
      do {                                      \
        *--buflim = digits[value % Base];       \
      } while ((value /= Base) != 0);           \
      break

    SPECIAL (10);
    SPECIAL (16);
    //    SPECIAL (8); screw octal
    default:
      do {
        *--buflim = digits[value % base];
      } while ((value /= base) != 0);
  }
  if(sign > 0){
    *--buflim = '+';
  } else if(sign < 0){
    *--buflim = '-';
  }
  int len = max_length - (buflim - buf);
  return make_string_unboxed(buflim, len, 1);
}

void init_print_symbols(void){
  /*
    NOTE: Don't try to overwrite the %s specifier, it messes things up
   */
  //register the printf handler, overwriting 'a' and 'A'
  register_printf_specifier('a',printf_lisp_obj, printf_lisp_obj_arginfo);
  register_printf_specifier('A',printf_lisp_obj, printf_lisp_obj_arginfo);
  INIT_SUBR(print, 2);
  INIT_SUBR(prin1, 2);
  INIT_SUBR(sprint, 1);
//  INIT_SUBR(format, MANY);
}
