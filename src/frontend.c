#include "frontend.h"
#include "scilisp.h"
static int start_repl=1;

void usage(){
  printf("Usage: SciLisp [options]\n\t");
  printf("Options:\n\t-e|--eval form, evaluate form in the toplevel scope\n\t");
  printf("-q|--quit, quit after argument parsing is finished\n\t");
  printf("-x|--execute form, has the same effect as -q -e form\n\t");
  printf("-l|--load file, load file\n\t");
  printf("-h|--help, print this help message and exit\n\t");
  printf("-s|--script file, ignore the first line of file if it starts with #!.\n\t\t");
  printf("Then evaluate file, with the debugger and error messages disabled");



  return;
}

static const struct option long_options[]={
  /*{name, required/optional/not required, flag(just set to 0), value}*/
  {"eval",1,0,'e'},
  {"execute",0,0,'x'},//same as -q -e expr
  {"help",0,0,'h'},
  {"load",1,0,'l'},
  {"norc",0,0,'n'},//don't load .scilisprc, n isn't actually a short option
  {"quit",0,0,'Q'},//quit after option processing
  {"script",0,0,'s'},//skip #! line, be quiet
  //'-' isn't accepted as a short option, it's just used to test the long opt
  {"stdin",0,0,'-'},//read from stdin, exit on EOF
  {0,0,0,0}};
static const char *optstring = "e:x:l:s:nQh";

int scilisp_getopt(int argc, char **argv){
  optind = 1;
  int option_index = 0, c;
  int quit = 0;
  while(1){
    c = getopt_long(argc,argv,optstring,long_options,&option_index);
    if(c == -1){
      break;
    }
    switch(c){
      case 'e':{
        sl_eval_cstr(optarg);
        break;
      }
      case 'h':{
        usage();
        exit(0);
      }
      case 'l':{
        sl_obj code = lread(sl_open(optarg, SL_read_only));
        Feval(code);
        break;
      }
      case 'n':{
      }
      case 's':{
      }
      case 'x':{
        sl_eval_cstr(optarg);

        exit(EXIT_SUCCESS);
      }
      case 'Q':{
        quit = 1;
        break;
      }
      case '-':{
      }
      default:
        fprintf(stderr,"unknown option %s\n",argv[optind-1]);
        exit(1);
    }
  }
  if(quit){
    exit(EXIT_SUCCESS);
  }
  return optind;
}
/*
  Temporary hack to return to the top level repl after a non-fatal error
*/
static void jmp_to_repl(int sig){
  assert(sig == SIGUSR1 || sig == SIGUSR2);
  longjmp(toplevel_repl, 1);
}
jmp_buf toplevel_repl;
static struct sigaction toplevel_act = {.sa_handler = jmp_to_repl};
int main(int argc, char *argv[]){
  //  sl_environment *thread_env_save = thread_env;
  scilisp_init();
  scilisp_getopt(argc, argv);
  //this is probably unecessary, but it's more portable
  sigemptyset(&toplevel_act.sa_mask);
  sigaction(SIGUSR1, &toplevel_act, NULL);
  sigaction(SIGUSR2, &toplevel_act, NULL);
  init_repl();
  read_eval_print_loop();
  return 0;
}
