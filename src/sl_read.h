/*
  Header for the lisp reader
*/
#ifndef _SL_READ_H_
#define _SL_READ_H_
#include "common.h"
#include "char_types.h"
sl_obj Fread(sl_obj stream);
sl_obj lread(sl_stream *stream);
//should probably prefix these next functions with sl 
sl_obj read_from_string(sl_obj string);
//to make reading in C eaiser
sl_obj read_from_c_string(const char *str, uint32_t len);
sl_obj Qquote;
typedef sl_obj(*read_dispatch_fn)(sl_char,sl_stream*);
typedef struct sl_readtable sl_readtable;
struct sl_readtable {
  read_dispatch_fn dispatch_table[127];
};
/*
  Parse an integer from string, reading at most len characters from str.
  The first invalid character is stored in endptr, if it is not NULL.
  base is an integer from 2-36 specifying the base the number is in.
  The number is automatically converted into a bigint if necessary
*/
sl_obj parse_integer(char *str, char **endptr, int len, int base);
/*
  these includes are only for internal use, which is
  why they are guarded by a macro, use at your own risk.

  They're just a convience anyway.

  TODO: Move this into the .c file?
*/
#ifdef INTERNAL_READER
#include "sl_string.h"
#include "cons.h"
#include "vector.h"
#include "predicates.h"
#include "stream.h"
#include "print.h"
#include "symbol.h"
#define STRING_BUF_MACROS
#include "string_buf.h"
//since we work with mostly characters in the reader
//redefine buf_append from buf_append_str to buf_append_char
#undef buf_append
#define buf_append(buf,c) buf_append_char(buf,c)

SL_INLINE int32_t parse_hex_digit_char(sl_char c){
  if(c >= '0' && c <= '9'){
    return c - '0';
  }
  if(c >= 'a' && c <= 'f'){
    return (c - 'a') + 10;
  }
  if(c >= 'A' && c <= 'F'){
    return (c - 'A') + 10;
  }
  return -1;
}
//macros to make writing the reader a bit eaiser
#define read_char stream_read_char
#define peek_char stream_peek_char
#define unread_char stream_unread_char

#define check_eof(c)                            \
  if(SL_UNLIKELY(c == EOF)){                    \
    raise_error(0,0);                           \
  }
#endif
/* symbol list:
   Function read args:1
*/
#endif /*_SL_READ_H_*/
