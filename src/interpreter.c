/*
  The SciLisp interpreter
*/
#include "common.h"
#include "symbol.h"
#include "cons.h"
#include "sl_string.h"
#include "vector.h"
#include "print.h"
#include "sl_read.h"
inline sl_obj funcall_lambda(sl_lambda *l, sl_obj args);
//returns quickly for atoms, branches to another function to evaluate conses
DEFUN("eval",eval,1,1,"(eval expr) evaluate the given expression in the current "
      "lexical environment")
  (sl_obj expr){
  if(!CONSP(expr)){
    if(SYMBOLP(expr)){
//      DEBUG_PRINTF("Interpreting a symbol\n");
      if(SL_UNLIKELY(XSYMVAL(expr) == SL_UNDEF)){
        raise_error_temp();
      } else {
        return get_symval(expr);
      }
    } else {
      return expr;
    }
  } else {
//    DEBUG_PRINTF("Evaluating an sexp\n");
    return sl_funcall(XCAR(expr),XCDR(expr));
  }
}
/*
  A simple function useful for evaluating lisp code from C
*/
//Since the point of this is to be called from C it takes arguments
//with C types rather than sl types
sl_obj sl_eval_string(const char *str, uint32_t len){
  sl_obj code = read_from_c_string(str, len);
  return Feval(code);
}
         
  
  
//This isn't used much, but it's so long it's worth making a macro
#define FUNCALL_INTERNAL(subr, arglist)                                 \
  ({sl_obj retval;                                                      \
    switch(subr->maxargs){                                              \
      case MANY:                                                        \
        retval = subr->fMANY(numargs, arglist); break;            \
      case 0:                                                           \
        retval = subr->f0(); break;                                     \
      case 1:                                                           \
        retval = subr->f1(arglist[0]); break;                     \
      case 2:                                                           \
        retval = subr->f2(arglist[0], arglist[1]); break;   \
      case 3:                                                           \
        retval = subr->f3(arglist[0], arglist[1], arglist[2]); \
        break;                                                          \
      case 4:                                                           \
        retval = subr->f4(arglist[0], arglist[1], arglist[2], \
                          arglist[3]); break;                     \
      case 5:                                                           \
        retval = subr->f5(arglist[0], arglist[1], arglist[2], \
                          arglist[3], arglist[4]); break;   \
      case 6:                                                           \
        retval = subr->f6(arglist[0], arglist[1], arglist[2], \
                          arglist[3], arglist[4], arglist[5]); \
        break;                                                          \
      case 7:                                                           \
        retval = subr->f7(arglist[0], arglist[1], arglist[2], \
                          arglist[3], arglist[4], arglist[5], \
                          arglist[6]); break;                     \
      case 8:                                                           \
        retval = subr->f8(arglist[0], arglist[1], arglist[2], \
                          arglist[3], arglist[4], arglist[5], \
                          arglist[6], arglist[7]); break;   \
      default:                                                          \
        raise_error_fmt(0,"Error, unimplemented number of arguments %d.", \
                        numargs);                                       \
    }                                                                   \
    retval;})
//I'm not really sure where else to put this
DEFUN("identity", identity, 1, 1, "(identity arg), return arg unchanged")
  (sl_obj arg){
  return arg;
}
DEFUN("apply",apply,1,MANY,"(apply fun &rest args) call fun with args, the"
      "last argument should be a list and it's elements are passed to fun."
      "i.e (apply + 1 2 '(3 4)) is akin to (+ 1 2 3 4).")
  (uint64_t numargs, sl_obj *args){
  //the way funcall currently works it expands the rest argument already
  //so all we need to do is call the function
  sl_obj subr = args[0];
  if(!SUBRP(subr)){
    raise_error_temp();
  }
  if(SL_UNLIKELY(numargs == 1)){
    return XSUBR(subr)->f0();
  }
  //this is the same as: sl_obj rest_arg = args[numargs-1];
  //but by popping the stack we can eaisly push the elements
  //of the rest arg onto the stack.
  sl_obj rest_arg = env_stack_pop();
  if(!LISTP(rest_arg)){
    raise_error_cstr(0,"Error, last argument to apply must be a list");
  }
  //instead of computing the length here, we could do it as we pop the args
  //but I feel like it's best to do it here
  sl_obj nargs = (numargs-1) + CONS_LENGTH(rest_arg);
  //check to see if the number of args passed is compatable with the function
  if(XSUBR(subr)->maxargs < 0 ||
     (XSUBR(subr)->minargs <= nargs &&
      XSUBR(subr)->maxargs >= nargs)){
    //points to the 1st argument
    sl_obj *arglist_start = thread_env->stack_ptr - (numargs - 1);
    while(CONSP(rest_arg)){
      env_stack_push(POP(rest_arg));
    }
    sl_obj retval = FUNCALL_INTERNAL((XSUBR(subr)), arglist_start);
    thread_env->stack_ptr = arglist_start;
    return retval;
  } else {
    raise_error_fmt(0, "Invalid number of arguments, %d, passed to %.*s",
                    nargs, XSUBR(subr)->symbol_name.len, XSUBR(subr)->symbol_name.str);
  }
  //  SL_UNREACHABLE;
}

//TODO: keyword arguments (and lambdas)
//call a function/macro/special form
sl_obj sl_funcall(sl_obj f, sl_obj args){
  if(!SYMBOLP(f)){
    raise_error_temp();
  }
  sl_obj subr_obj = get_symval(f);
//  DEBUG_PRINTF("Got symbol value\n");
  if(!SUBRP(subr_obj)){
    raise_error_fmt(0,"Invalid function %s\n", c_sprint(f));
  }
  sl_subr *subr = XSUBR(subr_obj);
  long numargs;
  //even if the arguments are unevalled we still need a minimum number of args
  if(!LISTP(args)){
    raise_error_temp();
  } else if (NILP(args)){
    numargs = 0;
  } else {
    numargs = CONS_LENGTH(args);
//    DEBUG_PRINTF("Have %d args\n",numargs);
  }
  if(numargs < subr->minargs){
    raise_error_temp();
  } else if(subr->maxargs >= 0 && numargs > subr->maxargs){
    raise_error_temp();
  }
  sl_obj *return_location = thread_env->stack_ptr;
  //pass the arguments as an unevaluated list
  if(subr->maxargs == UNEVALED){
    return subr->fUNEVALED(args);
  }
  if(subr->maxargs == LAMBDA){
    return funcall_lambda(subr->fLAMBDA, args);
  }
  sl_obj *arglist_start = thread_env->stack_ptr;
  int i;
  for(i=0;i<subr->minargs;i++){
    env_stack_push(Feval(POP(args)));
  }
  if(subr->maxargs == MANY){
    while(args != NIL){
      env_stack_push(Feval(POP(args)));
      i++;
    }
  } else {
    while(args != NIL){
      env_stack_push(Feval(POP(args)));
      i++;
    }
    while(i < subr->maxargs){
      env_stack_push(NIL);
      i++;
    }
  }
  /* /\* */
  /*   Currently the rest arg is passed in as vector, i.e a single object, */
  /*   This could be implemented by passing each of the extra arguments */
  /*   as indivual arguments as well. */
  /*  *\/ */
  /* sl_obj rest_arg = NIL; */
  /* if(args != NIL){ */
  /*   rest_arg = make_cons(Feval(POP(args)), NIL); */
  /*   sl_obj tail = rest_arg; */
  /*   while(args != NIL){ */
  /*     XCDR(tail) = make_cons(Feval(POP(args)), NIL); */
  /*     tail = XCDR(tail); */
  /*   } */
  /* } */
  /* env_stack_push(rest_arg); */
  //this should happen After the function returns
  sl_obj retval = FUNCALL_INTERNAL(subr, arglist_start);
  //Since we're pushing the arguments onto the C stack, this could go
  //before the function call since it doesn't matter if we overwrite
  //the old arguments. Still it should go after the function returns;
  thread_env->stack_ptr = arglist_start;
  return retval;
}
/*
  TODO: Integrate lambdas with the internal call/arg stack somehow

  TODO: Clean this whole function up since I've had to change the way
  I store/pass lambda args since I started writing it, so right now
  it's pretty clunky.
*/
inline sl_obj funcall_lambda(sl_lambda *l, sl_obj args){
  DEBUG_PRINTF("Calling lambda function\n");
  int i = 0;
  sl_obj arg, arglist_start = args;
  int opt_args_offset = l->args.num_req_args + l->args.num_opt_args;
  int total_args = opt_args_offset + l->args.has_rest_arg;
  //I forgot that using the args field in the lambda would overwrite
  //default args, so I'm doing this for now, I may revise the lambda
  //structure to have a more efficent way of storing formal arguments
  sl_obj *arg_env = sl_malloc((total_args + 2)*sizeof(sl_obj));
  memcpy(arg_env, l->args.args, (total_args+2)*sizeof(sl_obj));
  //we already know that args has at least 'num_req_args' elements
  while(i<l->args.num_req_args){
    arg = Feval(POP(args));
    XSETCDR(arg_env[++i], arg);
  }
  while(CONSP(args)){
    if(i < opt_args_offset){
      arg = Feval(POP(args));
      XSETCDR(arg_env[++i], arg);
    } else if (l->args.has_rest_arg){
      sl_obj tail = make_cons(NIL, NIL);
      sl_obj rest = tail;
      while(1){
        arg = Feval(POP(args));
        XSETCAR(tail, arg);
        if(!CONSP(args)){
          break;
        } else {
          XSETCDR(tail, make_cons(tail, NIL));
          tail = XCDR(tail);
        }
      }
      XSETCDR(arg_env[++i], rest);
      break;
    } else {
      raise_error_cstr(0, "Error, excess arguments passed to lambda function");
    }
  }
  arg_env[0] = thread_env->lexical_env;
  sl_obj arg_vector = make_svector_obj(arg_env, total_args + 2);
  VSET(thread_env->lexical_env,
       VECTOR_LENGTH(thread_env->lexical_env)-1, arg_vector);
  thread_env->lexical_env = arg_vector;
  sl_obj retval = Feval(l->body);
  thread_env->lexical_env = VREF(thread_env->lexical_env, 0);
  VSET(thread_env->lexical_env,
       VECTOR_LENGTH(thread_env->lexical_env)-1, NIL);
  return retval;
}
  
void init_interpreter_symbols(void){
  INIT_SUBR(eval,UNEVALED);
  INIT_SUBR(identity, 1);
  //I'm not sure how to deal with this
  /* DEFCONST_LISP("&optional",and_optional,Qand_optional,""); */
  /* DEFCONST_LISP("&rest",and_rest,Qand_rest,""); */
  /* DEFCONST_LISP("&key",and_key,Qand_key,""); */
  /* INIT_SUBR(lambda); */
}
