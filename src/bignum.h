#ifndef _SL_BIGNUM_H_
#define _SL_BIGNUM_H_
#include "common.h"
/*
  GMP and MPFR functions work by side effect, that is they store the result
  into a pointer passed to the function, rather than returning it.
  
  By default all SciLisp bignum functions are pure, they don't change their
  arguments, this means most calls to gmp/mpfr functions need to be wrapped
  in a call that creates a new bignum, stores the result in that and returns
  it.
*/
/*
  Functions to create a bignum from a given number.
*/
SL_INLINE sl_bigint* int_to_bigint(uint64_t val){
  sl_bigint *bigint = sl_malloc(sizeof(sl_bigint));
  mpz_init_set_ui(bigint, val);
  return bigint;
}
//essently this just truncates val to its least significant
//63 bits and sets the proper sign
SL_INLINE int64_t bigint_to_int(sl_bigint *val){
  return mpz_get_si(val);
}
SL_INLINE double bigint_to_float(sl_bigint *val){
  return mpz_get_d(val);
}
SL_INLINE sl_bigint* float_to_bigint(double val){
  sl_bigint *bigint = sl_malloc(sizeof(sl_bigint));
  //mpz_init_set_d truncates rather than rounds it's argument
  mpz_init_set_ui(bigint, lrint(val));
  return bigint;
}
SL_INLINE sl_bigint *string_to_bigint(char *str, int base){
  sl_bigint *bigint = sl_malloc(sizeof(sl_bigint));
  mpz_init(bigint);
  mpz_set_str(bigint, str, base);
  return bigint;
}
/*
  parsing bigints/bigfloats from strings requires c_strings, so
  if we have an sl_string we need to make a c_string copy, we can
  do this on the stack.
*/
static sl_bigint *parse_bigint(sl_string *str, int base){
  /* This could be faster, but seems too unsafe
     if(str->str[str->len] == 0){
     return bigint_from_string(str->str);
     }
  */
  char *cstr = strndupa(str->str,str->len);
  return string_to_bigint(cstr, base);
}
  
//too rare a case to justify inlining
sl_bigint* bigfloat_to_bigint(sl_bigfloat *val);
SL_INLINE sl_bigint* copy_bigint(sl_bigint *val){
  sl_bigint *bigint = sl_malloc(sizeof(sl_bigint));
  mpz_init_set(bigint, val);
  return bigint;
}
SL_INLINE sl_bigfloat* bigint_to_bigfloat(sl_bigint *val){
  sl_bigfloat *bigfloat = sl_malloc(sizeof(sl_bigfloat));
  mpfr_init_set_z(bigfloat, val, MPFR_RNDN);
  return bigfloat;
}
SL_INLINE sl_bigfloat* int_to_bigfloat(uint64_t val){
  sl_bigfloat *bigfloat = sl_malloc(sizeof(sl_bigfloat));
  mpfr_init_set_ui(bigfloat, val, MPFR_RNDN);
  return bigfloat;
}
SL_INLINE sl_bigfloat* float_to_bigfloat(double val){
  sl_bigfloat *bigfloat = sl_malloc(sizeof(sl_bigfloat));
  mpfr_init_set_d(bigfloat, val, MPFR_RNDN);
  return bigfloat;
}
SL_INLINE sl_bigfloat* copy_bigfloat(sl_bigfloat *val){
  sl_bigfloat *bigfloat = sl_malloc(sizeof(sl_bigfloat));
  mpfr_init_set(bigfloat, val, MPFR_RNDN);
  return bigfloat;
}
static sl_bigfloat *string_to_bigfloat(char *str){
  sl_bigfloat *bigfloat = sl_malloc(sizeof(sl_bigfloat));
  /*It might be better to use mpfr_init2 so I can guarantee the precision*/
  mpfr_init(bigfloat);
  mpfr_set_str(bigfloat, str, 0, MPFR_RNDN);
  return bigfloat;
}
/*
  parsing bigints/bigfloats from strings requires c_strings, so
  if we have an sl_string we need to make a c_string copy, we can
  do this on the stack.
*/
static sl_bigfloat *parse_bigfloat(sl_string *str){
  /* This could be faster, but seems too unsafe
     if(str->str[str->len] == 0){
     return bigfloat_from_string(str->str);
     }
  */
  char *cstr = strndupa(str->str,str->len);
  return string_to_bigfloat(cstr);
}
/*
  MPFR Rounding modes:
  MPFR_RNDN, round to nearest even.
  MPFR_RNDZ, round to zero.
  MPFR_RNDU, round to +infinity.
  MPFR_RNDD, round to -infinity.
  MPFR_RNDA, round away from zero.
*/
/*
  At some point I need to add a global variable for
  rounding mode, for now I'll just default to MPFR_RNDN.
  Also the rounding mode should also apply to singles/doubles
*/
sl_bigint *sl_obj_to_bigint(sl_obj obj);
sl_bigfloat *sl_obj_to_bigfloat(sl_obj obj);

/*
  Convience macros to create bignum sl_objs (should these be functions?)
*/
#define make_bigint_from_int(val) make_bigint(int_to_bigint(val))
#define make_bigint_from_float(val) make_bigint(float_to_bigint(val))
#define make_bigint_from_string(val) make_bigint(string_to_bigint(val))
#define make_bigint_from_obj(val) make_bigint(sl_obj_to_bigint(val))

#define make_bigfloat_from_int(val) make_bigfloat(int_to_bigfloat(val))
#define make_bigfloat_from_float(val) make_bigfloat(float_to_bigfloat(val))
#define make_bigfloat_from_bigint(val) make_bigfloat(bigint_to_bigfloat(val))
#define make_bigfloat_from_string(val) make_bigfloat(string_to_bigfloat(val))
#define make_bigfloat_from_obj(val) make_bigfloat(sl_obj_to_bigfloat(val))
#endif
