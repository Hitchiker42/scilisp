#ifndef _SL_HASH_H_
#define _SL_HASH_H_
/* 
   catchall hash function, produces a hash value for any sl_obj, if two objects
   are equal (not equalp) they will have the same hash value
   This hashes vectors,conses, etc recursively
*/
uint64_t sl_hash_obj(sl_obj obj);
/*
  fast hash function, if two objects are eq they have the same fast hash,
  for internal use only. Might not actually exist
*/
uint64_t sl_hash_fast(sl_obj obj);

//general hash function usable from lisp, uses sl_hash
sl_obj Fsxhash(sl_obj obj);

/*
  Specific hash functions
*/
/*FNV hash, really simple, ok distribution, and small enough to be inlined.
  However it hashes one byte at a time, the distribution is only
  ok, and there's no room for optimization
  Needs to be defined here to get inlined, which is it's only major
  advantage.
*/
#define fnv_prime_64 1099511628211UL
#define fnv_offset_basis_64 14695981039346656037UL
static inline uint64_t fnv_hash(const void *key,int keylen){
  const uint8_t *raw_data=(const uint8_t *)key;
  int i;
  uint64_t hash=fnv_offset_basis_64;
  for(i=0; i < keylen; i++){
    hash = (hash ^ raw_data[i])*fnv_prime_64;
  }
  return hash;
}
/*
  murmur3 hash, fast, great distribution and avalanche behavior,
  hashes 16 bytes per iteration. However it's big and can't be
  inlined and is (probably) slower than fnv hash for keys < 32 bytes,
  but I'd need to test that to be sure
*/
#include "murmur_hash3.h"
#ifndef SL_HASH /* Definable via configure, probably*/
#define sl_hash(key,len) murmur_hash3_64(key,len)
#else
#define sl_hash(key,len) SL_HASH(key,len)
#endif
#define sl_hash_str(__str) sl_hash((__str)->str, (__str)->len);
#endif
