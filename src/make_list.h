/*
  Non inline versions of make_list_n functions, to be called from wherever
  without causing massive code bloat. Though their existance still causes
  code bloat. Maybe it's worth using the va_args versions after all.
*/
#define make_list(...) VFUNC(make_list_, __VA_ARGS__)
sl_obj make_list_1(sl_obj a);
sl_obj make_list_2(sl_obj a, sl_obj b);
sl_obj make_list_3(sl_obj a, sl_obj b, sl_obj c);
sl_obj make_list_4(sl_obj a, sl_obj b, sl_obj c, sl_obj d);
sl_obj make_list_5(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e);
sl_obj make_list_6(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f);
sl_obj make_list_7(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f, sl_obj g);
sl_obj make_list_8(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f, sl_obj g, sl_obj h);
sl_obj make_list_9(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f, sl_obj g, sl_obj h, sl_obj i);
sl_obj make_list_10(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                    sl_obj f, sl_obj g, sl_obj h, sl_obj i, sl_obj j);
sl_obj make_list_11(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k);
sl_obj make_list_12(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k, sl_obj l);
sl_obj make_list_13(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m);
sl_obj make_list_14(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m, sl_obj n);
sl_obj make_list_15(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m, sl_obj n, sl_obj o);
sl_obj make_list_16(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m, sl_obj n, sl_obj o, sl_obj p);
sl_obj make_list_17(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m, sl_obj n, sl_obj o,
                    sl_obj p, sl_obj q);
sl_obj make_list_18(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m, sl_obj n, sl_obj o,
                    sl_obj p, sl_obj q, sl_obj r);
sl_obj make_list_19(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m, sl_obj n, sl_obj o,
                    sl_obj p, sl_obj q, sl_obj r, sl_obj s);
sl_obj make_list_20(sl_obj a, sl_obj b, sl_obj c,
                    sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                    sl_obj h, sl_obj i, sl_obj j, sl_obj k,
                    sl_obj l, sl_obj m, sl_obj n, sl_obj o,
                    sl_obj p, sl_obj q, sl_obj r, sl_obj s, sl_obj t);
