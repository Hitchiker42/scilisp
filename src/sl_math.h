#ifndef _SL_MATH_H_
#define _SL_MATH_H_
#include "common.h"
#include "sl_math_ops.h"
#include "bignum.h"
//if obj is a number return it's type, otherwize return -1
SL_INLINE SL_number_type get_number_type(sl_obj obj){
  if(INTP(obj)){
    return SL_primitive_int;
  } else if (IMMP(obj)) {
    return XIMM(obj).tag;
  } else if (IND_NUMBERP(obj)) {
    return XNUMBER(obj)->type;
  } else {
    return -1;
  }
}
/*
  Binary math functions come in pairs:
    1. A function of 2 arguments prefixed with 'sl_', primarily for use in c
    2. A function of arbitrarily many arguments, prefixed with 'F' implemented
       using the first function, primarily for use in lisp.
    Both of these act on sl_obj's rather than specific numeric types,
*/
sl_obj sl_add(sl_obj x, sl_obj y);
sl_obj sl_sub(sl_obj x, sl_obj y);
sl_obj sl_mul(sl_obj x, sl_obj y);
sl_obj sl_div(sl_obj x, sl_obj y);
sl_obj sl_exp(sl_obj x, sl_obj y);

sl_obj sl_num_eq(sl_obj x, sl_obj y);
sl_obj sl_num_ne(sl_obj x, sl_obj y);
sl_obj sl_num_lt(sl_obj x, sl_obj y);
sl_obj sl_num_le(sl_obj x, sl_obj y);
sl_obj sl_num_gt(sl_obj x, sl_obj y);
sl_obj sl_num_ge(sl_obj x, sl_obj y);
sl_obj sl_neg(sl_obj x);//no need for this in lisp, just use sub

sl_obj Fadd(uint64_t numargs, sl_obj *args);
sl_obj Fsub(uint64_t numargs, sl_obj *args);
sl_obj Fmul(uint64_t numargs, sl_obj *args);
sl_obj Fdiv(uint64_t numargs, sl_obj *args);
sl_obj Fexp(uint64_t numargs, sl_obj *args);
//comparisons
sl_obj Fnum_eq(uint64_t numargs, sl_obj *args);
sl_obj Fnum_ne(uint64_t numargs, sl_obj *args);
sl_obj Fnum_lt(uint64_t numargs, sl_obj *args);
sl_obj Fnum_le(uint64_t numargs, sl_obj *args);
sl_obj Fnum_gt(uint64_t numargs, sl_obj *args);
sl_obj Fnum_ge(uint64_t numargs, sl_obj *args);
//

sl_obj Fash(sl_obj integer, sl_obj count);
/*DEFCONST("pi", pi, M_PI, "");
  DEFCONST("e", e, M_E, "");*/
#endif
