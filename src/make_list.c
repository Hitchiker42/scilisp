/*
  code for make_list as a (fast) variadic function.
  calling make_list_n appends it's last argument to the front of the list
  and makes a tail coll to make_list_n-1

  it's messy I know
*/
#include <stdint.h>
#include <stdlib.h>
typedef uint64_t sl_obj;
extern sl_obj Fcons(sl_obj,sl_obj);
#define NIL ((sl_obj)NULL)

static inline  sl_obj _make_list_1(sl_obj acc){
  return acc;
}
static inline  sl_obj _make_list_2(sl_obj acc, sl_obj a){
  return _make_list_1(Fcons(a, acc));
}
static inline  sl_obj _make_list_3(sl_obj acc, sl_obj a, sl_obj b){
  return _make_list_2(Fcons(b,acc), a);
}
static inline  sl_obj _make_list_4(sl_obj acc, sl_obj a, sl_obj b, sl_obj c){
  return _make_list_3(Fcons(c,acc), a, b);
}
static inline  sl_obj _make_list_5(sl_obj acc, sl_obj a, sl_obj b, sl_obj c, 
                                  sl_obj d){
  return _make_list_4(Fcons(d,acc), a, b, c);
}
static inline  sl_obj _make_list_6(sl_obj acc, sl_obj a, sl_obj b, sl_obj c, 
                                  sl_obj d, sl_obj e){
  return _make_list_5(Fcons(e,acc), a, b, c, d);
}

static inline  sl_obj _make_list_7(sl_obj acc, sl_obj a, sl_obj b, sl_obj c, 
                                  sl_obj d, sl_obj e, sl_obj f){
  return _make_list_6(Fcons(f,acc), a, b, c, d, e);
}
static inline  sl_obj _make_list_8(sl_obj acc, sl_obj a, sl_obj b, sl_obj c, 
                                  sl_obj d, sl_obj e, sl_obj f, sl_obj g){
  return _make_list_7(Fcons(g,acc), a, b, c, d, e, f);
}
static inline  sl_obj _make_list_9(sl_obj acc, sl_obj a, sl_obj b, sl_obj c, 
                                  sl_obj d, sl_obj e, sl_obj f, sl_obj g,
                                  sl_obj h){
  return _make_list_8(Fcons(h,acc), a, b, c, d, e, f, g);
}
static inline  sl_obj _make_list_10(sl_obj acc, sl_obj a, sl_obj b, sl_obj c, 
                                  sl_obj d, sl_obj e, sl_obj f, sl_obj g, 
                                  sl_obj h, sl_obj i){
  return _make_list_9(Fcons(i,acc), a, b, c, d, e, f, g, h);
}
static inline  sl_obj _make_list_11(sl_obj acc, sl_obj a, sl_obj b, sl_obj c, 
                                  sl_obj d, sl_obj e, sl_obj f, sl_obj g, 
                                  sl_obj h, sl_obj i, sl_obj j){
  return _make_list_10(Fcons(j,acc), a, b, c, d, e, f, g, h, i);
}
sl_obj make_list_1(sl_obj a){
  return _make_list_2(NIL, a);
}sl_obj make_list_2(sl_obj a, sl_obj b){
  return _make_list_3(NIL, a, b);
}sl_obj make_list_3(sl_obj a, sl_obj b, sl_obj c){
  return _make_list_4(NIL, a, b,c);
}sl_obj make_list_4(sl_obj a, sl_obj b, sl_obj c, sl_obj d){
  return _make_list_5(NIL, a, b,c, d);
}
sl_obj make_list_5(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e){
  return _make_list_6(NIL, a, b, c, d, e);
}
sl_obj make_list_6(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f){
  return _make_list_7(NIL, a, b, c, d, e, f);
}
sl_obj make_list_7(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f, sl_obj g){
  return _make_list_8(NIL, a, b, c, d, e, f, g);
}
sl_obj make_list_8(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f, sl_obj g, sl_obj h){
  return _make_list_9(NIL, a, b, c, d, e, f, g, h);
}
sl_obj make_list_9(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                   sl_obj f, sl_obj g, sl_obj h, sl_obj i){
  return _make_list_10(NIL, a, b, c, d, e, f, g, h, i);
}
sl_obj make_list_10(sl_obj a, sl_obj b, sl_obj c, sl_obj d, sl_obj e,
                    sl_obj f, sl_obj g, sl_obj h, sl_obj i, sl_obj j){
  return _make_list_11(NIL, a, b, c, d, e, f, g, h, i, j);
}
/*
(defun gen-_make_list (n)
  (let* ((alphabet [?a ?b ?c ?d ?e ?f ?g ?h ?i ?j ?k ?l ?m
                    ?n ?o ?p ?q ?r ?s ?t ?u ?v ?w ?x ?y ?z
                    ?A ?B ?C ?D ?E ?F ?G ?H ?I ?J ?K ?L ?M
                    ?N ?O ?P ?Q ?R ?S ?T ?U ?V ?W ?X ?Y ?Z])
         (format-arglist (lambda (x) (if (and (not (zerop x)) (zerop (mod x 4)))
                                         (format "\n%ssl_obj %c," (make-string 34 ?\s) x)
                                       (format " sl_obj %c," x))))
                                                 
         (arglist (apply #'concat
                         (append (mapcar format-arglist (subseq alphabet 0 (1- n)))
                                 (list (format " sl_obj %c" (aref alphabet (1- n)))))))
         (args (concat (format "Fcons(%c,acc), " (aref alphabet (1- n)))
                       (apply #'concat (mapcar (lambda (x) (format "%c, " x))
                                               (subseq alphabet 0 (- n 2))))
                       (string (aref alphabet (- n 2))))))
     (format "static inline sl_obj _make_list_%d(%s){\n return _make_list_%d(%s);\n}\n"
             n arglist (1- n) args)))
(defun gen-make_list (n)
  (let* ((alphabet [?a ?b ?c ?d ?e ?f ?g ?h ?i ?j ?k ?l ?m
                    ?n ?o ?p ?q ?r ?s ?t ?u ?v ?w ?x ?y ?z
                    ?A ?B ?C ?D ?E ?F ?G ?H ?I ?J ?K ?L ?M
                    ?N ?O ?P ?Q ?R ?S ?T ?U ?V ?W ?X ?Y ?Z])
         (format-arglist (lambda (x) (if (and (not (zerop x)) (zerop (mod x 4)))
                                         (format "\n%ssl_obj %c," (make-string 34 ?\s) x)
                                       (format " sl_obj %c," x))))
                                                 
         (arglist (apply #'concat
                         (append (mapcar format-arglist (subseq alphabet 0 (1- n)))
                                 (list (format " sl_obj %c" (aref alphabet (1- n)))))))
         (args (concat (format "Fcons(%c,acc), " (aref alphabet (1- n)))
                       (apply #'concat (mapcar (lambda (x) (format "%c, " x))
                                               (subseq alphabet 0 (- n 2))))
                       (string (aref alphabet (- n 2))))))
     (format "sl_obj make_list_%d(%s){\n return _make_list_%d(%s);\n}\n"
     n arglist (1+ n) args)))
*/
