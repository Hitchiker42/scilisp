#include "control.h"
#include "cons.h"
#include "print.h"
DEFUN("if",if, 2,UNEVALED, "(if cond then &rest else) evaluate cond and if "
      "it is non-nil return the value of then, otherwise return the value "
      "of else or nil if there is no else, else is wrapped in an "
      "implicit progn.")
  (sl_obj args){
  //we get args as an unevaluated list, knowing we have 2 args minimum
  sl_obj cond = POP(args);
  sl_obj then = POP(args);
  sl_obj elses = args;
  cond = Feval(cond);
  if(!NILP(cond)){
    return Feval(then);
  } else if(NILP(elses)){
    return NIL;
  } else {
    return Fprogn(elses);
  }
}
DEFUN("progn",progn,0, UNEVALED, "(progn &rest body) evaluate each of the forms"
      " of body and return the value of the last body form")
  (sl_obj args){
  if(SL_UNLIKELY(NILP(args))){
    return NIL;
  }
  sl_obj form, value;
  do {
    form = POP(args);
    DEBUG_PRINTF("Evaluating %s\n",c_sprint(form));
    value = Feval(form);
  } while (!NILP(args));
  return value;
}
//TODO: Allow breaking out of a loop
DEFUN("while",while,1,UNEVALED, "(while test &rest body) Evaluate test, if "
      "non-nil evaluate body, and repeat.")
     (sl_obj args){
  sl_obj test = XCAR(args);
  sl_obj body = XCDR(args);
  while(!NILP(Feval(test))){
    Fprogn(body);//implicit progn
  }
  return NIL;
}
/*
  Temporary, these should be implemented in lisp
*/
DEFUN("and",and,0,UNEVALED,"(and &rest forms), return nil if any of forms"
      "evaluate to nil, otherwise return the value of the last form")
  (sl_obj args){
  sl_obj value;
  while(CONSP(args)){
    sl_obj form = POP(args);
    value = Feval(form);
    if(NILP(value)){
      return NIL;
    }
  }
  return value;
}
DEFUN("or",or,0,UNEVALED,"(or &rest forms), return the result of the first"
      "form that evaluates to a non-nil value, or nil if none do")
  (sl_obj args){
  while(CONSP(args)){
    sl_obj form = POP(args);
    sl_obj value = Feval(form);
    if(!NILP(value)){
      return value;
    }
  }
  return NIL;
}

/*
  ;;Macros implemented in lisp
  (defmacro prog1 (form-1 &rest forms)
   (let ((retval (gensym)))
    `(let ((,retval ,form-1))
       (progn ,@forms))))
;;I need to be able to break out of loops to do this eaisly
  (defmacro cond (&rest cases)

*/
void init_control_symbols(void){
  INIT_SUBR(if, UNEVALED);
  INIT_SUBR(while, UNEVALED);
  INIT_SUBR(progn, UNEVALED);
  INIT_SUBR(and, UNEVALED);
  INIT_SUBR(or, UNEVALED);
}
