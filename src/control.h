#ifndef _SL_CONTROL_H_
#define _SL_CONTROL_H_
#include "common.h"
/*
  Control structures, loops, conditionals, non-local exits (though I may
  have a frames.c/h for some of that code), etc...
*/
/*
  There should be a minimal ammount of these defined as special forms,
  as opposed to macros. Any special forms will need to be implemented
  in llvm, but macros won't.

  We need:
  A looping construct (a while)
  (Optional) A binding looping construct (for/foreach)
  A simple conditional (if/cond/both)
  A throw and catch, using stack frames of some kind.
  A sequencing construct (progn) (and prog1, but that's a macro)
  
*/
sl_obj Fif(sl_obj args);
sl_obj Fprogn(sl_obj args);
sl_obj Fwhile(sl_obj args);
sl_obj Fthrow(sl_obj args);//most code is probably in frames.c/h
sl_obj Fcatch(sl_obj args);
#endif
