#include "vector.h"
#include "cons.h"
DEFUN("vref",vref,2,2,"(vref vec ind) return the element at the position"
      "ind in the simple vector or string vec, raise an error if there are"
      "less than ind elements in vec")
  (sl_obj vec, sl_obj ind){
  //it might be faster to test if vec is a vector, extract it
  //then test the tag field, but this makes for cleaner code
  //and the compiler can probably optimize it
  if(!SVECTORP(vec) || !INTP(ind)){
    raise_error(0,0);
  }
  int64_t index = XINT(ind);
  if(VECTOR_LENGTH(vec) < index){
    raise_error(0,0);
  }
  if(VECTOR_TYPE(vec) == SL_untyped_svector){
    return VREF(vec, index);
  } else {
    return TYPED_VREF(vec,index);
  }
}
sl_obj typed_vset(sl_svector *vec, int64_t index, sl_obj obj){
  raise_error(0,0);
}
DEFUN("vset",vset,3,3, "(vset vector index value) set the element of 'vector'"
      "at the index 'index' to 'value'")    
  (sl_obj vec, sl_obj ind, sl_obj obj){
  if(!SVECTORP(vec) || !INTP(ind)){
    raise_error(0,0);
  }
  int64_t index = XINT(ind);
  if(VECTOR_LENGTH(vec) < index){
    raise_error(0,0);
  }
  if(VECTOR_TYPE(vec) == SL_untyped_svector){
    return (VREF(vec, index) = obj);    
  } else {
    return typed_vset(XSVECTOR(vec), index, obj);
  }
}
DEFUN("make-vector",make_vector, 1, 2,
      "(make-vector length &optional initial-element) create a vector of "
      "length 'length' with each element initialized to initial-element, "
      "if inital-element is not provided the contents are undefined")
  (sl_obj length, sl_obj initial_element){
  if(!INTP(length)){
    raise_error(0,0);
  }
  sl_svector *vec = make_svector(XINT(length));
  if(initial_element != NIL){
    int64_t i;
    for(i = 0; i < XINT(length); i++){
      vec->value[i] = initial_element;
    }
  }
  return PACK_VECTOR(vec);
}
sl_obj reverse_vector(sl_obj obj){
  sl_obj *arr = XSVECTOR(obj)->value;
  uint32_t len = XSVECTOR(obj)->len;
  sl_obj *rra = sl_malloc(sizeof(sl_obj) * len);
  int i;
  for(i=0;i<len;i++){
    rra[i] = arr[(len-1)-i];
  }
  return make_svector_obj(rra, len);
}
sl_obj nreverse_vector(sl_obj obj){
  //all that really matters for typed verisions
  //is the size of the elements
  int i;
  sl_obj *arr = XSVECTOR(obj)->value;
  uint32_t len = XSVECTOR(obj)->len;
  for(i = 0; i < len/2; i++){
    sl_obj temp = arr[i];
    arr[i] = arr[(len-1)-i];
    arr[(len-1)-i] = temp;
  }
  return obj;//I think
}
/*
  It's a little klunky to have a seperate case for functions
  which take MANY args, but I'm not sure how else to do it in
  a way that retains the speed gained by calling function pointers,
  instead of having to call functions indirectly
*/
sl_obj map_vector(sl_obj fn, sl_obj vec){
  //this version copies its argument
  sl_obj copy = make_new_svector_obj(VECTOR_LEN(vec));
  int i;
  if(XSUBR(fn)->maxargs >= 1){
    sl_obj(*fp)(sl_obj) = XSUBR(fn)->f1;
    for(i=0;i<VECTOR_LEN(vec);i++){
      VSET(copy, fp(VREF(vec, i)), i);
    }
  } else if(XSUBR(fn)->maxargs == MANY){
    sl_obj *arg_ptr;
    sl_obj(*fp)(uint64_t,sl_obj*) = XSUBR(fn)->fMANY;
    for(i=0;i<VECTOR_LEN(vec);i++){
      arg_ptr = &(VREF(vec, i));
      VSET(copy, fp(1, arg_ptr), i);
    }
  } else {
    raise_error_temp();
  }
  return copy;
} 
sl_obj reduce_vector(sl_obj fn, sl_obj vec, sl_obj start){
  //typecheck
  int i = 0;
  sl_obj acc = NILP(start) ? VREF(vec, i++) : start;
  if(XSUBR(fn)->maxargs >= 2){
    sl_obj(*fp)(sl_obj,sl_obj) = XSUBR(fn)->f2;
    while(i<VECTOR_LEN(vec)){
      acc = fp(acc, VREF(vec,i++));
    }
  } else if(XSUBR(fn)->maxargs == MANY){
    sl_obj(*fp)(uint64_t,sl_obj) = XSUBR(fn)->fMANY;
    sl_obj args[2];
    while(i<VECTOR_LEN(vec)){
      args[0] = acc; args[1] = VREF(vec, i++);
      acc = fp(2, args);
    }
  }
  return acc;
}
void init_vector_symbols(void){
  INIT_SUBR(vref, 2);
  INIT_SUBR(vset, 3);
  INIT_SUBR(make_vector, 2);
}
