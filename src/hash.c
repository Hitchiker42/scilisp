#include "common.h"
#include "hash.h"
/*
  A lot of the core code is the same as the code for 
  symbol tables but more general.
*/
//a hash table uses the vector tag, so it's first few
//fields need to be in a specific order
struct sl_hash_table {
  uint8_t tag;//tag to indicate that this is a hash table
  int padding : 24;//padding for alignment
  uint32_t size;//lines up with length field of vector
  //the table holds pointers to symbols so it can be atomic
  struct sl_hash_entry **table;//implementation detail
  uint32_t entries;
  float load_factor;
  sem_t sem;
  sl_obj(*test)(sl_obj,sl_obj);//eq/eqv/equal/equalp
  volatile int rehash_pending;
  volatile int num_threads_using;
};
struct sl_hash_entry {
  sl_obj val;
  uint64_t hv;
  struct sl_hash_entry *next;
};
static uint64_t sxhash_internal(sl_obj obj);
static uint64_t hash_cons(sl_obj cons){
  //I'm not sure if this works (the xoring that is)
  uint64_t hv = sxhash_internal(POP(cons));
  while(CONSP(cons)){
    hv ^= sxhash_internal(POP(cons));
  }
  if(!NILP(cons)){
    hv ^= sxhash_internal(cons);
  }
  return hv;
}

static uint64_t hash_number(sl_obj num){
  switch(type){
    case(SL_bigint):{
      //put in a special case for num_limbs = 0 (i.e
      //it fits in an integer
      sl_bigint *bigint = XBIGINT(num);
      size_t num_limbs = mpz_size(bigint);
      mp_limb_t *limbs = mpz_limbs_read(bigint);
      //it shouldn't matter what the size of mp_limb is
      return sl_hash(limbs, num_limbs*sizeof(mp_limb_t));
    }
    case(SL_bigfloat):{
      //need to define mpfr_get_limbs, and other stuff
    }
    case(SL_primitive_int):{
      return sl_hash(XINT(num),sizeof(sl_obj));
    }
    case(SL_double):{
      return sl_hash(XDOUBLE(num),sizeof(double));
    }
    default:
      raise_error_temp();
  }
}
static uint64_t hash_symbol(sl_obj sym){
  return XSYMBOL(sym)->hv;
}
DEFUN("make-hash-table",make_hash_table,0,MANY,
      "(make-hash-table &key :test :size :rehash_threshold :growth-factor) "
      "make a hash table with parameters specified by the given keyword "
      "arguments. Test is the function used to compare hash entries and must "
      "be one of eq, eqv, equal, or equalp")
  (uint64_t numargs, sl_obj *args){
}
