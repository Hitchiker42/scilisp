#ifndef _SL_STREAM_H_
#define _SL_STREAM_H_
#include "common.h"
#include "fcntl.h"
#define SL_read_only O_RDONLY
#define SL_write_only O_WRONLY
#define SL_read_write O_RDWR
/*
  Currently streams are read only, either I need to add write capability to 
  these streams or have a seperate stream api for lisp streams
*/
/*
  There are two distinct uses of streams, streams internal to the
  implementation and streams avaiable to the user. Streams
  used by the implementation need to be optimized for the read
  function, since reading things needs to be fast. While user
  streams should provide more functionality and flexibility.*/
enum SL_stream_type {
  SL_mem_stream = 0,
  SL_file_stream = 1,
  SL_string_stream = 2
};

struct sl_stream {
  void *stream;
  uint32_t mode;//read/write/exec kind of a thing
  uint32_t type;
};
/*
  General stream functions,
  some of these may be implemented as macros
*/
sl_char stream_read_char(sl_stream *);
sl_char stream_peek_char(sl_stream *);
void stream_unread_char(sl_stream *);
long stream_seek(sl_stream *stream, long offset, int whence);
void stream_cleanup(sl_stream *stream);
off_t stream_get_pos(sl_stream *stream);
//return -1 if stream doesn't have a length (e.g a pipe/terminal/FIFO)
long stream_length(sl_stream *stream);
//these are mostly to allow optimization in the reader
//also for efficiency's sake the strings returned may share
//structure with the stream
const char *stream_read_chars(sl_stream *s, int chars);
sl_string *stream_read_until_char(sl_stream *s, sl_char c);
sl_string *stream_read_accept(sl_stream *s, sl_string *accept);
sl_string *stream_read_reject(sl_stream *s, sl_string *reject);
/*
  stream output functions, not all streams can be used for output,
  specifically string streams can't, a special stream, the null stream
  can only be used for output, and basically acts as /dev/null
*/
sl_char stream_write_char(sl_stream *stream, sl_char c);
int stream_write_string(sl_stream *stream, sl_string *str);
//same as stream_write_string, but appends a newline to str
int stream_write_line(sl_stream *stream, sl_string *str);
/*
  stream creation
*/
sl_stream *sl_open(const char *filename, int mode);
sl_stream *file_descriptor_to_stream(int fd, int mode);
sl_stream *FILE_to_stream(FILE *f, int mode);
sl_stream *buffer_to_stream(uint8_t *buf, uint32_t size, int mode);
//strings are always readonly (for now)
sl_stream *string_to_stream(sl_string *str);
SL_INLINE sl_obj make_stream_obj(sl_stream *stream){
  sl_misc *misc = sl_malloc(sizeof(sl_misc));
  misc->type = SL_stream;
  misc->value = stream;
  return PACK_MISC(misc);
  return make_misc(stream, SL_stream);
}
//these are just for convience
/* static sl_obj filename_to_stream_obj(const char *filename){ */
/*   return make_stream_obj(sl_open_stream(filename)); */
/* } */
/* static sl_obj file_descriptor_to_stream_obj(int fd){ */
/*   return make_stream_obj(file_descriptor_to_stream(fd)); */
/* } */
/* static sl_obj FILE_to_stream_obj(FILE *f){ */
/*   return make_stream_obj(FILE_to_stream(f)); */
/* } */
/* static sl_obj string_to_stream_obj(sl_string *str){ */
/*   return make_stream_obj(string_to_stream(str)); */
/* } */
/*
  lisp stream functions
*/
sl_obj Fread_char(sl_obj obj);
sl_obj Funread_char(sl_obj obj);
sl_obj Fpeek_char(sl_obj obj);

sl_obj Fread_byte(sl_obj obj);
sl_obj Fsl_open(sl_obj obj, sl_obj mode);

sl_obj Fwrite_char(sl_obj stream, sl_obj c);
sl_obj Fwrite_string(sl_obj stream, sl_obj str);
sl_obj Fwrite_line(sl_obj stream, sl_obj str);

/*
  different types of streams
*/
typedef struct mem_stream mem_stream;
typedef struct file_stream file_stream;
typedef struct string_stream string_stream;
struct file_stream {
  FILE *file;
  sl_char last_char;
  uint32_t size;//can be 0, e.g. for stdin
};
/*
  This is really a string input stream, I'll likely add a string output stream
  that's just a wrapper around a string buf
*/
struct string_stream {
  sl_string str;
  uint32_t pos;
};
//this is what gets used in read, if possible
struct mem_stream {
  uint8_t *buf;
  uint8_t *bufptr;//current location
  uint32_t size;//memory allocated to the buffer
  uint32_t len;//length of the buffer that is actually used
};
/*
  The character based funcitons on streams are always inlined for speed,
  as for any other stream function it's left to the compiliers discretion
*/
SL_INLINE sl_char mem_stream_read_char(mem_stream *stream){
  return *stream->bufptr++;
}
SL_INLINE sl_char file_stream_read_char(file_stream *stream){
  sl_char c = stream->last_char = fgetc(stream->file);
  return (sl_char)(uint8_t)c;
}
SL_INLINE sl_char string_stream_read_char(string_stream *stream){
  if(stream->pos >= stream->str.len){
    return EOF;
  } else {
    return stream->str.str[stream->pos++];
  }
}
SL_INLINE sl_char mem_stream_peek_char(mem_stream *stream){
  return *stream->bufptr;
}
SL_INLINE sl_char file_stream_peek_char(file_stream *stream){
  sl_char c = fgetc(stream->file);
  ungetc(c, stream->file);
  return c;
}
SL_INLINE sl_char string_stream_peek_char(string_stream *stream){
  return stream->str.str[stream->pos]; 
}
SL_INLINE void mem_stream_unread_char(mem_stream *stream){
  stream->bufptr--;
}
SL_INLINE void file_stream_unread_char(file_stream *stream){
  ungetc(stream->last_char, stream->file);
}
SL_INLINE void string_stream_unread_char(string_stream *stream){
  stream->pos--;
}
SL_INLINE sl_stream *make_stream(void *stream,
                                 enum SL_stream_type type, int mode){
  sl_stream *retval = sl_malloc(sizeof(sl_stream));
  retval->stream = stream;
  retval->type = type;
  retval->mode = mode;
  return retval;
}
/*
  functions on specific types of streams
*/
mem_stream *buffer_to_mem_stream(uint8_t *buf, uint32_t size);
mem_stream *string_to_mem_stream(sl_string *str);
mem_stream *file_to_mem_stream(char *filename);

file_stream *file_stream_from_pathname(char *filename);
file_stream *file_stream_from_FILE(FILE *file);

string_stream *string_to_string_stream(sl_string *str);

long mem_stream_seek(mem_stream*,long,int);
long string_stream_seek(string_stream*,long,int);
long file_stream_seek(file_stream*,long,int);
//temporary
#ifdef STREAM_TEMP
#define sl_stream mem_stream
#define sl_read_char(stream) mem_stream_read_char(stream);
#define sl_unread_char(stream) mem_stream_unread_char(stream);
#endif
#endif
