/*
  Definations of lisp types, and typechecking macros.
  Is included by common.h, so sholdn't have to be included by
  any other file
*/
#ifndef SL_TYPES_H
#define SL_TYPES_H
/*
  The 3 least significant bits of a pointer will always be 0, because
  pointers are aligned to 8 byte boundries.
  Furthermore because the garbage collecter is compiled with
  ALL_INTERIOR_POINTERS defined those bits can be set and the
  object will still be kept.

  This allows us to use the least significant 3 bits of a sl_obj to store
  type information, for 8 different types:

  -We use 2 values for integers to give us 62 bit instead of 61 bit integers
  -A pointer to a cons cell
  -A pointer to a vectorlike object (array, string, actual vector)
  -A pointer to a symbol
  -A pointer to a number (i.e a double, rational, complex or bignum)
  -A pointer to any other type of object
  -A non 62 bit integer immediate (character, float or finite sized integers)

  Pointers to vectorlike objects, numbers and miscellaneous objects point
  to a struct which contains more specific type information, pointers
  to conses and symbols point directly to the object.

  Since type information is somewhat scattered about at some point I
  intend to make an enum of all type values to be used as the values
  of type objects
*/
/*
  An explaination of the naming rules used in the scilisp code:
  -Any defined type should have a leading sl_
  -enum members should have a leading SL_
  -Macros should generally be all upper case (though there are exceptions)
  -structs and unions should be typedefed unless there is a compelling
  reason not to, but enums shouldn't be typedefed.

  -functions need three names:
  --the name used in calling the function from lisp,
  --the name used in calling the function from C,
  --the name of the symbol holding the function in C
  --The name in C is F followed by the lisp name converted to C
  --The symbol name is Q followed by the lisp name converted to C

  -As there are more acceptable characters for names in lisp than C
  there are rules to translate lisp names to C
  -Keep in mind, these are only for variables used in both C and lisp, not
   for lisp only variables
  -These are applied in order
  --Non leading/trailing '-'s are converted to '_'s
  (this imples that variable-name -> variable_name but 1- doesn't go to 1_)
  --leading numbers are transated to the number spelled out
  (i.e 8-name (a horrible variable name but eh) goes to eight_name)
  --A trailing '?' is translated to a trailing 'p'
  (i.e cons? -> consp)
  --A trailing '!' is translated to a leading 'n', i.e append! -> nappend
    there are some exceptions, i.e set!->setq
  --A variable enclosed in '*'s, i.e (*stdin*), has those stars removed,
    this is the convention used to denote global variables
  --any '*' is translated to "_star", while any '+', is translated to "_plus"
    if not at the end of a symbol a second '_' is appended (i.e _star_)

*/
/*All typedefs not local to a specific file should go here, gcc is fine
  with repeated typedefs in c99 but clang throws a fit*/
/* any object that isn't part of the sl_type enum */
typedef struct sl_misc sl_misc;
/* Vectors, strings and vectorlike objects (arrays, hashtables, etc) */
/* Actual vectors have the type sl_svector */
typedef struct sl_vector sl_vector;
typedef struct sl_svector sl_svector;
/* Numbers other than <62 bit integers or 32 bit reals */
typedef struct sl_number sl_number;
/* lisp symbols */
typedef struct sl_symbol sl_symbol;
/* non integer immediates */
typedef union sl_imm sl_imm;
/* cons cells */
typedef struct sl_cons sl_cons;
/* strings, same structure as a vector */
typedef struct sl_string sl_string;
/* may be misc or vector type, I haven't decided*/
typedef struct sl_subr sl_subr;
/* type of an input/output stream */
typedef struct sl_stream sl_stream;
/* Internal per thread environment */
typedef struct sl_environment sl_environment;
typedef sl_environment sl_env;
/* hash table used to store symbols */
typedef struct sl_symbol_table sl_symbol_table;
/* Used for lambda-expressions and closures */
typedef struct sl_lambda sl_lambda;
/* types for real numbers (i.e floats and doubles)*/
typedef float single;
typedef double real64_t;
typedef single real32_t;
typedef single float32_t;
typedef double float64_t;
typedef int_fast32_t fast_int;
typedef uint32_t sl_char;
typedef uint8_t byte;
/*The macros are for use in statically allocated objects*/
enum SL_obj_tag {
  /* Integers take up two tags so that they can be 62 bits instead of 61 */
  SL_int0 = 2,
  #define SL_INT0 2
  SL_int1 = 6,
  #define SL_INT1 6
  //by having a symbol have no bits set NULL can be identicial to NIL
  SL_symbol = 0, //pointer to a symbol
  #define SL_SYMBOL 0
  SL_cons = 1, //pointer to a cons cell
  #define SL_CONS 1
  SL_number = 3, //pointer to a number (rational, complex or bignum)
  #define SL_NUMBER 3
  SL_vector = 4, //pointer to a vectorlike object
  #define SL_VECTOR 4
  SL_misc = 5, //pointer to anything else
  #define SL_MISC 5
  SL_imm = 7, //non integer immediate
  #define SL_IMM 7
};
#define SL_int_mask 0x2UL
#define SL_int_case case SL_int0: case SL_int1
#define sl_type_bits(obj) (obj & 0x7UL)
#define XSL_TYPE(obj) (obj & 0x7UL) //use this in preference to sl_type_bits
/* Macros to extract values from sl_objs */
/* Ints default to being signed, this is important because
   (int64_t)((int64_t)-1 >> 2) = -1 while
   (int64_t)((uint64_t)-1 >> 2) = INT64_T_MAX/2 - 2
   this is because since shifting a uint is a logical shift, while 
   shifting an int is an arithmatic shift.
   
   It is worth nothing that although
   (uint64_t)((int64_t)-1>>2) != (uint64_t)((uint64_t)-1>>2)
   this is not an issue, since any number large enough to trigger this
   is automatically converted to a bigint.
*/
#define XINT(obj) (((int64_t)(obj)) >> 2)
#define XUINT(obj) (((uint64_t)(obj)) >> 2)
#define XSINT(obj) ((int64_t)(obj)) >> 2)
#define XPTR(obj) ((void*)(obj & (~0x7)))
#define XCONS(obj) ((sl_cons*)XPTR(obj))
#define XVECTOR(obj) ((sl_vector*)XPTR(obj))
#define XSYMBOL(obj) ((sl_symbol*)XPTR(obj))
#define XNUMBER(obj) ((sl_number*)XPTR(obj))
#define XMISC(obj) ((sl_misc*)XPTR(obj))
//I think this should work since sl_imm is a union...
#define XIMM(obj) ((sl_imm)obj)
//convience macros, they do the same thing as make_X but don't
//require any type casting.
#if !(defined __clang__)
#define PACK_INT(obj) ((obj)<<2 | SL_int_mask)
#define PACK_CONS(obj) ((sl_obj)obj | SL_CONS)
#define PACK_STRING(obj) ((sl_obj)obj | SL_VECTOR)
#define PACK_VECTOR(obj) ((sl_obj)obj | SL_VECTOR)
#define PACK_SYMBOL(obj) ((sl_obj)obj | SL_SYMBOL)
#define PACK_NUMBER(obj) ((sl_obj)obj | SL_NUMBER)
#define PACK_IMM(obj) ({.val = obj, .sl_type = SL_IMM})
#define PACK_MISC(obj) ((sl_obj)obj | SL_MISC)
#define PACK_SL_OBJ(obj, type) ((sl_obj)obj | type)
#else
/* Clang is a bit annoying, it doesn't expand macros when cheking for
   constant expressions, so we need to do this*/

#define PACK_INT(obj) ((obj)<<2 | 0x2)
#define PACK_CONS(obj) ((sl_obj)obj | 0x1)
#define PACK_STRING(obj) ((sl_obj)obj | 0x4)
#define PACK_VECTOR(obj) ((sl_obj)obj | 0x4)
#define PACK_SYMBOL(obj) ((sl_obj)obj)
#define PACK_NUMBER(obj) ((sl_obj)obj | 0x3)
#define PACK_IMM(obj) ({.val = obj, .sl_type = 0x7})
#define PACK_MISC(obj) ((sl_obj)obj | 0x5)
#define PACK_SL_OBJ(obj, type) ((sl_obj)obj | type)
// These insure that if the values of */
static_assert(SL_SYMBOL == 0, "");
static_assert(SL_int_mask == 2, "");
static_assert(SL_CONS == 1, "");
static_assert(SL_VECTOR == 4, "");
static_assert(SL_NUMBER == 3, "");
static_assert(SL_IMM == 7, "");
static_assert(SL_MISC == 5, "");
#endif

//this checks if either of the two most significant bits are set, if they
//are then obj would overflow when packed into an sl_obj
#define CHECK_INT_OVERFLOW(obj) (obj & (3 << 62))
/* Clang can't deal with bitwise operations on compile time constants
   So we have to do this. Its a hack, but there's no other way to define
   constants, that are actually constant in c.
 */

/* NIL and T are the boolean values of scilisp. The internal representation
   of NIL is NULL, so truth is the same in c and lisp. Any nonzero value is
   true, while 0 is false. I'm not sure if I should keep things this way
   or change to having a #f and #t ala scheme.
*/
#define NIL ((sl_obj)NULL)
//for completeness sake
//static const sl_obj Qt;
#define SL_T Qt
#define SL_NIL NIL
#define SL_BOOLEAN(expr) ((expr) ? SL_T : NIL)
#define SL_UNBOUND Qunbound
#define SL_UNDEF ((sl_obj)&Qunbound)
//static const sl_obj Qnil = NIL;
//two objs are eq iff they are identicial (this has an important implication
//in that two doubles aren't eq unless they are at the same memory location
//even if they are numerically identical


//trivial function to make an sl_object, used because it
//hides the internal implementation of sl_obj so it can be
//changed without effecting other code
SL_INLINE sl_obj make_sl_obj(uint64_t val, enum SL_obj_tag type){
  return (sl_obj)(val|type);
}
#define make_int_const(val) ((val << 2 ) | SL_INT0)
SL_INLINE sl_obj make_int(uint64_t val){
  return (sl_obj)((val << 2) | SL_int0);
}
//put these here so cons.h doesn't need to be included
//in order to get them
#define XCAR(obj) (XCONS(obj)->car)
#define XCDR(obj) (XCONS(obj)->cdr)
struct sl_cons {
  sl_obj car;
  sl_obj cdr;
};
#define SL_number_int_bit (1<<8)
#define SL_number_float_bit (1<<9)
#define SL_number_complex_bit (1<<10)
#define SL_number_rational_bit (1<<11)
#define SL_number_finite_bit (1<<12)
//set if type fits into 64 bits
#define SL_number_word_sized_bit ((1<<13) | SL_number_finite_bit)
#define SL_number_imm_bit ((1<<14) | SL_number_word_sized_bit)
//mask away bits that contain extra information
#define SL_number_type_mask (0x7f)
#define SL_number_is_finite(obj) (obj->type & SL_number_finite_bit)
#define SL_number_is_integer(obj) (obj->type & SL_number_int_bit)
#define SL_number_is_float(obj) (obj->type & SL_number_float_bit)
#define SL_number_is_bignum(obj) (!SL_number_is_finite(obj))
//key, used in the same capacity as a key for a hash table
//allows for faster switches/dispatch tables
#define SL_number_key(obj) (XNUMBER(obj)->type & 0xf)
#define SL_number_type_key(type) (type & 0xf)
#define XNUMBER_TYPE(obj) (XNUMBER(obj)->type)
//requires atleast 16 bits to store
//TODO: streamline integer types, remove all fixed sized ints (other than
//the basic 62 bit int), They're only needed when interfacing with C, so
//they don't need to be here
//TODO: think about making the 'key' types the default
typedef enum SL_number_type {
  //these 4(5) are the types acutally used in lisp
  SL_primitive_int = 0 |(SL_number_int_bit  | SL_number_imm_bit),
  SL_primitive_int_key = 0,
  SL_float64 = 1 | (SL_number_float_bit | SL_number_word_sized_bit),
  SL_float64_key = 1,
  SL_double = SL_float64,
  SL_double_key = 1,
  SL_bigint = 2 | (SL_number_int_bit),
  SL_bigint_key = 2,
  SL_bigfloat = 3 | (SL_number_float_bit),
  SL_bigfloat_key = 3,
  //these are only for c interop
  SL_int8 = 4 | (SL_number_int_bit | SL_number_imm_bit),
  SL_int16 = 5 | (SL_number_int_bit | SL_number_imm_bit),
  SL_int32 = 6 | (SL_number_int_bit | SL_number_imm_bit),
  SL_int64 = 7 | (SL_number_int_bit | SL_number_word_sized_bit),
  SL_float32 = 8 | (SL_number_float_bit | SL_number_imm_bit),
  //this is special
  SL_char = 9,//since characters go in immediates

} SL_number_type;//typedefed because the _type suffix keeps it unique

/* This extracts the actual value from an immediate, it masks
 out all but the 32 most significant bits. I got a bit confused
 before, since numbers are written most->least significant but
 structs are written least->most significant
*/
#define sl_imm_mask 0xffffffff00000000

#define UNMASK_IMM(obj) (obj & sl_imm_mask)
union sl_imm {
  uint64_t val;
  struct {
    uint8_t sl_type; //these are the type bits from sl_obj
    uint8_t padding; /* can be removed if the space is needed*/
    //TODO: change this from tag to type to match with sl_number
    uint16_t tag;/* should be an SL_number_type*/
    union {
      float float32;
      uint32_t char32; /*since char is a reserved word in C*/
      /* exact sized integers*/
      uint32_t uint32;
      uint16_t uint16;
      uint8_t uint8;
    };
  };
};
static_assert((sizeof(union sl_imm) == 8),"sl_imm_to_big");

/*
   An sl_number is just a value and a type indicating what kind of
   number the value represents. A union is necessary since type punning
   with doubles breaks strict aliasing. The val field of the union
   is for convience, since only doubles need to worry about strict aliasing
*/
/* this is cheating, but it works
   since for mpz/mpfr/mpq, using mpz as an example:
   mpz_t is defined as typedef __mpz_struct mpz_t[1]
   mpz_ptr is defined as typedef __mpz_struct mpz_ptr*
   which is complicated (and a reason I hate when array types
   are used like this), so I cheat since the structs are defined in
   the gmp/mpfr headers.
*/
typedef __mpz_struct sl_bigint;
typedef __mpfr_struct sl_bigfloat;
typedef __mpq_struct sl_bigrat;
struct sl_number {
  union {
    uint64_t val;
    double float64;
    uint64_t uint64;

    sl_bigint *bigint;
    sl_bigfloat *bigfloat;
    sl_bigrat *big_rational;
    void *ptr;//anything else
  };
  uint16_t type;
};
/*
  An sl_vector is a simple vector, string or array
  a vector is a simple vector or a string
  a simple vector is a c_array of sl_objs, which may be typed for speed
  a string is a generally immutable c_array of characters
  an array is a multidimensional array of sl_objs which may be typed,
   all svectors are also arrays

  Conceptually strings are just vectors of characters, but for space
  reasons the internal implementation is more complicated than that, but
  this should be transparent to the user
*/
#define VECTOR_TYPE(x) (XVECTOR(x)->tag)
#define VECTOR_LEN(x) (XVECTOR(x)->len)
#define VECTOR_LENGTH(x) VECTOR_LEN(x)
#define XSVECTOR(obj) ((sl_svector*)XVECTOR(obj))
#define XSTRING(obj) ((sl_string*)XVECTOR(obj))
//These are here to avoid having into include vector.h to get them
#define VREF(obj,ind) (XSVECTOR(obj)->value[(ind)])
#define VSET(obj,ind,val) (XSVECTOR(obj)->value[(ind)] = (val))
enum SL_vector {//the actual numerical values of these should be irrevlent
  SL_untyped_svector = 0,
  SL_typed_svector,
  SL_string,
  SL_untyped_array,
  SL_typed_array,
  SL_hash_table,
};
struct sl_vector {
  uint8_t tag; //a value of type enum SL_vector indicating the specific
  //type of vector
  int padding :24;
  uint32_t len;
  sl_obj *value;
  //if more information is needed for a specific type of vector place
  //it after the actual value to retain binary compatablity
};
struct sl_svector {
  uint8_t tag;
  uint8_t padding;
  uint16_t type;
  uint32_t len;
  sl_obj *value;
};
/*
  typed vectors/arrays are unique in that they can hold various unboxed
  types for performance reasons. Any finite precision numeric type should
  fit into an array unboxed. While there are other types that could fit
  into an array unboxed (e.g cons, strings, other vectors) there's no real
  reason to do that.

  finite precision numerical types are:
  -(u)int{8,16,32,64}
  -float, double
  -optionally 128 bit integers/floats (added if needed)
  -32/64 bit rational types
  -complex versions of the above types
*/

struct sl_array {
  uint8_t tag;
  uint8_t type;
  uint16_t ndims;
  //2 dimensional array lengths should be accessable without
  //an extra memory access, but larger dimensional arrays
  //should still be possible
  union {
    struct {
      uint32_t rows;
      uint32_t columns;
    };
    uint32_t *dims;
  };
  sl_obj *value;
};

enum SL_encoding {
  SL_UTF8,
  SL_UTF16,
  SL_UTF32,
  SL_ASCII,
};
//TODO: Update strings to allow substrings to share memory with the initial string
//rather than always being read-only or copy on write
//I may need to add a size field to this, because the size of a string in bytes
//is not always the same as the length of the string in characters
struct sl_string {
  uint8_t tag;
  uint8_t type;
  uint8_t encoding;//
  uint8_t ascii : 4;//is the string made of only ascii characters
    //if non 0 make a copy of the string before modifying it
  //allows for efficent substring/string copying if the copy isn't changed
  uint8_t copy_on_write : 2;
  uint8_t read_only : 2;//if non 0, string can't be modified
  uint32_t len;
  const char *str;
};

//enum for various symbol properties, including interned state
enum SL_symbol_props {
  SL_SYMBOL_CONST = 1,//symbol value can not be changed
  SL_SYMBOL_SPECIAL = 2,//symbol uses dynamic binding
  SL_SYMBOL_TYPED = 4,
  SL_SYMBOL_PROTECTED = 8,//warn if user tries to change the symbol value
  //but allow it to be changed if forced
  SL_SYMBOL_KEYWORD = (SL_SYMBOL_PROTECTED<<1),//keyword symbol
  //perhaps unecessary
  SL_SYMBOL_PROPS_GENERAL_LAST = SL_SYMBOL_KEYWORD,
  //interned state
  SL_SYMBOL_INTERNED = (SL_SYMBOL_PROPS_GENERAL_LAST<<1),
};
//emacs lisp (from which I borrow a lot of my internal naming conventions)
//prefices symbol names with a Q, I'll do the same, I don't know their
//reasoning but I'll say it stands for quote, since symbols need to be quoted...
#define XSYMVAL(x) (XSYMBOL(x)->val)
#define SET_SYMVAL(x, y) (XSYMBOL(x)->val = y)
//generally a pointer to a string is more useful
#define XSYMNAME(x)(&((XSYMBOL(x)->name)))
#define XSYMPROPS(x) (XSYMBOL(x)->sym_props)
#define CONSTANTP(x) (SYMBOLP(x) && (XSYMPROPS(x) & SL_SYMBOL_CONST))
#define DYNAMICP(x) (SYMBOLP(x) && (XSYMPROPS(x) & SL_SYMBOL_SPECIAL))
#define SPECIALP(x) (SYMBOLP(x) && (XSYMPROPS(x) & SL_SYMBOL_SPECIAL))
#define MARK_INTERNED(x) (XSYMBOL(x)->sym_props |= SL_SYMBOL_INTERNED)
#define MARK_DYNAMIC(x) (XSYMBOL(x)->sym_props |= SL_SYMBOL_SPECIAL)
#define MARK_CONSTANT(x) (XSYMBOL(x)->sym_props |= SL_SYMBOL_CONST)
#define SETQ(sym, x) (XSYMBOL(sym)->val = x)
//documentation/plist fields to be added
struct sl_symbol {
  sl_obj val;
  sl_string name;
  sl_symbol *next; //for use in the symbol table
  uint64_t hv;
  uint8_t type; //optional, currently unused
  uint8_t sym_props;
  //sl_obj prop_list; may add, but I'm not sure it's totally necessary
};
enum SL_misc_type {
  SL_subr = 0,
  SL_stream = 1,
  SL_env = 2,
  SL_rand_state = 3,
  SL_llvm_value_ref = 4,
};
/*
  I want to find a more space efficent way to store the type for misc values,
  I can use the low 3 bits of the value pointer to store some information,
  but that limits be to 7 types, so what I think I'll do is use those bits
  to store info on opaque types (i.e llvm_value_ref), and store a type in
  the first byte of any misc struct I define.

  Currently I end up allocating 16 bytes to store a misc obj, in additon
  to the space of the space of the object it points to.
*/
struct sl_misc {
  void *value;
  uint8_t type;
};
#define XMISC_TYPE(obj) XMISC(obj)->type
struct sl_lambda_list {
  int num_req_args;
  int num_opt_args;
  int num_key_args;//like common lisp if you have a rest arg and keyword args
  int has_rest_arg;//the keyword arguments get put in the rest arg
  sl_obj *args;//a lexical environment containing the arguments
  //this is copied when the function is actually called, otherwise
  //default arguments would be overridden, I forgot about this, so I may
  //change the way this field works
};
//when evaluating a lambda scan through and replace any defined lexical
//variables with their defination (i.e make a closure), alternatively create
//a new lexical environment with the used variables and their definations, this
//second approach would allow a lambda to be printed as it was read, the first
//approach would remove some variable names
struct sl_lambda {
  struct sl_lambda_list args;//should this go in the subr structure?
  sl_obj body;
};

typedef sl_obj (*sl_fun_any)(); //Note that this means any number of arguments(in C)
typedef sl_obj (*sl_fun0) (void);
typedef sl_obj (*sl_fun1) (sl_obj);
typedef sl_obj (*sl_fun2) (sl_obj,sl_obj);
typedef sl_obj (*sl_fun3) (sl_obj,sl_obj,sl_obj);
typedef sl_obj (*sl_fun4) (sl_obj,sl_obj,sl_obj,sl_obj);
typedef sl_obj (*sl_fun5) (sl_obj,sl_obj,sl_obj,sl_obj,sl_obj);
typedef sl_obj (*sl_fun6) (sl_obj,sl_obj,sl_obj,sl_obj,sl_obj,sl_obj);
typedef sl_obj (*sl_fun7) (sl_obj,sl_obj,sl_obj,sl_obj,sl_obj,sl_obj,sl_obj);
typedef sl_obj (*sl_fun8) (sl_obj,sl_obj,sl_obj,sl_obj,
                           sl_obj,sl_obj,sl_obj,sl_obj);
typedef sl_obj (*sl_fun_many) (uint64_t, sl_obj*);
typedef sl_obj (*sl_fun_unevalled) (sl_obj);
typedef sl_lambda *sl_lambda_expr;
enum psuedo_args {
  MANY = -1,//takes a rest arg
  UNEVALED = -2,//special form
  LAMBDA = -3,
  MACRO = -4, //functionally the same as unevalled, but evaluated at compile time
  LISP_MACRO = -5,//macro defined in lisp, uses a lambda to hold the macro body
  //named kinda backwards since variables can't start with numbers
  ARGS_0 = 0, ARGS_1 = 1, ARGS_2 = 2, ARGS_3 = 3, ARGS_4 = 4,
  ARGS_5 = 5, ARGS_6 = 6, ARGS_7 = 7, ARGS_8 = 8,
};
/*
  TODO: add a field to store the required types of the arguments so I can do typechecking
  outside of functions, which will make writing code eaiser, and will allow me to use
  non-typechecked versions of the functions for speed.
  
  I first need to make a lisp type and c enum/struct for lisp types.
  
*/
/*
  TODO: set subr_type field in 
*/
struct sl_subr {
  union {
    sl_fun_any fany;
    sl_fun0 f0;
    sl_fun1 f1;
    sl_fun2 f2;
    sl_fun3 f3;
    sl_fun4 f4;
    sl_fun5 f5;
    sl_fun6 f6;
    sl_fun7 f7;
    sl_fun8 f8;
    sl_fun_many fMANY;
    sl_fun_unevalled fUNEVALED;
    sl_lambda_expr fLAMBDA;
  };
  //we need this to be able to print the name of the subr
  //it may be empty for lambdas
  sl_string symbol_name;
  int16_t minargs;
  int16_t maxargs;
  int16_t subr_type;
  int16_t padding;
//make this a union, or something similar, so I can store data for a lambda
//even if I make it callable as a function pointer
  const char *doc;
};
/*
rough idea for how to represent types in an enum. Each distinct type has a
number, which is ored with any supertype/interface/whatever you want to call it.
enum sl_obj_type {
  SL_type_primitive_int = 0 | SL_TYPE_NUMBER | SL_TYPE_INTEGER,
  SL_type_cons = 1 | SL_TYPE_SEQUENCE,
  SL_type_symbol = 2,
  SL_type_svector = 3 | SL_TYPE_SEQUENCE | SL_TYPE_VECTOR | SL_TYPE_ARRAY,
  SL_type_string = 4 | SL_TYPE_SEQUNCE | SL_TYPE_VECTOR | SL_TYPE_ARRAY,
  SL_type_double = 5 | SL_TYPE_NUMBER | SL_TYPE_FLOAT,
  SL_type_bigint = 6 | SL_TYPE_NUMBER | SL_TYPE_INTEGER,
  SL_type_bigfloat = 7 | SL_TYPE_NUMBER | SL_TYPE_FLOAT,
  SL_type_char = 8,
  SL_type_lambda = 9 | SL_TYPE_SUBR,
  SL_type_builtin = 10 | SL_TYPE_SUBR,
  SL_type_macro = 11 | SL_TYPE_SUBR,
  SL_type_hashtable = 12,
  SL_type_array = 13,
  SL_type_stream = 14, //should be a supertype with different subtypes
  }*/
//
/* Functions to make objects of various types */
#ifndef __cpluscplus
//we need a seperate case for floats so we don't break strict aliasing
SL_INLINE sl_obj make_float(float value){
  sl_imm imm = {.float32 = value, .tag = SL_float32, .sl_type = SL_imm};
  return imm.val;
}
SL_INLINE sl_obj make_char(sl_char value){
  sl_imm imm  = {.char32 = value, .tag = SL_char, .sl_type = SL_imm};
  return imm.val;
}
SL_INLINE sl_obj make_immediate(uint32_t value, uint8_t type){
  sl_imm imm = {.uint32 = value, .tag = type, .sl_type = SL_imm};
  return imm.val;
}

//make_bignuml
//pointers and number for integers, to avoid excessive casting
#define make_bigint(val) make_bignum(val, SL_bigint)
#define make_bigfloat(val) make_bignum(val, SL_bigfloat)
SL_INLINE sl_obj make_bignum(void *val, uint16_t type){
  sl_number *new_number = sl_malloc(sizeof(sl_number));
  new_number->ptr = val;
  new_number->type = type;
  return make_sl_obj((sl_obj)new_number, SL_number);
}
SL_INLINE sl_obj make_number(uint64_t val, uint16_t type){
  sl_number *new_number = sl_malloc(sizeof(sl_number));
  new_number->val = val;
  new_number->type = type;
  return make_sl_obj((sl_obj)new_number, SL_number);
}
SL_INLINE sl_obj make_double(double val){
  sl_number *new_number = sl_malloc(sizeof(sl_number));
  new_number->float64 = val;
  new_number->type = SL_double;
  return make_sl_obj((sl_obj)new_number, SL_number);
}
SL_INLINE sl_obj make_misc(void *value, uint32_t type){
  sl_misc *misc = sl_malloc(sizeof(sl_misc));
  misc->type = type;
  misc->value = value;
  return make_sl_obj((sl_obj)misc, SL_misc);
}
#define make_llvm_value_ref(value) make_misc(value, sl_llvm_value_ref)
SL_INLINE sl_obj make_subr(void *fn_name, const char *lname, uint64_t minargs,
                           uint64_t maxargs, const char *doc){
  sl_subr* new_subr = sl_malloc(sizeof(sl_subr));
  new_subr->fany = fn_name;
  sl_string subr_name= {.tag = SL_vector, .type = SL_string,
                        .str = lname, .len = strlen(lname)};
  new_subr->symbol_name = subr_name;
  new_subr->minargs = minargs;
  new_subr->maxargs = maxargs;
  new_subr->doc = doc;
  return make_misc(new_subr, SL_subr);
}
#endif
#endif
