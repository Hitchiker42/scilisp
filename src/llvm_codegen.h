#ifndef _LLVM_CODEGEN_H_
#define _LLVM_CODEGEN_H_
#ifdef __cplusplus
extern "C"{
  //probably won't work without some guards added to some headers
  #include "common.h"
}
//this includes LLVMContext.h, BasicBlock.h, Instructions.h
#include <llvm/IR/IRBuilder.h> 
#include <llvm/ExecutionEngine/ExecutionEngine.h> 
#include <llvm/ExecutionEngine/GenericValue.h> 
//Add llvm includes as conflicts appear
typedef struct sl_llvm_context sl_llvm_context;
struct sl_llvm_context {
  llvm::LLVMContext *context;
  llvm::Module *module;
  llvm::IRBuilder *builder;
};
llvm::Value *sl_codegen(sl_obj obj);

#define with_global_ctx(fun, args...) fun(getGlobalContext() ,##args)
#endif
/* Local Variables: */
/* mode: c++ */
/* End: */
