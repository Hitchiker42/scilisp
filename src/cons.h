#ifndef _SL_CONS_H
#define _SL_CONS_H
#include "common.h"
/* Nothing outside of this file/cons.h should need to manipulate
   literal cons cells, that is to say only pointers to conses
   need to be dealt with.
*/
#define XSETCAR(obj, val) (XCAR(obj) = val)
#define XSETCDR(obj, val) (XCDR(obj) = val)
#define SETCAR(obj, val) (XCAR(obj) = val)
#define SETCDR(obj, val) (XCDR(obj) = val)
#define POP(list)                               \
  ({sl_obj temp = XCAR(list);                   \
    list = XCDR(list);                          \
    temp;})
#define PUSH(elt, list)                          \
  (list = make_cons(elt, list))
#define CONS_LENGTH(list)                               \
  ({uint64_t len=0;                                     \
    sl_obj temp = list;                                 \
    while(temp){temp = XCDR(temp);len++;}               \
    len;})
//this should be used in C code in favor of Fcons for
//creating cons cells
SL_INLINE sl_obj make_cons(sl_obj car, sl_obj cdr){
  sl_cons *new = alloc_cons();
  new->car = car;
  new->cdr = cdr;
  return make_sl_obj((sl_obj)new, SL_cons);
}
#define make_empty_cons() make_cons(NIL,NIL)
sl_obj Fcons(sl_obj car,sl_obj cdr);
#include "make_list.h"
sl_obj Flist(uint64_t numargs, sl_obj *args);
sl_obj Flist_star(uint64_t numargs, sl_obj *args);
sl_obj make_int_list(sl_obj car,...);
//for compatability with existing code, fix later
#define make_list1(a) make_list_1(a)
#define make_list2(a,b) make_list_2(a,b)
sl_obj Fnappend(uint64_t numargs, sl_obj *args);
sl_obj Fappend(uint64_t numargs, sl_obj *args);
sl_obj make_list1(sl_obj car);
sl_obj make_list2(sl_obj car, sl_obj caar);
sl_obj Fnth(sl_obj list, sl_obj n);
sl_obj Fhead(sl_obj list, sl_obj n);
sl_obj Ftail(sl_obj list, sl_obj n);
sl_obj Fsplit(sl_obj list, sl_obj n);
sl_obj Fcopy_list(sl_obj list);
sl_obj Fcopy_tree(sl_obj tree);
//functions on alists
sl_obj Fassoc(sl_obj key, sl_obj list);
sl_obj Frassoc(sl_obj key, sl_obj list);
sl_obj Fassq(sl_obj key, sl_obj list);
sl_obj Frassq(sl_obj key, sl_obj list);
//pop/push are effectively macros (they don't eval their arguments) 
//defined in sequence.c, but works on conses so should be in cons.h as well
sl_obj Flength(sl_obj obj);

sl_obj reverse_cons(sl_obj obj);
sl_obj nreverse_cons(sl_obj obj);
sl_obj reduce_cons(sl_obj fn, sl_obj ls, sl_obj start);
//C only, return the sl_obj containing the last cell of the list ls
//Doesn't check that ls is actually a list.
sl_obj get_pointer_to_tail(sl_obj ls);
/*
  Below here is emacs lisp code to generate the prototypes,
  defination and macros for the c[ad{2,4]r family of functions
  followed by the prototypes and macros themselves, the acutal
  functions are in cons.c
*/
/*
(defun gen-cadrs ()
  (let ((strings nil)
        (funs nil)
        (macros nil)
        (inits nil))
    (dolist (i '(?a ?d))
      (dolist (j '(?a ?d))
        (push (string i j) strings)
        (dolist (k '(?a ?d))
          (push (string i j k) strings)
          (dolist (l '(?a ?d))
            (push (string i j k l) strings)))))
    (setq strings
          (sort strings (lambda (x y) (if (= (length x) (length y))
                                          (string-lessp x y)
                                        (< (length x) (length y))))))
    (dolist (str strings)
      (let ((name (concat "c" str "r")))
        (push
         (format "INIT_SUBR(%s);\n" name) inits)
        (push 
         (concat "DEFUN(" name "," name "1,1," "\"return the " name
                 " of the given cons cell\")\n(sl_obj obj){\n\treturn "
                 (mapconcat (lambda (x) (format "c%cr(" x)) str "")
                 "obj" (make-string (length str) ?\))
                 ";\n}\n") funs)
        (push
         (concat "#define X" (upcase name) "(obj) ("
                 (mapconcat (lambda (x) (format "XC%cR(" x)) (upcase str) "")
                 "obj" (make-string (1+ (length str)) ?\)) "\n")
         macros)))
    (remove-duplicates funs)
    (remove-duplicates macros)
    (remove-duplicates inits)
         (mapc #'insert (reverse inits))
         (mapc #'insert (reverse funs))
         (mapc #'insert (reverse macros))))
; this generates function prototypes
;(insert (concat "sl_obj F" name " (sl_obj obj);\n"))
*/
sl_obj Fsafe_car (sl_obj obj);
sl_obj Fsafe_cdr (sl_obj obj);
                  
sl_obj Fcar    (sl_obj obj);
sl_obj Fcdr    (sl_obj obj);
sl_obj Fcaar   (sl_obj obj);
sl_obj Fcadr   (sl_obj obj);
sl_obj Fcdar   (sl_obj obj);
sl_obj Fcddr   (sl_obj obj);
sl_obj Fcaaar  (sl_obj obj);
sl_obj Fcaadr  (sl_obj obj);
sl_obj Fcadar  (sl_obj obj);
sl_obj Fcaddr  (sl_obj obj);
sl_obj Fcdaar  (sl_obj obj);
sl_obj Fcdadr  (sl_obj obj);
sl_obj Fcddar  (sl_obj obj);
sl_obj Fcdddr  (sl_obj obj);
sl_obj Fcaaaar (sl_obj obj);
sl_obj Fcaaadr (sl_obj obj);
sl_obj Fcaadar (sl_obj obj);
sl_obj Fcaaddr (sl_obj obj);
sl_obj Fcadaar (sl_obj obj);
sl_obj Fcadadr (sl_obj obj);
sl_obj Fcaddar (sl_obj obj);
sl_obj Fcadddr (sl_obj obj);
sl_obj Fcdaaar (sl_obj obj);
sl_obj Fcdaadr (sl_obj obj);
sl_obj Fcdadar (sl_obj obj);
sl_obj Fcdaddr (sl_obj obj);
sl_obj Fcddaar (sl_obj obj);
sl_obj Fcddadr (sl_obj obj);
sl_obj Fcdddar (sl_obj obj);
sl_obj Fcddddr (sl_obj obj);
#define XCAAR(obj) (XCAR(XCAR(obj)))
#define XCADR(obj) (XCAR(XCDR(obj)))
#define XCDAR(obj) (XCDR(XCAR(obj)))
#define XCDDR(obj) (XCDR(XCDR(obj)))
#define XCAAAR(obj) (XCAR(XCAR(XCAR(obj))))
#define XCAADR(obj) (XCAR(XCAR(XCDR(obj))))
#define XCADAR(obj) (XCAR(XCDR(XCAR(obj))))
#define XCADDR(obj) (XCAR(XCDR(XCDR(obj))))
#define XCDAAR(obj) (XCDR(XCAR(XCAR(obj))))
#define XCDADR(obj) (XCDR(XCAR(XCDR(obj))))
#define XCDDAR(obj) (XCDR(XCDR(XCAR(obj))))
#define XCDDDR(obj) (XCDR(XCDR(XCDR(obj))))
#define XCAAAAR(obj) (XCAR(XCAR(XCAR(XCAR(obj)))))
#define XCAAADR(obj) (XCAR(XCAR(XCAR(XCDR(obj)))))
#define XCAADAR(obj) (XCAR(XCAR(XCDR(XCAR(obj)))))
#define XCAADDR(obj) (XCAR(XCAR(XCDR(XCDR(obj)))))
#define XCADAAR(obj) (XCAR(XCDR(XCAR(XCAR(obj)))))
#define XCADADR(obj) (XCAR(XCDR(XCAR(XCDR(obj)))))
#define XCADDAR(obj) (XCAR(XCDR(XCDR(XCAR(obj)))))
#define XCADDDR(obj) (XCAR(XCDR(XCDR(XCDR(obj)))))
#define XCDAAAR(obj) (XCDR(XCAR(XCAR(XCAR(obj)))))
#define XCDAADR(obj) (XCDR(XCAR(XCAR(XCDR(obj)))))
#define XCDADAR(obj) (XCDR(XCAR(XCDR(XCAR(obj)))))
#define XCDADDR(obj) (XCDR(XCAR(XCDR(XCDR(obj)))))
#define XCDDAAR(obj) (XCDR(XCDR(XCAR(XCAR(obj)))))
#define XCDDADR(obj) (XCDR(XCDR(XCAR(XCDR(obj)))))
#define XCDDDAR(obj) (XCDR(XCDR(XCDR(XCAR(obj)))))
#define XCDDDDR(obj) (XCDR(XCDR(XCDR(XCDR(obj)))))

#endif
