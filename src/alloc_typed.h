/*
  Allocation functions which require knowlege of scilisp types
*/
#define FAST_ALLOC_CONS
#ifdef FAST_ALLOC_CONS
#ifndef _SL_COMMON_H_
#error "Don't include typed_alloc.h directly, use common.h"
#endif
#ifndef _SL_TYPED_ALLOC_H_
#define _SL_TYPED_ALLOC_H_

#ifdef CUSTOM_ALLOC
SL_INLINE void* alloc_cons(){
  sl_malloc(sizeof(sl_cons));
}
#else
SL_INLINE sl_cons* alloc_cons(){
  /*
    This is a bit weird, but gc_malloc_many allocates an extra word for
    each block of memory to store the adress of the next block, since
    we don't care about that adress once we've used the block we can
    re use the memory, meaning we need to allocate objects 1 word smaller
    than we actually need. This is important mainly because gc malloc many
    only allocates in 16 byte intervals, so if we allocate a cons cell
    which takes 16 bytes, we actually get 32 bytes per block.
   */
    if(thread_env->cons_fl == NULL){
      thread_env->cons_fl = GC_malloc_many(sizeof(sl_cons)-8);//-sizeof(void*));
    }
  sl_cons *new_cons = thread_env->cons_fl;
  void *fl_next = GC_NEXT(thread_env->cons_fl);
  //zero the car of our new cons cell, since that currently
  //contains the adress of fl_next.
  memset(new_cons,'\0',sizeof(sl_cons));
  thread_env->cons_fl = fl_next;
  return new_cons;
}
#endif
#endif
#else
/* IMPORTANT: Test actual alloc_cons and set it as the default*/
#define alloc_cons() sl_malloc(sizeof(sl_cons))
#endif
