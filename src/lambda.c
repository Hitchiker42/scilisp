#include "common.h"
#include "symbol.h"
//replace lexically bound symbols in form with their definations
//returns the updated form
static sl_obj make_closure(sl_obj form);
/*
  In emacs lisp lambda just quotes it's arguments and the arguments are
  parsed when it's called, but that seems inefficient to me.
*/
DEFUN("lambda",lambda,1,UNEVALED,"(lambda arglist &rest body), create an "
      "anonymous function, which evaluates body after binding arguments"
      "according to arglist.")
  (sl_obj args){
  sl_obj arglist = XCAR(args);
//sl_obj body = make_closure(XCDR(args));
  sl_obj body = make_cons(Qprogn, XCDR(args));
  sl_lambda *fun = sl_malloc(sizeof(sl_lambda));
  fun->body = body;  
  sl_obj arg = arglist;
  int opt = 0;
  while(!NILP(arg)){
    if(EQ(XCAR(arg), Koptional)){
      opt = 1;
      //this doesn't seem to be an error in common lisp or elisp
      //but I think it ought to be
      if(NILP(XCDR(arg))){
        raise_error_cstr(0,"Error, no arguments provided after :optional in arg list");
      }
    } else if(EQ(XCDR(arg), Krest)){
      fun->args.has_rest_arg = 1;
      if(NILP(XCDR(arg))){
        raise_error_cstr(0,"Error, no name provided after :rest in arg list");
      }
      break;
    } else {
      if(opt){
        fun->args.num_opt_args++;
      } else {
        fun->args.num_req_args++;
      }
    }
    arg = XCDR(arg);
  }
  int numargs = fun->args.num_opt_args + fun->args.num_req_args +
                fun->args.has_rest_arg;
  fun->args.args = sl_malloc(2 + numargs);
  int i;
  for(i=0;i<fun->args.num_req_args;i++){
    arg = POP(arglist);
    if(!SYMBOLP(arg)){
      raise_error_temp();
    }
    fun->args.args[i+1] = make_cons(arg, NIL);
  }
  if(fun->args.num_opt_args != 0){
    POP(arglist);//remove &optional
    for(;i<(fun->args.num_opt_args + fun->args.num_req_args);i++){
      arg = POP(arglist);
      sl_obj name, default_val;
      if(CONSP(arg)){//a default argument was given
        name = XCAR(arg);
        default_val = XCDR(arg);
      } else {
        name = arg;
        default_val = NIL;
      }
      if(!SYMBOLP(name)){
        raise_error_temp();
      }
      fun->args.args[i+1] = make_cons(name, default_val);
    }
    if(fun->args.has_rest_arg){
      arg = XCADR(arglist);
      fun->args.args[i+1] = make_cons(arg, NIL);
    }
  }
  sl_subr *subr = sl_malloc(sizeof(sl_subr));
  subr->fLAMBDA = fun;
  //these next two probably don't need to be set
  subr->minargs = fun->args.num_req_args;
  subr->maxargs = LAMBDA;
  return make_misc(subr, SL_subr);
}
static void *make_trampoline(uint64_t addr){
  /*
    This code does the following:
    moves the list of arguments into the second argument register
    loads the location of the lambda into the first argument register
    jumps to the location of funcall_lambda.

    The location of the lambda and the adress of funcall_lambda are
    stored immediately after this code, and loaded using rip relative adressing
   */
  static uint8_t trampoline[16] = {
    0x48, 0x89, 0xfe, /*mov %rdi, %rsi*/
    0x49, 0x92, 0x07, 0x00, 0x00, 0x00, /*mov 0x7(%rip), %rdi*/
//         0x25 = ModR/M byte RIP relative adressing,
//                mod = 00, 100 = opcode specific value, R/M = 101
    0xff, 0x25, 0x09, 0x00, 0x00 ,0x00, /*jmp *0x9(%rip)*/
    //nop to align
    0x90
  };
  //next 8 bytes are addr, the 8 after that are funcall_lambda
  uint8_t *tramp = sl_malloc(sizeof(trampoline) + 2*sizeof(uint64_t));
  memcpy(tramp, trampoline, sizeof(trampoline));
  *(uint64_t*)(tramp + 16) = (uint64_t)addr;
  *(uint64_t*)(tramp + 24) = (uint64_t)funcall_lambda;
  return tramp;
}
/*
  Creates a subr object which, when called with an unevaluated list
  of arguments, calls 'lambda' with those arguments.
*/
sl_obj generate_lambda_trampoline(sl_obj lambda){
  sl_subr *subr = sl_malloc(sizeof(sl_subr));
  void *tramp = make_trampoline((uint64_t)XSUBR(lambda)->fLAMBDA);
  subr->maxargs = UNEVALED;
  subr->minargs = 0;
  subr->f1 = tramp;
  return make_misc(subr, SL_subr);
}
static sl_obj find_closure_binding(sl_obj sym){
  if(DYNAMICP(sym)){
    return sym;
  }
  sl_obj binding = get_lexical_binding(sym);
  return (EQ(binding, Qunbound) ? sym : binding);
}
//inplace version of replace_symbols_with_definitions
static sl_obj make_closure(sl_obj form){
  sl_obj ret = form;
  while(CONSP(form)){
    if(CONSP(XCAR(form))){
      make_closure(XCAR(form));
    } else if(SYMBOLP(XCAR(form))){
      XSETCAR(form, find_closure_binding(XCAR(form)));
    }
    form = XCDR(form);
  }
  return ret;
}
void init_lambda_symbols(void){
  INIT_SUBR(lambda, UNEVALED);
}
