#include "bignum.h"
sl_bigint* bigfloat_to_bigint(sl_bigfloat *val){
  sl_bigint *bigint = sl_malloc(sizeof(sl_bigint));
  mpz_init(bigint);
  //this sets bigint to null if val is inf/nan
  mpfr_get_z(bigint, val, MPFR_RNDN);
  return bigint;
}
sl_bigint *sl_obj_to_bigint(sl_obj obj){
  if(!NUMBERP(obj)){
    return NULL;
  }
  if(INTP(obj)){
    return int_to_bigint(XINT(obj));
  } else if(IMMP(obj)){
    sl_imm imm = XIMM(obj);
    if(imm.tag == SL_float32){
      return float_to_bigint(imm.float32);
    } else {
      return int_to_bigint(imm.uint32);
    }
  } else {
    //this has to be a pointer to struct sl_number
    sl_number *num = XNUMBER(obj);
    if(SL_number_is_finite(num)){
      if(SL_number_is_float(num)){
        return float_to_bigint(num->float64);
      } else {
        return int_to_bigint(num->uint64);
      }
    } else {
      if(SL_number_is_float(num)){
        return bigfloat_to_bigint(num->bigfloat);
      } else {
        return copy_bigint(num->bigint);
      }
    }
  }
}
sl_bigfloat *sl_obj_to_bigfloat(sl_obj obj){
  if(!NUMBERP(obj)){
    return NULL;
  }
  if(INTP(obj)){
    return int_to_bigfloat(XINT(obj));
  } else if(IMMP(obj)){
    sl_imm imm = XIMM(obj);
    if(imm.tag == SL_float32){
      return float_to_bigfloat(imm.float32);
    } else {
      return int_to_bigfloat(imm.uint32);
    }
  } else {
    //this has to be a pointer to struct sl_number
    sl_number *num = XNUMBER(obj);
    if(SL_number_is_finite(num)){
      if(SL_number_is_float(num)){
        return float_to_bigfloat(num->float64);
      } else {
        return int_to_bigfloat(num->uint64);
      }
    } else {
      if(SL_number_is_float(num)){
        return copy_bigfloat(num->bigfloat);
      } else {
        return bigint_to_bigfloat(num->bigint);
      }
    }
  }
}
