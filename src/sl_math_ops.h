#ifndef _SL_MATH_OPS_H_
#define _SL_MATH_OPS_H_
#include "common.h"

#define declare_math_ops(name)                                          \
  sl_obj uint_uint_##name(sl_obj x, sl_obj y);                      \
  sl_obj int_int_##name(sl_obj x, sl_obj y);                          \
  sl_obj int_float_##name(sl_obj x, sl_obj y);                         \
  sl_obj uint_float_##name(sl_obj x, sl_obj y);                       \
  sl_obj int_bigint_##name(sl_obj x, sl_obj y);                    \
  sl_obj uint_bigint_##name(sl_obj x, sl_obj y);                  \
  sl_obj int_bigfloat_##name(sl_obj x, sl_obj y);                \
  sl_obj uint_bigfloat_##name(sl_obj x, sl_obj y);              \
  sl_obj float_int_##name(sl_obj x, sl_obj y);                         \
  sl_obj float_uint_##name(sl_obj x, sl_obj y);                       \
  sl_obj float_float_##name(sl_obj x, sl_obj y);                        \
  sl_obj float_bigint_##name(sl_obj x, sl_obj y);                   \
  sl_obj float_bigfloat_##name(sl_obj x, sl_obj y);               \
  sl_obj bigint_int_##name(sl_obj x, sl_obj y);                    \
  sl_obj bigint_uint_##name(sl_obj x, sl_obj y);                  \
  sl_obj bigint_float_##name(sl_obj x, sl_obj y);                   \
  sl_obj bigint_bigint_##name(sl_obj x, sl_obj y);              \
  sl_obj bigint_bigfloat_##name(sl_obj x, sl_obj y);          \
  sl_obj bigfloat_int_##name(sl_obj x, sl_obj y);                \
  sl_obj bigfloat_uint_##name(sl_obj x, sl_obj y);              \
  sl_obj bigfloat_float_##name(sl_obj x, sl_obj y);               \
  sl_obj bigfloat_bigint_##name(sl_obj x, sl_obj y);          \
  sl_obj bigfloat_bigfloat_##name(sl_obj x, sl_obj y);

#define declare_math_funs(name)                 \
  sl_obj uint_##name(sl_obj x);                 \
  sl_obj int_##name(sl_obj x);                  \
  sl_obj float_##name(sl_obj x);                \
  sl_obj bigint_##name(sl_obj x);               \
  sl_obj bigfloat_##name(sl_obj x);

#define declare_math_bitops(name)                               \
  uint64_t int_int_##name(uint64_t x, uint64_t y);              \
  sl_bigint* bigint_int_##name(sl_bigint *x, uint64_t y);       \
  sl_bigint* bigint_bigint_##name(sl_bigint *x, sl_bigint *y);

//math operations
declare_math_ops(add);
declare_math_ops(sub);
declare_math_ops(mul);
declare_math_ops(div);
//math comparisions
declare_math_ops(eq);
declare_math_ops(ne);
declare_math_ops(lt);
declare_math_ops(gt);
declare_math_ops(le);
declare_math_ops(ge);

declare_math_funs(sin);
declare_math_funs(cos);
declare_math_funs(tan);
declare_math_funs(asin);
declare_math_funs(acos);
declare_math_funs(atan);
declare_math_funs(sinh);
declare_math_funs(cosh);
declare_math_funs(tanh);
declare_math_funs(asinh);
declare_math_funs(acosh);
declare_math_funs(atanh);
declare_math_funs(log);
declare_math_funs(log10);
declare_math_funs(log2);

sl_bigint *bigint_shift(sl_bigint *bigint, int64_t count);
uint64_t int_shift(uint64_t val, int64_t count);
#endif
