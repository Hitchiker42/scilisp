#include "symbol.h"
#include "sl_string.h"
#include "atomic.h"
#include "print.h"
#include "cons.h"
#include "vector.h"
static inline int maybe_rehash(sl_symbol_table *ht);
/*
  add sym to the table, if not already present, and return the
  symbol table entry coorsponding to sym. (this is still a symbol
  it's just a global version of the symbol).
*/
sl_symbol *add_symbol(sl_symbol_table *ht, sl_symbol *sym){
//  DEBUG_PRINTF("Looking for/Adding symbol %.*s to table\n",sym->name.len,sym->name.str);
  uint64_t hv = sym->hv;
  uint32_t index = hv % ht->size;
  sl_atomic_inc(&ht->num_threads_using);
  if(sl_atomic_load_n(&ht->rehash_pending)){
    sl_atomic_dec(&ht->num_threads_using);
    sem_wait(&ht->sem);
    sl_atomic_inc(&ht->num_threads_using);
  }
  sl_symbol *entry, *bucket;
 retry:
//  DEBUG_PRINTF("Entering atomic block\n");
  entry = ht->table[index];
  bucket = entry;
  /* search for the symbol in the bucket, then try to add the new
     symbol to the start of the bucket atomically.

     For atomic things I'm going to use goto's with the lable retry for atomic
     stuff.
   */
  while(entry){
//    DEBUG_PRINTF("looking for symbol %.*s\n", sym->name.len,sym->name.str);
    if(entry->hv == hv) {
      if(sl_string_equal(&entry->name,&sym->name)){
        sl_atomic_dec(&ht->num_threads_using);
  //      DEBUG_PRINTF("found symbol %.*s, value = ", sym->name.len,sym->name.str);
  //      DEBUG_PRINT_STR(sl_sprint(sym->val));
        return entry;
      }
    }
    entry = entry->next;
  }
  sym->next = bucket;
  if(sl_atomic_cmpxchg_n(ht->table+index, &bucket, sym)){
    sym->sym_props |= SL_SYMBOL_INTERNED;
    maybe_rehash(ht);//decrements num threads using
    return sym;
  }
  goto retry;
}
/*
  add symbol to the table without checking if it already exists,
  mainly/only for use when populating the symbol table at startup
*/
void add_new_symbol(sl_symbol_table *ht, sl_symbol *sym){
  //this only gets called when initializing the symbol table
  //we don't have the hash values for default symbols
  uint64_t hv = sym->hv = sl_hash(sym->name.str, sym->name.len);
  uint32_t index = hv % ht->size;
  //make this atomic so I can initialize the hash table in parallel
  //or not
  sl_symbol *entry = ht->table[index];
  if(entry){
    sym->next = entry;
  }
  ht->table[index] = sym;
}
/*
  remove symbol from the symbol table atomically, if it exists.
  return 0 if symbol not found.
  return 1 if symbol found.
  return -1 if there's an error.
*/
int remove_symbol(sl_symbol_table *ht, sl_symbol *sym){
  sl_atomic_inc(&ht->num_threads_using);
  uint64_t hv = sym->hv;
  uint32_t index = hv % ht->size;
  /*
    Find the symbol, if it's the first entry in the bucket just set
    the bucket to null, otherwise set the previous element to the next element
    (which may be null, but that doesn't matter).
    the swap needs to be atomic so use cmpxchg and try again if it fails.

   */
  sl_symbol *entry, *prev;
 retry:
  entry = ht->table[index];
  prev = ht->table[index];
  while(entry){
    if(entry->hv == hv) {
      if(sl_string_equal(&entry->name,&sym->name)){
        break;
      }
    }
    prev = entry;
    entry = entry->next;
  }
  if(entry == NULL){
    sl_atomic_dec(&ht->num_threads_using);
    return 0;
  } else if(entry == ht->table[index]){
    if(sl_atomic_cmpxchg_n(ht->table+index, &entry, NULL)){
      sl_atomic_dec(&ht->num_threads_using);
      return 1;
    }
  } else if(sl_atomic_cmpxchg(&prev->next,&entry,&entry->next)){
    sl_atomic_dec(&ht->num_threads_using);
    return 1;
  }
  goto retry;
}
static inline sl_symbol *find_symbol_by_name_and_hv(sl_symbol_table *ht,
                                                    sl_string *str,
                                                    uint64_t hv){
  DEBUG_PRINTF("finding symbol by name and hv\n");
  sl_atomic_inc(&(ht->num_threads_using));
  DEBUG_PRINTF("looking for symbol %.*s\n",str->len,str->str);
  uint32_t index = hv % ht->size;
  sl_symbol *entry = ht->table[index];
  while(entry){
    if(entry->hv == hv) {
      if(sl_string_equal(&entry->name,str)){
        DEBUG_PRINTF("Found symbol\n");
        return entry;
      }
    }
    entry = entry->next;
  }
  sl_atomic_dec(&ht->num_threads_using);
  return NULL;
}
sl_symbol *find_symbol_by_name(sl_symbol_table *ht, sl_string *str){
  DEBUG_PRINTF("Finding symbol %.*s\n", str->len, str->str);
  uint64_t hv = sl_hash(str->str,str->len);
  DEBUG_PRINTF("Hash value of symbol is %#0lx\n", hv);
  return find_symbol_by_name_and_hv(ht,str,hv);
}

sl_symbol *find_symbol(sl_symbol_table *ht, sl_symbol *sym){
  //I may put a field for the hash value in the symbol struct
  return find_symbol_by_name_and_hv(ht,&sym->name,sym->hv);
}

static void rehash(sl_symbol_table *ht);
static inline int maybe_rehash(sl_symbol_table *ht){
  int need_rehash = ((float)ht->entries/(float)ht->size) > ht->load_factor;
  //This should use a cmpxchg rather than a load, inc then dec
  if(need_rehash && !sl_atomic_load_n(&ht->rehash_pending)){
    //if we increment and it's greater than 1, someone else is rehashing first
    if(sl_atomic_inc(&ht->rehash_pending) != 1){
      sl_atomic_dec(&ht->num_threads_using);
      return 0;
    }
    sl_atomic_dec(&ht->num_threads_using);
    rehash(ht);//rehash is responsible for waking up threads,etc..
    return 1;
  }
  sl_atomic_dec(&ht->num_threads_using);
  return 0;
}
static inline void rehash_table(sl_symbol_table *ht){
  //actual rehash, all the atomic stuff goes in the main rehash function
  uint32_t new_size = ht->size*2;
  sl_symbol **new_table = sl_malloc(new_size*sizeof(sl_symbol*));
  int i;
  for(i=0;i<ht->size;i++){
    sl_symbol *entry = ht->table[i];
    while(entry){
      int index = entry->hv % new_size;
      sl_symbol *bucket = new_table[index];
      if(bucket){
        while(bucket->next){bucket = bucket->next;}
        bucket->next = entry;
      } else {
        new_table[index] = entry;
      }
      sl_symbol * temp = entry;
      entry = entry->next;
      temp->next = NULL;
    }
  }
  //table is probably pretty big, and we're not too concerned about
  //time, since rehashing is slow no matter what, so free the old table
  sl_free(ht->table);//might be a no-op if custom allocation is used
  ht->size = new_size;
  ht->table = new_table;
  return;
}
static void rehash(sl_symbol_table *ht){
  /* Spin until we have the table locked*/
  while(sl_atomic_load_n(&ht->num_threads_using)){
    sl_spin_pause();
  }
  rehash_table(ht);
  ht->rehash_pending=0;//we have the table locked, so no need for atomicicy
  int cnt;
  sem_getvalue(&ht->sem, &cnt);
  while(cnt--){
    sem_post(&ht->sem);
  }
}
sl_symbol_table *make_symbol_table(uint32_t size, float load_factor){
  //the closest we can get to optional arguments
  if(!size){size = 128;}
  if(!load_factor){load_factor = 0.8;}
  sl_symbol_table *table = sl_malloc(sizeof(sl_symbol_table));
  table->size = size;
  table->load_factor = load_factor;
  sem_init(&table->sem, 0,0);
  table->table = sl_malloc_big(size*sizeof(sl_symbol*));
  return table;
}
/*
  Recursively search through the current lexical scope to find a value
  for the symbol sym, starting at the innermost scope and moving to
  the top level scope.
*/
static inline sl_obj get_lexical_binding_recurse(sl_obj sym, sl_obj bindings){
  uint32_t len = VECTOR_LENGTH(bindings);
  if(len <= 2){
    return NIL;
  }

  int i=1;
  for(i=1;(i+1)<len;i++){
    if(EQ(sym, XCAR(VREF(bindings, i)))){
      return VREF(bindings, i);
    }
  }
  get_lexical_binding_recurse(sym, VREF(bindings, 0));
  SL_UNREACHABLE;
}
sl_obj set_lexical_binding(sl_obj sym, sl_obj val){
  sl_obj binding = get_lexical_binding_recurse(sym, thread_env->lexical_env);
  if(CONSP(binding)){
    XSETCDR(binding, val);
  } else {
    //set toplevel value
    SET_SYMVAL(sym, val);
  }
  return val;
}
sl_obj get_lexical_binding(sl_obj sym){
  sl_obj binding = get_lexical_binding_recurse(sym, thread_env->lexical_env);
//  DEBUG_PRINTF("found lexical binding %s\n",c_sprint(binding));
  if(CONSP(binding)){
    return XCDR(binding);
  } else {
    return XSYMVAL(sym);
  }
}
sl_obj make_lexical_env(sl_obj bindings){
  int num_bindings = CONS_LENGTH(bindings);
  sl_obj env = make_new_svector_obj(num_bindings + 2);
  int i = 1;
  while(CONSP(bindings)){
    sl_obj binding = POP(bindings);
    VSET(env, i++, make_cons(XCAR(binding), Feval(XCDR(binding))));
  }
  return env;
}
  
/*
  I need to decide what should happen when let binding a dynamic variable.
  A quick test in common lisp makes it seem like a let binding of a dynamic
  variable implicitly acts as a progv instead.
*/
sl_obj get_symval(sl_obj sym){
  //special variables use dynamic binding, thread_local_bindings is a alist
  //containing any thread local dynamic bindings.
  /*    DEBUG_PRINTF("Getting the value of symbol %.*s\n", XSYMNAME(sym)->len,
        XSYMNAME(sym)->str);*/
  if(DYNAMICP(sym)){
    //look through thread local bindings here
    return XSYMVAL(sym);
  }
  DEBUG_PRINTF("Looking for lexical binding\n");
  return get_lexical_binding(sym);
}

DEFUN("intern",intern,1,2,"(intern sym &optional (table *global-symbol-table*)"
      "Find symbol sym in table, adding it to the table if not found\n")
  (sl_obj sym_obj, sl_obj table){
  if(!SYMBOLP(sym_obj)){
    raise_error_temp();
  }
  sl_symbol *sym = XSYMBOL(sym_obj);
  return PACK_SYMBOL(add_symbol(thread_env->obarray, sym));
}

DEFUN("unintern",unintern,1,2,
      "(unintern sym &optional (table *global-symbol-table*)\n"
      "Remove sym from table, return t if the symbol was actually removed")
  (sl_obj sym_obj, sl_obj table){
  if(!SYMBOLP(sym_obj)){
    raise_error_temp();
  }
  sl_symbol *sym = XSYMBOL(sym_obj);
  return SL_BOOLEAN(remove_symbol(thread_env->obarray, sym));
}
DEFUN("make-symbol",make_symbol,1,1,"(make-symbol name) "
      "Return a new uninterned symbol with name 'name'")
  (sl_obj name){
  if(!STRINGP(name)){
    raise_error_temp();
  }
  sl_string *name_str = XSTRING(name);
  return make_symbol_from_string(name_str);
}
DEFUN("gensym",gensym,0,0,"(gensym) return a unique uninterned symbol")
  (void){
  uint64_t sym_number = sl_atomic_inc(&gensym_counter);
//  DEBUG_PRINTF("Gensym number %ld\n",sym_number);
  sl_string *sym_name = sl_sprintf("#:G%d",0,sym_number);
  return make_symbol_from_string(sym_name);
}
uint64_t inc_gensym_counter(void){
  uint64_t retval = sl_atomic_inc(&gensym_counter);
  return retval;
}
/*
  I need a single special form used to modify symbol properties
  (i.e constness, dynamic-ness, etc). I'm not sure how common lisp does it,
  and I haven't looked into how scheme does it yet.

  Idea, have define take keyword arguments used to set special properties,
  i.e (defconst x 5) = (define x :constant 5)
      (defvar x 5) = (define x :special 5) || (define x :dynamic 5)
  should also work for functions
      (define (x (y)) :dynamic (+ x y))
  Similar rules to docstrings should apply, so that (define x :dynamic)
  sets the value of x to :dynamic
  Also perhaps the same idea could be used for docstrings
  i.e (define x :doc "x = 5" 5)

  It'd be best to find some way to speed this up as parsing keyword args
  is kinda slow
*/
static sl_obj parse_declarations(sl_obj var, sl_obj declarations){
}
DEFUN("declare",declare,0,MANY,"(declare &rest declarations)\n"
      "Declare declarations, the meaning of which depends on where"
      "the declare form is located.")
  (uint64_t nargs, sl_obj *args){
}
static sl_obj internal_defun(sl_obj var, sl_obj args, sl_obj body){    
  if(CONSP(body) && CONSP(XCAR(body)) && EQ(XCAAR(body), Qdeclare)){
    sl_obj declarations = XCDAR(body);
    body = XCDR(body);
    parse_declarations(var, declarations);
  }
  sl_obj proc = Flambda(make_list2(args, body));
  XSYMVAL(var) = proc;
  return proc;
}
static sl_obj internal_defvar(sl_obj var, sl_obj body){
  if(!CONSP(body)){
    //check for non nil value (i.e (define x . 5), should be an error)
    XSYMVAL(var) = NIL;
    return NIL;
  }
  if(CONSP(XCAR(body)) && EQ(XCAAR(body), Qdeclare)){
    sl_obj declarations = XCDAR(body);
    body = XCDR(body);
    parse_declarations(var, declarations);
  }
  sl_obj val = Feval(body);
  XSYMVAL(var) = val;
  return val;
}
DEFUN("define",define,1,UNEVALED,"(define var [(declare declarations)] expr) or "
      "(define (var arglist) [(declare declarations)] body)\n"
      "In the first form bind var to expr, if no declarations are specified "
      "this is identical to a top level (set! var expr).\nIn the second form "
      "bind var to a procedure with args according to arglist and body "
      "'body'\nIf provided declarations is a set of 1 or more keywords or "
      "keyword value pairs specifying properties of var.\nPossible "
      "declarations are :constant, :dynamic, :define-once or "
      ":documentation doc.\n:constant indicates that var cannot be modified.\n"
      ":dynamic indicates that var should have dynamic scope.\n"
      ":define-once indicates that var should only be assigned to if there "
      "is no current binding for var.\n:documentation doc sets the "
      "documentation string of var to the string doc.\n"
      "Returns the new value of var.\n")
  (sl_obj args){
  if(SYMBOLP(XCAR(args))){
    return internal_defvar(XCAR(args), XCDR(args));
  } else if(CONSP(XCAR(args))){
    return internal_defun(XCAAR(args), XCDAR(args), XCDR(args));
  } else {
    raise_error_temp();
  }
}
  
void init_symbol_symbols(void){
  //special symbol that shouldn't be interned
  static const sl_symbol __Qunbound = make_lit_symbol("#:Qunbound", NIL,
                                                      SL_SYMBOL_CONST);
  Qunbound  = PACK_SYMBOL(&__Qunbound);
  INIT_SUBR(gensym, 0);
  INIT_SUBR(declare, MANY);
  INIT_SUBR(define, UNEVALED);
}
