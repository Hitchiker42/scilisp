#ifndef _SL_SEQ_H_
#define _SL_SEQ_H_
#include "common.h"
#include "cons.h"
#include "vector.h"
#include "sl_string.h"
/*
  Sequence functions, these are functions that make sense for various 
  sequences (really just lists, vectors and strings) and dispatch to
  the correct function based on the type of the sequences. Ideally
  these should be user extensable (that is the user can define a new sequence
  type and implement these functions) but that will require a more complicated
  object system than I have.
*/

sl_obj Flength(sl_obj obj);
sl_obj Felt(sl_obj obj, sl_obj ind);

/*
  TODO: Use C11 generic macros/__builtin_choose_expr
  to make generic sequence macros, this might be impossible due to 
  the fact I'm using my own type system.
*/
#endif
