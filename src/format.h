/*
  Header for internal use only
*/
#ifndef SL_FORMAT_H
#define SL_FORMAT_H
/*
  Put a check here to insure we're being used internally
*/
#include "common.h"
#include "print.h"
#include "util.h"
#include "char_types.h"
#define STRING_BUF_MACROS
#include "string_buf.h"
/*
  //I may change this to be more complex, like common lisp, at a later date
  regex for a format spec, a simplified version of a printf format spec:
  %(#?\+?-?0?)([0-9]*)(\.[0-9]*)?[[:fmt_control_char:]]
  \1 are modifier chars, '#' means use an alternate verison 
  (dependent on control char), '+' means prepend '+' to positive numbers,
  '-' means put padding on the left, '0' mean use 0 padding for numbers.
  \2 is the minimum width, and \3 is the precision.
  Duplicate modifier chars are not allowed (i.e %##x is an invalid specifier)
  This seems to be different than what happens in C.
  
 
  Idea for format spec more akin to common lisp
  %(@?:?-?)(([0-9]*)?((\.[0-9]*)?(\..)?)?)?[:format-control:]
  @/: are modifier chars, whose behavior is dependent on the format-control,
  '-' is the same, it forces left justification.
  the next 3 are width, precision and pad char, each seperated by a '.'  
*/
//similar to the struct use to hold printf specifiers in libc
struct format_spec {
  int32_t width;
  int32_t prec;
  uint32_t spec_char; //format letter
  uint8_t alt : 1;//# flag
  uint8_t left : 1;//- flag (left justify)
  uint8_t zero : 1;//0 pad
  uint8_t sign : 1;//+ flag 
  uint8_t pad : 4;
};
struct format_spec_experimental {
  int32_t prec;
  int32_t width;
  uint32_t pad_char;
  uint32_t spec_char;
  uint8_t alt_1 : 1;//'@' flag
  uint8_t alt_2 : 1;//':' flag
  uint8_t left : 1;//'-' flag
  uint8_t padding : 5;
};
static const uint8_t fmt_control_chars[256] =
  {//builtin C format specifiers
    ['d'] = 1,//decimal int
    ['x'] = 1, ['X'] = 1,//hexidecimal int, lower/upper case respectively
    ['f'] = 1,//decimal float
    ['e'] = 1, ['E'] = 1,//scientific notation float, use e/E respectively
    ['g'] = 1, ['G'] = 1,//float in decimal/scientific notation, whichever's best
    ['c'] = 1,//character
    ['%'] = 1,//literal % sign, dosen't consume an argument
//mpfr/gmp specifiers aren't needed, since bignum support should be transparent
/*    //mpfr/gmp format specifiesr
    ['F'] = 2,//mpf_t, float
    ['Q'] = 2,//mpq_t, rational
    ['M'] = 2,//mp_limb_t, int
    ['N'] = 2,//mp_libm_t array, ints
    ['Z'] = 2,//mpz_t, int
    ['P'] = 2,//mpfr_prec_t, int
    ['R'] = 2,//mpfr_t, float*/
    //lisp format specifiers
    ['a'] = 4, ['A'] = 4,//print the same as with princ
    ['s'] = 4, ['S'] = 4,//print readably, overrides C %s specifier
    ['$'] = 4,//variable interpolation, doesn't consume an argument
    //conditional/default printing usage "%[fmt_control%:default%]",arg
    //if arg is nil print default, otherwise format arg according to fmt_control
    ['['] = 4, [']'] = 4,
    ['r'] = 4, ['R'] = 4,//radix, print integer in specified base
    //More fancy things from lisp go here
  };
static const uint8_t fmt_control_modifiers[256] =
  {['0' ... '9'] = 1, ['#'] = 1, ['.'] = 1, ['-'] = 1, ['+'] = 1};
//maximum number of characters following a '%' that can be part of a format specifier
//MAX_CONTROL_LENGTH-1 is the maximum number of characters following a '%' that
//are not in fmt_control_chars before there is an error
#define MAX_CONTROL_LENGTH 12
#endif
