/*
  The lisp reader
*/
#define INTERNAL_READER
#include "sl_read.h"
#include "bignum.h"
#include "sl_math.h"
#include "utf8.h" //contains read_utf8_char
#include "util.h"
#include "cons.h"


/*
  The lisp reader is implemented by means of a dispatch table. Each character
  read by the (main) reader indexes into this table to find a function to call
  which is called with that character and the stream currently being read
  from. this function returns a scilisp object. This dispatch table
  can be customized (it's stored in a global variable) to change how
  lisp reads things. If there is no dispatch function for a particular
  character it is interpreted as the first character of a symbol.

  Scilisp expects input to be encoded in UTF-8 and only dispatches on
  the first 127 bytes (i.e ascii characters) any byte > 127 is assumed
  to be part of a symbol name.

*/
/*
  default dispatch characters:
  0-9 dispatch to read_number, since 0-9 can also be the first character
  of a symbol read_number (which might get called read_number_or_symbol) can
  return a symbol

  '\'' reads the next expression and returns a two element list
  (quote expr) where expr is the expression that has been read.

  '`' sets a flag and reads the next expression with special semantics
  for the ',' character, which is normally invalid, and returns
  a list (quote expr) where expr is the expression, inside of a '`'
  a comma not followed by @ will replace the following expression with
  the result of evaluating that expression. a ',' followed by an @ expects
  the next argument to be either a list or a varible which evaluates
  to a list, this list is then `spliced` into the resultant expression.
  This means that the elements of the list are inserted into the expression
  instead of the entire list, e.g `(1 ,@(2 3) 4) evaluates to (1 2 3 4)
  while `(1 (2 3) 4) evaluates to (1 (2 3) 4).

  ',' is normally an invalid character unless inside of a backquote in which
  case it behaves as described above.

  '#' is a macro character, it has a second dispatch table assoicated
  with it and dispatches based on the following character.
  The characters that are valid following a '#' are:


  '(' is the start of an s-expression, which is closed by ')',
  lisp programs are generally composed mainly of s-expressions,
  the first expression in a s-expression should be a macro, function
  or specaial form. The remaning expressions are passed to this
  subroutine as arguments.

  '['']' are used to create literal vectors, a '[' starts reading
  a vector which is closed by a ']'. There is no way to write an
  array, thus if there is another vector inside of the original
  vector it will be interpreted the same as anyother element.

  '"' is enterpreted as the start of a string which is terminated by
  another '"', to include a '"' in a string prefix it with a '\'

  '|' is used to quote a symbol, any character between a '|' and
  another '|' is not interpreted as normally, this can be used
  to write symbols with e.g. whitespace, this is rarely a good thing
  to do.

  '?' is used to write a character, the next character following '?'
  is interpreted as a character literal. '?' can be safely used in
  a symbol name provided it is not the first character.

  '.' is used to seperate the car and cdr of a cons cell. For
  the sake of convience lists can be written '(a b c d ....)
  the last element of which is implicitly nul. however if
  a '.' is placed before the last element of a list that element
  will be placed in the cdr rather than nil.
  ex. '(1 2) is read as (1 . (2 . nil)) while '(1 . 2) is just (1 . 2)

  ';' begins a single line comment, all text untill the next newline
  is discarded by the reader.

  ':' '{' '}' are reserved for future use

  ' ' '\t' '\n' (i.e whitespace) is used to seperate expressions
  any amount of whitespace is equlivent. expressions can also
  sepreated by opening/closing delimiters

  Any character < 20, other than new line (e.g the control characters)
  shouldn't be used, they should behave the same as any other character
  but its still a bad idea.

  any character not metioned is interpreted as the start of a symbol,
  which ends at the first dispatch character (excluding '.' and '?')
  or whitespace.

  If a file starts with #! the first line will be ignored
  on the assumption that it's a shell directive
*/
//temporary hack
DEFUN("quote",quote,1,UNEVALED,"return the argument unevaluated")
     (sl_obj obj){
  return Fcar(obj);
}
/*
  I need to make these available to the user, since they
  go in the default readtable and so should be accessable,
  I'll probably write trivial wrapppers around them
  and export those

*/
sl_obj read_cons(sl_char c, sl_stream *s);
sl_obj read_vector(sl_char c, sl_stream *s);
sl_obj read_string(sl_char c, sl_stream *s);
//this can return a symbol because it is legal to have a symbol
//name that starts with a valid numbef (e.g 1+)
sl_obj read_number(sl_char c, sl_stream *s);
sl_obj read_sharp(sl_char c, sl_stream *s);
sl_obj read_space(sl_char c, sl_stream *s);
sl_obj read_comment(sl_char c, sl_stream *s);
sl_obj read_comma(sl_char c, sl_stream *s);
sl_obj read_quote(sl_char c, sl_stream *s);
sl_obj read_backquote(sl_char c, sl_stream *s);
sl_obj read_sl_char(sl_char c, sl_stream *s);
sl_obj read_symbol(sl_char c, sl_stream *s);
sl_obj read_utf8_char(sl_char c, sl_stream *s);
//I may use this, but for now colons just specify keyword symbols
static sl_obj read_colon(sl_char c, sl_stream *s);
//intern the symbol with the name 'str'
static sl_obj make_symbol_reader(sl_string *str){
//perhaps not the most efficent way of doing this but it should be fast enough
  sl_obj sym = make_symbol_from_string(str);
//  DEBUG_PRINTF("Interning symbol %.*s\nHash value = %#0lx\n",
//               str->len,str->str,XSYMBOL(sym)->hv);
  return Fintern(sym, NIL);
}

DEFUN("read",read, 0, 1, "(read &optional (stream *standard-input*))"
      "read an object from stream or standard input if stream in nil\n"
      "If stream is a string read one object from it and return it along"
      "with the index of the next character in the string")
  (sl_obj stream){
  if(STRINGP(stream)){
    return read_from_string(stream);
  } else if (STREAMP(stream)) {
    return lread(XSTREAM(stream));
  } else {
    raise_error_cstr(0,"Type error in read, expected a stream");
  }
}

static sl_obj default_read(sl_stream *stream);

sl_obj read_from_string(sl_obj string){
  return default_read(string_to_stream(XSTRING(string)));
}
sl_obj read_from_c_string(const char *str, uint32_t len){
  len = (len ? len : strlen(str));
  return read_from_string(make_string_unboxed_nocopy(str, len, 0));
}

sl_obj lread(sl_stream *stream){
  /*
    //if not using the default readtable just dispatch on the character
    if(!thread_env->default_readable){
      sl_char c = read_char(stream);
      return thread_env->readtable[sl_char](c, stream);
    }
   */
  return default_read(stream);
}
static const read_dispatch_fn default_readtable[256] = {
  [' '] = read_space, ['\n'] = read_space, ['\t'] = read_space,
  ['('] = read_cons,
  ['['] = read_vector,
  [';'] = read_comment,
  ['-'] = read_number, ['+'] = read_number,
  ['0' ... '9'] = read_number, ['.'] = read_number,
  ['?'] = read_sl_char,
  ['`'] = read_backquote,
  [','] = read_comma,
  ['\''] = read_quote,
  ['#'] = read_sharp,
  ['"'] = read_string
};
sl_obj read_multiline_comment(sl_char c, sl_stream *s);
sl_obj read_hex_int(sl_char c, sl_stream *s);
sl_obj read_binary_int(sl_char c, sl_stream *s);
static sl_obj read_int_base(sl_char c, sl_stream *s, int base);
sl_obj read_unreadable_object(sl_char c, sl_stream *s);
sl_obj read_uninterned_symbol(sl_char C, sl_stream *s);
static const read_dispatch_fn sharp_dispatch_table[256] = {
  ['|'] = read_multiline_comment,
  ['x'] = read_hex_int, ['X'] = read_hex_int,
  ['b'] = read_binary_int, ['B'] = read_binary_int,
//  ['f'] = read_bigfloat,
  //screw octal
  ['<'] = read_unreadable_object,
  [':'] = read_uninterned_symbol
};

//reading a '.' is kinda tricky since it's rather location dependent

SL_INLINE sl_obj read_dispatch(sl_char c, sl_stream *stream){
  if(!default_readtable[c]){
    return read_symbol(c, stream);
  } else {
    return default_readtable[c](c,stream);
  }
}
static sl_obj default_read(sl_stream *stream){
  sl_char c = read_char(stream);
  sl_obj retval = read_dispatch(c, stream);
  return retval;
}


//routine to consume whitespace, returns the first non whitespace character
//read, even if this was EOF
SL_INLINE sl_char skip_space(sl_stream *stream){
  sl_char c;
  while(sl_is_space(c = read_char(stream)));
  return c;
}
sl_obj read_comment(sl_char c, sl_stream *stream){
  assert(c == ';');
  //  DEBUG_PRINTF("Reading comment\n");
  while((c = read_char(stream)) != '\n' && c != EOF);
  if(c == EOF){return NIL;}//I'm not sure if this is an error or not
  //skip any whitespace so that read_space doesn't need to be called
  c = skip_space(stream);
  return read_dispatch(c, stream);
}
sl_obj read_multiline_comment(sl_char c, sl_stream *stream){
  assert(c == '|');
  int nesting = 0;
  c = read_char(stream);
  while(1){
    if(c == '!'){
      c = read_char(stream);
      if(c == '#'){
        if(nesting == 0){
          break;
        } else {
          nesting--;
        }
      }
    } else if(c == '#'){
      c = read_char(stream);
      if(c == '|'){
        nesting++;
      }
    }
  }
  return NIL;
}
sl_obj read_space(sl_char c, sl_stream *stream){
  assert(sl_is_space(c));
  c = skip_space(stream);
  return read_dispatch(c, stream);
}
sl_obj read_unreadable_object(sl_char c, sl_stream *s){
  //Obviously we can't actually read an unreadable object so
  //this just signals an error
  raise_error_cstr(0,"Read error, encountered unreadable object");
}
/*SL_INLINE sl_obj sl_string_to_int(sl_string *str, int base){
  errno = 0;
  //maybe go straight to a bignum if string is of a certain length
  //specifically if str->len >= sizeof(STRINGIFY(UINT64_MAX))
  //This assumes input is NULL terminated, make sure this is true,
  //or else use parse_integer
  uint64_t val = strtoul((char*)str->str, NULL, base);
  //need to check to make sure val can fit in 62 bits
  if(errno == ERANGE) {
    DEBUG_PRINTF("Reading bigint\n");
    sl_bigint *bignum = parse_bigint(str, 0);
    return make_bignum(bignum, SL_bigint);
  } else {
    //the only other error returned frem strtol is EINVAL for an unsupported
    //character in the current base, but we would've caught that already.
    //thus the assert
    assert(!errno);
    return PACK_INT(val);
  }
}*/
SL_INLINE sl_obj sl_string_to_int(sl_string *str, int base){
  SL_UNUSED char *endptr;
  sl_obj retval = parse_integer((char*)str->str, &endptr, str->len, base);
//  assert((endptr - str->str) == str->len);
  return retval;
}
SL_INLINE sl_obj sl_string_to_float(sl_string *str){
  errno = 0;
  double val = strtod((char*)str->str, NULL);
  if(errno == ERANGE){
    //if you specified a constant which can't fit in a double presumably
    //you want a bigfloat, whereas in a computation overflow or underflow
    //with doubles would just return inf or 0
    sl_bigfloat *bignum = parse_bigfloat(str);
    return make_bignum(bignum, SL_bigfloat);
  } else {
    assert(!errno);
    return make_double(val);
  }
}

sl_obj read_number(sl_char c, sl_stream *stream){
  int is_float = (c == '.' ? 1 : 0);
  //a number matiches the regular expression:
  //[0-9]+(\.[0-9]*([eE][+-]?[0-9]+)?)? | '.'[0-9]+([eE][+-]?[0-9]+)?
  //numbers is bases other than 10 are prefixed with a '#' and are
  //thus in the read sharp function
  sl_string_buf *buf = string_buf_alloca();
  do {
    //no need to check the first character, since it has to be valid for
    //this function to be called in the first place
    buf_append(buf, c);
  }  while(sl_is_digit(c = read_char(stream)));
  if(c == '.'){
    is_float = 1;
    do {
      buf_append(buf, c);
    } while(sl_is_digit(c = read_char(stream)));
  }
  if (c == 'e' || c == 'E') {
    buf_append(buf, c);
    c = read_char(stream);
    if(sl_is_digit(c) || c == '-' || c == '+'){
      do {
        buf_append(buf, c);
      } while(sl_is_digit(c = read_char(stream)));
    } else {
      //invalid read syntax
      DEBUG_PRINTF("read error, invalid floating point exponent format\n");
      raise_error_temp();
    }
  }
  //for now sl_is_delim(c) is (sl_isspace(c) || c == ')' || c == '(' || c == EOF)
  if (sl_is_delim(c)) {
    unread_char(stream);
    buf_append(buf,'\0');//to appese the strtoX functions
    sl_string *str = string_buf_to_string(buf);
    errno = 0;
    if(is_float){
      return sl_string_to_float(str);
    } else if(sl_is_digit(str->str[0]) || sl_is_digit(str->str[1])){
      return sl_string_to_int(str, 0);
    } else {
      /*this case only happens for an isolated +/-
        We use substring because the extra '\0' changes the hash value */

      return make_symbol_reader(quick_substring(str,0,1));
    }
  } else if(sl_is_symbol_char(c)){
    DEBUG_PRINTF("Reading symbol begining with a digit\n");
    do {
      buf_append(buf,c);
    } while (sl_is_symbol_char(c = read_char(stream)));

    if(sl_is_delim(c)){
      unread_char(stream);
      return make_symbol_reader(string_buf_to_string(buf));
    }
  } else {
    sl_string *str = string_buf_to_string(buf);
    DEBUG_PRINTF("Unexpected character %c, in read number\n"
                 "Text read was %.*s\n",c,str->len,str->str);
  }
  raise_error_temp(); //I don't know what else we could do here
}
sl_obj read_bigfloat(sl_char c, sl_stream *stream){
  //bigfloat starts with a #f
  //I'll do this later
  assert(0);
}
sl_obj read_hex_int(sl_char c, sl_stream *stream){
  assert(c == 'X' || c == 'x');
  sl_string_buf *buf = string_buf_alloca();
  while(sl_is_xdigit((c = read_char(stream)))){
    buf_append(buf, c);
  }
  if(sl_is_delim(c)){
    unread_char(stream);
    sl_string *str = string_buf_to_string(buf);
    return sl_string_to_int(str, 16);
  } else {
    //this is always an error since there's no way this could be a symbol
    raise_error_temp();
  }
}
sl_obj read_binary_int(sl_char c, sl_stream *s){
  assert(c == 'b' || c == 'B');
  sl_string_buf *buf = string_buf_alloca();
  c = read_char(s);
  while(c == '0' || c == '1'){
    c = read_char(s);
  }
  if(sl_is_delim(c)){
    unread_char(s);
    sl_string *str = string_buf_to_string(buf);
    return sl_string_to_int(str, 2);
  } else {
    raise_error_temp();
  }
}
sl_obj read_cons(sl_char c, sl_stream *stream){
  assert(c == '(');
  c = read_char(stream);
  if(c == ')'){return NIL;}
  sl_obj next = read_dispatch(c, stream);
  sl_cons *list = alloc_cons();
  list->car = next; list->cdr = NIL;
  sl_obj retval = PACK_CONS(list);
  c = read_char(stream);
  while(c != ')'){
    if(SL_UNLIKELY(c == '.')){
      next = read_dispatch(read_char(stream), stream);
      list->cdr = next;
      return retval;
    }
    next = read_dispatch(c, stream);
    list->cdr = make_cons(next, NIL);
    list = XCONS(list->cdr);
    //if there is a space before the last ')' calling read_space will dispatch on ')'
    //which is bad, so just skip the space ourselves
    c = skip_space(stream);
  }
  check_eof(c);
  return retval;
}

sl_obj read_symbol(sl_char c, sl_stream *stream){
  assert(sl_is_symbol_char(c),"c = %d\n",c);
  sl_string_buf *buf = string_buf_alloca();
 // DEBUG_PRINTF("reading symbol\n");
  do {
    buf_append(buf,c);
  } while (sl_is_symbol_char(c = read_char(stream)));
  if(sl_is_delim(c)){
    unread_char(stream);
    sl_string *str = string_buf_to_string(buf);
    sl_obj sym = make_symbol_reader(str);
//    DEBUG_PRINTF("read symbol\n");
    return sym;
  } else {
    sl_string *str = string_buf_to_string(buf);
    DEBUG_PRINTF("Unexpected character %c, in read symbol\n"
                 "Text read was %.*s\n",c,str->len,str->str);
    raise_error_temp();
  }
}
sl_obj read_uninterned_symbol(sl_char c, sl_stream *s){
  raise_error_temp();
}
sl_obj read_vector(sl_char c, sl_stream *s){
  assert(c == '[');
  c = read_char(s);
  if(c == ']'){
    return PACK_VECTOR(make_svector(0));
  }
  //could possibly use a dedicated temporary data stack in the environment
  //for scratch space when building the vector
  uint64_t vec_size = 8, vec_len = 0;
  sl_obj *vector_contents = sl_malloc(vec_size*sizeof(sl_obj));
  do{
    sl_obj next = read_dispatch(c, s);
    if(vec_len >= vec_size){
      vec_size *= 2;
      vector_contents = sl_realloc(vector_contents, vec_size);
    }
    vector_contents[vec_len++] = next;
  } while ((c = read_char(s)) != ']');
  return make_svector_obj(vector_contents, vec_len);
}

SL_INLINE sl_char parse_escape(sl_stream *s){
  sl_char c = read_char(s);
  //number of digits allowed in a unicode escape, set to 8 if \U is used
  int unicode_esc_digits = 4;
  switch(c){
    case 'a':
      return '\a';
    case 'b':
      return '\b';
    case 'd':
      return 0x7f;
    case 'e':
      return '\e';
    case 'f':
      return '\f';
    case 'n':
      return '\n';
    case 'r':
      return '\r';
    case 't':
      return '\t';
    case 'v':
      return '\v';
    case '?':
      return '?';
    case '\\':
      return '\\';
    case '\"':
      return '\"';
    case 'x': {
      int32_t x=0, temp, count=0;
      while(1){
        c = read_char(s);
        if((temp = parse_hex_digit_char(c)) != -1){
          x *= 16;
          x += temp;
        } else {
          break;
        }
        count++;
      }
      if(count > 2){
        DEBUG_PRINTF("Count = %d\n",count);
        raise_error_fmt(Krange_error,"Hex character out of range \\x%x", x);
      }
      return x;
    }
    case 'U':
      unicode_esc_digits = 8;
    case 'u': {
      uint32_t u = 0, count=0, temp;
      while(count < unicode_esc_digits){
        c = read_char(s);
        temp = parse_hex_digit_char(c);
        if(temp != -1){
          u = (u<<4) + temp;
        } else if(sl_is_delim(c)){
          break;
        } else {
          raise_error_fmt(Kread_error, "Non hex digit %c used "
                          "in unicode escape", c);
        }
        count++;
      }
      if(u > MAX_UNICODE_CODEPOINT){
        raise_error_fmt(Krange_error, "Invalid unicode codepoint %#x", u);
      }
      return u;
    }
    default://read the next character as a normal character
      if(sl_is_ascii(c)){
        return c;
      } else {
        return read_utf8_char(c, s);
      }
  }
}

sl_obj read_string(sl_char c, sl_stream *s){
  assert(c == '"');
  //because of escapes it's (probably) faster, and definately eaiser
  //to use a string buffer
  sl_string_buf *buf = string_buf_alloca();
  while((c = read_char(s)) != '"'){
    if(c == '\\'){
      sl_char esc = parse_escape(s);
      buf_append(buf, esc);
    } else if(sl_is_ascii(c)){
      buf_append(buf, c);
    } else {
      buf_append(buf, read_utf8_char(c,s));
    }
  }
  return PACK_STRING(string_buf_to_string(buf));
}
sl_obj read_sharp(sl_char c, sl_stream *s){
  assert(c == '#');
  c = read_char(s);
  if(!sharp_dispatch_table[c]){
    raise_error_fmt(0, "Error, unknown sharp dispatch character %c",c);
  }
  return sharp_dispatch_table[c](c, s);
}
sl_obj read_quote(sl_char c, sl_stream *s){
  assert(c == '\'');
  c = read_char(s);
  sl_obj obj = read_dispatch(c, s);
  return make_list2(Qquote, obj);
}

//these will get put in the right place later
sl_obj read_comma(sl_char c, sl_stream *s){
  assert(c == ',');
  if(!thread_env->backquote_flag){
    raise_error_cstr(0, "Error, comma outside of a backquote");
  }
  /*
    I may (and probably will) change these to evaluate these as they
    are read
  */
  if(peek_char(s) == '@'){
    read_char(s);
    thread_env->backquote_flag--;
    sl_obj spliced_obj = read_dispatch(read_char(s),s);
    /* not sure what the semantics should be here
       if(!CONSP(spliced_obj)){raise_error_temp();}*/
    thread_env->backquote_flag++;
    return make_cons(Kbackquote_splice, spliced_obj);
  } else {
    thread_env->backquote_flag--;
    sl_obj unquoted_obj = read_dispatch(read_char(s),s);
    thread_env->backquote_flag++;
    return make_cons(Kbackquote_unquote, unquoted_obj);
  }
}

sl_obj read_backquote(sl_char c, sl_stream *s){
  c = read_char(s);
  if(c != '('){
    //If it's not a list being quasiquoted it's the same as a normal quote
    return make_list2(Qquote, read_dispatch(c, s));
  }
  thread_env->backquote_flag++;
  sl_obj list = read_dispatch(c, s);
  thread_env->backquote_flag--;
  //  return make_list2(Qquasiquote, list);
  //  DEBUG_PRINTF("Processing backquote form %s\n",c_sprint(list));
  /*
    This is going to be slow, but it should work. I create a new list, I'm
    basically reimplementing append (I should at least figure out how to do,
    this with a call to append), and I have to treat the head of the list
    specially, but it does work.
  */
  sl_obj retval;
  sl_obj tail;
  /*
    This treats the head of the list specially, I'm not sure if there's
    a way to avoid this
   */
  if(CONSP(XCAR(list))){
    if(EQ(XCAAR(list), Kbackquote_splice)){
      retval = Feval(XCDAR(list));
      if(!CONSP(retval)){
        if(CONSP(XCDR(list))){
          raise_error_temp();
        } else {
          return retval;
        }
      }
      tail = get_pointer_to_tail(retval);
    } else if(EQ(XCAAR(list), Kbackquote_unquote)){
      retval = make_cons(Feval(XCDAR(list)), NIL);
      tail = retval;
    }
  } else {
    retval = make_cons(XCAR(list), NIL);
    tail = retval;
  }
  list = XCDR(list);
  while(CONSP(list)){
    if(CONSP(XCAR(list))){
      if(EQ(XCAAR(list), Kbackquote_splice)){
        //        DEBUG_PRINTF("Processing backquote splice\n");
        sl_obj to_splice = Feval(XCDAR(list));
        //        DEBUG_PRINTF("Splicing in %s\n",c_sprint(to_splice));
        sl_obj next = XCDR(list);
        XCDR(tail) = to_splice;
        if(!CONSP(to_splice)){
          if(CONSP(XCDR(list))){
            raise_error_temp();
          } else {
            return retval;
          }
        }
        tail = get_pointer_to_tail(tail);
      } else if(EQ(XCAAR(list), Kbackquote_unquote)){
        sl_obj to_insert = Feval(XCADR(list));
        XCDR(tail) = make_cons(to_insert, NIL);
        tail = XCDR(tail);
      }
    } else {
      XCDR(tail) = make_cons(XCAR(list), NIL);
      tail = XCDR(tail);
    }
    list = XCDR(list);
  }
  XCDR(tail) = list;
  //  DEBUG_PRINTF("Returning '%s\n",c_sprint(retval));
  return make_list2(Qquote, retval);
}
/*
sl_obj read_backquote(sl_char c, sl_stream *s){
  c = read_char(s);
  if(c != '('){
    //If it's not a list being quasiquoted it's the same as a normal quote
    return make_list2(Qquote, read_dispatch(c, s));
  }
  thread_env->backquote_flag++;
  sl_obj retval = read_dispatch(c, s);
  thread_env->backquote_flag--;
  DEBUG_PRINTF("Processing backquote form %s\n",c_sprint(retval));
  /
    This is going to be slow, but it should work.
  /
  sl_obj list = retval;
  sl_obj tail = list;
  /
    `(foo ,@(a b c) ,d) -> (foo (backquote_splice (a b c)) d) ->
    (append 'foo (a b c))
  /
  while(CONSP(list)){
    if(CONSP(XCAR(list))){
      if(EQ(XCAAR(list), Kbackquote_splice)){
        DEBUG_PRINTF("Processing backquote splice\n");
        sl_obj to_splice = Feval(XCDAR(list));
        DEBUG_PRINTF("Splicing in %s\n",c_sprint(to_splice));
        sl_obj next = XCDR(list);
        if(EQ(list, tail)){

        XCAR(list) = to_splice;
        /
        //        sl_obj tail = list;

        //I'm not sure if I can append in place or not
        //I think this is ok, I'm not change the list I'm splicing in
        while(CONSP(to_splice)){
          sl_obj val = POP(to_splice);
          XCDR(tail) = make_cons(val, NIL);
          tail = XCDR(tail);
        }
        XCDR(tail) = next;
        list = next;
        continue;/
      } else if(EQ(XCAAR(list), Kbackquote_unquote)){
        sl_obj to_eval = XCADR(list);
        sl_obj evaled = Feval(to_eval);
        XCAR(list) = evaled;
      }
    }
    tail = list;
    list = XCDR(list);
  }
  return make_list2(Qquote, retval);
}
*/
sl_obj read_sl_char(sl_char c, sl_stream *s){
  assert(c == '?');
  c = read_char(s);
  check_eof(c);
  sl_char retval = 0;
  if (c == '\\') {
    retval = parse_escape(s);
  } else if(sl_is_ascii(c)){
    retval = c;
  } else {
    retval = read_utf8_char(c, s);
  }
  return make_char(retval);
}

sl_obj read_utf8_char(sl_char c, sl_stream *stream){
  assert(c & 0x80);//c should be a non ascii utf8 lead byte
  sl_char retval, min;
  int remain;
  if((c & 0xE0) == 0xC0) {
    min = 0x80;
    remain = 1;
    retval = c & 0x1F;
  } else if((c & 0xF0) == 0xE0) {
    min = 0x800;
    remain = 2;
    retval = c & 0x0F;
  } else if((c & 0xF8) == 0xF0) {
    min = 0x10000;
    remain = 3;
    retval = c & 0x07;
  } else if((c & 0xFC) == 0xF8) {
    min = 0x200000;
    remain = 4;
    retval = c & 0x03;
  } else if((c & 0xFE) == 0xFC) {
    min = 0x4000000;
    remain = 5;
    retval = c & 0x01;
  } else {
    raise_error_temp();
  }
  while(remain--) {
    c = read_char(stream);
    if((c & 0xC0) != 0x80) {
      raise_error_temp();
    }
    retval <<= 6;
    retval |= c & 0x3F;
  }
  if(retval < min){
    raise_error_temp();
  }
  return make_char(retval);
}
//assumes xdigit is in 0-9, a-f or A-F
static inline int digit_to_number(char digit){
  return digit - 0x30;
}
//convert a letter to a number, where 'a' = 'A' = 11 and 'z' = 'Z' = 36
static inline int letter_to_number(char letter){
  return (letter | 0x20) - 0x56;
}
static inline int char_to_number(char c){
  if(sl_is_digit(c)){
    return digit_to_number(c);
  } else {
    return letter_to_number(c);
  }
}
#define sl_is_binary_digit(c) (c == '0' || c == '1')
#define sl_is_oct_digit(c) (c >= '0' && c <= '8')
static const uint8_t valid_digits[] =
  "0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
//max_length_per_base[n] is the length of the longest string in base n
//than can possibly fit in a 64 bit integer (I should make it 62 bit)
//the formula for m bits in base n is just m/lg(n)
static int max_length_per_base[35] =
  {64, 41, 32, 28, 25, 23, 22, 21, 20, 19, 18, 18,
   17, 17, 16, 16, 16, 16, 15, 15, 15, 15, 14, 14,
   14, 14 ,14, 14, 14, 13, 13, 13, 13, 13, 13};
/* static int max_lengh_per_base_62[35] = */
/*   {62, 40, 31, 27, 24, 23, 21, 20, 19, 18, 18, 17, */
/*    17, 16, 16, 16, 15, 15, 15, 15, 14, 14, 14, 14, */
/*    14, 14, 13, 13, 13, 13, 13, 13, 13, 13, 12} */
/*
  If for some reason this is too slow, change the calling convention to
  require that str point to a non-whitespace character and len > 0;

  Currently this should serve as a working base for a lisp version
  of parse integer, since it skips whitespace and accepts an empty
  string, if I change it make sure to keep this version to use in lisp.
*/
//assumes base is between 2 and 36 inclusive
sl_obj parse_integer(char *str, char **endptr, int len, int base){
  sl_obj ret = sl_0;
  if(len == 0){
    len = INT_MAX;
  }
  //we need to use an unsigned number since signed overflow is undefined
  //and we use overflow to decide when to shift to a bigint
  uint64_t num = 0;
  int i = 0, negitive = 0;
  int overflow = 0;
  //ignore leading space
  while(sl_is_space(str[i]) && ++i < len);
  if(SL_LIKELY(i < len)){
    if(str[i] == '-'){
      negitive = 1;
      i++;
    }
  }
  if(SL_UNLIKELY(i == len)){
    //strtol puts str in endptr in this case
    if(endptr){*endptr = (str + i);}
    return ret;
  }
//I'm pretty sure this will cause an error if the string is something like
//0x abcdefg. It should be read as a 0, but I'm not sure what will happen
  if(base == 0 && ((i+1) < len)){
    if(str[i] == '0' && str[i+1] == 'x'){
      base = 16;
      i+=2;
    } else if(str[i] == '0' && str[i+1] == 'o'){
      base = 8;
      i+=2;
    } else if(str[i] == '0' && str[i+1] == 'b'){
      base = 2;
      i+=2;
    } else {
      base = 10;
    }
  }
  while(str[i] == '0' && ++i < len);//read leading zeros
  if(SL_UNLIKELY(i== len)){
    if(endptr){*endptr = (str+i);}
    return sl_0;
  }
#define TO_NUMBER(start, end, base, to_number)          \
  ({uint64_t number = 0, next = 0;                      \
    int j;                                              \
    for(j = start; j < end; j++){                       \
      next = (number * base) + to_number(str[j]);       \
      if(next < number){/*overflow*/                    \
        overflow = j;                                   \
        break;                                          \
      } else {                                          \
        number = next;                                  \
      }                                                 \
    }                                                   \
    number;})
//two cases for bigints since the compilier can't optimize bigint multiplication
#define TO_BIGNUM(start, end, base, to_number, init)    \
  ({sl_bigint *x = sl_malloc(sizeof(sl_bigint));        \
    int j;                                              \
    mpz_init_set_ui(x, init);                          \
    for(j=start; j<end; j++){                           \
      mpz_mul_ui(x,x,base);                           \
      mpz_add_ui(x,x,to_number(str[j]));              \
    }                                                   \
    x;})
//for power of 2 base, base_exp = lg(base)
#define TO_BIGNUM_POW_2_BASE(start, end, base, to_number, init)         \
  ({sl_bigint *x = sl_malloc(sizeof(sl_bigint));                        \
    int j;                                                              \
    mpz_init_set_ui(x, init);                                           \
    for(j=start; j<end; j++){                                           \
      mpz_mul_2exp(x,x,LOG_2_FLOOR(base));                              \
      mpz_add_ui(x,x,to_number(str[j]));                                \
    }                                                                   \
    x;})
  int start = i;
  /*
    Use special cases for 2, 8, 10, and 16 to speed them up.
    Multiplies in base 2, 8, or 16 become shifts, and for x86 at least
    a multiply by 10 becomes two lea instructions.
   */
#define DO_CASE(to_number, to_bignum)                           \
  if(i <= max_length_per_base[base]){                           \
    num = TO_NUMBER(start, i, base, to_number);                 \
  }                                                             \
  if(i > max_length_per_base[base] || overflow > 0){            \
    sl_bigint *temp = to_bignum(start + overflow, i, base,       \
                               to_number, num);                 \
    ret = make_bigint(temp);                                   \
  } else {                                                      \
    ret = PACK_INT(num);                                        \
  }

  if(base == 10){
    while(sl_is_digit(str[i]) && ++i < len);
    DO_CASE(digit_to_number, TO_BIGNUM);
  } else if(base == 16){
    while(sl_is_xdigit(str[i]) && ++i < len);
    DO_CASE(char_to_number, TO_BIGNUM_POW_2_BASE);
  } else if(base == 8){
    while(sl_is_oct_digit(str[i]) && ++i < len);
    DO_CASE(digit_to_number, TO_BIGNUM_POW_2_BASE);
  } else if(base == 2){
    while(sl_is_binary_digit(str[i]) && ++i < len);
    DO_CASE(digit_to_number, TO_BIGNUM_POW_2_BASE);
  } else if(base < 10){
    i = memspn((uint8_t*)str + i, len - i, valid_digits, base);
    DO_CASE(digit_to_number, TO_BIGNUM);
  } else {
    i = memspn((uint8_t*)str + i, len -i, valid_digits, 10 + (base-10)*2);
    DO_CASE(char_to_number, TO_BIGNUM);
  }
  if(endptr){*endptr = (str + i);}
  if(negitive){
    ret = sl_neg(ret);//could be optimized
  }
  return ret;
}
DEFUN("parse-integer", parse_integer, 1, 2, "(parse-integer string &optional (base 10))\n"
      "Return integer in base `base` parsed from string, "
      "and the index of the first invalid character")
  (sl_obj str_obj, sl_obj base){
  if(!STRINGP(str_obj)){
    raise_error_temp();
  }
  if(NILP(base)){
    base = sl_0;
  }
  if(!INTP(base) || XINT(base) > 36 || XINT(base) < 2){
    raise_error_temp();
  }
  sl_string *str = XSTRING(str_obj);
  sl_obj num = parse_integer((char*)str->str, NULL, str->len, XINT(base));
  return num;
}
#define LG_10 3.3219280948873626
/*
  How to read a float:
  Read the mantissa as an integer, ignoring the decimal point,
    into an arbitary precision integer, so for 1.223 this would be 1223
  While doing this record where the decimal point is to get a base
    exponent, i.e 100.333 -> 100333 with an exponent of 3
  Read the exponent if there is one (i.e the e#### part)
    add this to the base exponent to get the actual exponent.
  Convert the exponent into binary and subtract the offset.
  Right shift the mantissa so it fits in the number of bits
    in the significand
  Shift/Or the exponent with the significand to get the number.
  Set the sign bit if necessary.
*/
//force base 10 for now
//I need a dynamic variable to set the precision of floating point numbers,
//so I can decide when to use a bigfloat.
sl_obj parse_float(char *str, char **endptr, int len){
  int i = 0;
  if(len == 0){len = INT_MAX;}
  int negitive;
  sl_obj ret = sl_float_0;
  while(sl_is_space(str[i++]) && i < len);
  if(SL_LIKELY(i < len)){
    if(str[i] == '-'){
      negitive = 1;
      i++;
    }
  }
  if(SL_UNLIKELY(i == len)){
    if(endptr){*endptr = (str + i);}
    return ret;
  }
  //read leading zeros
  while(str[i++] == '0' && i < len);
  if(SL_UNLIKELY(i == len)){
    if(endptr){*endptr = (str + i);}
    return ret;
  } else {
    i--;
  }
  //ideally we'd use mpn functions for speed, but for now the library
  //functions are going to be faster
  sl_bigint mant;
  mpz_init(&mant);
  int point = 0;
  int64_t exp = 0;
  if(str[i] == '.'){
    do {
      exp--;
    } while(str[++i] == '0' && i < len);
    if(SL_UNLIKELY(i == len)){
      if(endptr){*endptr = (str + i);}
      return ret;
    }
    point = 1;
  }
  int start = i;
  while(i < len){
    //read the mantissa
    if(sl_is_digit(str[i])){
      uint64_t digit = digit_to_number(str[i++]);
      mpz_mul_ui(&mant, &mant, 10);
      mpz_add_ui(&mant, &mant, digit);
    } else if(str[i] == '.'){
      if(point){
        break;
      } else {
        point = 1;
        exp = i-start-1;
        i++;
      }
    } else {
      break;
    }
  }
  int mant_digits = i - point;
  int nbits = ceil(mant_digits * LG_10);
  int read_exp = 0;
  if(str[i] == 'e' || str[i] == 'E'){
    read_exp = 0;
    int neg = str[++i] == '-';
    while(i < len && sl_is_digit(str[i])){
      read_exp = (read_exp*10) + digit_to_number(str[i++]);
    }
    if(neg){read_exp *= -1;}
  }
  exp += read_exp;
  //this is using a double, using a bigfloat is similar
  //convert exp to binary and add offset, clip to allowed range of exponents
  uint64_t offset_exp = CLIP_INT(ceil((exp + 1023)*LG_10),0,2047);
  int nlimbs = mpz_size(&mant);
  uint64_t frac = 0;
  if(nlimbs == 1){
    frac = mpz_getlimbn(&mant, 0);
  } else {
    mp_limb_t limbs[2] = {mpz_getlimbn(&mant, nlimbs-1),
                          mpz_getlimbn(&mant, nlimbs-2)};
    size_t shift_cnt = __builtin_clzl(limbs[0]);
    mpn_lshift(limbs, limbs, 2, shift_cnt);
    frac = limbs[0];
  }
  int round_bit = (frac & (1 << 11));
  //truncate to 52 most significant digits
  frac >>= 12;
  //round (to nearest even) if necessary
  frac += (round_bit & (frac & ~1));
  //set exponent
  frac |= (offset_exp << 52);
  //set sign
  if(negitive){
    frac |= ((uint64_t)1 << 63);
  }
  sl_number *n = sl_malloc(sizeof(sl_number));
  n->val = frac;
  n->type = SL_double;
  return PACK_NUMBER(n);
}

void init_read_symbols(void){
  INIT_SUBR(read, 1);
  INIT_SUBR(quote, UNEVALED);
  INIT_SUBR(parse_integer, 2);
}
/*
  parse integer in the case len = 0, premature optimization,
  if it's even an optimization

    errno = 0;
    int64_t num = strtoul(str, endptr, base);
    if(errno = ERANGE){
      int64_t str_len = memspn((uint8_t*)str, UINT_MAX,
                               valid_digits, 10 + (base-10)*2);
      char temp = str[str_len];
      str[str_len] = '\0';
      sl_bigint x;
      mpz_set_str(&x, str, base);
      str[str_len] = temp;
      ret = make_bigint(x);
    } else {
      ret = PACK_INT(num);
    }
    return ret;
  }*/

