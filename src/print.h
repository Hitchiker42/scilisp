/*
  Header file for functions to print lisp objects
*/
#ifndef _SL_PRINT_H_
#define _SL_PRINT_H_
#include "common.h"
/*
  TODO: Update print functions to take a stream argument, using
  a string_output_stream (which I need to write first) to output strings,
  Also add a flag to internal print functions to control printing readably
*/
//the core printing function, that actually does the work of transforming
//lisp objects into text.
sl_string* sl_sprint(sl_obj obj);
//sl_string* sl_sprinc(sl_obj obj);//print human readably
//tempory untill I implement sl_sprinc
#define sl_sprinc(obj) sl_sprint(obj)
//currently this is essentially the same as asprintf, but I'll add to it
sl_string* sl_sprintf(char *fmt,size_t sz,...);
//useful for testing and interfacing w/c
char* c_sprint(sl_obj obj);
/*
  It may be useful to add an sl_asprinf for internal use, but that's an
  optimization I'll worry about later.
*/
//convert an integer into a string in the given base, if upcase is true
//use uppercase letters, othewise use lowercase
sl_string* sl_itoa(int64_t value, uint32_t base, int upcase, int sign);
sl_obj Fsprint(sl_obj obj); //print object to a string
/*
  Common lisp print functions.
  Should provide a function to print a variable number of arguments, either
  (defun print stream &rest args) probably this, or
  (defun print &key (stream *stdout*) &rest args), probably to confusing
  
  Also just as a note to self, all lisp fuctions return an sl_obj, even
  ones like print which work by side effect
*/
sl_obj Fprint(sl_obj obj, sl_obj stream);//readably after newline
sl_obj Fprin1(sl_obj obj, sl_obj stream);//readably
sl_obj Fprinc(sl_obj obj, sl_obj stream);//human readably
//void Fpprint(sl_obj obj, sl_obj stream);//readably between newlines
//the defination for this is in format.c, since it's rather complicated
sl_obj Fformat(uint64_t num_args, sl_obj *args);
#if (defined DEBUG) && !(defined NDEBUG)
//mark_point() updates the position in tests, so it's useful
//to use in debug_print, but obviously debug_print needs
//to work outside of tests as well, so define it as an empty
//macro if necessary
#ifndef mark_point
#define mark_point()
#endif
#define debug_print(obj)                                \
  ({mark_point();                                       \
    sl_string *temp = sl_sprint(obj);                   \
    fprintf(stderr, "%.*s\n", temp->len, temp->str);})
#endif
#endif
