#include "cord.h"
#include "stdint.h"
//extensions to the cord package for use in scilisp


static char fetch_ith_char(size_t i, void *client_data){
  uint8_t *str = client_data;
  return str[i];
}
//convience function to create a cord from a sequence of
//arbitary bytes, primarally used to create a cord with
//embedded nuls
CORD CORD_from_bytes(uint8_t *str, size_t len){
  return CORD_from_fn(fetch_ith_char, str, len);
}

char * CORD_to_char_arr(CORD x, int *len){
  size_t n = CORD_len(x);
  char *result = GC_MALLOC_ATOMIC(n);
  if (result == 0) {OUT_OF_MEMORY;}
  CORD_fill_buf(x, 0, n, result);
  if(len) {*len = n;}
  return result;
}
