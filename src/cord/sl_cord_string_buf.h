#include "sl_cord.h"
//defined in such a way that cord.h doesn't need to be included
typedef struct CORD_buf sl_string_buf;
//put this here since it needs to be somewhere inside of an
//!defined CUSTOM_STRING_BUF conditional
/*
  For some reason using alloca makes string buffers fail
*/
#define string_buf_init(__buf)                  \
  cord_buf_init(__buf)
//the default string buffer functions assume that the strings it recieves
//will remain constant for the duration of the life of the buffer and
//so don't copy them until the whole buffer gets turned into a string
void string_buf_append_char(sl_string_buf *buf, uint32_t c);
//The printed representation of obj is appended to the buffer
void string_buf_append_obj(sl_string_buf *buf, sl_obj obj);
//these functions don't copy their arguments
void string_buf_append_str(sl_string_buf *buf, sl_string* s);
void string_buf_append_cstr(sl_string_buf *buf, const char *s);
void string_buf_append_cstrn(sl_string_buf *buf, const char *s, int len);
//these functions copy their arguments, so are safe to use with
//strings that will be modified/destroyed
void string_buf_append_str_copy(sl_string_buf *buf, sl_string* s);
void string_buf_append_cstr_copy(sl_string_buf *buf, const char *s);
void string_buf_append_cstrn_copy(sl_string_buf *buf, const char *s, int len);
//this only works for one character, and only if it was added with
//sl_string_buf_append_char
void string_buf_unread_char(sl_string_buf *buf);
//I'd rather not include this, but for the sake of speed
//these ought to be macros or inline functions
#include "cord/cord_buf.h"
#define buf_append(__buf,s)                           \
  CORD_buf_append_char_star(__buf, s->str, s->len)
#define buf_append_str(__buf,s)               \
  CORD_buf_append_char_star(__buf, s->str, s->len)
#define buf_append_char(__buf,c)              \
  CORD_buf_append(__buf, c)
#define buf_append_cstr(__buf,s)              \
  CORD_buf_append_cord(__buf, s)
#define buf_append_cstrn(__buf,s,n)                \
  CORD_buf_append_char_star(__buf, s, n)
#define buf_unread_char(__buf)                \
  CORD_buf_unread_char(__buf)
#endif

