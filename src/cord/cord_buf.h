# ifndef CORD_BUF_H
# define CORD_BUF_H

# ifndef CORD_H
#  include "cord.h"
# endif
# ifndef CORD_BUF_SIZE
#   define CORD_BUF_SIZE 128
# endif
extern void *memcpy(void *__restrict dest,const void *__restrict src, size_t sz);
//Essentially the same as extensable cords, but with the added function
//of being able to delete the last character appended (akin to unread_char)
struct CORD_buf {
  CORD stored_cord;
  char *buf_ptr;
  char buf[CORD_BUF_SIZE+1];
};
typedef struct CORD_buf CORD_buf;
static __attribute__((unused))
void CORD_buf_flush(CORD_buf *x){
  int len=x->buf_ptr-x->buf;
  if(len == 0){return;}
  char *str=GC_MALLOC_ATOMIC(len*sizeof(char));
  memcpy(str,x->buf,len);
  str[len]='\0';
  x->stored_cord=CORD_cat_char_star(x->stored_cord,str,len);
  x->buf_ptr=x->buf;
  return;
}
//initialize a CORD_buf, if __buf is a null pointer allocate
//the space for the buffer on the stack
static inline __attribute__((unused))
void CORD_buf_init(CORD_buf *buf){ 
  buf->buf_ptr = buf->buf;
  buf->stored_cord = 0;
  memset(buf->buf,'\0',CORD_BUF_SIZE);
}

#define CORD_buf_alloca()                             \
  ({CORD_buf *__buf = alloca(sizeof(CORD_buf));        \
    __buf->buf_ptr = __buf->buf;                      \
    __buf;})
static inline __attribute__((unused))
void CORD_buf_append(CORD_buf *buf, unsigned int c){
  if(buf->buf_ptr==buf->buf+CORD_BUF_SIZE){
    CORD_buf_flush(buf);
  }
  *buf->buf_ptr++=c;
}
static __attribute__((unused))
void CORD_buf_append_cord(CORD_buf *buf,CORD s){
  CORD_buf_flush(buf);
  buf->stored_cord=CORD_cat(buf->stored_cord,s);
}
static inline __attribute__((unused))
void CORD_buf_append_char_star(CORD_buf *buf,const char *str,int len){
  if(buf->buf_ptr + len >= buf->buf+CORD_BUF_SIZE){
    CORD_buf_flush(buf);
    buf->stored_cord=CORD_cat_char_star(buf->stored_cord,str,len);
  } else {
    memcpy(buf->buf_ptr, str, len);
    buf->buf_ptr += len;
  }
}
//prepending is more complicated (and much slower)
static __attribute__((unused))
void CORD_buf_prepend(CORD_buf *buf, unsigned int c){
  CORD_buf_flush(buf);
  char temp[2] = {(unsigned char)c, '\0'};
  buf->stored_cord = CORD_cat(temp, buf->stored_cord);
}
static __attribute__((unused))
void CORD_buf_prepend_cord(CORD_buf *buf, CORD s){
  CORD_buf_flush(buf);
  buf->stored_cord = CORD_cat(s, buf->stored_cord);
}
static __attribute__((unused))
void CORD_buf_prepend_char_star(CORD_buf *buf, const char *str,int len){
  CORD_buf_flush(buf);
  //this is necessary since str doesn't have to be nul terminated
  CORD temp = CORD_cat_char_star(0, str, len);
  buf->stored_cord = CORD_cat(temp, buf->stored_cord);
}
static inline __attribute__((unused))
CORD CORD_buf_to_cord(CORD_buf *buf){
  CORD_buf_flush(buf);
  return buf->stored_cord;
}
static inline __attribute__((unused))
const char *CORD_buf_to_char_star(CORD_buf *buf){
  CORD_buf_flush(buf);
  return CORD_to_char_star(buf->stored_cord);
}
static __attribute__((unused))
void CORD_buf_unread_char(CORD_buf *buf){
  buf->buf_ptr--;
}
static __attribute__((unused))
int CORD_buf_len(CORD_buf *buf){
  CORD_buf_flush(buf);
  return CORD_len(buf->stored_cord);
}
#endif
