/*
  Implementation of string buffers using cords.

*/
#include "sl_cord.h"
sl_string_buf *malloc_string_buf(){
  cord_buf *buf = sl_malloc(sizeof(cord_buf));
  cord_buf_init(buf);
  return buf;
}
void string_buf_append_str(sl_string_buf *buf, sl_string *str){
  cord_buf_append_char_star(buf, str->str, str->len);
}
void string_buf_append_obj(sl_string_buf *buf, sl_obj obj){
  string_buf_append_str(buf,XSTRING(obj));
}
void string_buf_append_char(sl_string_buf *buf, uint32_t c){
  cord_buf_append(buf, c);
}
void string_buf_append_cstr(sl_string_buf *buf, const char *s){
  cord_buf_append_cord(buf, s);
}
void string_buf_append_cstrn(sl_string_buf *buf, const char *s, int len){
  cord_buf_append_char_star(buf, s, len);
}

//Again prepending should be use sparingly if at all
void string_buf_prepend_str(sl_string_buf *buf, sl_string *str){
  cord_buf_prepend_char_star(buf, str->str, str->len);
}
void string_buf_prepend_char(sl_string_buf *buf, uint32_t c){
  cord_buf_prepend(buf, c);
}
void string_buf_prepend_cstr(sl_string_buf *buf, const char *s){
  cord_buf_prepend_cord(buf, s);
}

void string_buf_unread_char(sl_string_buf *buf){
  cord_buf_unread_char(buf);
}
void string_buf_concat(sl_string_buf *buf1, sl_string_buf *buf2){
  sl_string *temp = string_buf_to_string(buf2);
  string_buf_append_str(buf1, temp);
}

sl_string *string_buf_to_string(sl_string_buf *buf){
  uint32_t len = cord_buf_len(buf);
  const char *str = cord_buf_to_char_star(buf);
  assert(len == strlen(str));
  sl_string *sl_str = sl_malloc(sizeof(sl_string));
  sl_str->str = str;
  sl_str->len = len;
  sl_str->type = SL_vector;
  sl_str->tag = SL_string;
  return sl_str;
}
const char *string_buf_to_cstr(sl_string_buf *buf, uint32_t *len){
  if(len){
    *len = cord_buf_len(buf);
  }
  return cord_buf_to_char_star(buf);
}
sl_obj string_buf_to_string_obj(sl_string_buf *buf){
  sl_string *str = string_buf_to_string(buf);
  return PACK_STRING(str);
}
