#ifndef _SL_C_MACROS_
#define _SL_C_MACROS_
/*
  Some generic macros
*/
#define CAT(x,y) x ## y
#define CAT3(x,y,z) x ## y ## z
#define _STRINGIFY(x) #x
#define STRINGIFY(x) _STRINGIFY(x)
#define SWAP(x,y)                               \
  ({__typeof(x) __temp = x;                     \
    x = y;                                      \
    y = __temp;})
#define ARR_SWAP(arr,i,j)                       \
  ({__typeof(arr[i]) __temp = arr[i];           \
    arr[i] = arr[j];                            \
    arr[j] = __temp;})
#define IS_POW_OF_2(num) (!(num & (num-1)))
//really nearest pow of 2 greater than num
#define NEAREST_POW_OF_2(num)                   \
  ({int leading_zeros = __builtin_clzl(num);    \
    (1UL << (64 - leading_zeros));})
//position of msb set, plus 1 if num isn't a power of 2
#define LOG_2_CEIL(num)                         \
  (__builtin_clzl(num) + (num & (num-1)))
#define LOG_2_FLOOR(num)                          \
  (__builtin_clzl(num))
#define MIN(_x,_y)                                \
  ({__typeof(_x) x = _x;                          \
    __typeof(_y) y = _y;                          \
    x<y ? x : 0;})
#define MAX(_x,_y)                                \
  ({__typeof(_x) x = _x;                          \
    __typeof(_y) y = _y;                          \
    x>y ? x : y;})
//generic and bitwise macros for dealing with signs
#define SIGN(x) ((x) < 0)
#define NEG(x) (-(x))
#define ABS(x) ((x) < 0 ? -(x) : (x))
#define SIGNBIT(x)                              \
  ({__typeof(x) tmp = x;                        \
    int shift = (sizeof(x) * CHAR_BIT)-1;       \
    (x & (1 << shift))>>shift;})
#define BITNEG(x) (~(x)+1)
#define BITABS(x) (SIGNBIT(x) ? BITNEG(x) : x)
#define set_bit(x,n) (x |= (1 << n))
#define unset_bit(x,n) (x &= (~(1 << n)))
#define check_bit(x,n) (x & (1 << n))
#define CLIP_INT(x,low,high) (x > high ? high : (x < low ? low : x))
#ifndef static_assert
#define static_assert(stmt, msg) _Static_assert(stmt, msg)
#endif
#define DOWNCASE_ASCII(c) (c > 0x40 && c < 0x5B ? c | 0x20 : c)
#define UPCASE_ASCII(c) (c > 0x60 && c < 0x7B ? c & (~0x20) : c)
#define DIGIT_TO_NUMBER(c) (c - 0x30)
//assign should be a 0 or a 1, so the if will get eliminated at compile time
#define FOREACH_ARRAY(arr, len, fn, assign)     \
  ({int i;                                      \
    for(i = 0; i < len; i++){                   \
      if(assign){                               \
        arr[i] = fn(arr[i]);                    \
      } else {                                  \
        fn(arr[i]);                             \
      }                                         \
    }                                           \
    arr;})
/*
  Compile time overloading via macros
*/
//overload w/upto 8 args, each overload can be uniquely named
//Usage, For a macro FOO with 3 variants, FOO3, FOO2 and FOO1 define as
//#define FOO(...) GET_MACRO(3, __VA_ARGS__, FOO3, FOO2, FOO1)(__VA_ARGS__)
//However unless there's a reason to name the variants uniquely, rather than
//above (i.e FOO1, FOO2, FOO3), it's better to use VFUNC
#define GET_MACR0_1(_1,NAME,...) NAME
#define GET_MACRO_2(_1,_2,NAME,...) NAME
#define GET_MACRO_3(_1,_2,_3,NAME,...) NAME
#define GET_MACRO_4(_1,_2,_3,_4,NAME,...) NAME
#define GET_MACRO_5(_1,_2,_3,_4,_5,NAME,...) NAME
#define GET_MACRO_6(_1,_2,_3,_4,_5,_6,NAME,...) NAME
#define GET_MACRO_7(_1,_2,_3,_4,_5,_6,_7,NAME,...) NAME
#define GET_MACRO_8(_1,_2,_3,_4,_5,_6,_7,_8,NAME,...) NAME
#define GET_MACRO(n,...) GET_MACRO_##n(__VA_ARGS__)
// overload w/upto 63 args, all overloads must be of the form
// base_n, where n is the number of args
//__NARG__ in effect, computes the number of arguments passed to it
#define __NARG__(...)  __NARG_I_(__VA_ARGS__,__RSEQ_N())
#define __NARG_I_(...) __ARG_N(__VA_ARGS__)
#define __ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, _9,_10,         \
                _11,_12,_13,_14,_15,_16,_17,_18,_19,_20,        \
                _21,_22,_23,_24,_25,_26,_27,_28,_29,_30,        \
                _31,_32,_33,_34,_35,_36,_37,_38,_39,_40,        \
                _41,_42,_43,_44,_45,_46,_47,_48,_49,_50,        \
                _51,_52,_53,_54,_55,_56,_57,_58,_59,_60,        \
                _61,_62,_63,N,...) N
#define __RSEQ_N() 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53,  \
                   52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42,  \
                   41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31,  \
                   30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20,  \
                   19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9,   \
                   8, 7, 6, 5, 4, 3, 2, 1, 0

#define _VFUNC_(name, n) name##n
#define _VFUNC(name, n) _VFUNC_(name, n)
// expands into func##n(__VA_ARGS__) where n is the number of arguments
#define VFUNC(func, ...) _VFUNC(func, __NARG__(__VA_ARGS__))(__VA_ARGS__)
/*
  Usage:
  say we want to merge the functions make_list1, make_list2, make_list3
  into one make_list_n function which is overloaded based on it's arguments.
  do the following:
  #define make_list_n(...) VFUNC(make_list_, __VA_ARGS__)

  this invokes a seperate funciton for each number of arguments, to implement
  optional arguments the same function needs to be implemented for each
  number of arguments.
  Say we wanted to implement an add function which passed 0 for any ommited
  paramater (we could do this the same as above, but ignore that).
  define the function:
  int add_base(int a,int b, int c){ //in lisp (add &optional a b c)
    return a+b+c;
  }
  #define add0() add_base(0,0,0)
  #define_add1(a) add_base(a,0,0)
  #define_add2(a,b) add_base(a,b,0)
  #define_add3(a,b,c) add_base(a,b,c)
  #define add(...) GET_MACRO_3(

*/
/*
  Some lisp specific macros

*/
#define raise_error_fmt(tag,fmt,args...)               \
  ({sl_string *str = sl_sprintf(fmt,0,##args);         \
    raise_error(tag, PACK_STRING(str));})
#define raise_error_cstr(tag,str)               \
  raise_error(tag, make_string_const(str))
//raises an error message useful for debugging
#ifndef NDEBUG
#define raise_error_temp()                      \
  raise_error_fmt(0,"Error in function %s, in file %s, at line %d",     \
                  __func__,__FILE__,__LINE__)
#else
#define raise_error_temp() raise_error(0,0)
#endif
#define TYPECHECK_1(type, val, err, msg)        \
  if(!(type##P(val))){                 \
    raise_error(err, msg);                      \
  }
#define TYPECHECK_1_TEMP(type, val)        \
  if(!(type##P(val))){                 \
    raise_error_temp();                      \
  }
#define TYPECHECK_1_CSTR(type, val, err, msg)   \
  if(!(type##P(val))){                          \
    raise_error(err, make_string_const(msg));   \
  }
#define TYPECHECK_2(type, val1, val2, err, msg) \
  if(!(type##P(val1)) || !(type##P(val2))){     \
    raise_error(err, msg);                      \
  }
#define TYPECHECK_2_TEMP(type, val1, val2)      \
  if(!(type##P(val1)) || !(type##P(val2))){     \
    raise_error_temp();                         \
  }
#define TYPECHECK_2_CSTR(type, val1, val2, err, msg)    \
  if(!(type##P(val1)) || !(type##P(val2))){             \
    raise_error(err, make_string_const(msg));           \
  }
#define TYPECHECK_3(type, val1, val2, val3, err, msg)                   \
  if(!(type##_check_upcase(val1)) || !(type##_check_upcase(val2)) ||    \
     !(type##_check_upcase(val3)))){                                    \
    raise_error(err, msg);                                              \
  }
#define TYPECHECK_3_CSTR(type, val1, val2, val3, err, msg)              \
  if(!(type##_check_upcase(val1)) || !(type##_check_upcase(val2)) ||    \
     !(type##_check_upcase(val3)))){                                    \
    raise_error(err, make_string_const(msg));                           \
  }
/*
  There really isn't any way to do compile time string manipulation in C
  so this is a really hacky workaround to be able to upcase type names,
  i.e so TYPECHECK_1(cons,...) will call CONSP.

  The better way, and what I'll probably do is to use a perl script
  to generate a header with all the various type related macros, including
  upcase stuff.
*/
#define int_check_upcase(obj) INTP(obj)
#define cons_check_upcase(obj) CONSP(obj)
#define number_check_upcase(obj) NUMBERP(obj)
#define misc_check_upcase(obj) MISCP(obj)
#define vector_check_upcase(obj) VECTORP(obj)
#define symbol_check_upcase(obj) SYMBOLP(obj)
#define string_check_upcase(obj) STRINGP(obj)
/*
  Macros for initializing/defining lisp objects.

  This code borrows heavily (read: is copied from) the implementation
  of functions/subroutines in emacs lisp
*/
#ifndef __cplusplus
//number of args, followed by pointer to array of args
#define DEFUN_ARGS_MANY		(uint64_t, sl_obj*)
//cons cell containing a list of the arguments, unevaulated
#define DEFUN_ARGS_UNEVALED	(sl_obj)
#define DEFUN_ARGS_0	(void)
#define DEFUN_ARGS_1	(sl_obj)
#define DEFUN_ARGS_2	(sl_obj, sl_obj)
#define DEFUN_ARGS_3	(sl_obj, sl_obj, sl_obj)
#define DEFUN_ARGS_4	(sl_obj, sl_obj, sl_obj, sl_obj)
#define DEFUN_ARGS_5	(sl_obj, sl_obj, sl_obj, sl_obj, sl_obj)
#define DEFUN_ARGS_6	(sl_obj, sl_obj, sl_obj, sl_obj, sl_obj, sl_obj)
#define DEFUN_ARGS_7	(sl_obj, sl_obj, sl_obj, sl_obj, \
                         sl_obj, sl_obj, sl_obj)
#define DEFUN_ARGS_8	(sl_obj, sl_obj, sl_obj, sl_obj, \
                         sl_obj, sl_obj, sl_obj, sl_obj)

#define DEFUN_ARGS_MANY_DUMMY		(uint64_t a, sl_obj* b)
//cons cell containing a list of the arguments, unevaulated
#define DEFUN_ARGS_UNEVALED_DUMMY	(sl_obj b)
#define DEFUN_ARGS_0_DUMMY	(void)
#define DEFUN_ARGS_1_DUMMY	(sl_obj a)
#define DEFUN_ARGS_2_DUMMY	(sl_obj a, sl_obj b)
#define DEFUN_ARGS_3_DUMMY	(sl_obj a, sl_obj b, sl_obj c)
#define DEFUN_ARGS_4_DUMMY	(sl_obj a, sl_obj b, sl_obj c, sl_obj d)
#define DEFUN_ARGS_5_DUMMY	(sl_obj a, sl_obj b, sl_obj c, sl_obj d,\
                                 sl_obj e)
#define DEFUN_ARGS_6_DUMMY	(sl_obj a, sl_obj b, sl_obj c, sl_obj d,\
                                 sl_obj e, sl_obj f)
#define DEFUN_ARGS_7_DUMMY	(sl_obj a, sl_obj b, sl_obj c, sl_obj d,\
                                 sl_obj e, sl_obj f, sl_obj g)
#define DEFUN_ARGS_8_DUMMY	(sl_obj a, sl_obj b, sl_obj c, sl_obj d,\
                                 sl_obj e, sl_obj f, sl_obj g, sl_obj h)

#define make_lit_subr(cname, lname, min_args, max_args, docstr)       \
  {.f##max_args = cname, .symbol_name = make_lit_string(lname),       \
   .minargs = min_args, .maxargs = max_args, .doc = docstr}

//val should be a string literal
#define make_lit_string(val)                                  \
  {.tag = SL_string, .ascii = 1,                              \
   .len = sizeof(val)-1, .str = val}
#define make_string_const(val)                  \
  ({sl_string *str = sl_malloc(sizeof(sl_string));      \
    *str = (sl_string)make_lit_string(val);             \
    PACK_STRING(str);})
//lname is assumed to be a c string literal
#define make_lit_symbol(lname, value, props)                    \
  {.val = value, .sym_props = props,                            \
   .name = make_lit_string(lname)}

//The internals of the subr structure need to be finalized
//but they shouldn't matter for the most part

//for now doc is just a string, I might do what emacs does and make
//doc a comment and extract it later
/**
   define a function that can be called from both C and lisp.
   @param 'lname' is the name of the lisp function as a string
   (though it doesn't need to be, because I can make it a string using #)
   @param 'cname' is the basis for the name of the function in C and the
   name of the variable contining the subr object.
   the function name is formed by prepennding F to cname and the
   variable name is formed by prepending S.
   @param 'minargs' a c integer containing the minimum number of arguments
   DEFUN_ARGS_N needs to be updated if this is more than 8.
   @param 'maxargs' a c integer specifing the maximum number of arguments, or
   else MANY for functions taking and &rest parameter or UNEVALED for
   macros/special forms.

   example usage:
   DEFUN("car", car, 1, 1, "Return the car of cell, if cell is nil return nil\n"
   "if cell is not a cons cell raise an error")
   (sl_obj cell) {
   if(CONSP(cell)){
   return XCAR(cell);
   } else if (NILP(cell)) {
   return NIL;
   } else {
   raise_error();
   }
   }
   For the sake of portability any initializatons using other variables should
   be done at run time rather that compile time.
*/
#define DEFUN(lname, cname, minargs, maxargs, doc, types...)    \
  sl_obj F##cname DEFUN_ARGS_ ## maxargs;                       \
  static sl_subr __S##cname =                                   \
    make_lit_subr(F##cname, lname, minargs, maxargs, doc);      \
  static sl_symbol __Q##cname =                                 \
    make_lit_symbol(lname, NIL, SL_SYMBOL_SPECIAL);             \
  sl_obj Q##cname;                                              \
  sl_obj F##cname
//just an idea for how to set the value of a symbol for a subr
//needs to be called in a function (i.e not top-level)
//optional arguments to allow extra information to be used by
//the symbol parser
#define INIT_SUBR(name,maxargs,...)                             \
  Q##name = (PACK_SYMBOL(&__Q##name));                          \
  XSYMBOL(Q##name)->val = make_misc(&(__S##name), SL_subr);     \
  XSYMBOL(Q##name)->hv = sl_hash_str(XSYMNAME(Q##name));

/**
    Define a global variable in lisp. The macro will be parsed to extract
    the symbol name, so Q##name will already be declared, so it shouldn't
    have a type.
*/
#define DEFVAR(lname, cname, default_val, _doc, init...)                \
  Q##cname = make_symbol(lname, sizeof(lname)-1,                        \
                         default_val, SL_SYMBOL_SPECIAL, _doc);         \
  init
#define DEFVAR_ALIAS(lname, cname, original)    \
  static sl_symbol __Q##cname = __Q##original;  \
  __Q##cname.name = make_lit_string(lname);     \
  sl_obj Q##cname = PACK_SYMBOL(&__Q##cname);
//dosen't expand to anything, to be used by symbol parser to write
//constant defination in symbol_list.h
//only works in headers, Qname shouldn't be predeclared, I don't know how
//I'll get it in the symbol table though
#define DEFCONST(lname, cname, default_val, doc)                        \
  static const sl_symbol __Q##cname = make_lit_symbol(lname, default_val, \
                                                      SL_SYMBOL_CONST); \
  static const sl_obj Q##cname = PACK_SYMBOL(&__Q##cname);
#define DEFCONST_C(name)
//works in init functions Qname needs to be predclared, the defined value
//won't be a constant in C, only lisp
#define DEFCONST_LISP(lname, cname, default_val, doc)                   \
  static sl_symbol __Q##cname = make_lit_symbol(lname, default_val,     \
                                                      SL_SYMBOL_CONST); \
  sl_obj Q##cname = PACK_SYMBOL(&__Q##cname);
//A keyword symbol is ultimately just a symbol that evaluates
//to itself, the important thing is that the c name is the normal
//c name for the lisp symbol (sans the :) followed my _key
#define DEFKEY(lname, cname)                                            \
  static sl_symbol __Q##cname##_key =                                   \
    make_lit_symbol(":"lname, NIL, SL_SYMBOL_KEYWORD);          \
  Q##cname##_key = PACK_SYMBOL(&__Q##cname##_key);                        \
  XSYMBOL(Q##cname##_key)->hv = sl_hash_str(XSYMNAME(Q##cname##_key));  \
  K##cname = Q##cname##_key;                                    \
  __Q##cname##_key.val = Q##cname##_key;

#endif /* not C++ */
#endif
