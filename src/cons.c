#include "cons.h"
//these 2 functions are for easily generating lisp lists in C
//without having to invoke the reader, essentially they translate
//va_arg lists into lisp lists, they're not super efficent

//make a lisp list given a NULL terminated list of sl_objs
//it is assumed that there are atleast 2 non null arguments
//use make_list1/make_list2 for lists of 1 or 2 elements
sl_obj make_list_va(sl_obj car,...){
  va_list ap;
  sl_obj arg;
  sl_obj retval = make_cons(car, NIL);
  sl_obj tail = retval;
  va_start(ap, car);
  arg = va_arg(ap, sl_obj);
  do {
    XCDR(tail) = make_cons(arg, NIL);
    tail = XCDR(tail);
  } while ((arg = va_arg(ap, sl_obj)) != NIL);
  va_end(ap);
  return retval;
}
//for convience, make a list of lisp integers given c integers
//again a minimum of 2 elements is required
sl_obj make_int_list(uint64_t car,...){
  va_list ap;
  uint64_t arg = car;
  sl_obj retval = make_cons(car, NIL);
  sl_obj tail = retval;
  va_start(ap, car);
  do {
    XCDR(tail)=make_cons(make_int(arg), NIL);
    tail = XCDR(tail);
  } while ((arg = va_arg(ap, uint64_t)) != NIL);
  va_end(ap);
  return retval;
}
sl_obj get_pointer_to_tail(sl_obj ls){
  sl_obj last = ls;
  while(CONSP(XCDR(last))){
    last = XCDR(last);
  }
  return last;
}
//If another make_X_list function is needed extract the core code
//out into a macro
/*sl_obj make_list1(sl_obj car){
  return make_cons(car, NIL);
}
sl_obj make_list2(sl_obj car, sl_obj caar){
  return make_cons(car, make_cons(caar, NIL));
}*/
DEFUN("cons",cons,2,2, "create a new cons cell with `car` and `cdr` as the\n"
      "car and cdr respectively")
  (sl_obj car, sl_obj cdr){
  return make_cons(car,cdr);
}
DEFUN("safe-car",safe_car,1,1,
      "return the car of the given cons cell, or nil if not given a cons cell")
  (sl_obj obj){
  if(CONSP(obj)){
    return XCAR(obj);
  }
  return NIL;
}
DEFUN("car",car,1,1, "return the car of the given cons cell")
  (sl_obj obj){
  if(CONSP(obj)){
    return XCAR(obj);
  } else if (NILP(obj)) {
    return obj;
  } else {
    raise_error_temp();
  }
}
DEFUN("safe-cdr",safe_cdr,1,1,
      "return the cdr of the given cons cell, or nil if not given a cons cell")
  (sl_obj obj){
  if(CONSP(obj)){
    return XCDR(obj);
  }
  return NIL;
}
DEFUN("cdr",cdr,1,1, "return the cdr of the given cons cell")
  (sl_obj obj){
  if(CONSP(obj)){
    return XCDR(obj);
  } else if (NILP(obj)){
    return obj;
  } else {
    raise_error_temp();
  }
}
DEFUN("list",list,0,MANY,"(list &rest args) return a list with the"
      "given argument as elements")
  (uint64_t numargs, sl_obj *args){
  /* build list in reverse, starting with NIL */
  sl_obj val = NIL;
  while(numargs-- > 0){
    val = Fcons(args[numargs], val);
  }
  return val;
}
DEFUN("list*",list_star,1, MANY,"(list* first &rest list) like list, except"
      "the last argument is the tail of the new list")
  (uint64_t numargs, sl_obj *args){
  if(numargs == 1){
    return args[0];
  }
  numargs -= 1;
  /* build list in reverse, starting with the last argument*/
  sl_obj list = args[numargs];
  while(numargs-- > 0){
    list = Fcons(args[numargs], list);
  }
  return list;
}
DEFUN("nth",nth,2,2, "(nth list n) return the nth value of the given list")
  (sl_obj obj, sl_obj n){
  sl_obj err;
  if(!LISTP(obj) || !FIXNUMP(n)){
    //err = type_err
    raise_error_temp();
  }
  if(!obj){return NIL;}
  //subtract 1 so we can work with a 0 based list
  int i = XFIXNUM(n);
  while(i > 0 && XCDR(obj)){
    obj = XCDR(obj);
    i--;
  }
  return (i==0 ? XCAR(obj) : NIL);
}
//return a copy of the first n elements of list
//if next is not NULL store a pointer to the n+1th element
//in next
sl_obj internal_head(sl_obj list, int n, sl_obj *next){
  sl_obj cur = make_cons(POP(list),NIL);
  sl_obj head = cur;
  while(--n>0 && list){
    XSETCDR(cur, make_cons(POP(list),NIL));
    cur = XCDR(cur);
  }
  if(next){*next = list;}
  return head;
}
DEFUN("head",head,2,2,"(head list n) return a copy of the first"
      "n elements of list")
  (sl_obj list, sl_obj n){
  if(!LISTP(list) || !FIXNUMP(n)){
    raise_error_temp();
  }
  if(!list){return NIL;}
  sl_obj retval = internal_head(list, XFIXNUM(n), NULL);
  return retval;
}
DEFUN("tail",tail,2,2,"(tail list n) return the last n elements of list"
      "the returned list shares structure with the original list")
  (sl_obj list,sl_obj n_elems){
  if(!LISTP(list) || !FIXNUMP(n_elems)){
    raise_error_temp();
  }
  if(!list){return NIL;}
  int len = CONS_LENGTH(list);
  int n = XFIXNUM(n_elems);
  if(len > n){//if len <=n we just return the list
    n = len - n;
    while(n--){
      list = XCDR(list);
    }
  }
  return list;
}

DEFUN("split",split,2,2, "(split lst len) split list into two sublists where"
      "the first list is of length len and the second list is the remanining"
      "elements")
  (sl_obj list, sl_obj n){
  sl_obj head,tail;
  if(!LISTP(list) || !FIXNUMP(n)){
    raise_error_temp();
  }
  if(!list){
    return make_cons(NIL,NIL);
  }
  head = internal_head(list, XFIXNUM(n), &tail);
  return make_cons(head,tail);
}

static sl_cons *reverse_acc(sl_cons *new_list, sl_cons *original_list){
  if(!original_list){
    return new_list;
  } else {
    new_list->cdr = make_cons(original_list->car, NIL);
    if(!(CONSP(original_list->cdr))){
      //needed to deal with improper lists
      new_list->cdr = original_list->cdr;
      return new_list;
    }
    reverse_acc(XCONS(new_list->cdr), XCONS(original_list->cdr));
  }
  SL_UNREACHABLE;
}
//reverse is a sequence function
sl_obj reverse_cons(sl_obj obj){
  sl_obj retval = make_cons(XCAR(obj), NIL);
  if(!CONSP(XCDR(obj))){
    XCDR(retval) = XCDR(obj);
    return retval;
  }
  reverse_acc(XCONS(retval), XCONS(XCDR(obj)));
  return retval;
}
sl_obj nreverse_cons(sl_obj ls){
  sl_obj sl = make_cons(Fcar(ls), NIL);
  ls = Fcdr(ls);
  while(CONSP(ls)){
    sl_obj temp = XCDR(ls);
    XCDR(ls) = sl;
    sl = ls;
    ls = temp;
  }
  return sl;
}

DEFUN("pop", pop, 1, UNEVALED, "(pop ls) set ls to (cdr ls) "
      "and return (car ls)")
  (sl_obj ls){
  //we can be sure ls is a list
  if(XCDR(ls) != NIL){//too many args
    raise_error_temp();
  }
  sl_obj sym = XCAR(ls);
  sl_obj val = XSYMVAL(sym);
  if(!LISTP(val)){
    raise_error_temp();
  }
  sl_obj ret = Fcar(val);
  XSYMVAL(sym) = Fcdr(val);
  return ret;
}
DEFUN("push", push, 2, UNEVALED, "(push elt place) set place to "
      "(cons elt place)")
  (sl_obj args){
  if(XCDDR(args) != NIL){
    raise_error_temp();
  }
  sl_obj elt = XCAR(args);
  sl_obj place = XCADR(args);

  XSYMVAL(place) = make_cons(elt, XSYMVAL(place));
  return XSYMVAL(place);
}

DEFUN("append", append, 0, MANY, "(append &rest args) return new list created"
      "from concatenating args, all but the last argument must be lists")
  (uint64_t numargs, sl_obj *args){
  if(numargs == 0){
    return NIL;
  } else if (numargs == 1){
    return args[0];
  }
  //retval is what we return, tail points to the current end of the list
  sl_obj retval, tail, list;
  int i = 0;
  list = args[0];
  //Fcar will check list
  retval = make_cons(Fcar(list), NIL);
  tail = retval;
  list = Fcdr(list);
  while(i < numargs - 1){
    if(!LISTP(list) && i > 0){
      raise_error_temp();
    }
    while(CONSP(list)){
      sl_obj val = POP(list);
      XCDR(tail) = make_cons(val, NIL);
      tail = XCDR(tail);
    }
    //include the last element iff it's non nil so that
    //(append (1 . 2) 3) (append (1 2) 3) = (1 2 3)
    if(!NILP(list)){
      XCDR(tail) = make_cons(list, NIL);
      tail = XCDR(tail);
    }
    list = args[++i];
  }
  if(!CONSP(list)){
    XCDR(tail) = list;
  } else {
    while(CONSP(list)){
      sl_obj val = POP(list);
      XCDR(tail) = make_cons(val, NIL);
      tail = XCDR(tail);
    }
  }
  return retval;
}
DEFUN("nappend", nappend, 0, MANY, "(append &rest args) return list created"
      "from concatenating args, all but the last argument must be lists, "
      "and all but the last argument may be modified")
  (uint64_t numargs, sl_obj *args){
  if(numargs == 0){
    return NIL;
  } else if (numargs == 1){
    return args[0];
  }
  //retval is what we return, tail points to the current end of the list
  sl_obj retval = args[0];
  sl_obj tail = retval;
  int i = 0;
  //since nil is a list but XCDR only works on conses
  while(tail == NIL){
    tail = make_cons(tail, NIL);
    tail = args[++i];
  }
  while(i < numargs - 1){
    if(!LISTP(tail)){
      raise_error_temp();
    }
    while(CONSP(XCDR(tail))){
      tail = XCDR(tail);
    }
    XCDR(tail) = args[++i];
    tail = XCDR(tail);
  }
  //XCDR(tail) is already args[numargs-1] so we're done
  return retval;
}
//For now limit to creating lists of positive (fixed size) integers,
//later add the ability to make vectors, negitive integers, floats, etc...
DEFUN("iota",iota,1,3,"(iota start &optional stop (step 1))\n"
      "With one argument 'n' create a list with integer elements from "
      "1 to n inclusive. With 2-3 arguments create a list with elements "
      "starting at 'start' incrementing by step untill the next element "
      "would be larger than stop")
  (sl_obj start_obj, sl_obj stop_obj, sl_obj step_obj){
  if(!INTP(start_obj)){
    raise_error_temp();
  }
  int start = 1, stop = 0, step = 1;
  if(NILP(stop_obj)){
    stop = XINT(start_obj);
  } else {
    if(INTP(step_obj)){
      step = XINT(step_obj);
    } else if(!NILP(step_obj)){
      raise_error_temp();
    }
    if(!INTP(stop_obj)){
      raise_error_temp();
    }
    stop = XINT(stop_obj);
    start = XINT(start_obj);
  }
  if(SL_UNLIKELY(stop < start)){
    return NIL;
  }
  sl_obj retval = make_cons(make_int(start), NIL);
  sl_obj tail = retval;
  int i = start + step;
  while(i <= stop){
    XCDR(tail) = make_cons(make_int(i), NIL);
    tail = XCDR(tail);
    i+=step;
  }
  return retval;
}
//make a copy of ls formed by appling the function f to each element
//of ls in turn, f must be able to take 1 argument, this is not checked
//ls must be a list with at least 1 element, this is also not checked
sl_obj mapcar_internal(sl_obj ls, sl_obj f){
  sl_obj tail = make_cons(NIL,NIL);
  sl_obj ret = tail;
  sl_obj elem;
  while(1){
    elem = POP(ls);
    XSETCAR(tail, XSUBR(f)->f1(Feval(elem)));
    if(!CONSP(ls)){
      return ret;
    } else {
      XCDR(tail) =  make_cons(tail, NIL);
      tail = XCDR(tail);
    }
  }
}
/*
  TODO: implement support for mapcar with multiple lists.
*/
DEFUN("mapcar",mapcar,2,MANY,"(mapcar function list &rest lists),"
      "return a list created by applying function to successive cars of\n"
      "the given lists.")
  (uint64_t numargs, sl_obj *args){
  sl_obj subr = args[0];
  if(!SUBRP(subr)){
    raise_error_temp();
  }
  sl_obj ls = args[1];
  if(!CONSP(ls)){
    if(NILP(ls)){//nil is a valid list
      return NIL;
    } else {
      raise_error_temp();
    }
  }
  //I could allocate the entire list here, but there are various reasons not to
  if(XSUBR(subr)->maxargs > 0){
    sl_obj retval = make_cons(NIL, NIL);
    sl_obj tail = retval;
    while(1){
      XCAR(tail) = XSUBR(subr)->f1(POP(ls));
      if(!CONSP(ls)){
        break;
      } else {
        XCDR(tail) = make_cons(NIL, NIL);
      }
    }
    return retval;
  } else if(XSUBR(subr)->maxargs == MANY){
    sl_obj *arg_vec = sl_malloc(numargs - 1);
    sl_obj retval = make_cons(NIL, NIL);
    sl_obj tail = retval;
    while(1){
      arg_vec[0] = POP(ls);
      XCAR(tail) = XSUBR(subr)->fMANY(numargs-1, arg_vec);
      if(!CONSP(ls)){
        break;
      } else {
        XCDR(tail) = make_cons(NIL, NIL);
      }
    }
    return retval;
    //TODO: Support for lambdas
  } else {
    raise_error_temp();
  }
}
/*
  TODO:
  mapcdr, mapcar but with cdr's instead of cars
  mapcan, mapcar, but concatenate results
  mapcon, mapcdr, but concatenate results
  mapc, mapcar, but only for side effects
  mapl, mapcdr, but only for side effects
*/
/*
  TODO: Add support for functions which take MANY arguments
*/
sl_obj reduce_cons(sl_obj fn, sl_obj ls, sl_obj start){
  sl_obj(*fp)(sl_obj,sl_obj) = XSUBR(fn)->f2;
  sl_obj acc = NILP(start) ? POP(ls) : start;
  sl_obj next;
  while(CONSP(ls)){
    next = POP(ls);
    acc = fp(acc, next);
  }
  return acc;
}
/*
  Find 1st elemnt in ls which satisifies the predicate test
*/
sl_obj find_if_cons(sl_obj test, sl_obj ls){
  sl_obj(*fp)(sl_obj) = XSUBR(test)->f1;
  while(CONSP(ls)){
    sl_obj l = POP(ls);
    if(!NILP(fp(l))){
      return l;
    }
  }
  return NIL;
}
/*
  Find 1st element in ls which is 'equal' to obj, equality
  is determined by test, which is a function of 2 arguments.
*/
sl_obj find_cons(sl_obj ls, sl_obj obj, sl_obj test){
  sl_obj(*fp)(sl_obj, sl_obj) = XSUBR(test)->f2;
  while(CONSP(ls)){
    sl_obj l = POP(ls);
    if(!NILP(fp(l, obj))){
      return l;
    }
  }
  return NIL;
}
  
DEFUN("quasiquote",quasiquote,1,1,"Function implementing the '`' mechanic")
  (sl_obj list){
/*
    This is going to be slow, but it should work. I create a new list, I'm
    basically reimplementing append (I should at least figure out how to do,
    this with a call to append), and I have to treat the head of the list
    specially, but it does work.
  */
  sl_obj retval = NIL;
  sl_obj tail = NIL;
  /*
    This treats the head of the list specially, I'm not sure if there's
    a way to avoid this
   */
  if(CONSP(XCAR(list))){
    if(EQ(XCAAR(list), Kbackquote_splice)){
      retval = Feval(XCDAR(list));
      if(!CONSP(retval)){
        if(CONSP(XCDR(list))){
          raise_error_temp();
        } else {
          return retval;
        }
      }
      tail = get_pointer_to_tail(retval);
    } else if(EQ(XCAAR(list), Kbackquote_unquote)){
      retval = make_cons(Feval(XCDAR(list)), NIL);
      tail = retval;
    }
  } else {
    retval = make_cons(XCAR(list), NIL);
    tail = retval;
  }
  list = XCDR(list);
  while(CONSP(list)){
    if(CONSP(XCAR(list))){
      if(EQ(XCAAR(list), Kbackquote_splice)){
        //        DEBUG_PRINTF("Processing backquote splice\n");
        sl_obj to_splice = Feval(XCDAR(list));
        //        DEBUG_PRINTF("Splicing in %s\n",c_sprint(to_splice));
        sl_obj next = XCDR(list);
        XCDR(tail) = to_splice;
        if(!CONSP(to_splice)){
          if(CONSP(XCDR(list))){
            raise_error_temp();
          } else {
            return retval;
          }
        }
        tail = get_pointer_to_tail(tail);
      } else if(EQ(XCAAR(list), Kbackquote_unquote)){
        sl_obj to_insert = Feval(XCADR(list));
        XCDR(tail) = make_cons(to_insert, NIL);
        tail = XCDR(tail);
      }
    } else {
      XCDR(tail) = make_cons(XCAR(list), NIL);
      tail = XCDR(tail);
    }
    list = XCDR(list);
  }
  XCDR(tail) = list;
  //  DEBUG_PRINTF("Returning '%s\n",c_sprint(retval));
  return make_list2(Qquote, retval);
}

/*
  return the first cons cell in ls with a car/cdr equal to key,
  the function pred is used as the equality predicate, while
  get is used to specify if car or cdr should be used

  This the the closest I can get to templates in C
 */
#define assoc_templ(key, ls, get, pred)                         \
  if(!CONSP(ls)){                                               \
    if(NILP(ls)){                                               \
      return NIL;                                               \
    } else {                                                    \
      raise_error_temp();                                       \
    }                                                           \
  }                                                             \
  sl_obj head = ls;                                             \
  do {                                                          \
    /*Fcar is used to check that the car of head is a list*/    \
    if(pred(get(XCAR(head)), key) != NIL){                      \
      return XCAR(head);                                        \
    }                                                           \
  } while((head = XCDR(head)) != NIL);                          \
  return NIL;

DEFUN("assoc",assoc,2,2,"(assoc key list) returns the first cons cell in list "
      "who's car is equal to key, or nil if none are found")
  (sl_obj key, sl_obj ls){
  assoc_templ(key, ls, Fcar, Fequal);
}
DEFUN("rassoc",rassoc,2,2,"(rassoc key list) returns the first cons cell in "
      "list who's cdr is equal to key, or nil if none are found")
  (sl_obj key, sl_obj ls){
  assoc_templ(key, ls, Fcar, Fequal);
}
DEFUN("assq",assq,2,2,"(assq key list) returns the first cons cell in list "
      "who's car is eq to key, or nil if none are found")
  (sl_obj key, sl_obj ls){
  assoc_templ(key, ls, Fcar, Feq);
}
DEFUN("rassq",rassq,2,2,"(rassq key list) returns the first cons cell in "
      "list who's cdr is eq to key, or nil if none are found")
  (sl_obj key, sl_obj ls){
  assoc_templ(key, ls, Fcdr, Feq);
}

DEFUN("caar",caar,1,1,"return the caar of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcar(obj));
}
DEFUN("cadr",cadr,1,1,"return the cadr of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcdr(obj));
}
DEFUN("cdar",cdar,1,1,"return the cdar of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcar(obj));
}
DEFUN("cddr",cddr,1,1,"return the cddr of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcdr(obj));
}
DEFUN("caaar",caaar,1,1,"return the caaar of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcar(Fcar(obj)));
}
DEFUN("caadr",caadr,1,1,"return the caadr of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcar(Fcdr(obj)));
}
DEFUN("cadar",cadar,1,1,"return the cadar of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcdr(Fcar(obj)));
}
DEFUN("caddr",caddr,1,1,"return the caddr of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcdr(Fcdr(obj)));
}
DEFUN("cdaar",cdaar,1,1,"return the cdaar of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcar(Fcar(obj)));
}
DEFUN("cdadr",cdadr,1,1,"return the cdadr of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcar(Fcdr(obj)));
}
DEFUN("cddar",cddar,1,1,"return the cddar of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcdr(Fcar(obj)));
}
DEFUN("cdddr",cdddr,1,1,"return the cdddr of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcdr(Fcdr(obj)));
}
DEFUN("caaaar",caaaar,1,1,"return the caaaar of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcar(Fcar(Fcar(obj))));
}
DEFUN("caaadr",caaadr,1,1,"return the caaadr of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcar(Fcar(Fcdr(obj))));
}
DEFUN("caadar",caadar,1,1,"return the caadar of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcar(Fcdr(Fcar(obj))));
}
DEFUN("caaddr",caaddr,1,1,"return the caaddr of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcar(Fcdr(Fcdr(obj))));
}
DEFUN("cadaar",cadaar,1,1,"return the cadaar of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcdr(Fcar(Fcar(obj))));
}
DEFUN("cadadr",cadadr,1,1,"return the cadadr of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcdr(Fcar(Fcdr(obj))));
}
DEFUN("caddar",caddar,1,1,"return the caddar of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcdr(Fcdr(Fcar(obj))));
}
DEFUN("cadddr",cadddr,1,1,"return the cadddr of the given cons cell")
  (sl_obj obj){
  return Fcar(Fcdr(Fcdr(Fcdr(obj))));
}
DEFUN("cdaaar",cdaaar,1,1,"return the cdaaar of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcar(Fcar(Fcar(obj))));
}
DEFUN("cdaadr",cdaadr,1,1,"return the cdaadr of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcar(Fcar(Fcdr(obj))));
}
DEFUN("cdadar",cdadar,1,1,"return the cdadar of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcar(Fcdr(Fcar(obj))));
}
DEFUN("cdaddr",cdaddr,1,1,"return the cdaddr of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcar(Fcdr(Fcdr(obj))));
}
DEFUN("cddaar",cddaar,1,1,"return the cddaar of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcdr(Fcar(Fcar(obj))));
}
DEFUN("cddadr",cddadr,1,1,"return the cddadr of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcdr(Fcar(Fcdr(obj))));
}
DEFUN("cdddar",cdddar,1,1,"return the cdddar of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcdr(Fcdr(Fcar(obj))));
}
DEFUN("cddddr",cddddr,1,1,"return the cddddr of the given cons cell")
  (sl_obj obj){
  return Fcdr(Fcdr(Fcdr(Fcdr(obj))));
}
void init_cons_symbols(void){
  INIT_SUBR(cons, 2);
  INIT_SUBR(append, many);
  INIT_SUBR(nappend, many);
  INIT_SUBR(nth, 2);
  INIT_SUBR(head, 2);
  INIT_SUBR(tail, 2);
  INIT_SUBR(split, 2);
  INIT_SUBR(assoc, 2);
  INIT_SUBR(rassoc, 2);
  INIT_SUBR(assq, 2);
  INIT_SUBR(rassq, 2);
  INIT_SUBR(car, 1);
  INIT_SUBR(cdr, 1);
  INIT_SUBR(safe_car, 1);
  INIT_SUBR(safe_cdr, 1);
  INIT_SUBR(pop, 1);
  INIT_SUBR(push, UNEVALED);
  INIT_SUBR(list, MANY);
  INIT_SUBR(list_star, MANY);

  INIT_SUBR(caar, 1);
  INIT_SUBR(cadr, 1);
  INIT_SUBR(cdar, 1);
  INIT_SUBR(cddr, 1);
  INIT_SUBR(caaar, 1);
  INIT_SUBR(caadr, 1);
  INIT_SUBR(cadar, 1);
  INIT_SUBR(caddr, 1);
  INIT_SUBR(cdaar, 1);
  INIT_SUBR(cdadr, 1);
  INIT_SUBR(cddar, 1);
  INIT_SUBR(cdddr, 1);
  INIT_SUBR(caaaar, 1);
  INIT_SUBR(caaadr, 1);
  INIT_SUBR(caadar, 1);
  INIT_SUBR(caaddr, 1);
  INIT_SUBR(cadaar, 1);
  INIT_SUBR(cadadr, 1);
  INIT_SUBR(caddar, 1);
  INIT_SUBR(cadddr, 1);
  INIT_SUBR(cdaaar, 1);
  INIT_SUBR(cdaadr, 1);
  INIT_SUBR(cdadar, 1);
  INIT_SUBR(cdaddr, 1);
  INIT_SUBR(cddaar, 1);
  INIT_SUBR(cddadr, 1);
  INIT_SUBR(cdddar, 1);
  INIT_SUBR(cddddr, 1);
  INIT_SUBR(iota, 3);
}
