#ifndef _SL_STRUCT_H_
#define _SL_STRUCT_H_
#include "common.h"
/*
  ultimately a structure is just an associtave array, a mapping between
  names and values. The names are represented by symbols (probably keywords),
  while the values are generic sl_objs.

  We use slot instead of field, since it's more lispy.

  Ultimately objects (actual oop objects) should be basically the same as a struct.
*/
struct sl_struct {
  int64_t num_slots;
  sl_obj *slot_names;
  sl_obj *slots;
};

#endif
