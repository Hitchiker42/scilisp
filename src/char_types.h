#ifndef _SL_CHAR_TYPES_H_
#define _SL_CHAR_TYPES_H_
#define MAX_UNICODE_CODEPOINT 0x10FFFF
enum SL_char_prop {
  SL_symbol_char = 1,
  SL_digit_char = (SL_symbol_char << 1),
  SL_xdigit_char = (SL_digit_char << 1),
  SL_whitespace_char = (SL_xdigit_char << 1),
  SL_utf8_lead_char = (SL_whitespace_char << 1),
  SL_utf8_cont_char = (SL_utf8_lead_char << 1),
  SL_delim_char = (SL_utf8_cont_char << 1),
  SL_invalid_char = (SL_delim_char << 1),
  SL_ascii_symbol_char = (SL_symbol_char | SL_utf8_lead_char),
  SL_space_char = (SL_whitespace_char | SL_utf8_lead_char | SL_delim_char),
  SL_number_char = (SL_ascii_symbol_char | SL_digit_char | SL_xdigit_char),
};
static const uint32_t SL_char_props[256] =
  { ['\0'] = SL_delim_char, //so c strings can be read correctly
    ['a'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['b'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['c'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['d'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['e'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['f'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['g'] = SL_ascii_symbol_char,
    ['h'] = SL_ascii_symbol_char,
    ['i'] = SL_ascii_symbol_char,
    ['j'] = SL_ascii_symbol_char,
    ['k'] = SL_ascii_symbol_char,
    ['l'] = SL_ascii_symbol_char,
    ['m'] = SL_ascii_symbol_char,
    ['n'] = SL_ascii_symbol_char,
    ['o'] = SL_ascii_symbol_char,
    ['p'] = SL_ascii_symbol_char,
    ['q'] = SL_ascii_symbol_char,
    ['r'] = SL_ascii_symbol_char,
    ['s'] = SL_ascii_symbol_char,
    ['t'] = SL_ascii_symbol_char,
    ['u'] = SL_ascii_symbol_char,
    ['v'] = SL_ascii_symbol_char,
    ['w'] = SL_ascii_symbol_char,
    ['x'] = SL_ascii_symbol_char,
    ['y'] = SL_ascii_symbol_char,
    ['z'] = SL_ascii_symbol_char,
    ['A'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['B'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['C'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['D'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['E'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['F'] = SL_ascii_symbol_char | SL_xdigit_char,
    ['G'] = SL_ascii_symbol_char,
    ['H'] = SL_ascii_symbol_char,
    ['I'] = SL_ascii_symbol_char,
    ['J'] = SL_ascii_symbol_char,
    ['K'] = SL_ascii_symbol_char,
    ['L'] = SL_ascii_symbol_char,
    ['M'] = SL_ascii_symbol_char,
    ['N'] = SL_ascii_symbol_char,
    ['O'] = SL_ascii_symbol_char,
    ['P'] = SL_ascii_symbol_char,
    ['Q'] = SL_ascii_symbol_char,
    ['R'] = SL_ascii_symbol_char,
    ['S'] = SL_ascii_symbol_char,
    ['T'] = SL_ascii_symbol_char,
    ['U'] = SL_ascii_symbol_char,
    ['V'] = SL_ascii_symbol_char,
    ['W'] = SL_ascii_symbol_char,
    ['X'] = SL_ascii_symbol_char,
    ['Y'] = SL_ascii_symbol_char,
    ['Z'] = SL_ascii_symbol_char,
    ['0'] = SL_number_char,
    ['1'] = SL_number_char,
    ['2'] = SL_number_char,
    ['3'] = SL_number_char,
    ['4'] = SL_number_char,
    ['5'] = SL_number_char,
    ['6'] = SL_number_char,
    ['7'] = SL_number_char,
    ['8'] = SL_number_char,
    ['9'] = SL_number_char,
    ['.'] = SL_ascii_symbol_char,
    ['!'] = SL_ascii_symbol_char,
    ['?'] = SL_ascii_symbol_char,
    ['%'] = SL_ascii_symbol_char,
    ['&'] = SL_ascii_symbol_char,
    ['*'] = SL_ascii_symbol_char,
    ['+'] = SL_ascii_symbol_char,
    ['-'] = SL_ascii_symbol_char,
    ['/'] = SL_ascii_symbol_char,
    ['<'] = SL_ascii_symbol_char,
    ['='] = SL_ascii_symbol_char,
    ['>'] = SL_ascii_symbol_char,
    ['^'] = SL_ascii_symbol_char,
    ['_'] = SL_ascii_symbol_char,
    ['~'] = SL_ascii_symbol_char,
    [' '] = SL_space_char,
    ['\t'] = SL_space_char,
    ['\n'] = SL_space_char,
    ['"'] = SL_delim_char | SL_utf8_lead_char,
    ['\''] = SL_delim_char | SL_utf8_lead_char,
    ['('] = SL_delim_char | SL_utf8_lead_char,
    [')'] = SL_delim_char | SL_utf8_lead_char,
    ['['] = SL_delim_char | SL_utf8_lead_char,
    [']'] = SL_delim_char | SL_utf8_lead_char,
    ['{'] = SL_delim_char | SL_utf8_lead_char,//curly braces aren't currently used
    ['}'] = SL_delim_char | SL_utf8_lead_char,
    ['|'] = SL_delim_char | SL_utf8_lead_char,//nor are pipes 
    [':'] = SL_delim_char | SL_utf8_lead_char,
    [128 ... 191] = SL_utf8_cont_char | SL_symbol_char,
    [192 ... 253] = SL_utf8_lead_char | SL_symbol_char,
    [254] = SL_invalid_char,
    /*EOF is a perfectly valid delimiter, it's just not valid if actually read*/
    [255] = SL_invalid_char | SL_delim_char
  };
//TODO: name these consistantly (either with/witout an '_' after is)
SL_INLINE int sl_is_ascii(sl_char c){
  return c <= 127;//hopefully gcc turns this into !(c & 0xc0)
}
SL_INLINE int sl_is_digit(sl_char c){
  return SL_char_props[c] & SL_digit_char;
}
SL_INLINE int sl_is_xdigit(sl_char c){
  return SL_char_props[c] & SL_xdigit_char;
}
SL_INLINE int sl_is_delim(sl_char c){
    return SL_char_props[c] & SL_delim_char;
}
SL_INLINE int sl_is_symbol_char(sl_char c){
    return SL_char_props[c] & SL_symbol_char;
}
SL_INLINE int sl_is_space(sl_char c){
  return SL_char_props[c] & SL_whitespace_char;
}
#endif
