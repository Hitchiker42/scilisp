#include "common.h"
#include "cons.h"
#include "vector.h"
#include "sequence.h"
#include "sl_math.h"
sl_obj integer_equal(sl_obj x, sl_obj y){
//I'd normally used a dispatch table for this, but there are only
//4 different possibilities
  if(INTP(x)){
    if(INTP(y)){
      return SL_BOOLEAN(x == y);
    } else {
      return int_bigint_eq(x,y);
    }
  } else {
    if(INTP(y)){
      return bigint_int_eq(x,y);
    } else {
      return bigint_bigint_eq(x,y);
    }
  }
}
/*
  predicate functions, i.e type testing, equality, comparisions,etc...
*/
DEFUN("eq",eq, 2, 2, "return t if the two arguments are identical, two inegers"
      " or two characters may or may not be eq")
  (sl_obj obj1, sl_obj obj2){
//we don't even need to bother extracting the objects
  return SL_BOOLEAN(obj1==obj2);
}
DEFUN("eqv",eqv, 2, 2, "return t if the two arguments are identical or are"
      " numbers/characters or with the same type and value")
  (sl_obj obj1, sl_obj obj2){
//  if(NUMBERP(obj1) && NUMBERP(obj2)){
  if(INTEGERP(obj1) && INTEGERP(obj2)){
    return integer_equal(obj1,obj2);
  } else {
    return SL_BOOLEAN(obj1 == obj2);
  }
}
DEFUN("equal",equal, 2, 2, "return t if the two arguments are equal\n"
      "two lisp objects are equal if they have the same type and have"
      "equal contents.\n Conses are compared by comparing the car and cdr\n"
      "Vectors/arrays/strings are compared element by element\n"
      "Numbers are equal if they are both integral or both floating point\n"
      "types and are numerically equal.\n(use = to compare numbers solely"
      "based on numerical values, i.e integers can equal floats.")
  (sl_obj obj1, sl_obj obj2){
  if(obj1 == obj2){return SL_T;}//needed for symbols/nil, also makes equality
  //comparisons for identical values as fast as eq
  if(NUMBERP(obj1)){
    if(!NUMBERP(obj2)){return NIL;}
    if(INTEGERP(obj1) && INTEGERP(obj2)){
      return integer_equal(obj1,obj2);
    }
    if(XNUMBER_TYPE(obj1) == XNUMBER_TYPE(obj2)){
      //I might change this for bigfloats later
      return SL_BOOLEAN(XNUMBER(obj1)->val == XNUMBER(obj2)->val);
    }
    return NIL;
  }
  //check type bits after checking for numbers
  //because numbers can be equal even with different type bits
  if(sl_type_bits(obj1) != sl_type_bits(obj2)){
    return NIL;
  }
  if(CHARP(obj1)){
      return SL_BOOLEAN(XCHAR(obj1) == XCHAR(obj2));
  }
  //recursively compare conses
  if(CONSP(obj1)){
    return Fequal(XCAR(obj1),XCAR(obj2)) &&
      Fequal(XCDR(obj1), XCDR(obj2));
  }
  if(VECTORP(obj1)){
    //equal only compare svectors and strings, use equalp for arrays
    //also while a string is really just a vector of characters
    //it has a distinct type so I'm not sure if vectors of characters
    //should be equal to equivlent strings
    if ((STRINGP(obj1) && STRINGP(obj2)) ||
        (SVECTORP(obj1) && SVECTORP(obj2))) {
      if (VECTOR_LEN(obj1) != VECTOR_LEN(obj2)) {
        return NIL;
      }
      if (STRINGP(obj1)) {
        int i;
        for(i=0;i<VECTOR_LEN(obj1);i++){
          if(STR_REF(obj1, i) != STR_REF(obj2, i)){
            return NIL;
          }
        }
        return SL_T;
      } else {
        int i;
        for(i=0;i<VECTOR_LEN(obj1);i++){
          if(!Fequal(VREF(obj1,i),VREF(obj2,i))){
            return NIL;
          }
        }
        return SL_T;
      }
    }
    return NIL;
  }
  return NIL;
}
DEFUN("equalp",equalp,2,2,"return t if two objects have the same structure and"
      "contents, or are numerically equal.\nTwo objects are equalp if: "
      "they are equal; are arrays of the same dimensions with equalp elements;"
      "\nare conses with equalp cars and cdrs; are vectors or strings with"
      "equalp elements;\nOr are numerically equal regardless of type")
  (sl_obj obj1, sl_obj obj2){
  if(SEQUENCEP(obj1) && SEQUENCEP(obj2)){
    if(Flength(obj1) == Flength(obj2)){
      uint32_t n = XINT(Flength(obj1)), i;
      for(i=0;i<n;i++){
        if(!Fequalp(Felt(obj1, PACK_INT(i)),Felt(obj2, PACK_INT(i)))){
          return NIL;
        }
      }
      return SL_T;
    }
    return NIL;
  }
  return Fequal(obj1,obj2);
}
DEFUN("cons?",consp,1,1,"return t if object is a cons cell")
  (sl_obj obj){
  return SL_BOOLEAN(CONSP(obj));
}
DEFUN("array?",arrayp,1,1,"return t if object is an array, string or vector")
  (sl_obj obj){
  return SL_BOOLEAN(VECTORP(obj));
}
DEFUN("vector?",vectorp,1,1,"return t if object is a string or vector")
  (sl_obj obj){
  return SL_BOOLEAN(SVECTOR_OR_STRINGP(obj));
}
DEFUN("svector?",svectorp,1,1,"return t if object is a vector")
  (sl_obj obj){
  return SL_BOOLEAN(SVECTORP(obj));
}
DEFUN("integer?",integerp,1,1,"return t if object is a finite precision integer")
  (sl_obj obj){
  return SL_BOOLEAN(INTEGERP(obj));
}
DEFUN("float?",floatp,1,1,"return t if object is a floating point number")
  (sl_obj obj){
  return SL_BOOLEAN(FLOATP(obj));
}
DEFUN("symbol?",symbolp,1,1,"return t if obj is a symbol")
  (sl_obj obj){
  return SL_BOOLEAN(SYMBOLP(obj));
}
DEFUN("defined?",definedp,1,1,"return t if symbol is defined")
  (sl_obj obj){
  if(!SYMBOLP(obj)){
    raise_error_temp();
  }
  return !(EQ(XSYMVAL(obj), SL_UNDEF));
}
DEFUN("zero?", zerop, 1, 1, "(zero? arg), return t if arg is '=' to 0")
  (sl_obj arg){
  return sl_num_eq(sl_0, arg);
}
/*
  For even? and odd? define a function in sl_math.c to determine if a number
  is even, then use that.
*/ 
DEFUN("not",not,1,1,"(not arg), if arg is false return t, otherwise return nil")
  (sl_obj arg){
  return SL_BOOLEAN(FALSEP(arg));
}
void init_predicate_symbols(void){
  INIT_SUBR(consp, 1);
  INIT_SUBR(arrayp, 1);
  INIT_SUBR(vectorp, 1);
  INIT_SUBR(svectorp, 1);
  INIT_SUBR(integerp, 1);
  INIT_SUBR(floatp, 1);
  INIT_SUBR(symbolp, 1);
  INIT_SUBR(definedp, 1);
  INIT_SUBR(not, 1);
  INIT_SUBR(eq, 2);
  INIT_SUBR(eqv, 2);
  INIT_SUBR(equal, 2);
  INIT_SUBR(equalp, 2);
}
