#include "sequence.h"
/*
  TODO: Sequence functions should be generic to allow user deifned sequences
  this means using dispatch tables, which need to be extensible in some manner. 
*/
//I'm hoping the compilier will eliminate the 3rd branch in the
//cases when the string and vector functions are the same 
#define seq_dispatch(base_name, seq, args...)   \
  if(LISTP(seq)){                               \
    return base_name##_cons(seq, ##args);       \
  } else if(SVECTORP(seq)){                     \
    return base_name##_vector(seq, ##args);     \
  } else if(STRINGP(seq)){                      \
    return base_name##_string(seq, ##args);     \
  }
#define elt_cons(a,b) Fnth(a,b)
#define elt_vector(a,b) Fvref(a,b)
#define elt_string(a,b) STR_REF(a,b)
DEFUN("elt",elt,2,2,"(elt seq n) return the nth value of seq")
  (sl_obj obj, sl_obj elt){
  if(!SEQUENCEP(obj) || !FIXNUMP(elt)){
    raise_error_temp();
  }
  seq_dispatch(elt, obj, elt);
  raise_error_temp();
}
#define length_cons(obj) make_int(CONS_LENGTH(obj))
#define length_vector(obj) PACK_INT(VECTOR_LEN(obj))
//this should probably return the number of characters not bytes
#define length_string(obj) PACK_INT(VECTOR_LEN(obj))
DEFUN("length",length,1,1,
      "(length seq) return the length of the given sequence")
  (sl_obj obj){
  seq_dispatch(length, obj);
  raise_error_temp();
}
#define reverse_string(x) reverse_vector(x)
DEFUN("reverse",reverse,1,1,
      "(reverse seq) return a copy of seq in reverse order")
  (sl_obj obj){
  seq_dispatch(reverse, obj);
  raise_error_temp();
}
#define nreverse_string(x) reverse_vector(x)
DEFUN("nreverse",nreverse,1,1,"(nreverse seq) return seq in reverse order,"
      " this may destructively modify seq")
  (sl_obj obj){
  seq_dispatch(nreverse, obj);
  raise_error_temp();
}
#define reduce_string(x,...) reduce_vector(x,__VA_ARGS__)
DEFUN("reduce",reduce,2,3,"(reduce fn seq &optional start)\n"
      "Apply 2 argument function fn to each element of seq,\n"
      "'fn' is called with the previous return value as the first argument\n"
      "and the next element as the second.\nIf start is provided it is used\n"
      "as the initial argument, otherwise the first element of the seq is used")
  (sl_obj obj, sl_obj fn, sl_obj start){
  if(!SEQUENCEP(obj)){
    raise_error_temp();
  }
  if(!SUBRP(fn) ||
     XSUBR(fn)->minargs > 2 || ((uint64_t)XSUBR(fn)->maxargs) < 2){
    raise_error_temp();
  }
  seq_dispatch(reduce, obj, fn, start);
  raise_error_temp();
}
/*DEFUN("map",map,2,3,"(map [type] fn seq &rest sequences)\n"
      "Create a sequence of type 'type' by mapping fn across the given"
      "sequences, if type is ommited it defaults to the type of the first"
      "sequence, if provided it should be a keyword symbol.")
  (sl_obj type, sl_obj fn, sl_obj seq){
  / *
    This behaves a bit weirdly for the sake of convience, I really
    hate having to provide a type everytime I call map in common lisp
    so it's optional here. This means the different arities of map
    take different types of arguments.
   * /
  if(NILP(seq)){//type was ommited
    seq = fn;
    fn = type;
    type = NIL;
    //needs to be defined 
    //    type = SEQUNCE_TYPE(seq);
  }
  }*/
    
void init_sequence_symbols(void){
  INIT_SUBR(elt, 2);
  INIT_SUBR(length, 1);
  INIT_SUBR(reverse, 1);
  INIT_SUBR(nreverse, 1);
}
