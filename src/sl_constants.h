/*
  Header containing variables which are constant in both C and lisp. 
  We need this seperate header because C constants can't be declared
  before they are defined
*/
#ifndef _SL_COMMON_H_
#error "do not include sl_constants.h directly it is included by common.h"
#endif
#ifndef _SL_CONSTANTS_H_
#define _SL_CONSTANTS_H_
DEFCONST("t",t,PACK_INT(1),"");
DEFCONST("nil",nil,NIL,"");
//DEFCONST("pi",pi,make_double(M_PI)
static const sl_obj sl_undef = SL_UNDEF; //not Qundef, since it's not a symbol
#endif

