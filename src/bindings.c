#include "common.h"
#include "vector.h"
//replace all lexically bound, defined symbols in form with their values
sl_obj replace_symbols_with_definitions(sl_obj form){
  if(!CONSP(form)){
    if(SYMBOLP(form) && !DYNAMICP(form)){
        sl_obj lexical_binding = get_lexical_binding(form);
        /*
          If a symbol is unbound it could later be defined as a dynamic
          variable and resolved at runtime, so just leave the symbol as is
          if that's the case.
         */
        return (EQ(lexical_binding, Qunbound) ? form : lexical_binding);
    } else {
      return form;
    }
  }
  //for now create a new list, it should be easy to write an inplace version
  sl_obj ret = make_cons(NIL,NIL);
  sl_obj tail = ret;
  while(CONSP(form)){
    XCAR(tail) = replace_symbols_with_definitions(POP(form));
    XCDR(tail) = make_cons(NIL,NIL);
    tail = XCDR(tail);
  }
  //we allocated an extra cons cell, so make sure it get's gc'ed
  XCDR(tail) = NIL;
  return ret;
}

/*//setq should be roughly like this
sl_obj Fsetq(sl_obj args){
  //check these references
  sl_obj sym = XCAR(args);
  sl_obj val = XCADR(args);
  /*
    here should be
    if(XSYMPROPS(sym) & SL_SYMBOL_SPECIAL){
      search thread_env->thread_local_bindings;
    } else {
      try to lex bind
    }
    bind gloabl value
   /
  sl_obj lex_binding = search_lexical_env(sym, thread_env->lexical_env);
  if(lex_binding){
    //this is a bit cheating, but since we have a pointer to a cons cell
    //we can just set the cdr of that cell, I think
    return (XCDR(lex_binding) = Feval(val));
  } else {
    return (XSYMVAL(sym) = Feval(val));
  }
}*/
