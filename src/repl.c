#include "frontend.h"
#include "sl_string.h"
#include "string_buf.h"
#include "sl_read.h"
static sl_string null_string = make_lit_string("");
/**
   Read a line from standard input, using the readline library if
   avaiable. A pointer to the line read is returned, with memory
   for the line allocated with the sl_malloc function.
   If readline is being used and the line is not empty it is
   added to the history.

   @param prompt The prompt to display to the user
   @return A pointer to the line read, as an sl_string
*/
SL_INLINE sl_string* sl_readline(const char *prompt){
  char *line_read = NULL;
  sl_string *retval = NULL;
#if (defined HAVE_READLINE)
  line_read = readline(prompt);
  if(line_read && *line_read){
    add_history(line_read);
    int len = strlen(line_read);
    //readline strips the newline, so add a space to compensate
    //we can do this because the string is null terminated
    line_read[len] = ' ';
    retval = make_string_unboxed(line_read, len+1, 0);
  } else if(!line_read){

    //this is a hack to make Ctrl-D work
    fputs("\n",stderr);
    exit(0);
  }
#else
  fputs(prompt,stdout);
  int num_chars = getline(&line_read,0,stdin);
  if(line_read && *line_read){
    retval = make_string_unboxed(line_read, num_chars, 0);
  }
#endif
  free(line_read);
  return retval;
}
/**
  find the ending quote of a string which may contain escaped quotes.
  @p str should point to a character in a string somewhere past
  the starting quote.
  @param str the string to search
  @param len the maximum number of characters to read
  @return one less than the number of characters read-1, or len
  if no closing quote was found.
*/
static int skip_string(const char *str, int len){
  int i = 0;
  if(len == 0){//just in case
    return 0;
  }
  do {
    if(str[i] == '\\'){
      i+=2;//skip the backslash and the next character
      continue;
    }
    if(str[i] == '"'){
      return i+1;
    }
  } while(++i < len);
  //if we get here we didn't find a quote
  return len;
}

/**
   Count the number of unmatched parenthesis in a string. Parenthesis inside
   of strings or used as character literals are correctly ignored. Left
   parantheses '(' are counted as positive, and right parantheses ')' are
   counted as negitive. The @p in_string parameter is used to indicate if the
   @p str is inside of a lisp string literal, it is set if @p str ends inside
   during a string literal, and the same value should be passed to further
   invocations of count_parens.

   @param str The string to search
   @param len The length of the string
   @param in_string A pointer to an integer indicating if the string
   is part of a multiline string literal

   @return The number of unmatched parentheses
 */
static int count_parens(const char *str, int len, int *in_string){
  int count = 0, i = 0, c;
  /*
    We need to be a little clever here to deal with
    characters, string literals and comments, since something like:
    (+ ?( ?() is a correct sexp
   */
  if(*in_string){
    int skip = skip_string(str, len);
    if(skip == len){
      return 0;
    } else {
      *in_string = 0;
      i+=skip;
    }
  }
  while(i < len){
    c = str[i++];
    switch(c){
      case '(':
        count++; break;
      case ')':
        count--; break;
      case '?':
        if(i < len){//make sure the string is long enough
          if(str[i++] == '\\'){//increment i and skip a \, since ?\( == ?(
            i++;//increment i again if there was a '\'
          }
        }
        break;
      case ';'://comment until the end of the line
        goto END;
      case '"'://ignore parenthesis in strings
        i += skip_string(str+i,len - i);
        if(i>=len){*in_string = 1;}
        break;
      /*TODO: multiline comments
        #| starts comment, keep track of nesting level
        each #| increments nesting level, each #| decrements it.
        when nesting level = 0, or i >= len break.
         */
      default://just for completeness sake
        break;
    }
  }
  END:
    return count;
}
/**
   Read a possibly multi-line lisp expression from standard input
 */
sl_string *read_sexp(void){
  int in_string = 0, num_parens = 0;
  sl_string *line = NULL;
  //optimize the case where everything is on one line
  line = sl_readline(toplevel_prompt);
  if(!line){
    return NULL;
  }
  num_parens = count_parens(line->str, line->len, &in_string);
  if(num_parens == 0){
    return line;
  }
  //A string buf is a bit big (due to the internal buffer), but it's allocated
  //on the stack, and convient to use
  sl_string_buf *sexp = string_buf_from_string(line);
  while(num_parens != 0){
    line = sl_readline(nested_prompt);
    if(!line){//skip blank lines
      continue;
    }
    string_buf_append_str(sexp, line);
    num_parens += count_parens(line->str, line->len, &in_string);
  }
  return string_buf_to_string(sexp);
}
/**
   Start a read-eval-print loop, reading from stdin and printing to stdout
 */
sl_obj Q_;
//sl_obj Qrepl_last;
void SL_NORETURN read_eval_print_loop(){
  sl_string *sexp;
  setjmp(toplevel_repl);
  sexp = NULL;
  while(1){
    while(sexp == NULL){
      sexp = read_sexp();
    }
    sl_obj sexp_obj = read_from_string(PACK_STRING(sexp));
    if(sexp_obj != NIL){
      sl_obj result = Feval(sexp_obj);
      Fprint(result,NIL);
      SETQ(Q_, result);
    }
    sexp = NULL;
  }
}
/* initialize some special repl symbols
   _ is the last result, *last* should be the last form,but
   I can't get it to work
*/

void init_repl(void){
  Q_ = make_symbol("_",1,NIL,SL_SYMBOL_SPECIAL,"");
  //  Qrepl_last = make_symbol("repl_last",9,NIL, SL_SYMBOL_SPECIAL,"");
  add_symbol(thread_env->obarray, XSYMBOL(Q_));
  //  add_symbol(thread_env->obarray, XSYMBOL(Qrepl_last));
}
